#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Evolution of an initially unstable sandpile following (Prighozin, 1996) sandpile model.

Note that the MosekProblem is redefined at each time step which is
evidently suboptimal. Including reoptimization in fenics_optim will
be supported in future releases.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import grad, tan, pi
from dolfin import (
    UnitSquareMesh,
    FunctionSpace,
    Function,
    DirichletBC,
    Constant,
    Expression,
    XDMFFile,
)
from fenics_optim import MosekProblem, L2Ball, QuadraticTerm
import numpy as np

mesh = UnitSquareMesh(50, 50, "crossed")
V = FunctionSpace(mesh, "CG", 1)

bc = DirichletBC(V, Constant(0), "on_boundary")

# Define initial height
uold = Function(V, name="Height")
uold.interpolate(
    Expression("fabs(x[0]-0.5) <= 0.4 && fabs(x[1]-0.5) <= 0.2 ? 0.2: 0.", degree=2)
)

# Zero source
f = Function(V, name="Source")

# Data term for variational problem
g = Function(V)

# Output file
ffile = XDMFFile("results/sandpile.xdmf")
ffile.parameters["functions_share_mesh"] = True
ffile.parameters["flush_output"] = True

time_steps = np.linspace(0, 0.5, 101)
t = 0
for dt in np.diff(time_steps):
    ffile.write(uold, t)
    # Update data term with current solution and source
    g.vector().set_local(f.vector().get_local() * dt + uold.vector().get_local())

    # Define variational problem
    prob = MosekProblem("sandpile")
    u = prob.add_var(V, bc=bc, lx=0)

    F = QuadraticTerm(u, x0=g)
    prob.add_convex_term(F)

    K = L2Ball(grad(u), k=tan(pi / 6))
    prob.add_convex_term(K)

    # disable Mosek log for all iterations
    prob.parameters["log_level"] = 0

    prob.optimize()

    # update field
    uold.assign(u)
    t += dt
    print("Time:", t)
