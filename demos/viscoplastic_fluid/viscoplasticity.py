#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Steady-state 2D flow of a viscoplastic Bingham fluid in a lid-driven cavity.

See more details in (Bleyer, 2015 and 2016)
https://dx.doi.org/10.1016/j.cma.2017.11.006
https://dx.doi.org/10.1016/j.cma.2014.10.008

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    UnitSquareMesh,
    near,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    dx,
)
from ufl import sym, grad, div, as_vector, sqrt
from fenics_optim import MosekProblem, QuadraticTerm, L2Norm
import matplotlib.pyplot as plt
import numpy as np

N = 75
mesh = UnitSquareMesh(N, N, "crossed")

# fluid viscosity and yield stress
# with unit length and unit imposed viscosity, the Bingham number
# is Bi = tau0/mu
mu, tau0 = Constant(1.0), Constant(20.0)


# get top boundary and remaining part of the cavity
def top(x, on_boundary):  # noqa
    return near(x[1], 1.0) and x[0] > 0.0 and x[0] < 1.0


def sides(x, on_boundary):  # noqa
    return near(x[1], 0.0) or near(x[0], 0.0) or near(x[0], 1.0)


prob = MosekProblem("Viscoplastic fluid")

# P2 interpolation for velocity
V = VectorFunctionSpace(mesh, "CG", 2)
bc = [
    DirichletBC(V, Constant((1.0, 0.0)), top),
    DirichletBC(V, Constant((0.0, 0.0)), sides),
]
u = prob.add_var(V, bc=bc)

# P1 interpolation for pressure (Lagrange multiplier)
Vp = FunctionSpace(mesh, "CG", 1)


def mass_conserv(p):
    """Mass conservation condition."""
    return p * div(u) * dx


prob.add_eq_constraint(Vp, mass_conserv)


def strain(v):
    """
    Express strain tensor in vectorial notation.

    such that ||strain(v)||_2^2 = inner(E, E) with E = sym(grad(v)).
    """
    E = sym(grad(v))
    return as_vector([E[0, 0], E[1, 1], sqrt(2) * E[0, 1]])


visc = QuadraticTerm(strain(u), degree=2)
plast = L2Norm(strain(u), degree=2)

# add viscous term mu*||strain||_2^2 (factor 2 because 1/2 in QuadraticTerm)
prob.add_convex_term(2 * mu * visc)
# add plastic term sqrt(2)*tau0*||strain||_2
prob.add_convex_term(sqrt(2) * tau0 * plast)

prob.optimize()

# compare horizontal velociy profile against previous solutions for some Bingham numbers
# along vertical line x=0.5
plt.figure()
Bi = int(float(tau0 / mu))
Bi_data_values = [0, 2, 5, 20]
try:
    i = Bi_data_values.index(Bi)
    data = np.loadtxt("viscoplastic_data.csv", skiprows=1)
    plt.plot(
        data[:, i + 1],
        data[:, 0],
        "o",
        markersize=4,
        label="solution from [Bleyer et al., 2015]",
    )
except:
    pass
y = np.linspace(0, 1, 200)
u_mid = [u(0.5, yi)[0] for yi in y]
plt.plot(u_mid, y, label="present computation")
plt.legend()
plt.xlabel("$y$ coordinate")
plt.ylabel("Velocity $u_x$")
plt.show()
