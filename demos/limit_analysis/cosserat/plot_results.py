#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Scale effect of jointed rock slope stability factors using a Cosserat model
2 mesh sizes are compared, dashed lines represent solutions of standard
homogenization (Cauchy medium)

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt

res = np.loadtxt("cosserat_limit_loads.csv", skiprows=1, delimiter=",")

ell = res[:, 0]
plt.semilogx(ell, res[:, 1], "-o", label="$N_{el}=2673$")
plt.semilogx(ell, res[0, 1] + 0 * ell, "--C0", linewidth=1)
plt.semilogx(ell, res[:, 2], "-s", label="$N_{el}=18556$")
plt.semilogx(ell, res[0, 2] + 0 * ell, "--C1", linewidth=1)
plt.legend()
plt.xlabel("Relative joints spacing $\ell/H$")
plt.ylabel(r"Stability factor $\displaystyle{\left(\frac{\gamma H}{c_m}\right)^+}$")
plt.ylim(0, 8)
plt.xlim(0, 0.25)
plt.savefig("cosserat_results.pdf")
plt.show()