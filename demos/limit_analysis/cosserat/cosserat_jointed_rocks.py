#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stability factor of a jointed rock slope.

This implementation uses a Cosserat model using a mixed formulation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    MeshFunction,
    VectorFunctionSpace,
    FunctionSpace,
    VectorElement,
    MixedElement,
    FiniteElement,
    AutoSubDomain,
    dx,
    plot,
    near,
    DirichletBC,
    XDMFFile,
    Function,
    project,
    Point,
)
from ufl import sym, grad, dot, as_matrix, as_vector, tan, cos, sin, inner, split
from fenics_optim import MosekProblem, ConvexFunction
import fenics_optim.limit_analysis as la
import matplotlib.pyplot as plt
from mshr import Polygon, generate_mesh
from math import pi


cm = Constant(1)
cj = Constant(0.1)
phim = Constant(35 * pi / 180)
phij = Constant(20 * pi / 180)
ell = Constant(0.000244141)
theta = Constant(10 * pi / 180)

L, H = 2.0, 1.0
a = H / tan(75 * pi / 180)

domain = Polygon([Point(0, 0), Point(L, 0), Point(L, H), Point(a, H)])
mesh = generate_mesh(domain, 80)
print("Number of cells", mesh.num_cells())


def border(x, on_boundary):  # noqa
    return (near(x[1], 0) or near(x[0], L)) and on_boundary


def top(x, on_boundary):  # noqa
    return near(x[1], 0)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(border).mark(facets, 1)

f = Constant((0.0, -1))


class CauchyJointedRocks(ConvexFunction):
    """Cauchy strength condition of jointed rocks."""

    def conic_repr(self, X, cj, phij, cm, phim, ell):  # noqa
        self.add_ineq_constraint(X[0] * tan(phij) + X[2], bu=cj)
        self.add_ineq_constraint(X[0] * tan(phij) - X[2], bu=cj)
        self.add_ineq_constraint(X[1] * tan(phij) + X[2], bu=cj)
        self.add_ineq_constraint(X[1] * tan(phij) - X[2], bu=cj)


class CosseratJointedRocks(ConvexFunction):
    """
    Cosserat strength condition of jointed rocks.

    Parameters
    ----------
    cj : float
        joint cohesion
    phij : float
        joint friction angle
    cm : float
        rock (matrix) cohesion
    phim : float
        rock (matrix) friction angle
    ell : float
        characteristic length
    """

    def conic_repr(self, X, cj, phij, cm, phim, ell):  # noqa
        self.add_ineq_constraint(X[0] * tan(phij) + X[3], bu=cj)
        self.add_ineq_constraint(X[0] * tan(phij) - X[3], bu=cj)
        self.add_ineq_constraint(X[1] * tan(phij) + X[2], bu=cj)
        self.add_ineq_constraint(X[1] * tan(phij) - X[2], bu=cj)
        self.add_ineq_constraint(ell / 2 * X[0] + X[4], bu=cj / tan(phij) * ell / 2)
        self.add_ineq_constraint(ell / 2 * X[0] - X[4], bu=cj / tan(phij) * ell / 2)
        self.add_ineq_constraint(ell / 2 * X[1] + X[5], bu=cj / tan(phij) * ell / 2)
        self.add_ineq_constraint(ell / 2 * X[1] - X[5], bu=cj / tan(phij) * ell / 2)
        b = ell * cm / 2 / cos(phim)
        self.add_ineq_constraint(X[4], bu=b, bl=-b)
        self.add_ineq_constraint(X[5], bu=b, bl=-b)


def Cosserat(ll):
    """Limit load computation with Cesserat model."""
    ell = Constant(ll)

    prob = MosekProblem("Cosserat jointed rocks")
    R = FunctionSpace(mesh, "R", 0)
    Sige = VectorElement("DG", mesh.ufl_cell(), degree=1, dim=4)
    He = VectorElement("DG", mesh.ufl_cell(), degree=1)
    VS = FunctionSpace(mesh, MixedElement([Sige, He]))
    lamb, S = prob.add_var([R, VS])

    Ue = VectorElement("CG", mesh.ufl_cell(), degree=2)
    Omegae = FiniteElement("CG", mesh.ufl_cell(), degree=1)
    V = FunctionSpace(mesh, MixedElement([Ue, Omegae]))
    bc = DirichletBC(V, Constant((0, 0, 0)), border)

    (Sig, H) = split(S)
    sig = as_matrix([[Sig[0], Sig[2]], [Sig[3], Sig[1]]])

    def equilibrium(w):
        (u, omega) = split(w)
        Omega = as_matrix([[0, -omega], [omega, 0]])
        return (
            lamb * dot(u, f) * dx
            - inner(sig, grad(u) - Omega) * dx
            - dot(H, grad(omega)) * dx
        )

    prob.add_eq_constraint(V, A=equilibrium, bc=bc)

    c, s = cos(theta), sin(theta)
    R = as_matrix(
        [
            [c ** 2, s ** 2, s * c, s * c],
            [s ** 2, c ** 2, -s * c, -s * c],
            [-c * s, c * s, c ** 2, -(s ** 2)],
            [-c * s, c * s, -(s ** 2), c ** 2],
        ]
    )
    RH = as_matrix([[cos(theta), sin(theta)], [-sin(theta), cos(theta)]])
    Sc = as_vector([S[0], S[1], 0.5 * (S[2] + S[3])])
    Sigr = dot(R, Sig)
    Hr = dot(RH, H)
    Sr = as_vector([Sigr[i] for i in range(4)] + [Hr[0], Hr[1]])
    crit = CosseratJointedRocks(
        Sr, parameters=(cj, phij, cm, phim, ell), quadrature_scheme="vertex"
    )
    critMC = la.MohrCoulomb2D(cm, phim).criterion(Sc, quadrature_scheme="vertex")
    prob.add_convex_term(crit)
    prob.add_convex_term(critMC)
    prob.add_obj_func([1, None])
    prob.optimize(sense="maximize")

    ffile = XDMFFile("demos/cosserat/results.xdmf")
    ffile.parameters["functions_share_mesh"] = True
    u = prob.y[0].sub(0, True)
    omega = prob.y[0].sub(1, True)
    u.rename("Velocity", u.name())
    V0 = FunctionSpace(mesh, "DG", 0)
    dissC = Function(V0, name="Cosserat part")
    diss = Function(V0, name="Total")
    Omega = as_matrix([[0, -omega], [omega, 0]])
    diss.assign(project(inner(sig, grad(u) - Omega) + dot(H, grad(omega)), V0))
    dissC.assign(
        project(
            0 * (sig[1, 0] - sig[0, 1]) * (grad(u)[1, 0] - grad(u)[0, 1] - omega)
            + dot(H, grad(omega)),
            V0,
        )
    )
    ffile.write(diss, 0)
    ffile.write(dissC, 0)
    ffile.write(u, 0)
    domega = Function(V0, name="Relative rotation")
    domega.assign(project((grad(u)[1, 0] - grad(u)[0, 1] - omega), V0))
    ffile.write(domega, 0)

    (u, omega) = split(prob.y[0])
    p = plot(0.01 * u, mode="displacement")
    plt.colorbar(p)
    plt.show()

    return prob.pobj


def Cauchy():
    """Limit load computation with Cauchy model."""
    prob = MosekProblem("Cauchy jointed rocks")
    R = FunctionSpace(mesh, "R", 0)
    Sige = VectorElement("DG", mesh.ufl_cell(), degree=1, dim=3)
    VS = FunctionSpace(mesh, Sige)
    lamb, Sig = prob.add_var([R, VS])

    V = VectorFunctionSpace(mesh, "CG", 2)
    bc = DirichletBC(V, Constant((0, 0)), facets, 1)

    sig = as_matrix([[Sig[0], Sig[2]], [Sig[2], Sig[1]]])

    def equilibrium(u):
        return lamb * dot(u, f) * dx - inner(sig, sym(grad(u))) * dx

    prob.add_eq_constraint(V, A=equilibrium, bc=bc)

    RR = la.rotation_matrix(theta, dim=2)
    Sigr = dot(RR, Sig)
    crit = CauchyJointedRocks(
        Sigr, parameters=(cj, phij, cm, phim, ell), quadrature_scheme="vertex"
    )
    critMC = la.MohrCoulomb2D(cm, phim).criterion(Sig, quadrature_scheme="vertex")
    prob.add_convex_term(crit)
    prob.add_convex_term(critMC)
    prob.add_obj_func([1, None])
    prob.optimize(sense="maximize")

    u = prob.y[0]
    p = plot(0.01 * u, mode="displacement")
    #    p = plot(u[1])
    plt.colorbar(p)
    plt.show()


Cosserat(0.01)
