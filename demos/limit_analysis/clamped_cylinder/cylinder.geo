r=1.;
H = 10;
n_d = 1;

Point(1) = {0, 0, 0, 0};
Point(2) = {-r, 0, 0, n_d};
Point(3) = {0, -r, 0, n_d};
Point(4) = {r, 0, 0, n_d};
Point(5) = {0, r, 0, n_d};
Point(6) = {-r, 0, H, n_d};
Point(7) = {0, -r, H, n_d};
Point(8) = {r, 0, H, n_d};
Point(9) = {0, r, H, n_d};
Point(10) = {0, 0, H, n_d};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};
Circle(5) = {6, 10, 7};
Circle(6) = {7, 10, 8};
Circle(7) = {8, 10, 9};
Circle(8) = {9, 10, 6};

Line(11) = {2,6};
Line(12) = {3,7};
Line(13) = {4,8};
Line(14) = {5,9};

Line Loop(9) = {1,12,-5,-11};
Ruled Surface(9) = {9};
Line Loop(10) = {2,13,-6,-12};
Ruled Surface(10) = {10} ;
Line Loop(11) = {3, 14, -7,-13};
Ruled Surface(11) = {11} ;
Line Loop(12) = {4, 11, -8,-14};
Ruled Surface(12) = {12};


Transfinite Line{1,2,3,4} = n_d*11;
Transfinite Line{5,6,7,8} = n_d*11;
Transfinite Line{11,12,13,14} =n_d*(4*H+1);
Transfinite Surface{9,10,11,12};

Physical Surface(1) = {9,10,11,12};
//Physical Line(1) = {1,2,3,4};
//Physical Line(2) = {5,6,7,8};
// Physical Line(3) = {11};


// Physical Line(1) = {5,7};
// Physical Line(2) = {6,8};
