#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Cylindrical shell under self-weight problem.

This implementation uses lower bound limit analysis shell elements
with a von Mises material

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""

from dolfin import (
    Constant,
    Mesh,
    MeshFunction,
    FacetNormal,
    Function,
    FunctionSpace,
    VectorFunctionSpace,
    VectorElement,
    MixedElement,
    Measure,
    AutoSubDomain,
    dx,
    XDMFFile,
    UserExpression,
    project,
    near,
)
from ufl import (
    sqrt,
    grad,
    dot,
    as_matrix,
    as_vector,
    jump,
    cross,
    outer,
    split,
    avg,
    unit_vector,
    shape,
)
from fenics_optim import MosekProblem, to_mat
import fenics_optim.limit_analysis as la
import numpy as np
import meshio

sig0 = 1.0
thick = 0.01
filename = "./cylinder.xdmf"


# from J. Dokken website
def create_mesh(mesh, cell_type):
    """Generate a proper mesh from a meshio data structure."""
    cells = np.vstack([cell.data for cell in mesh.cells if cell.type == cell_type])
    data = np.hstack(
        [
            mesh.cell_data_dict["gmsh:physical"][key]
            for key in mesh.cell_data_dict["gmsh:physical"].keys()
            if key == cell_type
        ]
    )
    mesh = meshio.Mesh(
        points=mesh.points, cells={cell_type: cells}, cell_data={"name_to_read": [data]}
    )
    return mesh


mmesh = meshio.read(filename.replace("xdmf", "msh"))
mmesh.points = mmesh.points[:, ::-1]  # change orientation between Gmsh and FEniCS
shell_mesh = create_mesh(mmesh, "triangle")
meshio.write(filename, shell_mesh)
mmesh = meshio.read(filename)
mesh = Mesh()
with XDMFFile(filename) as mesh_file:
    mesh_file.read(mesh)

L = max(mmesh.points[:, 0])
print(L)


class Normal(UserExpression):
    """Local normal vector."""

    def eval_cell(self, value, x, ufc_cell):
        nodes = mmesh.cells_dict["triangle"][ufc_cell.index, :]
        x = mmesh.points[nodes, :]
        x0, x1, x2 = np.array(x).reshape(3, 3)
        value[:] = np.cross(x[1, :] - x[0, :], x[2, :] - x[0, :])
        value /= np.linalg.norm(value)

    def value_shape(self):
        return (3,)


normal = Normal(mesh, degree=0)
V0 = VectorFunctionSpace(mesh, "DG", 0)

nu = Function(V0, name="Normal")
nu.assign(project(normal, V0))

ei = [unit_vector(i, 3) for i in range(3)]
ea = [unit_vector(i, 2) for i in range(2)]

a1 = cross(as_vector([0, 1, 0]), nu)
a1 /= sqrt(dot(a1, a1))
a1 = as_vector([1, 0, 0])
a2 = cross(nu, a1)
a2 /= sqrt(dot(a2, a2))
aa1 = Function(V0, name="a1")
aa2 = Function(V0, name="a2")
aa1.assign(project(a1, V0))
aa2.assign(project(a2, V0))

Ploc = outer(ei[0], a1) + outer(ei[1], a2) + outer(ei[2], nu)
Ploc_plane = outer(ea[0], a1) + outer(ea[1], a2)


def divT(x):
    """In-plane divergence operator."""
    return as_vector(
        [
            sum(
                [
                    grad(x)[k, alp, j] * Ploc_plane[alp, j]
                    for alp in range(2)
                    for j in range(3)
                ]
            )
            for k in range(shape(x)[0])
        ]
    )


n = FacetNormal(mesh)
n_plan = dot(Ploc_plane, n)
t = cross(nu, n)


def border(x, on_boundary):  # noqa
    return near(x[0], 0) or near(x[0], L)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(border).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets, domain=mesh)
dS = Measure("dS", subdomain_data=facets, domain=mesh)

q_beam = 32 / np.pi * sig0 * thick * (1 / L) ** 2
f = Constant((0, 0, -q_beam))

prob = MosekProblem("Shell lower bound limit analysis")
R = FunctionSpace(mesh, "R", 0)
Ne = VectorElement("DG", mesh.ufl_cell(), 1, dim=3)
Me = VectorElement("DG", mesh.ufl_cell(), 2, dim=3)
Qe = VectorElement("DG", mesh.ufl_cell(), 1, dim=2)
W = FunctionSpace(mesh, MixedElement([Ne, Me, Qe]))

lamb, Sig = prob.add_var([R, W])

prob.add_obj_func([1])

(N, M, Q) = split(Sig)
T = as_matrix([[N[0], N[2]], [N[2], N[1]], [Q[0], Q[1]]])
M = to_mat(M)

V_f_eq = VectorFunctionSpace(mesh, "DG", 0, dim=3)


def force_equilibrium(u):
    """Force equilibrium conditions."""
    u_loc = dot(Ploc, u)
    return dot(u, f) * lamb * dx + dot(u_loc, divT(T)) * dx


prob.add_eq_constraint(V_f_eq, A=force_equilibrium)

V_m_eq = VectorFunctionSpace(mesh, "DG", 1, dim=2)


def moment_equilibrium(theta):
    """Moment equilibrium conditions."""
    return dot(theta, divT(M) + Q) * dx


prob.add_eq_constraint(V_m_eq, A=moment_equilibrium)

V_f_jump = VectorFunctionSpace(mesh, "Discontinuous Lagrange Trace", 1, dim=3)
Tglob = dot(Ploc.T, T)


def force_continuity(v):
    """Resultant force continuity conditions."""
    return dot(avg(v), jump(Tglob, n_plan)) * dS


prob.add_eq_constraint(V_f_jump, A=force_continuity)

V_m_jump = VectorFunctionSpace(mesh, "Discontinuous Lagrange Trace", 2, dim=3)
Mglob = dot(Ploc_plane.T, M)


def moment_continuity(vtheta):
    """Moment continuity condition."""
    return dot(avg(vtheta), cross(jump(Mglob, n_plan), avg(nu))) * dS


prob.add_eq_constraint(V_m_jump, A=moment_continuity)


NM = as_vector([Sig[i] for i in range(6)])
mat = la.vonMises_shell(sig0, thick, 2, "piecewise")
crit = mat.criterion(NM, quadrature_scheme="lobatto", degree=2)

prob.add_convex_term(crit)

prob.optimize(sense="maximize")

u = Function(V0, name="Mechanism")
u.assign(project(prob.y[0], V0))
NN = Function(V0, name="Normal force")
MM = Function(V0, name="Bending moment")
NN.assign(project(as_vector([Sig[i] for i in range(3)]), V0))
MM.assign(project(as_vector([Sig[i] for i in range(3, 6)]), V0))


with XDMFFile("results.xdmf") as ffile:
    ffile.parameters["functions_share_mesh"] = True
    ffile.write(nu, 0)
    ffile.write(aa1, 0)
    ffile.write(aa2, 0)
    ffile.write(u, 0)
    ffile.write(NN, 0)
    ffile.write(MM, 0)
