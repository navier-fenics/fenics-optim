#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plotting clamped cylinder collapse load results
- present: lower bound estimates computed with fenics_limit_analysis package with ngz=6
- LB reference: lower bound elements described in [1]
- UB reference: upper bound elements described in [1]

[1]  Bleyer, Jeremy, and Patrick De Buhan. "A numerical approach to the yield
strength of shell structures." European Journal of Mechanics-A/Solids 59 (2016): 178-194.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt


res = np.loadtxt("comparison_limit_loads.csv", skiprows=2, delimiter=",")

slenderness = res[:, 0]
plt.plot(slenderness, res[:, 5], "-o", linewidth=1.0, label="Reference (UB)")
plt.plot(slenderness, res[:, 4], "-s", linewidth=1.0, label="Reference (LB)")
plt.plot(
    slenderness, res[:, 2], "-x", linewidth=2.0, markersize=10, label="Present (LB)"
)
plt.legend()
plt.xlabel("Cylinder slenderness $2L/R$")
plt.ylabel(r"Normalized collapse load $q^+/q_{\textrm{beam}}^+$")
plt.xlim((5, 40))
# plt.savefig("cylinder_results.pdf")
plt.show()

slenderness = res[:, 0]
plt.plot(slenderness, res[:, 5], "-o", linewidth=1.0, label="Reference (UB)")
plt.plot(slenderness, res[:, 4], "-s", linewidth=1.0, label="Reference (LB)")
plt.plot(
    slenderness, res[:, 6], "-x", linewidth=2.0, markersize=10, label="Present (pUB)"
)
plt.legend()
plt.xlabel("Cylinder slenderness $2L/R$")
plt.ylabel(r"Normalized collapse load $q^+/q_{\textrm{beam}}^+$")
plt.xlim((5, 40))
plt.savefig("cylinder_CR_results.pdf")
plt.show()