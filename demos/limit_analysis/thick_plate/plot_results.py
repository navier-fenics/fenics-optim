#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Comparison of thick plate results without shear/bending interaction.

Comparison with analytical shear or bending failure mechanisms.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt, pi

res = np.loadtxt("results_thick_plate.csv", skiprows=1, delimiter=",")

thick = res[:, 0]
q_bend = 44.20 + 0 * thick
q_shear = (2 + sqrt(pi)) * 4 / sqrt(3) / thick
plt.semilogx(1 / res[:, 0], q_shear, ":k", label="shear failure")
plt.semilogx(1 / res[:, 0], q_bend, "-.k", label="bending failure")
plt.semilogx(1 / res[:, 0], res[:, 2], "ok", markerfacecolor="w", label="present")
plt.legend()
plt.xlim(1, 100)
plt.ylim(0, 60)
plt.xlabel("Plate slenderness $L/h$")
plt.ylabel("Normalized limit load $q^+(L^2/M_0)$")
plt.savefig("clamped_thick_plate.pdf")
plt.show()
