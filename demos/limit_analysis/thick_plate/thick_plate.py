#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a thin plate in bending.

Square plate with simple or clamped supports with a von Mises criterion
(bending strength m) and uniformly distributed loading.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    UnitSquareMesh,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    dx,
    plot,
    FacetNormal,
    MeshFunction,
    AutoSubDomain,
    Measure,
    dS,
    XDMFFile,
)
from ufl import dot, sym, grad, jump, perp, sqrt, as_vector, inner
from fenics_optim import MosekProblem, L2Norm, to_vect, AbsValue
import fenics_optim.limit_analysis as la
import matplotlib.pyplot as plt

# Plate material strength
sig0 = Constant(1.0)
# Plate thickness
h = Constant(0.1)
# Plate bending strength
M0 = sig0 * h ** 2 / 4
# Plate shear strength
V0 = sig0 / sqrt(3) * h
# Plate uniform load
load = Constant(-1)

# Support type:
# - "ss": simple supports
# - "clamped": clamped supports
supports = "ss"

N = 10
mesh = UnitSquareMesh(N, N, "left")

facets = MeshFunction("size_t", mesh, 1)
facets.set_all(0)
n = FacetNormal(mesh)


# Define boundary conditions
def boundary(x, on_boundary):  # noqa
    return on_boundary


AutoSubDomain(boundary).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets)

prob = MosekProblem("Bending plate limit analysis")

Vu = FunctionSpace(mesh, "CG", 2)
Vt = VectorFunctionSpace(mesh, "CG", 1)

bc = DirichletBC(Vu, Constant(0.0), boundary)
w, theta = prob.add_var([Vu, Vt], bc=[bc, None], name=["Displacement", "Rotation"])

R = FunctionSpace(mesh, "R", 0)


def Pext(lamb):
    """Power of external loads."""
    return lamb * dot(load, w) * dx


prob.add_eq_constraint(R, A=Pext, b=1)

chi = sym(grad(theta))
gamma = grad(w) - theta

mat_bending = la.vonMises_plane_stress(1.0)
pi_c = mat_bending.support_function(to_vect(chi), quadrature_scheme="vertex")
prob.add_convex_term(pi_c)

pi_shear = L2Norm(gamma, quadrature_scheme="vertex")
prob.add_convex_term(V0 / M0 * pi_shear)

t = perp(n)
if supports == "ss":
    pi_bend_disc = L2Norm(
        [as_vector([2 * jump(theta, n), jump(theta, t)])], k=1 / sqrt(3), on_facet=True
    )
elif supports == "clamped":
    pi_bend_disc = AbsValue(
        [
            as_vector([2 * jump(theta, n), jump(theta, t)]),
            as_vector([2 * dot(theta, n), dot(theta, t)]),
        ],
        k=1 / sqrt(3),
        on_facet=True,
        measure=[dS, ds(1)],
    )
else:
    raise (NotImplementedError)
# prob.add_convex_term(pi_bend_disc)
prob.parameters["presolve"] = True
prob.optimize()

p = plot(w)
plt.colorbar(p)
plt.show()

p = plot(inner(chi, chi) ** 0.5, cmap="Blues")
plt.colorbar(p)
plt.show()

p = plot(dot(gamma, gamma) ** 0.5, cmap="Blues", vmin=0, vmax=108)
plt.colorbar(p)
plt.show()

ffile = XDMFFile("results_left_thick_{}_{}.xdmf".format(float(h), "CG"))
ffile.write(w, N)
ffile.close()
