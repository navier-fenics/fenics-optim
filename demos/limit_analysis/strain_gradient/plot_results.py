#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Scale effect of a plate with hole made of a strain-gradient von Mises material
2 mesh sizes are compared, dashed lines represent solutions of standard
homogenization (Cauchy medium)

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt

res = np.loadtxt("strain_gradient_limit_loads.csv", skiprows=1, delimiter=",")

ell = res[:, 0]
plt.semilogx(ell, res[:, 1], "-o", label="$N_{el}=1024$")
plt.semilogx(ell, res[0, 1] + 0 * ell, "--C0", linewidth=1)
plt.semilogx(ell, res[:, 2], "-s", label="$N_{el}=9422$")
plt.semilogx(ell, res[0, 2] + 0 * ell, "--C1", linewidth=1)
plt.legend()
plt.xlabel("Internal length scale ratio $\ell/L$")
plt.ylabel(r"Normalized strength $Q^+/(kl)$")
plt.savefig("strain_gradient_results.pdf")
plt.ylim(0, 3)
plt.xlim(0, 0.25)
plt.savefig("strain_gradient_results.pdf")
plt.show()
