#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a strain-gradient von Mises material.

Plate with hole in traction.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Point,
    near,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    plot,
    FacetNormal,
    XDMFFile,
    Function,
    project,
)
from ufl import dot, sym, grad, jump, as_vector, sqrt, div, inner
from fenics_optim import MosekProblem, L2Norm, EqualityConstraint
from mshr import Rectangle, Circle, generate_mesh
import matplotlib.pyplot as plt

domain = Rectangle(Point(0, 0), Point(1, 1.5)) - Circle(Point(0.5, 0.75), 0.2)
mesh = generate_mesh(domain, 60)

k = Constant(1)
ell = Constant(0.01)

V = VectorFunctionSpace(mesh, "CG", 2)


def bottom(x, on_boundary):  # noqa
    return near(x[1], 0)


def top(x, on_boundary):  # noqa
    return near(x[1], 1.5)


bc = [
    DirichletBC(V, Constant((0.0, 0.0)), bottom),
    DirichletBC(V, Constant((0.0, 1.0)), top),
]

prob = MosekProblem("Strain gradient limit analysis")
u = prob.add_var(V, bc=bc, name="Velocity")

D = as_vector(
    [
        u[0].dx(0),
        u[1].dx(1),
        (u[0].dx(1) + u[1].dx(0)) / sqrt(2),
        ell * u[0].dx(0).dx(0),
        ell * u[0].dx(1).dx(1),
        sqrt(2) * ell * u[0].dx(0).dx(1),
        ell * u[1].dx(0).dx(0),
        ell * u[1].dx(1).dx(1),
        sqrt(2) * ell * u[1].dx(0).dx(1),
    ]
)
pi = L2Norm(D, quadrature_scheme="vertex")
prob.add_convex_term(k * sqrt(2) * pi)

isochoric = EqualityConstraint(div(u), quadrature_scheme="vertex")
prob.add_convex_term(isochoric)

n = FacetNormal(mesh)
pi_d = L2Norm([jump(grad(u), n)], on_facet=True)
prob.add_convex_term(k * sqrt(2) * ell * pi_d)

prob.optimize()

gu = sym(grad(u))
p = plot(sqrt(inner(gu, gu)))
plt.colorbar(p)
plt.show()

plot(0.1 * u, mode="displacement")

ffile = XDMFFile("demos/strain_gradient/results.xdmf")
ffile.parameters["functions_share_mesh"] = True
ffile.write(u, 0)
V0 = FunctionSpace(mesh, "DG", 0)
diss = Function(V0, name="Dissipation")
diss.assign(project(sqrt(dot(D, D)), V0))
ffile.write(diss, 0)
ffile.close()
