#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a thin plate in bending.

Square plate with simple or clamped supports with a von Mises criterion
(bending strength m) and uniformly distributed loading.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import RectangleMesh, Point, MeshFunction, near, AutoSubDomain, DOLFIN_EPS
from mshr import Rectangle, Circle, Polygon, generate_mesh
import numpy as np


def _limit_analysis_solve(mesh, facets, fixed_displ, method, **kwargs):
    if method == "yield_line":
        from yield_line_method import yield_line_method

        yield_line_method(mesh, facets, fixed_displ, **kwargs)
    else:
        from thin_plate_quadratic import thin_plate_quadratic

        thin_plate_quadratic(mesh, facets, fixed_displ, **kwargs)


def rectangular_plate(
    L: float = 1.0,
    H: float = 1,
    N: int = 10,
    supports: str = "ss",
    mesh_type: str = "crossed",
    method: str = "yield_line",
    **kwargs
):
    """Rectangular bending plate problem.

    Parameters
    ----------
    L : float, optional
        plate length, by default 1.0
    H : float, optional
        plate height, by default 1.0
    N : int, optional
        mesh resolution, by default 10
    supports : str, optional
        support type {"ss", "clamped"}, by default "ss"
    mesh_type : str, optional
        [description], by default "crossed"
    method : str, optional
        discretization method {"yield_line", "quadratic"}, by default "yield_line"
    """
    if mesh_type is not None:
        mesh = RectangleMesh(
            Point(0.0, 0.0), Point(L, H), int(N * L), int(N * H), mesh_type
        )
    else:
        domain = Rectangle(Point(0.0, 0.0), Point(L, H))
        mesh = generate_mesh(domain, N)

    facets = MeshFunction("size_t", mesh, 1)
    facets.set_all(0)

    # Define boundary conditions
    def left(x, on_boundary):
        return near(x[0], 0) and on_boundary

    def right(x, on_boundary):
        return near(x[0], L) and on_boundary

    def bottom(x, on_boundary):
        return near(x[1], 0) and on_boundary

    def top(x, on_boundary):
        return near(x[1], H) and on_boundary

    boundaries = [left, bottom, right, top]
    fixed_displ = []
    if supports == "clamped":
        for boundary in boundaries:
            AutoSubDomain(boundary).mark(facets, 1)
            fixed_displ = boundaries
    elif supports == "ss":
        fixed_displ = boundaries
    else:
        for (boundary, support) in zip(boundaries, supports):
            if support == "clamped":
                AutoSubDomain(boundary).mark(facets, 1)
                fixed_displ.append(boundary)
            elif support == "sym":
                AutoSubDomain(boundary).mark(facets, 1)
            elif support == "ss":
                fixed_displ.append(boundary)
            elif support == "free":
                pass

    _limit_analysis_solve(mesh, facets, fixed_displ, method, **kwargs)


def circular_plate(
    N: int = 10, supports: str = "ss", method: str = "yield_line", **kwargs
):
    """Circular bending plate problem (radius = 1.).

    Parameters
    ----------
    N : int, optional
        mesh resolution, by default 10
    supports : str, optional
        support type {"ss", "clamped"}, by default "ss"
    method : str, optional
        discretization method {"yield_line", "quadratic"}, by default "yield_line"
    """
    domain = Circle(Point(0.0, 0.0), 1.0, 100)
    mesh = generate_mesh(domain, N)
    facets = MeshFunction("size_t", mesh, 1)
    facets.set_all(0)

    def border(x, on_boundary):
        return on_boundary

    fixed_displ = [border]
    if supports == "clamped":
        AutoSubDomain(border).mark(facets, 1)
    _limit_analysis_solve(mesh, facets, fixed_displ, method, **kwargs)


def polygonal_plate(
    Nside: int, N: int = 10, supports: str = "ss", method: str = "yield_line", **kwargs
):
    """Polygonal plate problem (radius = 1).

    Parameters
    ----------
    Nside : int
        number of polygon sides
    N : int, optional
        mesh resolution, by default 10
    supports : str, optional
        support type {"ss", "clamped"}, by default "ss"
    method : str, optional
        discretization method {"yield_line", "quadratic"}, by default "yield_line"
    """
    points = [
        Point(np.cos(alp), np.sin(alp)) for alp in np.linspace(0, 2 * np.pi, Nside + 1)
    ][:-1]
    center = Point(0, 0)
    domain = Polygon(points + [points[0]])
    for i in range(Nside - 1):
        domain.set_subdomain(i + 1, Polygon([center, points[i], points[i + 1], center]))
    mesh = generate_mesh(domain, N)
    facets = MeshFunction("size_t", mesh, 1)
    facets.set_all(0)

    def border(x, on_boundary):
        return on_boundary

    fixed_displ = [border]
    if supports == "clamped":
        AutoSubDomain(border).mark(facets, 1)
    _limit_analysis_solve(mesh, facets, fixed_displ, method, **kwargs)


def floor_slab(N: int = 10, method: str = "yield_line", **kwargs):
    """Floor slab problem.

    Parameters
    ----------
    N : int, optional
        mesh resolution, by default 10
    method : str, optional
        discretization method {"yield_line", "quadratic"}, by default "yield_line"
    """
    ext_points = [
        Point(6.0, 0.0),
        Point(10.0, 0.0),
        Point(10.0, 2.0),
        Point(15.0, 2.0),
        Point(18.0, 5.0),
        Point(18.0, 9.0),
        Point(15.0, 11.0),
        Point(2.0, 11.0),
        Point(0.0, 9.0),
        Point(0.0, 4.0),
        Point(2.0, 3.0),
        Point(6.0, 3.0),
        Point(6.0, 0.0),
    ]
    domain = Polygon(ext_points) - Rectangle(Point(6.0, 6.0), Point(11.0, 8.0))
    mesh = generate_mesh(domain, N)

    facets = MeshFunction("size_t", mesh, 1)
    facets.set_all(0)

    def left_side(x, on_boundary):
        return x[0] <= 2.0 and 3 <= x[1] <= 4.0 and on_boundary

    def bottom_horz(x, on_boundary):
        return near(x[1], 0) and on_boundary

    def bottom_vert(x, on_boundary):
        return near(x[0], 6.0) and x[1] <= 3.0 and on_boundary

    def right_side(x, on_boundary):
        return x[0] >= 15.0 - DOLFIN_EPS and 2 <= x[1] <= 5.0 and on_boundary

    def top(x, on_boundary):
        return near(x[1], 11.0) and on_boundary

    fixed_displ = [left_side, bottom_horz, bottom_vert, right_side, top]
    for boundary in fixed_displ:
        AutoSubDomain(boundary).mark(facets, 1)

    _limit_analysis_solve(mesh, facets, fixed_displ, method, **kwargs)
