#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Rectangular bending plate limit analysis demo.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""

from plate_limit_analysis import rectangular_plate

rectangular_plate(
    L=1, H=1, N=10, supports="ss", mesh_type="crossed", method="quadratic"
)
