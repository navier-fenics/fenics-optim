#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Custom plotting routine for facet functions.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import inner, avg
from dolfin import FunctionSpace, Function, TestFunction, TrialFunction, dS, solve
import numpy as np


def facet_plot(u, mesh, ds, scale=1.0):
    """Plot expression u defined on facets.

    Parameters
    ----------
    u : list [u0, u1]
        a list of UFL expressions: u0 expression for the inner facets,
        u1 for the boundary facets
    mesh : dolfin.Mesh
        the mesh object
    ds : dolfin.MEasure
        the "ds" measure for the boundary (plots only the field on ds(1))
    scale : float, optional
        scaling factor for the plot width, by default 1.0

    Returns
    -------
    X : np.ndarray
        vertices X position
    Y : np.ndarray
        vertices Y position
    C : np.ndarray
        vertices color value
    connec : np.ndarray
        connectivity table for elements
    max_ampl : float
        Field max amplitude
    """
    Vf = FunctionSpace(mesh, "Discontinuous Lagrange Trace", 1)
    dtheta = Function(Vf)

    v_ = TestFunction(Vf)
    dv = TrialFunction(Vf)
    a = inner(avg(v_), avg(dv)) * dS + inner(v_, dv) * ds
    L = inner(avg(v_), u[0]) * dS + inner(v_, u[1]) * ds(1)
    solve(a == L, dtheta)

    max_ampl = max(abs(dtheta.vector().get_local()))
    facets_coor = Vf.tabulate_dof_coordinates()
    facet_dof = Vf.dofmap().entity_dofs(mesh, 1)
    facets_coor = Vf.tabulate_dof_coordinates()[facet_dof]
    Nf = mesh.num_facets()
    X = np.zeros((5 * Nf,))
    Y = np.zeros((5 * Nf,))
    connec = np.zeros((2 * Nf, 3))
    C = np.zeros((5 * Nf,))

    bmax = np.max(mesh.coordinates(), axis=0)
    bmin = np.min(mesh.coordinates(), axis=0)
    unit_scale = min(
        0.2 * 1 / max_ampl * mesh.hmin(), 0.05 / max_ampl * np.linalg.norm(bmax - bmin)
    )
    for i in range(Nf):
        c = facets_coor[2 * i : 2 * i + 2, :]
        ue = dtheta.vector().get_local()[facet_dof[2 * i : 2 * i + 2]]
        x = np.array([-0.5, 0.5, -0.5, 0.5, 0])
        y = np.array([-ue[0], -ue[1], ue[0], ue[1], 0]) * scale * unit_scale
        t = c[1, :] - c[0, :]
        X[5 * i + np.arange(5)] = (
            x * t[0] - y * t[1] / np.linalg.norm(t) + np.mean(c[:, 0])
        )
        Y[5 * i + np.arange(5)] = (
            x * t[1] + y * t[0] / np.linalg.norm(t) + np.mean(c[:, 1])
        )
        C[5 * i + np.arange(4)] = np.tile(ue, 2)
        connec[2 * i, :] = np.array([0, 1, 2]) + 5 * i
        connec[2 * i + 1, :] = np.array([2, 1, 3]) + 5 * i

    return X, Y, C, connec, max_ampl
