#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a thin plate in bending with quadratic interpolation.

The plate is made of an orthotropic Nielsen criterion in general (Johansen in the
isotropic case).

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import sym, grad, dot, jump, inner, sqrt
from dolfin import (
    Constant,
    FacetNormal,
    Measure,
    FunctionSpace,
    DirichletBC,
    dx,
    parameters,
    dS,
    plot,
    project,
)
from fenics_optim import MosekProblem, to_vect, AbsValue
import fenics_optim.limit_analysis as la

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa
from facet_plot import facet_plot

import warnings
from ffc.quadrature.deprecation import QuadratureRepresentationDeprecationWarning

parameters["form_compiler"]["representation"] = "quadrature"
warnings.simplefilter("ignore", QuadratureRepresentationDeprecationWarning)


def thin_plate_quadratic(
    mesh, facets, fixed_displ, load=Constant(-1), M0=Constant(1), **kwargs
):
    """Bending thin plate limit analysis with quadratic interpolation.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    facets : dolfin.MeshFunction
        facets MeshFunction
    fixed_displ : list
        list of functions for imposing fixed displacement
    load : UFL expression, optional
        distributed loading, by default Constant(-1)
    M0 : UFL expression, optional
        bending strength, by default Constant(1)
    """
    n = FacetNormal(mesh)

    ds = Measure("ds", subdomain_data=facets)

    Vu = FunctionSpace(mesh, "CG", 2)

    prob = MosekProblem("Bending plate limit analysis")

    bc = [DirichletBC(Vu, Constant(0.0), boundary) for boundary in fixed_displ]
    w = prob.add_var(Vu, bc=bc)

    R = FunctionSpace(mesh, "R", 0)

    def Pext(lamb):
        return lamb * dot(load, w) * dx

    prob.add_eq_constraint(R, A=Pext, b=1)

    theta = grad(w)
    chi = sym(grad(theta))

    M0 = kwargs.get("M0", Constant(1.0))
    M0x = kwargs.get("M0x", None)
    M0y = kwargs.get("M0y", None)
    if M0x is None and M0y is None:
        M0x = M0
        M0y = M0
    mat_bending = la.OrthotropicRankine2D(M0x, M0y, M0x, M0y)
    pi_c = mat_bending.support_function(to_vect(chi), quadrature_scheme="vertex")
    prob.add_convex_term(pi_c)

    k = n[0] ** 2 * Constant(M0x) + n[1] ** 2 * Constant(M0y)
    pi_bend_disc = AbsValue(
        [-k("+") * jump(theta, n), -k * dot(theta, n)],
        on_facet=True,
        measure=[dS, ds(1)],
    )

    prob.add_convex_term(pi_bend_disc)

    prob.parameters["presolve"] = True
    prob.parameters["log_level"] = kwargs.get("log_level", 0)
    prob.optimize()

    V0 = FunctionSpace(mesh, "DG", 0)

    bmax = np.max(mesh.coordinates(), axis=0)
    bmin = np.min(mesh.coordinates(), axis=0)

    X, Y, C, connec, camp = facet_plot(
        [-jump(theta, n), -dot(theta, n)], mesh, ds, kwargs.get("scale", 1)
    )

    bmax = np.max(mesh.coordinates(), axis=0)
    bmin = np.min(mesh.coordinates(), axis=0)

    Dx = bmax[0] - bmin[0]
    Dy = bmax[1] - bmin[1]
    pad = kwargs.get("pad", 0.1)

    plt.figure(figsize=(14, 4.2))
    plt.gcf()
    plt.suptitle(
        r"Collapse load factor = {:0.3f} [$qL^2/M_0$]".format(prob.pobj), fontsize=20
    )
    plt.subplot(1, 3, 1, projection="3d")
    plt.title("Collapse mechanism")
    plot(w, mode="warp")
    plt.subplot(1, 3, 2)
    plt.title("Yield lines")
    plt.tripcolor(
        X,
        Y,
        connec,
        C,
        shading="gouraud",
        cmap="bwr_r",
        linewidth=0.5,
        vmin=-camp,
        vmax=camp,
    )
    plt.xlim(bmin[0] - pad * Dx / 2, bmax[0] + pad * Dx / 2)
    plt.ylim(bmin[1] - pad * Dy / 2, bmax[1] + pad * Dy / 2)
    plot(mesh, linewidth=0.5)
    plt.colorbar(orientation="horizontal", pad=0.1)
    plt.gca().set_aspect("equal")
    plt.subplot(1, 3, 3)
    plt.title("Bending curvature")
    p2 = plot(project(sqrt(inner(chi, chi)), V0), cmap="Blues")
    plt.colorbar(p2, orientation="horizontal", pad=0.1)
    plot(mesh, linewidth=0.5)
    plt.xlim(bmin[0] - pad * Dx / 2, bmax[0] + pad * Dx / 2)
    plt.ylim(bmin[1] - pad * Dy / 2, bmax[1] + pad * Dy / 2)
    plt.gca().set_aspect("equal")
    plt.show()
