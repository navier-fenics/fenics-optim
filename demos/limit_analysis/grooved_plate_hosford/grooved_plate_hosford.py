#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a Hosford material.

The Hosford criterion involves a powwer-law exponent n which
reduces to Tresca for n=1 or infinity and von Mises for n=2.
The implementation uses the power cone available in Mosek v.9.
We consider a grooved plate under tension and compute its limit
load for various exponent values.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import as_matrix, dot, inner, sym, grad
from dolfin import (
    Point,
    near,
    MeshFunction,
    AutoSubDomain,
    VectorFunctionSpace,
    Measure,
    dx,
    Constant,
    DirichletBC,
    FunctionSpace,
    plot,
)
from fenics_optim import MosekProblem
import fenics_optim.limit_analysis as la
from mshr import Rectangle, Circle, generate_mesh
import matplotlib.pyplot as plt
import numpy as np

n_list = np.zeros((22,))
n_list[0] = 1.01
n_list[1:] = 1 + np.logspace(-1, 1, 21)
n_list[-1] = 10

a = 1.0
domain = Rectangle(Point(0, 0), Point(2 * a, 4 * a)) - Circle(Point(2 * a, 0.0), a)
mesh = generate_mesh(domain, 60)

V = VectorFunctionSpace(mesh, "CG", 2)


def bottom(x, on_boundary):
    return near(x[1], 0)


def left(x, on_boundary):
    return near(x[0], 0)


def top(x, on_boundary):
    return near(x[1], 4 * a)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(top).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets)

bc = [
    DirichletBC(V.sub(1), Constant(0.0), bottom),
    DirichletBC(V.sub(0), Constant(0.0), left),
]

results_head = ["n", "Nel", "Nvar", "Ncon", "iter", "time", "pobj"]
results = []
for n in n_list:
    mat = la.Hosford_plane_stress(Constant(1.0), n)

    prob = MosekProblem("Limit analysis with Hosford criterion")

    R = FunctionSpace(mesh, "R", 0)
    W = VectorFunctionSpace(mesh, "DG", 1, dim=3)
    lamb, Sig = prob.add_var([R, W])

    V = VectorFunctionSpace(mesh, "CG", 2)

    sig = as_matrix([[Sig[0], Sig[2]], [Sig[2], Sig[1]]])

    def equilibrium(u):
        return lamb * dot(u, Constant((0.0, 1.0))) * ds(1) - inner(
            sig, sym(grad(u))
        ) * dx(scheme="vertex", degree=1)

    prob.add_eq_constraint(V, A=equilibrium, bc=bc, name="Displacement")

    prob.add_obj_func([1])

    crit = mat.criterion(Sig, quadrature_scheme="vertex", degree=1)
    prob.add_convex_term(crit)

    prob.optimize(sense="maximize")
    results.append(
        [
            n,
            mesh.num_cells(),
            prob.get_solution_info()["opt_numvar"],
            prob.get_solution_info()["opt_numcon"],
            prob.get_solution_info()["intpnt_iter"],
            prob.get_solution_info()["optimizer_time"],
            prob.get_solution_info()["sol_itr_primal_obj"],
        ]
    )
X = np.array(results)
# np.savetxt("grooved_plate_hosford_info.csv", X, header=",".join(results_head),
# delimiter=",", comments='')

u = prob.get_lagrange_multiplier("Displacement")
plot(0.1 * u, mode="displacement")
plt.show()
