#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Computation of the homogenized criterion of a porous medium.

The solid skeleton material obeys the Drucker-Prager criterion
under periodicity conditions

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from mshr import Box, Sphere, generate_mesh
from ufl import dot, grad, sym, cos, sin, tan, pi
from dolfin import (
    Point,
    SubDomain,
    DOLFIN_EPS,
    near,
    VectorFunctionSpace,
    FunctionSpace,
    dx,
    Constant,
)
from fenics_optim import MosekProblem, to_mat
import fenics_optim.limit_analysis as la
import numpy as np
import matplotlib.pyplot as plt


L = 36.0
R = 10.0
domain = (
    Box(Point(0, 0, 0), Point(L, L, L))
    - Sphere(Point(L / 2, 0, 0), R)
    - Sphere(Point(0, L / 2, 0), R)
    - Sphere(Point(L / 2, L, 0), R)
    - Sphere(Point(L, L / 2, 0), R)
    - Sphere(Point(L / 2, 0, L), R)
    - Sphere(Point(0, L / 2, L), R)
    - Sphere(Point(L / 2, L, L), R)
    - Sphere(Point(L, L / 2, L), R)
    - Sphere(Point(L, 0, L / 2), R)
    - Sphere(Point(0, 0, L / 2), R)
    - Sphere(Point(0, L, L / 2), R)
    - Sphere(Point(L, L, L / 2), R)
)
mesh = generate_mesh(domain, 5)


class PeriodicDomain(SubDomain):
    """Periodic boundary conditions on the 3D box."""

    def __init__(self, tolerance=DOLFIN_EPS):
        SubDomain.__init__(self, tolerance)
        self.tol = tolerance

    def inside(self, x, on_boundary):
        """Return True if on left/bottom edge AND NOT on one of the two slave edges."""
        return bool(
            (
                near(x[0], 0, self.tol)
                or near(x[1], 0, self.tol)
                or near(x[2], 0, self.tol)
            )
            and (
                not (
                    (near(x[0], L, self.tol) and near(x[1], 0, self.tol))
                    or (near(x[0], L, self.tol) and near(x[2], 0, self.tol))
                    or (near(x[0], 0, self.tol) and near(x[1], L, self.tol))
                    or (near(x[2], 0, self.tol) and near(x[1], L, self.tol))
                    or (near(x[0], 0, self.tol) and near(x[2], L, self.tol))
                    or (near(x[1], 0, self.tol) and near(x[2], L, self.tol))
                )
            )
            and on_boundary
        )

    def map(self, x, y):
        """Map points to periodic counterpart."""
        if near(x[0], L, self.tol) and near(x[1], L, self.tol):
            y[0] = x[0] - L
            y[1] = x[1] - L
            y[2] = x[2]
        elif near(x[0], L, self.tol) and near(x[2], L, self.tol):
            y[0] = x[0] - L
            y[1] = x[1]
            y[2] = x[2] - L
        elif near(x[1], L, self.tol) and near(x[2], L, self.tol):
            y[0] = x[0]
            y[1] = x[1] - L
            y[2] = x[2] - L
        elif near(x[0], L, self.tol):
            y[0] = x[0] - L
            y[1] = x[1]
            y[2] = x[2]
        elif near(x[1], L, self.tol):
            y[0] = x[0]
            y[1] = x[1] - L
            y[2] = x[2]
        elif near(x[2], L, self.tol):
            y[0] = x[0]
            y[1] = x[1]
            y[2] = x[2] - L
        else:
            y[0] = x[0]
            y[1] = x[1]
            y[2] = x[2]


def homogenization(Sig, Sig0=None):
    """Perform limit analysis homogenization."""
    prob = MosekProblem("Homogenization")

    def Rd(d):
        """Real function space of dimension d."""
        return VectorFunctionSpace(mesh, "R", 0, dim=d)

    V = VectorFunctionSpace(
        mesh, "CG", 2, constrained_domain=PeriodicDomain(tolerance=1e-3)
    )
    R = FunctionSpace(mesh, "R", 0)

    Dmacro, u = prob.add_var([Rd(6), V], name=["Dmacro", "Mechanism"])

    def average(lamb):
        return dot(lamb, u) * dx

    prob.add_eq_constraint(Rd(3), A=average)

    def Pext(lamb):
        return lamb * dot(Sig, Dmacro) * dx

    prob.add_eq_constraint(R, A=Pext, b=1)

    if Sig0 is not None:
        prob.add_obj_func(-dot(Sig0, Dmacro) * dx)

    crit = la.DruckerPrager(1 / tan(pi / 6.0), pi / 6)
    pi_DP = crit.support_function(
        to_mat(Dmacro) + sym(grad(u)), quadrature_scheme="vertex"
    )
    prob.add_convex_term(pi_DP)

    prob.optimize()
    return prob.pobj


Sig = Constant((0.0,) * 6)  # macroscopic stress direction
Sig0 = Constant(
    (0.0,) * 6
)  # initial macroscopic stress from which we perform radial loading along Sig
S0 = (-1.0, -1, 0.0, 0.0, 0.0, 0)
t = np.concatenate(
    (
        np.linspace(pi / 4, pi / 2, 7),
        np.linspace(pi / 2, pi, 7)[1:],
        np.linspace(pi, 5 * pi / 4, 7)[1:],
    )
)
results = np.zeros((len(t),))
for (i, ti) in enumerate(t):
    Sig.assign(Constant((cos(ti), sin(ti), 0.0, 0, 0, 0)))
    Sig0.assign(Constant(S0))
    results[i] = homogenization(Sig, Sig0)

plt.figure()
plt.plot(S0[0] + results * np.cos(t), S0[1] + results * np.sin(t), "ok")
plt.plot(S0[0] + results * np.sin(t), S0[1] + results * np.cos(t), "ok")
plt.show()
