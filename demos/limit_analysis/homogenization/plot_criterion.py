#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Representation of the homogenized criterion.

Sigma_{xx}-Sigma_{yy} and Sigma_{xx}-Sigma_{xy} planes and
comparison with the skeleton Drucker-Prager criterion.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt
from math import pi, sin, sqrt, tan

plt.figure()
criterion = np.loadtxt("Sxx_Syy_plane.csv")
plt.plot(criterion[:, 0], criterion[:, 1], "ok", label="Homogenized criterion")

t = np.linspace(0, 2 * pi, 100)

phi = pi / 6
alp = sin(phi) / sqrt(3 + sin(phi) ** 2)
R = 3 * alp / tan(phi)

lamb = R / (np.sqrt(1 - np.cos(t) * np.sin(t)) + alp * (np.cos(t) + np.sin(t)))
plt.plot(lamb * np.cos(t), lamb * np.sin(t), "--k", label="Skeleton DP criterion")
plt.gca().set_aspect("equal")
plt.legend(loc=2, bbox_to_anchor=(0.0, 1.25))
plt.xlabel(r"$\Sigma_{xx}/c$", fontsize=16)
plt.ylabel(r"$\Sigma_{yy}/c$", fontsize=16)
plt.savefig("homogenized_criterion_Sxx_Syy.pdf")

plt.figure()
criterion = np.loadtxt("Sxx_Sxy_plane.csv")
plt.plot(criterion[:, 0], criterion[:, 1] / 2, "ok", label="Homogenized criterion")

t = np.linspace(0, 2 * pi, 100)

phi = pi / 6
alp = sin(phi) / sqrt(3 + sin(phi) ** 2)
R = 3 * alp / tan(phi)

lamb = R / (np.sqrt(np.cos(t) ** 2 + 3 * np.sin(t) ** 2) + alp * (np.cos(t)))
plt.plot(lamb * np.cos(t), lamb * np.sin(t), "--k", label="Skeleton DP criterion")
plt.gca().set_aspect("equal")
plt.legend(loc=2, bbox_to_anchor=(0.15, 1.3))
plt.xlabel(r"$\Sigma_{xx}/c$", fontsize=16)
plt.ylabel(r"$\Sigma_{xy}/c$", fontsize=16)
plt.savefig("homogenized_criterion_Sxx_Sxy.pdf")
