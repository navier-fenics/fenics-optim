#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mesh adaptation for a 2D slope stability problem.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    RectangleMesh,
    Point,
    near,
    Constant,
    DirichletBC,
    plot,
    FacetNormal,
    MeshFunction,
    AutoSubDomain,
)
from ufl import dot
import fenics_optim.limit_analysis as la
import matplotlib.pyplot as plt
from math import pi

L, H = 1.2, 1.0
mesh = RectangleMesh(Point(0, 0), Point(L, H), 5, 5, "crossed")

n = FacetNormal(mesh)


def border(x, on_boundary):  # noqa
    return near(x[1], 0) or near(x[0], 0)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(border).mark(facets, 1)

gamma = Constant((0.0, -1.0))

mat = la.MohrCoulomb2D(1.0, pi / 6.0)


kin = la.KinematicApproach(mesh, mat, interpolation="CG", degree=2)
kin.bc = [DirichletBC(kin.V, Constant((0.0, 0.0)), facets, 1)]
kin.measures["ds"] = kin.ds(1)
kin.prob.parameters["log_level"] = 0
kin.loading = lambda u: dot(u, gamma) * kin.dx
kin.optimize(refine_levels=5, refine_ratio=0.8)

plot(kin.mesh, linewidth=0.5)
plt.show()

plot(0.01 * kin.u, mode="displacement")
plt.show()

stat = la.StaticApproach(mesh, mat, facets=facets)
stat.bc = [DirichletBC(stat.V, Constant((0.0, 0.0)), facets, 1)]
stat.loading = lambda u: dot(u, gamma) * stat.dx
stat.prob.parameters["log_level"] = 0
stat.optimize(refine_levels=5, refine_ratio=0.8)

plot(stat.mesh, linewidth=0.5)
plt.show()

plot(0.01 * stat.prob.y[0], mode="displacement")
plt.show()
