#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Slope stability problem with Mohr-Coulomb material.

This demo uses a kinematic upper bound implementation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    RectangleMesh,
    Point,
    near,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    dx,
    plot,
    XDMFFile,
)
from ufl import dot, sym, grad
from fenics_optim import MosekProblem
import fenics_optim.limit_analysis as la
import matplotlib.pyplot as plt
from math import tan, pi

N = 50
mesh = RectangleMesh(Point(0, 0), Point(1.2, 1), N, N, "crossed")


def border(x, on_boundary):  # noqa
    return (near(x[1], 0) or near(x[0], 0)) and on_boundary


f = Constant((0.0, -1.0))
c, phi = 1.0, 30 * pi / 180
print("Reference solution:", 3.83 * tan(phi / 2 + pi / 4))

V = VectorFunctionSpace(mesh, "CG", 2)
bc = DirichletBC(V, Constant((0.0, 0.0)), border)

prob = MosekProblem("Upper bound limit analysis")
u = prob.add_var(V, bc=bc)

R = FunctionSpace(mesh, "R", 0)


def Pext(lamb):
    """Power of external loads."""
    return lamb * dot(f, u) * dx


prob.add_eq_constraint(R, A=Pext, b=1)

mat = la.MohrCoulomb2D(c, phi)
pi = mat.support_function(sym(grad(u)), quadrature_scheme="vertex")
prob.add_convex_term(pi)

prob.optimize()

c = pi.compute_cellwise()

plot(0.01 * u, mode="displacement")
plt.figure()

ffile = XDMFFile("results_slope.xdmf")
ffile.write(c, 0)
