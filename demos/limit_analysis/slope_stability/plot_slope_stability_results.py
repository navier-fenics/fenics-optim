#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot results of 2D slope stability problem with different
discretizations

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
import matplotlib.pyplot as plt
from math import pi

UB1 = {
    "name": "UB1",
    "degU": 1,
    "degS": 0,
    "B_scheme": "default",
    "C_scheme": "default",
    "degree": 0,
}
UB2 = {
    "name": "UB2",
    "degU": 2,
    "degS": 1,
    "B_scheme": "vertex",
    "C_scheme": "vertex",
    "degree": 1,
}
Disp1 = {
    "name": "Disp1",
    "degU": 2,
    "degS": 1,
    "B_scheme": "midside",
    "C_scheme": "midside",
    "degree": 1,
}
Disp2 = {
    "name": "Disp2",
    "degU": 2,
    "degS": 1,
    "B_scheme": "default",
    "C_scheme": "default",
    "degree": 2,
}
Mixed = {
    "name": "Mixed",
    "degU": 2,
    "degS": 1,
    "B_scheme": "midside",
    "C_scheme": "vertex",
    "degree": 1,
}
LB = {"name": "LB"}

res = np.loadtxt("results/LB.csv", delimiter=",")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "--o", label=r"\texttt{LB}")
for FE in [UB1, UB2, Disp1, Disp2, Mixed]:
    res = np.loadtxt("results/" + FE["name"] + ".csv")
    plt.semilogx(
        4 * res[:, 0] ** 2, res[:, 1], "-o", label=r"\texttt{{{}}}".format(FE["name"])
    )

fs = 6.69  # Chen
plt.semilogx([0, 1e6], [fs, fs], "-.k")
plt.annotate(
    "Chen upper bound: 6.69",
    xy=(4e5, fs),
    xytext=(6e3, 6),
    fontsize=14,
    arrowprops=dict(arrowstyle="->"),
)
plt.ylim(5, 10)
plt.xlabel("Number of elements")
plt.ylabel(r"Factor of safety $[\gamma H/c]$")
plt.legend(ncol=2)
plt.savefig("vertical_slope_stability.pdf")

plt.figure()
res = np.loadtxt("results/LB.csv", delimiter=",")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "-oC0", label=r"\texttt{LB}")
res = np.loadtxt("results/UB1.csv")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "-oC1", label=r"\texttt{UB1}")
res = np.loadtxt("results/UB1_disc.csv", delimiter=",")
plt.semilogx(
    4 * res[:, 0] ** 2,
    res[:, 1],
    "--sC1",
    markerfacecolor="w",
    label=r"\texttt{UB1-disc}",
)
res = np.loadtxt("results/UB2.csv")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "-oC2", label=r"\texttt{UB2}")
res = np.loadtxt("results/UB2_disc.csv", delimiter=",")
plt.semilogx(
    4 * res[:, 0] ** 2,
    res[:, 1],
    "--sC2",
    markerfacecolor="w",
    label=r"\texttt{UB2-disc}",
)

plt.semilogx([0, 1e6], [fs, fs], "-.k")
plt.annotate(
    "Chen upper bound: 6.69",
    xy=(4e5, fs),
    xytext=(6e3, 6),
    fontsize=14,
    arrowprops=dict(arrowstyle="->"),
)
plt.ylim(5, 8)
plt.xlabel("Number of elements")
plt.ylabel(r"Factor of safety $[\gamma H/c]$")
plt.legend(ncol=2)
plt.savefig("vertical_slope_stability_disc.pdf")


plt.figure()
res = np.loadtxt("results/LB.csv", delimiter=",")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "-oC0", label=r"\texttt{LB}")
res = np.loadtxt("results/LB_adapt.csv", delimiter=",")
plt.semilogx(
    res[:, 0], res[:, 1], "--sC0", markerfacecolor="w", label=r"\texttt{LB-adapt}"
)
res = np.loadtxt("results/UB2.csv")
plt.semilogx(4 * res[:, 0] ** 2, res[:, 1], "-oC1", label=r"\texttt{UB2}")
res = np.loadtxt("results/UB2_adapt.csv", delimiter=",")
plt.semilogx(
    res[:, 0], res[:, 1], "--sC1", markerfacecolor="w", label=r"\texttt{UB2-adapt}"
)

plt.semilogx([0, 1e6], [fs, fs], "-.k")
plt.annotate(
    "Chen upper bound: 6.69",
    xy=(4e5, fs),
    xytext=(6e3, 6),
    fontsize=14,
    arrowprops=dict(arrowstyle="->"),
)
plt.ylim(5, 8)
plt.xlabel("Number of elements")
plt.ylabel(r"Factor of safety $[\gamma H/c]$")
plt.legend(ncol=2)
plt.savefig("vertical_slope_stability_adapt.pdf")