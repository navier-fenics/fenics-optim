#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Slope stability problem with Mohr-Coulomb material:
static lower bound implementation

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    RectangleMesh,
    Point,
    FacetNormal,
    near,
    MeshFunction,
    Measure,
    dx,
    AutoSubDomain,
    Constant,
    DirichletBC,
    Function,
    FunctionSpace,
    VectorFunctionSpace,
    plot,
)
from ufl import dot, jump, avg, as_matrix, div, pi, tan
from fenics_optim import MosekProblem
import fenics_optim.limit_analysis as la
import numpy as np
import matplotlib.pyplot as plt

N = 80
mesh = RectangleMesh(Point(0, 0), Point(1.2, 1), N, N, "crossed")
n = FacetNormal(mesh)


def border(x, on_boundary):
    return near(x[1], 0) or near(x[0], 0)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(border).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets, domain=mesh)
dS = Measure("dS", subdomain_data=facets, domain=mesh)

f = Constant((0.0, -1.0))
c, phi = 1.0, 30 * pi / 180
mat = la.MohrCoulomb2D(c, phi)
print("Reference solution:", 3.833 * tan(phi / 2 + pi / 4))

prob = MosekProblem("Lower bound limit analysis")
R = FunctionSpace(mesh, "R", 0)
W = VectorFunctionSpace(mesh, "DG", 1, dim=3)
lamb, Sig = prob.add_var([R, W])
sig = as_matrix([[Sig[0], Sig[2]], [Sig[2], Sig[1]]])

V_eq = VectorFunctionSpace(mesh, "DG", 0)


def equilibrium(u):
    return dot(u, lamb * f + div(sig)) * dx


prob.add_eq_constraint(V_eq, A=equilibrium, name="Pseudo-mechanism")

V_jump = VectorFunctionSpace(mesh, "Discontinuous Lagrange Trace", 1)


def continuity(v):
    return dot(avg(v), jump(sig, n)) * dS + dot(v, dot(sig, n)) * ds(0)


prob.add_eq_constraint(V_jump, A=continuity)

prob.add_obj_func([1, None])

crit = mat.criterion(Sig, quadrature_scheme="vertex")
prob.add_convex_term(crit)

prob.optimize(sense="maximize")

u = prob.get_lagrange_multiplier("Pseudo-mechanism")
plot(0.01 * u, mode="displacement")
plt.show()

pl = plot(Sig[1])
plt.colorbar(pl)
plt.show()