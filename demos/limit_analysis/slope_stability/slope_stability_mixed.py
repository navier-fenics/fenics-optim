#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Slope stability problem with Mohr-Coulomb material.

This demo implements various mixed approaches.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    RectangleMesh,
    Point,
    near,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    dx,
)
from ufl import dot, sym, grad, inner, as_matrix
from fenics_optim import MosekProblem
import fenics_optim.limit_analysis as la
import numpy as np
import matplotlib.pyplot as plt
from math import pi


def compute_stability(FE, N=10):
    """Slope stability computation using various FE schemes."""
    degU = FE["degU"]
    degS = FE["degS"]
    B_scheme = FE["B_scheme"]
    C_scheme = FE["C_scheme"]
    quadrature_degree = FE["degree"]

    mesh = RectangleMesh(Point(0, 0), Point(1.2, 1), N, N, "crossed")

    def border(x, on_boundary):
        return near(x[1], 0) or near(x[0], 0)

    f = Constant((0.0, -1.0))
    c, phi = 1.0, 30 * pi / 180
    mat = la.MohrCoulomb2D(c, phi)

    prob = MosekProblem("Slope stability")
    R = FunctionSpace(mesh, "R", 0)
    W = VectorFunctionSpace(mesh, "DG", degS, dim=3)
    lamb, Sig = prob.add_var([R, W])

    V = VectorFunctionSpace(mesh, "CG", degU)
    bc = DirichletBC(V, Constant((0, 0)), border)

    sig = as_matrix([[Sig[0], Sig[2]], [Sig[2], Sig[1]]])

    def equilibrium(u):
        return lamb * dot(u, f) * dx - inner(sig, sym(grad(u))) * dx(
            scheme=B_scheme, degree=quadrature_degree
        )

    prob.add_eq_constraint(V, A=equilibrium, bc=bc)

    prob.add_obj_func([1, None])

    crit = mat.criterion(Sig, quadrature_scheme=C_scheme, degree=quadrature_degree)
    prob.add_convex_term(crit)

    prob.optimize(sense="maximize")

    return prob.pobj


N_list = np.array([2, 5, 10, 20, 50, 100, 200])
UB1 = {
    "name": "UB1",
    "degU": 1,
    "degS": 0,
    "B_scheme": "default",
    "C_scheme": "default",
    "degree": 0,
}
UB2 = {
    "name": "UB2",
    "degU": 2,
    "degS": 1,
    "B_scheme": "vertex",
    "C_scheme": "vertex",
    "degree": 1,
}
Disp1 = {
    "name": "Disp1",
    "degU": 2,
    "degS": 1,
    "B_scheme": "midside",
    "C_scheme": "midside",
    "degree": 1,
}
Disp2 = {
    "name": "Disp2",
    "degU": 2,
    "degS": 1,
    "B_scheme": "default",
    "C_scheme": "default",
    "degree": 2,
}
Mixed = {
    "name": "Mixed",
    "degU": 2,
    "degS": 1,
    "B_scheme": "midside",
    "C_scheme": "vertex",
    "degree": 1,
}
for FE in [UB1, UB2, Disp1, Disp2, Mixed]:
    res = []
    for N in N_list:
        res.append(compute_stability(FE, N))
    res = np.array(res)

    plt.semilogx(N_list, res)
