#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Slope stability problem with Mohr-Coulomb material.

This demo uses a discontinuous upper bound implementation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    RectangleMesh,
    Point,
    near,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    dx,
    plot,
    FacetNormal,
    MeshFunction,
    AutoSubDomain,
    Measure,
)
from ufl import dot, sym, grad, jump
from fenics_optim import MosekProblem, local_frame
import fenics_optim.limit_analysis as la
from math import tan, pi

N = 20
mesh = RectangleMesh(Point(0, 0), Point(1.2, 1), N, N, "crossed")


def border(x, on_boundary):  # noqa
    return near(x[1], 0) or near(x[0], 0)


facets = MeshFunction("size_t", mesh, 1)
AutoSubDomain(border).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets, domain=mesh)
dS = Measure("dS", subdomain_data=facets, domain=mesh)

f = Constant((0.0, -1.0))
c, phi = 1.0, 30 * pi / 180
print("Reference solution:", 3.77 * tan(phi / 2 + pi / 4))

V = VectorFunctionSpace(mesh, "DG", 2)
bc = DirichletBC(V, Constant((0.0, 0.0)), border)

prob = MosekProblem("Upper bound discontinuous limit analysis")
u = prob.add_var(V, bc=bc)

R = FunctionSpace(mesh, "R", 0)


def Pext(lamb):
    """Power of external loads."""
    return lamb * dot(f, u) * dx


prob.add_eq_constraint(R, A=Pext, b=1)

mat = la.MohrCoulomb2D(c, phi)
pi = mat.support_function(sym(grad(u)), quadrature_scheme="vertex")
prob.add_convex_term(pi)
n = FacetNormal(mesh)
P = local_frame(n("-"))
P2 = local_frame(n)
pi_d = mat.disc_support_function(
    [dot(P, jump(u)), -dot(P2, u)], measure=[dS, ds(1)], on_facet=True, degree=3
)
prob.add_convex_term(pi_d)
prob.optimize()

plot(0.01 * u, mode="displacement")
