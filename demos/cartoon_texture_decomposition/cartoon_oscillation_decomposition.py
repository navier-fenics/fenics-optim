#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Cartoon-texture oscillation decomposition demo.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import Constant, UnitSquareMesh, UserExpression, Cell, FunctionSpace, dx
from ufl import dot, div, grad
from fenics_optim import MosekProblem, L2Norm, L2Ball
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np

alpha = Constant(2e-4)

img = plt.imread("Barbara_512x512.png")
(Nx, Ny) = img.shape

mesh = UnitSquareMesh(Nx, Ny, "left/right")


class FE_image(UserExpression):
    """Original image on FE mesh."""

    def eval_cell(self, value, x, ufc_cell):
        p = Cell(mesh, ufc_cell.index).midpoint()
        i, j = int(p[0] * (Nx - 1)), int(p[1] * (Ny - 1))
        value[:] = img[-(j + 1), i]

    def value_shape(self):
        return ()


y = FE_image()

prob = MosekProblem("Cartoon/texture decomposition")
Vu = FunctionSpace(mesh, "CR", 1)
Vg = FunctionSpace(mesh, "RT", 1)
u, g = prob.add_var([Vu, Vg])


def constraint(lamb):
    """Constraint u + div(g) = y lhs."""
    return dot(lamb, u + div(g)) * dx


def rhs(lamb):
    """Constraint u + div(g) = y rhs."""
    return dot(lamb, y) * dx


prob.add_eq_constraint(Vu, A=constraint, b=rhs)

tv_norm = L2Norm(grad(u))
prob.add_convex_term(tv_norm)

g_norm = L2Ball(g, k=alpha)
prob.add_convex_term(g_norm)

prob.optimize()

decomp_img = np.zeros((Nx, Ny))
for i in range(Nx):
    for j in range(Ny):
        x = i / (Nx - 1)
        y = j / (Ny - 1)
        decomp_img[-(j + 1), i] = u(x, y)


rcParams["image.cmap"] = "gray"
plt.figure(figsize=(12, 4))
plt.subplot(1, 3, 1)
plt.imshow(img)
plt.title("Original image", fontsize=20)
plt.axis("off")
plt.subplot(1, 3, 2)
plt.imshow(decomp_img)
plt.title("Cartoon layer", fontsize=20)
plt.axis("off")
plt.subplot(1, 3, 3)
plt.imshow(img - decomp_img)
plt.title("Texture layer", fontsize=20)
plt.axis("off")
plt.savefig("decomposition.png", dpi=300)
plt.show()
