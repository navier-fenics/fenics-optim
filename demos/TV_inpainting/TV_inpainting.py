#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Total-variation inpainting problem.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""

from dolfin import (
    UnitSquareMesh,
    UserExpression,
    Function,
    FunctionSpace,
    VectorFunctionSpace,
)
from ufl import grad
from fenics_optim import MosekProblem, L2Norm
import numpy as np
import matplotlib.pyplot as plt

corruption_level = 50
img = plt.imread("cat_512x512.png")
(Nx, Ny, c) = img.shape
mask = np.random.rand(Nx, Ny) <= corruption_level / 100
original = np.copy(img)
img[mask, :] = 0

mesh = UnitSquareMesh(Nx, Ny, "left/right")


class FE_image(UserExpression):
    """Original image on FE mesh."""

    def eval(self, values, x):

        i, j = int(x[0] * (Nx - 1)), int(x[1] * (Ny - 1))
        for k in range(3):
            values[k] = img[-(j + 1), i, k]

    def value_shape(self):
        return (3,)


class FE_mask(UserExpression):
    """Masked image on FE mesh."""

    def eval(self, values, x):

        i, j = int(x[0] * (Nx - 1)), int(x[1] * (Ny - 1))
        if mask[-(j + 1), i]:
            values[0] = 0
        else:
            values[0] = 1

    def value_shape(self):
        return ()


V0 = FunctionSpace(mesh, "CR", 1)
V = VectorFunctionSpace(mesh, "CR", 1, dim=3)

u_known = Function(V)
u_known.interpolate(FE_image())

fe_mask = Function(V0)
fe_mask.interpolate(FE_mask())
mask_array = np.kron(fe_mask.vector().get_local(), np.ones((3,))).astype(bool)

inf = 1e30
ux = Function(V)
ux_array = inf * np.ones((V.dim(),))
ux_array[mask_array] = u_known.vector().get_local()[mask_array]
ux.vector().set_local(ux_array)
lx = Function(V)
lx_array = -inf * np.ones((V.dim(),))
lx_array[mask_array] = u_known.vector().get_local()[mask_array]
lx.vector().set_local(lx_array)

prob = MosekProblem("TV inpainting")
u = prob.add_var(V, ux=ux, lx=lx)

for i in range(3):
    tv_norm = L2Norm(grad(u[i]))
    prob.add_convex_term(tv_norm)

prob.optimize()

restored_img = np.zeros((Nx, Ny, 3))
for i in range(Nx):
    for j in range(Ny):
        x = i / (Nx - 1)
        y = j / (Ny - 1)
        restored_img[-(j + 1), i, :] = u(x, y)

plt.figure(figsize=(12, 4))
plt.subplot(1, 3, 1)
plt.imshow(original)
plt.axis("off")
plt.title("Original image", fontsize=20)
plt.subplot(1, 3, 2)
plt.title("Corrupted image", fontsize=20)
plt.imshow(img)
plt.axis("off")
plt.subplot(1, 3, 3)
plt.imshow(restored_img)
plt.axis("off")
plt.title("TV restored", fontsize=20)
plt.savefig("TV_inpainting_{}p.png".format(corruption_level), dpi=300)
plt.show()
