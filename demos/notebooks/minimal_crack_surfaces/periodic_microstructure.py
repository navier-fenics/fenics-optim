#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from dolfin import SubDomain, near, DOLFIN_EPS, UserExpression, Cell, UnitSquareMesh


class PeriodicBoundary(SubDomain):
    def __init__(self, vertices, tolerance=DOLFIN_EPS):
        """ vertices stores the coordinates of the 4 unit cell corners"""
        SubDomain.__init__(self, tolerance)
        self.tol = tolerance
        self.vv = vertices
        self.a1 = self.vv[1, :] - self.vv[0, :]  # first vector generating periodicity
        self.a2 = self.vv[3, :] - self.vv[0, :]  # second vector generating periodicity
        # check if UC vertices form indeed a parallelogram
        assert np.linalg.norm(self.vv[2, :] - self.vv[3, :] - self.a1) <= self.tol
        assert np.linalg.norm(self.vv[2, :] - self.vv[1, :] - self.a2) <= self.tol

    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the
        # bottom-right or top-left vertices
        return bool(
            (
                near(x[0], self.vv[0, 0] + x[1] * self.a2[0] / self.vv[3, 1], self.tol)
                or near(
                    x[1], self.vv[0, 1] + x[0] * self.a1[1] / self.vv[1, 0], self.tol
                )
            )
            and (
                not (
                    (
                        near(x[0], self.vv[1, 0], self.tol)
                        and near(x[1], self.vv[1, 1], self.tol)
                    )
                    or (
                        near(x[0], self.vv[3, 0], self.tol)
                        and near(x[1], self.vv[3, 1], self.tol)
                    )
                )
            )
            and on_boundary
        )

    def map(self, x, y):
        if near(x[0], self.vv[2, 0], self.tol) and near(
            x[1], self.vv[2, 1], self.tol
        ):  # if on top-right corner
            y[0] = x[0] - (self.a1[0] + self.a2[0])
            y[1] = x[1] - (self.a1[1] + self.a2[1])
        elif near(
            x[0], self.vv[1, 0] + x[1] * self.a2[0] / self.vv[2, 1], self.tol
        ):  # if on right boundary
            y[0] = x[0] - self.a1[0]
            y[1] = x[1] - self.a1[1]
        else:  # should be on top boundary
            y[0] = x[0] - self.a2[0]
            y[1] = x[1] - self.a2[1]


class OverlappingInclusions(object):
    def __init__(self, centers, radii, size):
        """
        Generate an image of random overlapping inclusions.

        Parameters
        ----------
        centers : array
            an array of center positions (in pixel units)
        radii : array
            an array of inclusion radius (in pixel units)
        N : tuple
            image size
        """
        self.centers = centers
        self.radii = radii
        self.size = tuple(size)
        self.d = len(self.size)
        self.phasemap = np.zeros(self.size, dtype=np.int8)
        self.initialize_phase_map()

    def initialize_phase_map(self):
        for i, center in enumerate(self.centers):
            r = self.radii[i]
            c = np.rint(center).astype(int)
            for s in np.ndindex(tuple(self.d * [2 * int(r) + 1])):
                ds = np.array(s) - int(r)
                n = c - ds
                if (n - center + 0.5).dot(n - center + 0.5) <= r * r:
                    self.phasemap[tuple(n % self.size)] = 1

    def get_phase(self, n):
        return self.phasemap[n]


class FE_image(UserExpression):
    """Transform an image into a UserExpression on a UnitSquareMesh."""

    def __init__(self, mesh, img):
        self.mesh = mesh
        self.img = img
        self.dims = self.img.shape
        super().__init__(self)

    def eval_cell(self, value, x, ufc_cell):
        p = Cell(self.mesh, ufc_cell.index).midpoint()
        i, j = int(p[0] * self.dims[1]), int(p[1] * self.dims[0])
        value[:] = self.img[-j, i]

    def value_shape(self):
        return ()


def generate_microstructure(centers, radii, dims):
    print("Generating microstructure...")
    img = OverlappingInclusions(centers, radii, dims).phasemap

    mesh = UnitSquareMesh(*dims, "crossed")

    return mesh, FE_image(mesh, img)