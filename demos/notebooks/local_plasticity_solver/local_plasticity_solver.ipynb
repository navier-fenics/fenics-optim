{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Local plasticity solver\n",
    "\n",
    "$\\newcommand{\\beps}{\\boldsymbol{\\varepsilon}}\n",
    "\\newcommand{\\bepsp}{\\boldsymbol{\\varepsilon}^\\text{p}}\n",
    "\\newcommand{\\Deps}{\\Delta\\boldsymbol{\\varepsilon}}\n",
    "\\newcommand{\\Depsp}{\\Delta\\boldsymbol{\\varepsilon}^\\text{p}}\n",
    "\\newcommand{\\bepsel}{\\boldsymbol{\\varepsilon}^\\text{el}}\n",
    "\\newcommand{\\bsig}{\\boldsymbol{\\sigma}}\n",
    "\\newcommand{\\bsigtr}{\\boldsymbol{\\sigma}^\\text{tr}}\n",
    "\\newcommand{\\bu}{\\boldsymbol{u}}\n",
    "\\newcommand{\\CC}{\\mathbb{C}}\n",
    "\\newcommand{\\KK}{\\mathbb{K}}\n",
    "\\newcommand{\\JJ}{\\mathbb{J}}\n",
    "\\newcommand{\\RR}{\\mathbb{R}}\n",
    "\\newcommand{\\Kk}{\\mathcal{K}}\n",
    "\\newcommand{\\tr}{\\operatorname{tr}}\n",
    "\\newcommand{\\dev}{\\operatorname{dev}}\n",
    "\\newcommand{\\bf}{\\boldsymbol{f}}\n",
    "\\newcommand{\\dOm}{\\text{d}\\Omega}\n",
    "\\DeclareMathOperator*{argmin}{arg\\min}\n",
    "\\newcommand{\\bC}{\\boldsymbol{C}}\n",
    "\\newcommand{\\bQ}{\\boldsymbol{Q}}\n",
    "\\newcommand{\\bx}{\\boldsymbol{x}}\n",
    "\\newcommand{\\T}{^\\text{T}}$\n",
    "\n",
    "In elastoplastic problems, the standard numerical strategy to solve the corresponding nonlinear equations is to resort to a Newton-Raphson method at the global structural scale and a local nonlinear solver at the material point level to solve the plasticity evolution equations. In such a setting, the latter problem is driven by a given strain increment and a predictor/corrector strategy is generally used. One indeed assumes a first elastic trial stress state. In the case the plasticity yield condition is violated, one must project the trial elastic field onto the yield surface. Various strategies for computing this projection are available in the litterature and most of them rely on a local Newton method as well. This approach may suffer from robustness issues if the elastic trial state is too large or in case of non-smooth yield surfaces (non-smooth yield criteria, multisurface plasticity, etc.).\n",
    "\n",
    "In this demo, we formulate the plasticity projection step as a minimization problem to be solved with `fenics_optim`.\n",
    "\n",
    "## Trial stress state projection\n",
    "\n",
    "Let us consider a linear elastic setting characterized by some elastic stiffness tensor $\\CC$. Consider also a previous stress state $\\bsig_n$ and a given strain increment $\\Deps$. The *trial* stress state is obtained assuming an elastic behaviour for the strain increment:\n",
    "\\begin{equation}\n",
    "\\bsigtr = \\bsig_n + \\CC:\\Deps\n",
    "\\end{equation}\n",
    "\n",
    "The *real* stress field is obtained by projecting this trial stress state onto the yield surface with respect to the distance induced by the elastic compliance stiffness tensor $\\CC^{-1}$. That is, one wants to solve:\n",
    "\\begin{equation}\n",
    "\\begin{array}{rl}\n",
    "\\displaystyle{\\bsig_{n+1} = \\argmin_{\\bsig}} & \\displaystyle{\\dfrac{1}{2}(\\bsig-\\bsigtr):\\CC^{-1}:(\\bsig-\\bsigtr)} \\\\\n",
    "\\text{s.t.} & \\bsig \\in G\n",
    "\\end{array}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 138,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dolfin import UnitIntervalMesh, VectorFunctionSpace, Constant, dx\n",
    "from fenics_optim import (\n",
    "    MosekProblem,\n",
    "    ConvexFunction,\n",
    "    QuadraticTerm,\n",
    "    Quad,\n",
    "    Exp,\n",
    "    tail,\n",
    "    concatenate,\n",
    "    to_vect,\n",
    "    to_mat,\n",
    "    SumExpr\n",
    ")\n",
    "import fenics_optim.limit_analysis as la\n",
    "from ufl import diag, as_vector, cos, sin, shape, sqrt\n",
    "import numpy as np\n",
    "\n",
    "mesh = UnitIntervalMesh(1)\n",
    "theta = Constant(0)\n",
    "sig0 = Constant(1.)\n",
    "sigtr = sig0*as_vector([cos(theta), sin(theta), 0, 0, 0, 0])\n",
    "\n",
    "metric = {3: diag(as_vector([1, 1, sqrt(2)])),\n",
    "          6: diag(as_vector([1, 1, 1, sqrt(2),sqrt(2),sqrt(2)]))}\n",
    "def project_stress(mat, rad, sigtr, plane_stress=True):\n",
    "    dim = len(sigtr)\n",
    "    V = VectorFunctionSpace(mesh, \"R\", 0, dim=dim)\n",
    "    prob = MosekProblem(\"Local plasticity solver\")\n",
    "    sig = prob.add_var(V)\n",
    "    \n",
    "    Q = metric[dim]\n",
    "    dist = QuadraticTerm(sig, Q=Q, x0=Constant(rad)*sigtr)\n",
    "    \n",
    "    crit = mat.criterion(sig)\n",
    "    \n",
    "    \n",
    "    prob.add_convex_term(crit)\n",
    "    prob.add_convex_term(dist)\n",
    "    if dim == 6 and plane_stress:\n",
    "        prob.add_eq_constraint(V.sub(0).collapse(), A=lambda x: x*sig[2]*dx)\n",
    "    \n",
    "    prob.parameters[\"log_level\"] = 0\n",
    "    prob.optimize()\n",
    "    it = prob.get_solution_info(False)[\"intpnt_iter\"]\n",
    "\n",
    "    return sig.vector()[:], it\n",
    "\n",
    "def plot_results(Sig, iterations):\n",
    "    vmin = min([min(it) for it in iterations.values()])\n",
    "    vmax = max([max(it) for it in iterations.values()])\n",
    "    c = np.linspace(0, 1, 50)\n",
    "    colors = plt.cm.plasma(c)\n",
    "    cmap = mpl.colors.ListedColormap(colors)\n",
    "    ax = plt.gca()\n",
    "    dummie_ax = ax.scatter(c, c, c=vmin+(vmax-vmin)*c, cmap=cmap)\n",
    "    ax.cla()\n",
    "    cbar = plt.colorbar(dummie_ax, cmap=cmap)\n",
    "    #plt.clim(vmin, vmax)\n",
    "    cbar.set_label(\"Number of iterations\")\n",
    "\n",
    "    def to_col(v):\n",
    "        loc = int((v-vmin)/(vmax-vmin)*(len(c)-1))\n",
    "        return colors[loc, :]\n",
    "    for r, Sig_r in Sig.items():\n",
    "        for (sig, it) in zip(Sig_r, iterations[r]):\n",
    "            Sig_r = np.asarray(Sig_r)\n",
    "            col = to_col(it)\n",
    "            plt.plot(sig[0], sig[1], \"o\", color=col)\n",
    "        new_Sig = np.vstack((Sig_r, Sig_r[[0], :]))\n",
    "        plt.plot(new_Sig[:, 0], new_Sig[:, 1], \"-k\", linewidth=0.5)\n",
    "    plt.gca().set_aspect(\"equal\")\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 130,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/bleyerj/Fenics/fenics-optim/fenics_optim/limit_analysis/materials/strength_criterion.py:99: UserWarning: \n",
      "-----------\n",
      "MohrCoulomb3D disc_support_function not implemented!\n",
      "-----------\n",
      "  warnings.warn(\n"
     ]
    }
   ],
   "source": [
    "mat = la.MohrCoulomb3D(1, np.pi/6)\n",
    "\n",
    "Sig = {}\n",
    "iterations = {}\n",
    "for rad in [5, 10, 20, 30]:\n",
    "    Sig[rad] = []\n",
    "    iterations[rad] = []\n",
    "    for t in np.linspace(-np.pi, np.pi, 41):\n",
    "        theta.assign(Constant(t))\n",
    "        sig, it = project_stress(mat, rad, sigtr)\n",
    "        Sig[rad].append(float(rad)*sig)\n",
    "        iterations[rad].append(it)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 139,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAW0AAAEACAYAAAB4ayemAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8li6FKAAAgAElEQVR4nO29e3xU9Z3//3xnEnIPuUASJUEIAi2g6BibRSnYiqV1u9uqYNVethdF3Hbt7n63iNt2a7f2J9Dd7q1bAVvbbne9oK1u19Va0Q00SqkhGgUqtwQh0QTJhdwvM/P5/XHODJNkkplJJnPmzPk8feRB5nMu837PMa/5nPd5f95vUUqh0Wg0GnuQYrUBGo1Go4kcLdoajUZjI7RoazQajY3Qoq3RaDQ2Qou2RqPR2Agt2hqNRmMjUuP1RiKyFTgB7AJuBtqVUk+a2yqAdUAd4AZ2KqU642WbRqPRTISIuIGHlFJXhNi2DigEGgCUUrun05Z4z7R3AI1Avl+w/eNKqW2ms08CW+Nsl0aj0YRERNaYv7pDbFsHVCildmKI9rRrl8RrcY2IrBsl1P7xCgzRvi5orEMpVRAXwzQajSYCREQppWTU2Aml1IJ42hH3mLZ5mxGMGxgTCjHFXKPRaBISU8s6g36PC/EU7QrzNqNBRLYGOVkYYt92ID9+pmk0Gk3UVADtZoikQUQ2BYVSpo24PYhUSm3z/y4iO4AXgKhuK0RkA7ABIDMz84ry8nIA0tPTcblc9PX1AZCamkpmZibd3d3+48jJyaGvrw+v1wtAdnY2w8PDDA0NAZCRkYGI0N/fD0BaWhrp6en09PQAkJKSQnZ2dkzO0dvbi8/nAyAnJ4fBwUGGh4cx/UIpxcDAAAAzZswgLS2N3t7ewOeQm5s7pXO4XC6ysrLo6enBHx7Lzc2lv78fj8cDQFZWFl6vl8HBwYg+41icY6LrNDg4iIjY5jr5P+OpniM1NZWhoSHbXKdY/T0dPXr0rFJqNlNgzUeyVFubN6J9X68bOgQMBA3tNOPUE1EIrPGHdkVkJ8Yzu2kN7cYzeyTfnxGilGoICn+EmlUXEiJkYn6IOwEuv/xy9dprr02jxYlJR0cHBQXOC/drv52FiLw91XO0tXmp3jcnon3z0xsHlFKVUb5FO0bGGwBKqU4RyReRCqVUQ5Tnipi4hEfMW4YXx9lcR4gQSTin/d/OTiN4xuQktN+aBKQuxFgnhphPG/GKadcCD/hfmDGgJ2GsOJsz8F3hTui/lXMaDQ3T9gWe0Gi/NYmGqV3tIpIPAe1qmO41JnEJj5i3DQ0isgnjm2iBUmp90C7rzW11gFspdWc87NJoNJpwmJECt/n7VuCFoAU064F7ReQExjO6a6fbnng+iKwj9O2E/xvL/6AyotVE6enpMbLMXsybN89qEyxB+62JFvEJrv4ZUz6PKdC7Oa9Rwds6gXum/CZRYNvaIy6Xy2oTLKGwMFSGZPKj/dZoDGwr2v50JKdRVxfyZiXp0X5rNAa2FW2NRqNxInGLacea1FTbmj4lnJizC9pvzSTwgWsg+RqX23amnZmZabUJlrB8+XKrTbAE7bdGY2Bb0XbqooM9e/ZYbYIlaL81GgPbirZTiVcp3URD+63RGGjRthkiEn6nJET7rdEY2PZpXm5urtUmWMLq1autNsEStN+aaBEFKf3Jd6di25m2v+Sj06ivr7faBEvQfms0BradaftrATuNjo4Oq02wBKf5ffTJx3l9Wwv972Rz5MJqLttUyqJ1n7LarJB8P//nzMwYxpWi8PqEcwNp/HXnZ602K2mx7Uxbo0lWjj75OL/f3EF/cw4oob85h99v7uDok49bbdoYvp//cwqyhkh1KUQg1aUoyBri+/k/t9q0pMW2M+2srCyrTbAEtzturegSimT2u6/2ZXqe2YWvo42UgiKa9+fh7Z/HOe852r3mHcYQnP7WMa6bF9cesmHpcB2ja1jR7u3g/emLyUrJIkVgZsaw1aYl7eIa24q2U5sgtLe3k5eXZ7UZcSdZ/e6rfZmux34Mw0abrpamJvb1NfEGGbj6L6Q4NajjVouipaXFIktD0606ER/8b89zKHxUZl4BgCsl+cQyUbCtaDu1CcLJkycdWa4zWf3ueWYXZzrP8dyJ0zR191KcncnHFpRz68JC9j531Yh9M+f08PGPf9wiS0PTOuMMqS5DoGemzAyMe306VXG6sK1oazR2pqWlhaeeeorDz+wOCHVZbnZgu1IjJyWuzGEu21QabzPDcm4gjYKsoRFjPmWMa6YH24q2U5sgVFRUhN8pCUkGv/1CferUKUpLS7nhhhu4qfUtfB1tY/ZVGZlkzumh/51sMi/sTdjskb/u/Czfz/85XhGUgMebONkj4gPpS74Zv21F26lNEJy6qMiufocS6rlz5wa293385hExbQDSZpB/8+e5edvVtujG/tedn+WZZwooLS2lsjLahuaaaLGtaDu1CUJ9fT3XXHON1WbEHTv5HU6og8mqvBpgRPZIzsdvDozbyW9NfLCtaGs0iUQ0Qj2arMqrAyKt0YQjbqItIm5gjfnySuAOf6t5s8PxCWAXcDPQrpR6cqLzObUJQlFRkdUmWEIi+j0VoY6URPRbYy1xUT4RyQcqlVLbzNfrgBeBK4J22wFsBR5QSu0Md06nNkFYunSp1SZYQqL4HQ+hDiZR/LYlPpCB5Fv0Ha/paiVGm3m/GO8GnhCRfHO2/apSKqrHvE5tgrB3715Hxjit9DveQh2MU6+3ZnziItpKqd0isj5oqMIc7wzeT0TcSindflpjOVYKtUYzEXELDI8S408B24JeV4jIGqDWjG8/Hk68nVoc3qmx/Hj4nUhC/cZDv6D6O+10tebwRslDXPPNQi6946ZpfU9drc8exF0BzPi2Wyl1nX/MH+s2t+8AXgDGVMYRkQ3ABoALL7yQ6upqwFh4kZubG6g9XFRUxNKlS9m7dy9g/MGvXLmSuro6urq6AKisrKS1tZXTp08DsHDhQtLT0zl48CAAxcXFLFq0iJqaGsBYzLNixQpqa2vp6ekBoKqqiqamJpqbmwFYvHgxLpeLw4cPA1BaWsr8+fPZt28fYMThq6qq2L9/f6Ae+IoVK2hsbAzUlFiyZAler5cjR44AMGfOHMrKyti/fz8AOTk5AOzbty+wlH/lypUcPXqUM2fOALBs2TIGBwc5duwYAOXl5ZSUlFBbWwtAXl4ebrebmpqaQInbVatWcejQIdrajIUey5cvp7u7m4aGBgDmzZtHYWEhdXXGd2lBQQHLly9nz549KKUQEVavXk19fX2gjKrb7aa9vZ2TJ09O+Tp5PB6qq6tjfp3a29upr6+nr6+P/v5+PvjBD+J2uwPXye//ZK5TZWXlpK5TzT/8hNe/PxPPgJGb3tWSy7P/r5d3mv+Nj/zdl6flOv2o/L8pyB4ixZwLBar1zfw519VcFvY69ff309TUFPjMY3GdpowSZCD51nNIvHvQmaJ8T3BoJCi27X+twsW4lyxZovzi6CTq6uqSuuLdeMTS70SaUQN07DlA638+y/DZDtJmFfD67zM5/oeyMfvllXZz96k7psWGH5c+HKghEozHK3yp5Ythj3/mmWdiurhGRA4opaZ0ssrFmerVByNbSZty7eEpv1+8iOtMW0Q2YQq2X6jNsMhWRmaShMWpVf78MxunMVW/E02o/XTsOUDzD3ehBo1SpsPvdTC7+F2eO13LyXMDuIL/RJsV7ffFaBY6itd7jZn5f/f8Dz+98EeBcV2tL/GIZ572OuDJoBn1GuBJoBZ4YPR+8bJLk7wkqlAH0/qfz6IGh2kb7OOld4/zTl8XBTMy+eTS93No/zUj9s0r7ebu+6Zppr099ExbV+szMNeZPKSUGndyKSI7lFJ3Trct8crTrgCeMH/3DzdgiriINJiz8E5ggVJqfegznSc7OzvcLkmJU2s7ROq3HYTaz5kzZ3jk1T00956jID2Lay9YQFl2PgBKwaGgfVMzhrjmm4XTZktSVuvzCWpg6hJnRgPagXHjc6aobwCSQ7SVUg3AuF/ZZqZIVKl+w8MJ0BnDAlpbWwMPI53ERH7bTaifeuopGhsbKS4u5oOLL6F0MMSfRlY6eaXddLXmkFfSM+3ZI/5qfT4lKIXOHglCKbUbwmasVWBMOqcd2+aPDQ0Nhd8pCTl9+jQLFiRWy6l4MNpvOwv1Jz/5yUCp2dExbQBJT6Ns4zrufvQKqqur47a45q87P0vXfSf4/H1fisv7JQsisk4p9aSIPBSP97OtaGucR7IIdTAFq40QaXD2SMlnrg+MaxIbM/TbEM/3tK1oZ2RkWG2CJSxcuNBqE+KKX6gPHTrEwoULk0KoR1Ow+opxRdpp19tCZolIbdDrnZHUQMJYcxLXxAnbirZTV0Q6oWNPqBn1+vXrmTVrltWmjWGyQh0pTrje04ZPoD/iB6lno83TNh9Q7o7ariliW9H2ryh0GgcPHkzKAkLhQh/xjO2GY7qFOphkvd5JxM1BE8h8c9X2bjP5YlqwrWhr7E8yxqg1zsGfVeLHzNOOJKQyJWwr2mlpNs4fnQLFxcVWmzAlJivUVvidCEJt9+udDJhhELf5+1bghWDBNuspbQjavkPPtEPg1FjfokWLrDYhamIxo46X34kg1MHY8XonDEpQg1OXOFOgdzOyMmnw9k5zW8jtsca2ou2vDOY0ampqbBHjjHXoYzr9TjShBqj516d54rvv0XY2i6JZfaz/+mxW3v3JsMfdNfNhLs1QZLqg3wtvDAgPngtf8EljH2wr2prEQ8eop0bDL49Tt6WW3uZeBryQ1p0OSmh7L5uH7zkHPD2hcN8182GuzFK4zA5bWalwZZbirpkPa+FOImwr2ikpydf7LRISLSwUL6GOhd9+oT558iSzZ89OCKH20/DL47yyqQZvv1G90iM9tKW9TsNwJz2eFBiAQ/cO8bH218c9xylPHe+aN6D+an2uFLg0Q1fqSzRE5GvACaXUL0VkF6AwYuEvhTvWtqLt1IJRK1assNoES2bUk/V7tFDfcMMNzJ8/P8bWTZ26LbWc6z3Hgf7XOOM9Q05KDu6My6jKKKXmPXMh2YDivvs+Pe45frrzx4RavpCZfH0AIkL5wBeDmPY00WAKtl+87xWRayM5MGE9CkdfX5/VJlhCbW2tJZX+rA59ROO3XYQaoK2tjaeeeoqn//BLslOycWdczurUDwa2BzcpKZo18f/z/V4jJBJqXJNwdJj/rsFoeg4wM5IDbSvaTm2CEM8HsFYLdTDh/LajUJ84cYLCwkJuuOEG8t+fQ29z75h9B7zG1HlG+jDrvz57wvO+MSAjYtoAXp8xrkk4FohIB0Yp6vFjXiGwrWhrpodEEupw2F2ob7/99sD2lM2MiGkDeJXieE8qRbN7I8oeefDcFwPZIwB9Hp09ksDswsjtvk5EZgL3AmcjOdC2ou3UmHZVVVXMz2kHofb7nWhC/fyjx9n+jVdpPd1DSXkOG++/krW3XgyEF+pgKm40jqnbUkvvO72QI/xvVwe7fcPMcXn4o6H8wHu9e6oblxKKVBqpKoVTeb3Udt4FEBDo1vtOc5cusZqwKKXOAd8LGtosInmRHGtb0XZqE4SmpqaYVH6zg1D7OXPmDDt27KCvry8hhNrP848eZ8vG3zLQZ3S0bznVw3c2PMdz/9dNZlF3WKEeTcWNF/PugIuffmUffe8oFMVc6OqkqaOPv/z7o1xzwR9oOXUBguATOMsws5nBxV25VOY/GBBujUmMFtdMFyIyD8gPGroTCHsRE9ejMDi1CUJzc/OkRdtuQh08o7744ou59dZbrTZrBNu/8SoDfR6GVQ8d6jUG1Huk9mWT+eureeHU34Y9ftdjh/j23+2hqamLsrI8blm7hMaftDLUByjo420KPGfo8vXQ7h3kt+8Ok+MzGvv2qXdIkTRccj0XqTTmdjnzztOuiMh2oBKjjZmfK0hm0dZEhp2FOnhGXV1dba1xo2hra+ONt59jwPceqZJNgVxGcYqR9dHbEv74XY8d4i++/Bz95iz99Oku/vGh31ExfA6POoqXYWaxgCLKyfJ52Zd6hq5BxWwpAMDLEEO048GIX2fi0Lw++3JAKbUxeEBEIuonZ1vRdmoThMWLF4fdJ1mEOphI/J5uRseoKy68nJ53x/atLCkP3csyeGadkiJ4vQqlFD51Go/3KOChIeUiPur7E1JlRuC4PBSuFOGC3EGyOi4MjPerGaSarVf7cWY2lY05EeHYGBJCtM2WPeswmvu6MbpGTNgk06lNEFyu0DOqRBTqlucP0rC9msHWLtJL8qjYeA2la5dN6mHieH5PNxM9TLy0bGRMGyAjK5WN918JjHxImVOYzvHeDs56+lFK4fGeCgh1Sko5M1JXI5KGT0Gqd8YIG/rxkpnm5bIC4VRH0AYFhaThwceRlLj0lLUXSvANJmw10AoRWQ8cMF8LhgauDXdgQog2xvLN6wBEpAHYSphW9E5tgnD48OFAuc5EFGo/Lc8f5MiWZ/ENmA/pTr/DL+7+O4YuK2R+1ZKoHyYG+x0LAnU+3ukl+8Js3JsrAxkckWZ9+LNE/MI8szCDQm8aP//M7/nlV1/ndHcvHR7j2UvX2QEyaQFVx5AMjhDqYLJk5JeTBx+teW388zcWUTjncv7+C9V4PT5SFOThwqWEN1PaWXTRxDncmoRjI0blwIKgsaJIDrRctM1ZdgClVIOI3EwY0U5mfvGFTXzg67vJLeukuymf3393DTf9xKj62N7ezoMPPpgwQr3rtk185LMvkTO7i5738vjNzz/MzY9so2F7Ne3d56hpP8S7gx3kp2WzsmAJ84bmcdVff8USW//lTzdzy/X7mVXUTXZbLg2ZH6BELaK3uZfn/9+v6dzdRdfM6LI+Tj71X/zD+jcozu/nTGcmP31hCY1t76f37BAzlYuz8jbvyhv4lIccmccC+WNOpIVe2ZiZlcqXb6vk7P900na6j6LyLC75zAxuv/8zI/b77u3/R/8g9OLl3ZRBimZkBmb3bz32H8zq/hm+Q3/g7EO/5Wzun/G+Wz4X0edz6pkf03/8ITy1j9Ddlk/38J3M/Xj4tMGm3+2g471tZBcM0HKqBM87d1L2R479842UO5RSrwUPiEhErcssF22McMiYezsRqZiokHiyNkH4xRc2sfr7/01atpHSmDe3k/fd8ws2rHiNomsqERE2btyYEDPqXbdt4hNfeYbUDGM2nVvSxVWffpoNH6inp3kgINQXZBQGjhls7ZrUe5WWlk7J1n/5081s+FQ16aats2d3c9utu/nL79cz8G4K2SnZrKy5mnvfCJ/14WfHzd9m3SW1ZMww4smlBf189ROvsam3mdoj5/DhYaaaz4Upa0lJMUIeCgWcF22XS/D5FGVleXzr71dz8y1L4d/Ov8dbb7014j2ze/fxFx+v5cHnC2nqVhTP7OMz1x4muzeDtx57hYt8PyS90IMIFBaeI3vwh7z1GGGF+9QzP6a48PukZw8gKdnkze4kc+D7nHqGCYW76Xc7yHz/95nxbi+S4iKztBPPzO/T9Du0cE+AX7BF5MPm65dGi/h4JIJoF4YYa2dk/uIYEq3aXaz4wNd3k5Y9zL6XBzj05hDNTR6KS1x8ceu7/NGqBxgcHEwY3z/y2ZdIzfDw8uv9vH5kkOYzHmYXuPjyXen0/upOBlvGCnR6SUTrB8Yw1bzsW67fT3qGh9rjvfz+WC9N7cMU5rj4f59J5eiODcZO7ROfYzRrKt4ICPb/HGrlzXe7GPL4uHLJWfqObCRVZqBQNMh5kR7GF/g9MyuVf/v3jxlCPQ6j/S5pfIrL3QPMn3OYw6093HRpEwDvnnya/Is6SS/0jNg/Pd3DrPafAROLdm7aDtIyRq59SMsYJjdtBzC+aKdeuIPUzJHHpWYOk3rhDqy+WVY+wTuQmJM7EZkPPIH5f53Z8Wa9UupkuGMTQbQjxmyauQGMNkz+NLCKigpyc3Opr68HoKioiKVLl7J3714AUlNTWblyJXV1dXR1GUJSWVlJa2srp0+fBmDhwoWkp6dz8OBB/OdftGgRNTU1gPElsWLFCmprawN1MKqqqmhqaqK52cidXbx4MS6Xi8OHDwPG7HD+/Pns27cPgMzMTKqqqti/f38gJr9ixQoaGxtpaTHyxC77I+Om4/pr3+WVujm8f4k5Q/P1UV1dTV9fH9dffz379u1jcHAQgJUrV3L06FHOnDkDwLJlyxgcHOTYsWMAlJeXU1JSQm1tLQB5eXm43W5qamrweIw/8lWrVnHo0CHa2toAWL58Od3d3TQ0GDc78+bNo7CwkLq6OgAKCgpYOtv4LL/5wzbu/3IRX/5UvmnrIL/tLEH+uw81eF5EZIaLks9eGbhu0VynP/zhD+Tk5Ez6Os0q6gbgL350ir/8kxL+/GPFpq1ejpr2uYpSqa6ujug6LVmyhOL8889V/urpQ7y5aTWZaS58Cl5+2rhu/pQ8gNQZKXRnDyI9MGv2DP72G1dx9QfzA59HqOvU1dVFamoqHo+H47v7+FTuAABH3+tlXkFm4NwlOf1k5Z8ztr0zQPms8w8zC/LPsWfPHpRSiAirV6+mvr6ejg7jqabb7Sa3yPj/bmBwZBnX3KJO3nzzzXGvU8US47gTx4dxV56fTGQUd3L27Nkp/T0lOdeO7v4uIn8D/EO4AyW4ipgViMg64E7/g0hzrAO4YqLwyOLFi9WRI0fiYWJcOX3cTd7cTv75Hzq588t5ZGYa1X+6TuVTfnFdQnUl73yuktySLv798U4+9ZFcZhUYD9G6W/PI/1jtuNkjk2Gqfrdu/xCzZ3fz8ItnWXNpHnNnG6L23nu5/Po7n8GV6eKqbSsDDyMj4cTmmygtMIT7uy8c4+vXGYueWjoy+da2m3DNSKE3z8c7nb1jlrhHSrDfX533NH9+688oyx/kvl8f4VtrFwWyqOpfr2Dh6kMUze7me0+38MVrZ1GUa8zJ2ttnMuuOFyd8n47nP8DZwfd48fd93HHj+WJzXe/lU7D29+Me13LKTWZpJ9/+Rjvfuv/8TXN/Sz6lc+ui8jUYETkwWtSixV02U/32L66OaN+czc9N+f2iQUSuVUq9GG4sFInQSaCOECGScI0xk7UJwu+/u4bh3pG3dMO9afz+u2sAY7aeKPzm5x/GMzDyZs0zkMpvfv5hAErXLuOqp77Ch175W6566iuTFmyYut+PPVvF4ChbBwdS+dmjHyB7TnbUgg2wu+FSBoZGZnsMDLn46QtLKJqbxYaHq3jyzG28MnQHT524NWrBhpF+t53u45HfLKOtVzEjNSUg2H1DKTTWX84rT/8RgwOptPd4AoI9OJDK2dw/m/A9OvYc4NQLV/CL3/TzyQ+dX1k5PJBG9/D4IY6+2pfp+u+lNL6lKCs//9l6+tPwvKPj2WEIFe+LKAZoeXjEzBYJvDazSXaFOy5ZC0bd9JNt/OILMDDrv1A+Y4YdnD0yHQWjJsvNj2xj120wkP8YShkzbH/2SKyZqt9f/dUW/uVPN+PLfxqljBn2Y89WsWnPlkmfc94Nn+Z7/zjEHWveQmHMsB/a/T4+9Dd/NimBDkWw30XlWZx47f38TcdbrP/gHHwK3jmXzi9fvpwLu6DzzYv5yfZ2LpjXifJBe1surzz9R3zi1+PHszv2HKD5h7tIGczkrVPFpF8/A+XrovtsPt2e8bNH+mpfpuuxH5M3rPjp7pnc8vVMlG+AgdaZeN7dqB9ChueAiPwG+A1Gqp8bI9U5LJaLtsl6EdmEubhGKRX2ivf2jq09nCzc9JNtHN9aRH7G3WRenEn5T85v279/f8IJ9w9+MJfUy24hf9Ysbv7Y9LxPLPz+6q+28PDDi3CtWUPJ3Ll8dWP4YyZi+zdepeXUPPbUz+O0d4jqPR8F4Mg3Xo2ZaAf7fdHHizjzYC+HG4d4uvEO/luEGVkuvrTzA7z7z/vpbe5lT30bq47fxX++ZKzKzJ4z8eSm9T+fRQ0O09TbyUW+Ypp2XgpA2uwC3vfQ+A8ge57ZBcNGDnp38zDF/+Vm4L8gpaCIsvsSQ7CVErwJWjBKKfWaiNyJsaBGgI1KqcZIjk0Ij8xQiH96FlGuos/nC79TEuLURUWJ6Hfr6dCNGcYbnwx+v3c9doitj+4nX8CFCwT6Uzy4P1/O1bfNpyHDyyubaujp6iUnxRBsV6YL9+aJw7TDZ42HkS++e4I/KX//mPHx8HUYD6xPd/VQlps9ZlwTHlOkA+VZRWRe0mWPaDSJREl5Di2nxgr0eLVHpsK3/24P/X0eulIOkuK6gKYUI+vprd/08Nd8kIobL6bxnUYu+se5MMyYVZ7jkTargOH3OugY7KMwPWvE+ESkFBTh62jjuRNNfHLRRSPGNWMRkQ8HN+0VkVArt9YTwTJ22z7Ny8mJ/R+GHUiExr5WkIh+b7z/SnrThznq6uC9lH6OujroTR8OrE6MBX6/m5qMFEifr5kUKQts948DHOh7jfsPPMCfNX2Jdb+/JaIHqyWfuZ6m4V7Kss9njEh6GiWfuX7C43I+fjOkzeC9vn6Ks82HpWkzjHFNKLaJyGVBrzdiLGEP/rHHMvbJ4s9RdhqNjY28733vs9qMuJOIfp+TQd5x9TLsNUJ1w+LjHVcv5yR2/282NjbyxuteUlIEj2cIJHVEsbSysvOLldrb2yksDLVWbXwKVl9B7eNZrC2ohN4h0mYVUPKZ6ylYfcWEx2VVXs2pd1so/4MRhk0pKCLn4zeTVRlZil1c8AneocSQuBDphLZexj4pnNq5pqWlJeHEKx4kot/f/rs9DA2NLIk6NOTl23+3Z8JVjtHw2CNv8uC/v43Xq/D4jpCasiiwLTMrlW/9/WoAjh49yqJFi8Y7zYT05qVz1Q+jz6L59YnTfO7Hj065xIBDCZRhNVdHzgcmTHP2Y9vwiEZjNcGhiUjGJ8PPf3Y60CjB52sKhEZcLhmxBP7pp5/mk5+cuPFvKBoaGqioqAi/YwhaW1u1YE+eQBxJKdVoxrvdkRxo25l2Ii0yiSdLliyx2gRLSES/y8ryOH16rEAHhyymytn3jLQ6pYZHhEZ8PjViNj+Z0AjAU089xec+F1kVwGDefvvthChaZifMruvXmi+vEJHR1W6uA34Z7jy2FUyjTTYAACAASURBVG2rl99bhdfrzA4liej32o8t4Ec7xxZmW/uxBTF7j4L8DNo7BszQyPnuPcFfDFMJjbz33nvMnh19Le6nnnqKW265ZVLvGS+UErxDsSkYJSJu4CGl1BUhxteYL6/EiFWH7EihlDonIq8B9wAVQHAnl07Opz1PiG1Fe2BgwGoTLOHIkSNccMEFVpsRdxLR7+efC90d6vnnTsC/xOD8jx4npzOVDmVkjaSmGjPrGTNcgVg2GKGRSDu+B6NDI5EhImswqvG5R43nA5VKqW3m63XAixgNekNi5mZvjLTOSCh0TFujmSTTHdPe/o1XyR1Op9Sbhos0BCFNpXBxVv6I0EhHR8ekQyM33HBD1Mc5LTSilNqtlApV/aoSY9bsZzfgNsU83DlHF4v68KiUwHGx7Ux7xowZ4XdKQubMmWO1CZaQiH6XleVx7uQAJSqLVl8mJZ58WqWPmfNi03Q6sLLSd5wFUkmO10jjlaBFh0ePHmXhwoWTOn8yh0bigVJqt9nn0U+FOR5Rw06zAYJf4IswFtfcFe442860k7VzTTjKysrC75SEJKLfN69dwhyVwwyMSn8zcDFH5XDz2tg8NJ1ZaIh/j3qbbOYFxoNXXOqsEWsZNQP/FBHGpUXkaxgZJLcAH8AIqTwRybG2Fe1kLhg1Efv377faBEuIhd/PP3qcH9yznxsWPMINCx7l+UePT+l8rz/7LikjniVBCsLrz747pfMCvPxII1md4PMNkSJpgayRtBkpI1Zc6tDI+CgleAbTIvoBZolIbdDPhmjeywyJuJVS94Td2aBTKbURI7yy3fw9ImwbHtFoouH5R4+zZeNvOdczwEyBllM9bNn4W4BJV+SbzoJRu/62nswhF4ojFMkyUJCKUJ6VHbBXh0ZiytkpNkHYihHeiJRaEclTSjUGdawJGwsHG4u2y+UKv1MS4tSaK1P1e/s3XmWgb2T/xIE+D9unUEa1pDyHnNYeqnKE3b3CmmzhhU7FWYSrZjxESXkOS/+4hKd+/RZNTV2UleWx9mMLeP65E4HX3/r71cxU6Wz/xqu0nu4JdLhpO9ULCH3qbZarm88vXW/38fyjx9n+jVepO/kky8o/ypzM41H5oEMjscUsK32PUqpTRPIjjGkXArtE5AqMWf7zGGl/yZunnZWVFX6nJKSyMm4dkRKKqfo9HbPiz90wl/ZH3iLV1NOWIUWbF3xmX8i3Tp/lpYfeDnSJPH26a0Re9+nTXWy+YzdzfDl4hoz6JS2nevjBnz9LHkV41ZCRNRJUa0QVprBl428Z6PPgUX20NxP1HYNeUBM7zDS/J4OEeg3wZLjjzOwR/23SZhG5FqiN5D1tK9pOjWnv27cvISveTTdT9Xs6yqh6954OCHaPr4dHzv2KoaA1X20pA/h8Ey8C6x/MoHFUXLyzM5U0UniPI1zCTYFxhaLdNcxAn4d+1UqGGM2Jo71jcEpoRCnBE4OCUWaettv8fSvwgpk5UoH58DDoi7WBCERbRB4HHlBKvW7YGnnOtm1F26lNEJxa3XCqfm+8/0pjRhqk2xlZqVMqo9r7zvmJw2fzP832MyP/n+xytTFKj8dwkacQGbVThTdrzJifc+3GorIO9RrF8sHAeKR3DDo0Ej1Kqd0YOdjbRo03EPYKj8tuv2D7GV1zezxsmz2i0UTD2lsvZvP2DzKzMAMRKJ2bw+btH5xSW7DsC0e28soZ9deUFsGf1zBjJx8DhF6yP4A3cGfgUX2kyvn3j/SOwQlZIzZBiciDInK7iNwoIjcCEfVps61oO/WB3MqVK602wRJi4ffaWy/mK1ureOrEbZPujh6Me3MlrszzD8SrsgmESwCKfZlhp2EdGYOkzhj5Zzg0sxPPKDH34GPOCmHj/VfiSz8bCI1AdHcMUwmNTEbsNeOyGWOWfjFGnvYHMBfnhMO2ou3UMMHRo0etNsESEtHvihsv5qptK43muQKXXZzLhi+/n9K5OYjA+8pn8Rd3fIDy8jxEoLw8j9s3XD7i9ZaH1vCNH60OHFM6N4ev/PB6ilco+vGgMP4tXqH415c/y9pbL+aSP+3gfeUro75jcFxoxGfEtCP5sYA7lVIblVKb/T9ARLnhto1pO7UJwpkzZxKyTOl0k6h+V9x48Zi2Xp/9p5H73M+HRw6EKCY1WnT9r6urq7nmmmtGbJt9kYsfPXJH1LbqrJHEQSn1oojcgbEg5y4ze+TVSI6Ny0xbRNwissn8eSK4oIqIbBWRDSKSb/67Lh42aTR2xKoFNTo0EltE5AHz1zoIZI+sGf+I80y7aAeXLzRLGD6OUb4wmB1AI5CvlAqbLgPObYKwbNkyq02wBO23ga41kjTUKqUeYmRudkSFpuIx0w5XvvBVpZQopQr8dWkjwalNEJway9d+G+haI0lDgflvsJBF1G5s2kXbzHEMW77Q7AARMU5tgnDs2DGrTbAE7bcOjUSLsbgmLaIfC2gUkd8A94rIA+Yy9lA1u8cQlweRYcoXVpgrjmrN1UaPj1NwHLPy1gaA4uJiqqurjRNUVJCbm0t9fT0ARUVFLF26lL179wKQmprKypUrqauro6vLKFBfWVlJa2srp0+fBmDhwoWkp6dz8OBB/OdftGgRNTU1AKSnp7NixQpqa2vp6TEWMlRVVdHU1ERzczMAixcvxuVycfjwYQBKS0uZP38++/btA4yQTlVVFfv376e/vx+AFStW0NjYSEtLC2D0QvR6vTQ0NLB3714qKiooKysLVLnr6+sDjBWC/lnYypUrOXr0KGfOnAGMW+rBwcHAH3x5eTklJSXU1hp3Ynl5ebjdbmpqavB4jHocq1at4tChQ7S1GcWaly9fTnd3Nw0NRoPoefPmUVhYSF2dcWkKCgpYvnw5e/bs4dixY7z88st84hOfoL6+no6ODgDcbjft7e2cPHlyytepp6eH6urqKV+nt956i0suuYSMjIyYXKcjR44ARr3v4OuUk5NDZWXllK8TELhOjz76KN/97nd58803o7pOubm5eDweqqurERFWr14d8XV69dVXufbaaykuLo7731MyYz6IbADWYdTS3qyUGtu7LgQSzzCDGRJ5Qil13TjbKzCWiIZtsnfJJZeoN998M9YmJgxbt27l7rvvHhO7P3HiBAsWxK4HYSz4wQ9+wC233MKsWbOm7T1i5ffDDz/MmjVrbHPLH+z3vffeywMPPBDmiLH84z/+I5/73Oeinmm//fbbPPvss9x1V9i6/DFHRA5MseoelxQVq6euvyn8jsDC/9w+5feLBrPCX5f5+3yMCMQJpdTJcMdOeqZtznon+it6wQyNBDOmfGFwVSylVIMp3GFxahOEkpISq02wBKf7rcuwJh03Az+CQN/IRnNV5MlwB05atJVSO6PZP1T5QjMsspUJGmGOh1MLRtXW1o7J23UCTvdbN++NHqWwauFMSERkJnCt+fIKEWkftct1JEpp1gnKF9YCD4zeLx42aZzHwUdPsnvz6xzu7OOiuXP50HcuZdmt86w2KyKmkjWiF9QkBkqpcyLyGkY2XQUji011EGGrsmkX7YnKF5qz7QZzFt4JLFBKRdT9walNEPwPp5zGVP0++OhJ/veuV+nvGQSBc6f6+N+7jAVoiSzceXl5OjSSRJihkI0icm005ViDmXbRDle+0MwUiSjVJRinNkFwu6PKjEwapur3/33zDYb7RlbPG+7z8n/ffCNhRbvhl8dp2HKUJ448yUcXfJSG/ONjlsxPeLyDQyOJTijBFpF50/og0mr8aUJOo6amxpGV/qbq97nTfVGNW03DL4/zyqYavP1een19pLQKr2wy0uUiFW6nh0aUTxgeTIyEhdG1skVk9AMKwUj/WxvuXLat8ufUFZH+vGqnMVW/Z5aHvjMbb9xq6rbU4u330uJpoSTVyCDx9nup2xJRRyrAmQtqEphtInJZ0OuNGKsi/T/5GPnaYbHtTFujiYYPfedSNnzuOQa8XhReUmSIFlcv3/zYAv7t4l9x7nQfM8uzJv1w0t9sN7g571Tqdfc09yJAXf9rrMo+36EmuFvOROjQSGIRIgf8jtGLaURkdIp0SGw7087NzbXaBEtYtWqV1SZYwlT9/pt7q5nrzSWdFAQhi1Qu8eXx+53HOXeqD9T5h5MHHz0Z1bmff/Q4Wzb+lpZTPShlNOfdsvG3PP/o8UnZevOchxkwbyx6VC85KecbfozuljMeutZIYhNq9WOkKyJtO9P2Ly92GocOHeKSSy6x2oy4M1W/s06lkEoKwwzQw3sopShKEbp9o56R98IvNu8m96qQi3ZD8v17nuVc78jY+GCvMf6+qz4Rta1973RRl+4jLfM4xa7z4Q2Pz+iWEwk6ayR5sa1oOzW266854TSm6ncmLvPfAjp4mx7OIIrQDXSb4aWXIk8pPfFOHaEesXS9Ay+9FP0d4VkO0zYodHjq+fTMT6IUDHiFY90uvhTBQ0gdGjFQShgeTowHkbHEtqKt0URDP16ySOUiqQqMLUgR0mSsaM+cm8UXvvCnEZ/7V/dn0HJqbDZT6dwcvvCFW6O29ddfSjOaAvtWc6QDjpjjoZoAh8LpWSOJiIh8DaO2SNgVj+GwbUzbqXnay5cvt9oES5iq331zfWOa5bb4fHhl5BQ5LcvFh75zaVTn3nj/lWRkjZz/RNNsdzTqgiF8jLTLh0JdMBTR8TprJCHpxOglAIRM+UNEPjx6LBS2FW2v1xt+pySku7vbahMsYap+//rk5+mc66HPbJbbh4e3y4e56acrmDk3C8SYYf/xg1dGnT2y9taL2bz9gyOa80babDcUu5q/iOeCQYYNqWYYH54LBtnV/MWwx+rQSMKyAKN29o1mYajrROT24B9GNosZF9uGR5zayaShocGRt7Cx8PvXJz8fcjwWKyLX3nrxlFL8RuMX6FCNfSdisqGRkydPJt3/V0YThMSQOKXUZhG5ifOVUf352cHoPG2NxmlMJTRy663Rx981kaOU+oX/dxHZ7bg87fT0dKtNsIR58+ZZbYIlaL/DM5XQyJkzZ3RoJI74BVtEPuyPZSd9nrZTq/xNpjxnMqD9Do8OjdgHs1vNE0C7+XorsD6SglG2nWn7eyU6DX/vP6eh/Q6PzhqZPkTELSIHQoxXiMgmEVlj/psf4SmvVUpVKqU+Yv5ciVEwKiy2nWlrNJrz6NDIWJQShmPQad3ssNUOhKoPvMPf89Zs1LsVuDOC0zaGGIsoPGLbmXZqqjO/bwoKRj9wdgba74mZ7GxZh0bCo5Tabdb9H8HofrZm74CbIzzt/AjHxmBb5Rvdpdwp6MU1ziJSv3XWiCW4MRbNjEBEKkwBn4gDIvIb4DcYqX5ujFl6WGw703bqIpM9e/ZYbYIlaL/HR4dGLCPUU+J2jNrYE2JmityJ0fxAgI3BTRImwrYzbafi1OYP2u/x0VkjoVFKGBqaYbUZ42L2i/xetMfZdqbtVCREgSMnoP0eH501EhNmiUht0M+GCI4JNasuJETIJJbEZaZt5iCeAHZhBOrblVJPmtsqMFJd6jDiOjuVUmGddmoThNWrV1ttgiVov0OjQyMx42yI7jLhqCNEiCSCePaUiOdMewdGmku+X7D940qpbUqp3cCTRBiMd2oThPr6eqtNsATtd2h01oh1jBZncwK6a7rfN14x7VeVUmPu80KlzIjIzUSQ5+jUJggdHR1Wm2AJ2u/Q6KyR8TFi2lOXODNP223+vhV4wZxkAqwXkU2YkQKlVCQ52lMirg8iRcQ9Kt9xKikzGo2j0aGR+GAK9G5gW4htDUHjERV8AhCRx4EHlFKvR2tPvES7wvy2qjW/qR43xTuqlBnz4cAGgAsvvJDq6mrj5BUV5ObmBm4li4qKWLp0KXv37gWMhTgrV66krq6Orq4uACorK2ltbeX06dMALFy4kPT0dA4ePAhAcXExixYtoqamBjAKVK1YsYLa2lp6eowuJVVVVTQ1NdHc3AzA4sWLcblcHD58GIDS0lLmz5/Pvn37ACO3vKqqiv379wfCOytWrKCxsZGWlhYAlixZgtfrpaGhgb1791JRUUFZWRn79+8P2AGwb9++QHnalStXcvToUc6cOQPAsmXLGBwc5NixYwCUl5dTUlJCbW0tAHl5ebjdbmpqagJ3LKtWreLQoUOBtl7Lly+nu7ubhgbju3PevHkUFhYGllUXFBSwfPly9uzZw7Fjx3j55Zf5xCc+QX19fWB26Ha7aW9v5+TJk1O+Tj6fj+rq6oS7TkeOGH1l5syZM+I65eTkUFlZOa3XadeuXfzVX/0Vp06diug6KaUQES666CJSUlICfz+xvE6x/HtKcnaPFmwR+XAkaX8S71QqMyTyglJqgSnC1yml1gdtP4FROGXCoguXXnqpeuONN6bZWuvYunUrd99995hFRCdPnky4inc/+MEPuOWWW5g1a9a0vUci+h0PJvJ78+bNbNmyJepz/tM//RO33nprQs+0ReTAJB4MjmBRdpn6t/fdHdG+H627Z8rvFw1m04MrgAOYRaOATymlPhXu2EnPtE3BXTDBLoG4j4jk+zNCzLi1/55u0ikzTm2CoMXLWYzntw6N2J7NGOGU4M4ZEV3QSYu2UmpnJPuZYZGtGN8qo7EkZUajsTt6QU14lC82BaOmiTuVUi8GD4jI5ZEcGI+Uv1rgAf8LEVmHkdo3pZQZpzZBmOzsyu5ov0eiF9TYG6XUiyJyh4g8CCAi12KsZQnLtD+IVEp1ikiDmRbTCSwIjmEzyZQZpzZBcOqiIu33eXRoxP6IyANAA4bu+UX8RuCX4Y6Ny+IapVSduYBmp1LqnlHbGvyLa5RSY1JqxsOpTRD0IhNnEcpvvaAmKahVSj2EEYnwE9Hyd10wSqOxGXpBTWQoJQwNJ2xM218oPTh9zw2ETfmzrWg7tQlCUVGR1SZYgvbbQIdGkoZGs552h9nxJvnraTu1CcLSpUutNsEStN8GOjSSHJiZI3dihEcE2BxpPW3birZTmyD4V6U5De23gc4aSR6UUo1Kqe8ppTabTREiwrairdE4DR0aiQ6fWTAqkh8rEJFdIuITEa8/9S8SbCvaTi2K79RYvvZbh0aSCRH5Gka56gKllAt4UkT+JpJjbfuXkJOTY7UJlrBy5UqrTRhBy/MHaXhoDzU/e5fisguo2HgNpWuXxfx9Es3veBHst84aSSrqgldEmnnaYXtLgo1n2k7N0/ZXb0sEWp4/yJEtz+LpGgAFgy1dHNnyLC3PH4z5eyWS3/Hkd9uf4ZUbfsB/uf8G9fzbk/psnRgasQGhKvUld5621+u12gRL8JfCTAQatlfjGxjZjMI34KFhe3XMZ9uJ5He8aHn+IP2PHIRhHzVth/nI7Ms5suVZgIg/Xx0aSQzM1Y7BXGmW7fBX+CvEWCEZFtuKtsZ6BltDC+l445roaNheDcM+ADo9veSnZUf9pejk0IhSwuBgwiyu2Qa8AJwLGrs4xH7Ju7gmOzvbahMsobIybiV/w5Jeksdgy1iBTi/Ji/l7JZLf8cL/5ffOQDsXpheMGY8EHRpJGMZU9Zssto1pDw8PW22CJbS2tlptQoCKjdeQkjHyez8lI5WKjdfE/L0Sye944f/yq2k/zMrCpWPGw6FDI4lDJIIdafaIbUV7aGjIahMswd/OKREoXbuMxZuvJzUvAwTSS/NYvPn6ackeSSS/40XFxmsgLSUQGoHovhT1gprExSzL2i4ibeZPOxEuY7dteESTGJSuXUbFsdWsnOZ2Y06kdO0yXvxtNXO7LzS+FEvyokqpdHpoxKeEweGELeGcr5Qa0QBGRO6I5EDbinZGRobVJljCwoULrTZhBLfkPsLxnlf5v79IJUNy8WYLj3XfFtje8vxBGrZXM9jaFbXoBJNofk83Xyt6hKwuYb/vdyyTa3h2Zi7fe+W28Aea6NBIwhOqc/vjkRxo2/CIU1dEJlLHnltyH8HVq5Cg/1y9iltyHwHO53EPtnRNOY87kfyebr5W9Ai5XUKaCH10k58yk9wu4WtFj0R8Dh0aSXg6RORGEbnM/0Oyh0f6+/utNsESDh48yDXXXGO1GQABwfYxzCDdgeUCqkdx9uxZ6v7lGYZGF/Yahrp/eYYPXBHdbfvLL7/M1VdfHSPLE5xz3fSL0O47Q4EUA5AiQlYUmZROD43YgI3AGs7naYPRR/eucAfaVrQ1iUM/nbzDa6Rg5MQqFI895uHoH14OfUAbNDwWXRmCY8eOOeZh5B9ULSihWZ3gOtctgfFI/1h1aMRAKWHQomJQEXBCKbU5eEBEborkwIT1KBxpaQmTNB9XiouLrTZhDG759IjXCsVXvvJpXnmR0HncpXlc9ZWvRPUehw8fZsmSJVOy0y6891ePkhYi/OcJsW8onLygxkaEWv0YUWNf28a0nRTjDGbRokVWmxDAmy2oUSUUFApvtiE4sczjTiS/p5u+PIVPjfxcfUrRlxeqXMVYWltbdWgk8ZkvIg+KyO3mzx0kUucaEXlivApWIrJVRDaISL7577pIztnT0xNbI21CTU2N1SYEeKz7toBw+/8Lzh7x53Gnl+ZNOY87kfyebr7XdhvdeYphpVDK+Lc7T/G9tvDZIydPnuSiiy6Kg5WaKbIRY0l7gfmTD0TUUy9e4ZF1wLpRGR/3BHVf34HxLfOAUmpnnGzSxIDg9L5QlK5dNi2LbZIdv0BXV1dH9eBZh0Zswx2ju9WISKg0wDFMu2iblayuU0rtDhrbECTOryqlos7fS0mxbWRnSjg1LKT9jgwdGjmPUjA4HBudMHXMn+1RAexUSkVUSjW0bSHbi0UU047HTLtdKRUIupvhj12jdxIRt1Iq4qLJTi0YtWLFCqtNsATtd3h0aGRaWRcUGUBEtgL3TPZkZl72aO4FPhXu2GkX7eBvIzOuXTjqG6pCRNYAteYH8fh44i0iG4ANACUlJVRXVxsnqKggNzeX+vp6AIqKili6dGmgKWpqaiorV66krq4uUJe5srKS1tbWQBrZwoULSU9P5+BBY+FHcXExixYtCsRS09PTWbFiBbW1tYF4elVVFU1NTTQ3NwOwePFiXC4Xhw8fBqC0tJT58+ezb98+wOggX1VVxf79+wN55itWrKCxsZGWlhYAlixZgtfrpaGhgb1791JRUUFZWRn79+8HjJorH/nIR9i3bx+Dg4OA0d3k6NGjnDlzBoBly5YxODjIsWPHACgvL6ekpITa2loA8vLycLvd1NTU4PEYOQmrVq3i0KFDtLW1AbB8+XK6u7tpaDC+b+fNm0dhYWGgGUFBQQHLly9nz549KKUQEVavXk19fT0dHR0AuN1u2tvbOXny5JSv05EjR8jKykq463TkyBEA5syZM+I65eTkUFlZOeXr5PP5GBgYiOg6PfHEE3z+85+nq6vLsusUy7+nBOM6jPKqfiLqMjMBLwGvYnRiB6g0X4dFlIrsiXQsMEX5gfFuK8xbkBeUUgvCnWvx4sXK/weTjGzdupW7776bzMzMEePRxjiTBe13eDZv3syWLVum16A4ISIHlFJTqsdb7qpQX824P6J9v9b36QnfT0ReMH9djyGwBId8o0VEblJK/WLU2LWRVAOc9EzbnPVOJK4vhHBqjVJqxC2FiOT7RVwp1WAKt0ajiQIdGhmLT8FAjGLaGGL9ItCIMfHcFmb/CRkt2P7hSI6dtGhHm+VhhkDaQ4xtxVi+GRVOjWlXVVVZbYIlaL8nRmeNTJlZIlIb9HrnKI2rxIhhVwA7RISpCLeI3D5qKB9jEhy2c008UzDcjG1cWQs84H9hPqR8MpKTObUJQlNTk9UmWIL2e2J01siUOauUqgz6CQi2effvVkrtNscXAPdG2j19HDaa5/HnaQuwecIjTOK5jL2TUYF2pVSniDSIyCZz+wKl1PpITubUJgjNzc2OK1MK2u+J0KGRaWcNQaVUzTDuToxmvJNN+xuTpx0pcRPt8cIpZqZIxKl+Go1mJDo0EhqfEga8MSnhvBtjgeAInQpOZQ6HiHxYKRUIfUxWsMHGBaOc2gRh8eLFVptgCdrv8dGhkenFnFn7IwINGDPsHVGeZquIPICxdN3/wNH/jRJ4ABks7ONhW9F2ahMElyth2ydNK9rv0OjQSHxQSkX0rG0CGpVSvxw9KCLzgJ3AfODOSE5k27XgTm2C4F8Q4jS036HRHWpsw5jVk2b39QPAAaXUwkhm2WDjmbZGo9GhEbuglGr0/24uYf8RRlikMnhbJNhWtJ3aBMGpf6Da77Ho0MjEKGAwfgu+I0JEtmMs1NmslHpoMuewbXjEqVXf5s+fb7UJlqD9HosOjdgHs4lvG0ZOdkUowR6niNQYbCvaTm2C4C9q5DS032PRoRFb8STwELAFo2vNZaN+LifZu7FrNE5Gh0Zsxzbg/+N8mt9oCjHSAcNiW9F2ahOE0VX/nIL2eyR6QU14fMBA4sS0dyilxna5Ps85EYmoPrdtlU8XjHIW2u+R6NCIvYgkQyTSLBLbinZvb6/VJliCv8i+09B+n0eHRpyNbUXb5/NZbYIlOHVRkfb7PDprxNnYNqat0TgVHRqJDIViILK+ArbCtjPtnJwcq02wBN3g1lmM9luHRjS2FW1/s1Sn0dgY1YrXpEH7baBDIxrbirZTO9f4u4E7De23gQ6NaGwr2hqN09ChEQ3Y+EGkUxdbLFmyxGoTLEH7rRfURIsP6JcIs8xs9LzStjNtpWz0KccQr9drtQmWoP3WoRGNgW1Fe2BgwGoTLOHIkSNWm2AJTvdbh0Y0fmIq2iLiFpEDIcYrRGSTiKwx/82PZJtGozHQWSMaPzGLaYvIGqAdcIfYvEMpdZ25XwNGCcI7I9g2LjNmzIiF2bZjzpw5VptgCU73W4dGoscHDOiY9vgopXYrpepGj4tIxaj9GoCbw20Lh1M715SVlVltgiU42W8dGtEEE4+YthvoHD1oCvZE2yZEF4xyFk72W4dGNMHEI+WvMMRYO5AfZtsYRGQDsAGguLiY6upqACoqMcJEcQAACXFJREFUKsjNzaW+vh6AoqIili5dyt69ewFITU1l5cqV1NXV0dVllLStrKyktbWV06dPA7Bw4ULS09M5ePAg/vMvWrSImpoawGhvtmLFCmprawNdc6qqqmhqaqK5uRmAxYsX43K5Ah20S0tLmT9/fqD7SGZmJlVVVezfvz9QCGjFihU0NjYGFlEsWbIEr9dLQ0MDe/fupaKigrKysoBo9fX1AUZHE/+q0JUrV3L06FHOnDkDwLJlyxgcHOTYsWMAlJeXU1JSQm1tLQB5eXm43W5qamrweDwArFq1ikOHDtHW1gbA8uXL6e7upqGhAYB58+ZRWFhIXZ1xM1VQUMDy5cvZs2cPSilEhNWrV1NfX09HRwcAbreb9vZ2Tp48OeXr1NPTQ3V1dcJdJ/+Dwjlz5oy4Tjk5OVRWVk75OgEcOHCAyy+/nLfeeivhr1Ms/540oZFYp86JiFJKSdDrDcB1Sqn1QWMnMJpbVo63LVSoJZglS5Yo/x9dMrJ161buvvvuMfnotbW1VFZWWmSVdTjV71/96lc0Nzdz1113WW1KXBGRA0qpKV3wfJmvVrq+HdG+/+v9sym/X7yYcKZtCu6CCXZ5QSm1O8x7hJo5F2KERSbaNiFZWVnhdrEt9838D970vcrP//mnpKh0mvpTue/c5wAcKVzgPL9/VPEQc/Lb+XXbXq7Ou5wffe8hbm+4w2qzNAnAhKKtlNoZg/eoI0QYRCnVICKMty3cSZM1pn3fzP/gouxhDveACKSlKC7KHua+mf/Bfec+x759+xxZ8c5Jfv+o4iHmzTqLy6Xo8PRwYU4W3syz/KhCC7cmDjHtIHEGAg8Zd4XbFo5kbYJQlukhRYxawENqKDBenDFMf38/XV1djmwI4CS/Z+W14hEvzf2dlMwwbkRdLsWc/HaLLdMkArHO03abv29lZOhkvYhswph1u5VSwXnYE21zHKkpxjOGATXA//XuIcVM8FFA97+eo6Ghgddff91CC63BSX4fb69BgFc6/sDXFtwYGE+f4cyl/JqRxEy0TYHejdEqfvS2hqDx3ZFum4hkbYLg8QlpLsW6vBtHjA97hdvv+SIej4fUVNvW+Zo0TvL7ucchI93LrXNWjRgfHHJZZJE98aEYIPm+6GxbeyRZmyA09afiG5XQ41PGOMDRo0ctsMp6nOR3c2chXq+MGPN6hebOUBmymnghIutEZINZcmONVXbYVrSTtQnCfec+x9u9aQx7BaWMGfbbvWmB7BF/jq/TcJLftzfcwcmzsxgYdKEUDAy6OHl2ln4IaSEisg6oMJMz/OU2LMEZ95s2wy/QGufiF+jq6mo+ds011hqjAdiqlFoAgZDuFVYZYlvRdmoThGXLllltgiVovzXR4gN6xTPl84hIoNyGiLjDLfybbmwbHnFqE4RkjeWHQ/utsZAKoN0MkTT4S0lbZYxtRdupTRD8dSqchvZbM83MEpHaoJ8NQdsKgTVKqSeVUp3ATuAJa8y0cXhEo9FoYsjZCWqPtGOsIwFAKdUpIvkiUhHJ6u1YY9uZtlObIJSXl1ttgiVovzUWEiqG7a+dFHdsO9N2ahOEkpISq02wBO23Jlp8ouiPwYNIs9xGu4jkm7PsCqDBDJXEHdvOtJO1YFQ4/LWWnYb2W2Mx64F7zVj3ncC1Vhli25m2RqPRxAtzVn2P1XaAjWfaLpcz6zD4u5k4De23RmNg25l2MjdBmAi3O1Sz++RH+62JFh+KfpKv3IVtZ9r+vnJOw99jz2lovzUaA9uKtlNXRPob8ToN7bdGY2Bb0dZoNBonYtuYdm5urtUmWMKqVavC75SEaL810eJDxaRgVKJh25m2U/oFjubQoUNWm2AJ2m+NxsC2ou3UWF9bW5vVJliC9lujMbCtaGs0Go0TiWlM2ywW/pBS6ooQ4/76s1cCd/jX7Zud208Au4CbgXal1JPh3supedrLly+32gRL0H5rNAYxm2kHFQV3jxrPByqVUtuUUtuAx4EXRx2+A2gE8iMRbACvN/m6LEdCd3e31SZYgvZbEy1eFF0pQxH92ImYibZSavc4bXgqGblmfzfgNsUc4FWllCilCkxRjwindvRoaIh7+d6EQPut0RhMe0xbKbUbo0KWnwpzfERZQzOEotFoNJoJiEue9qgZ+KeA4Bl1hRlaqTXj24+P1zjTLIvobwM0KCIHp8XgxGYWcNZqIyxA++0sFk/1BD7V/Hz3wL2zItzdNp+xxHo5uIgopZSMsy0feEIpdd042yuAF/yt6sO8T+0E7YGSFu23s9B+a0Yz4UzbnNlOJKAvmOGPSNnKyFAJ/m4QEOgQURHF+TQajcZRTCjaSqmdsXojEdkE3ONvimn+uwZDyK8Ic7hGo9FoiNPiGhFZBzwZ9PDRnx5YCzwwer8ITxuzLxSbof12FtpvzQhiFtM2Z81ujJnzNszQiRnuODFq9wZ/3Dpo4U0nsEAplRAtfTQajSYRifmDSI1Go9FMH7YtzarRJCvm3ek6oA7j7nXn6HUNycJEZSyc9DlEg20KRomIW0QOhBjfKiIbRCTf/Hdd0LYKEdkkImvMf/NHH5/oTOD3uL4lg9/BJPs1DsEOs+zDboxnPFutNmiaGa+MhdM+h8hQSiX8D0bM222YO2bbVkABHcCmUdteCPq9AuN/Asv9iZHf4/pmd7+ddI1D+FoR7JM51mG1XdPo7zr9OUT3Y4uZthq/rgmMU7tkdL63UqoB4/bLNozn90S+JYPfIUjaaxwCN8ZD+REk+/qFEGUsHPk5RIItRDsSHHbRJ/Itaf12yDUuDDHWDiRD2CcU/jIWDWYYzH+NnfY5REwyiLYTL/pEviWj3068xo5AmTFrZTxg3AE8YbVNiY7ts0dG3S7vAF5g4qX3GpvhsGsc6kunkBB3FMnABGUsHPU5RINloh2ruiZ2u+gx8nsi3xLS79FE8znY7RpPkTpC3EGY8fqkIkwZC8d8DtFimWirGNQ1seNFj4XfTOCbiDDethi8b8yI9HOw4zWeCkHXEAjE53dZZ9G0Mm4ZC4d9DlFh9/CIIy/6RL4lod9OvMbrzQJrdYBbKXWn1QZNB8ooGtdg+uovYxFcBdQRn0O02GIZ+3h1Tcxt49YuGb2iSkXRziwRCOP3uL7Z3e/RJPM11miixRairdFoNBqDZEj502g0GsegRVuj0WhshBZtjUajsRFatDUajcZGaNHWaDQaG6FFW6PRaGyEFm2NRqOxEVq0NRqNxkb8/6lgntFvgQOGAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 2 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot_results(Sig, iterations)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
