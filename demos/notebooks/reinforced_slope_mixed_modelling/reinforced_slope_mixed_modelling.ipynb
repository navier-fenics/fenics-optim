{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mixed 1D/2D modelling of a reinforced slope stability\n",
    "\n",
    "$\\newcommand{\\bt}{\\boldsymbol{t}}\n",
    "\\newcommand{\\bn}{\\boldsymbol{n}}\n",
    "\\newcommand{\\bf}{\\boldsymbol{f}}\n",
    "\\newcommand{\\dx}{\\,\\text{dx}}\n",
    "\\newcommand{\\dS}{\\,\\text{dS}}\n",
    "\\newcommand{\\bu}{\\boldsymbol{u}}\n",
    "\\newcommand{\\bx}{\\boldsymbol{x}}\n",
    "\\newcommand{\\bsig}{\\boldsymbol{\\sigma}}\n",
    "\\newcommand{\\jump}[1]{[\\![#1]\\!]}$\n",
    "\n",
    "In this demo, we illustrate how we can formulate a limit analysis problem involving 1D reinforcements in a 2D solid. In the present case, we consider the slope stability problem with three horizontal reinforcements. Note that the same approach could also be used for a 2D/3D mixed modelling. However, 1D/3D mixed modelling is much more involved and cannot be tackled using the present approach.\n",
    "\n",
    "## Mixed modelling approach\n",
    "\n",
    "We consider a bulk solid domain $\\Omega$ consisting of a material obeying the strength criterion $G$, e.g. a plane strain Mohr-Coulomb criterion in the subsequent application. The domain also consists of a set of uniaxial reinforcements $\\Gamma$ of unit tangent vector $\\boldsymbol{t}$. The reinforcements are assumed to be perfectly flexible so that the internal forces consist only of a normal force $N$. The latter is subjected to a uniaxial tension/compression criterion:\n",
    "\\begin{equation}\n",
    "|N|\\leq N_0\n",
    "\\end{equation}\n",
    "where $N_0$ is the corresponding uniaxial strength.\n",
    "\n",
    "Defining the different static variable including the bulk stress $\\bsig$ and the normal force $N$ with `fenics-optim` is rather straightforward. It therefore essentially remains the question of imposing the equilibrium equations. We have inside the bulk $\\Omega$:\n",
    "\\begin{equation}\n",
    "\\operatorname{div} \\bsig + \\lambda \\bf =0\n",
    "\\end{equation}\n",
    "where $\\lambda\\bf$ denotes the body force.\n",
    "\n",
    "We also have on $\\Gamma$, the normal force equibrium:\n",
    "\\begin{equation}\n",
    "\\dfrac{d(N\\bt)}{d s} + \\jump{\\bsig}\\cdot\\bn = 0\n",
    "\\end{equation}\n",
    "where $\\jump{\\bsig} = \\bsig^\\oplus - \\bsig^\\ominus$ denotes the stress jump through $\\Gamma$, $\\bn$ denotes the unit normal to $\\Gamma$ and $s$ is the curvilinear coordinate along $\\Gamma$.\n",
    "\n",
    "The above strong equilibrium equations can be enforced directly using a cell and a facet Lagrange multipliers respectively, as in a lower bound static approach. Alternatively, it can also be enforced weakly as in upper bound kinematic approaches written in a static form. In the latter case, considering a continuous virtual displacement $\\bu$, the virtual work principle reads:\n",
    "\\begin{equation}\n",
    "\\int_\\Omega \\bsig:\\nabla^s \\bu \\dx + \\int_{\\Gamma} N\\bt\\cdot \\dfrac{d\\bu}{ds} \\dS = \\lambda \\int_\\Omega \\bf \\cdot\\bu \\dx\n",
    "\\end{equation}\n",
    "and, since $\\dfrac{d\\bu}{ds} = \\dfrac{d\\bu}{d\\bx}\\cdot\\bt$, it also reads:\n",
    "\\begin{equation}\n",
    "\\int_\\Omega \\bsig:\\nabla^s \\bu \\dx + \\int_{\\Gamma} N\\bt \\otimes \\bt : \\nabla^s \\bu \\dS = \\lambda \\int_\\Omega \\bf \\cdot\\bu \\dx\n",
    "\\end{equation}\n",
    "\n",
    "## Implementation\n",
    "\n",
    "As mentioned above, we consider a vertical slope stability problem under self-weigth loading. The slope is reinforced by an array of uniaxial reinforcements of length $L_\\text{reinf}$ located at the heights $H/4, H/2, 3H/4$. We first define the soil mesh and identify the regions corresponding to the fixed boundary conditions and the reinforcement locations. In particular, the reinforcements $\\Gamma$ will correspond to the facet tag `2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dolfin import (\n",
    "    RectangleMesh,\n",
    "    Point,\n",
    "    FacetNormal,\n",
    "    near,\n",
    "    MeshFunction,\n",
    "    Measure,\n",
    "    dx,\n",
    "    AutoSubDomain,\n",
    "    Constant,\n",
    "    DirichletBC,\n",
    "    FunctionSpace,\n",
    "    VectorFunctionSpace,\n",
    "    plot,\n",
    ")\n",
    "from ufl import sym, dot, inner, avg, grad, pi, perp, outer\n",
    "from fenics_optim import MosekProblem, to_mat\n",
    "import fenics_optim.limit_analysis as la\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "Nmesh = 100\n",
    "L, H = 1.2, 1.0\n",
    "L_reinf = 0.8\n",
    "\n",
    "mesh = RectangleMesh(Point(0, 0), Point(L, H), Nmesh, Nmesh, \"crossed\")\n",
    "\n",
    "\n",
    "def border(x, on_boundary):\n",
    "    return near(x[1], 0) or near(x[0], 0)\n",
    "\n",
    "\n",
    "reinf_heights = [H / 4, H / 2, 3 * H / 4]\n",
    "\n",
    "\n",
    "def reinforcement(x, on_boundary):\n",
    "    return any([near(x[1], Hi) for Hi in reinf_heights]) and (L - L_reinf <= x[0] <= L)\n",
    "\n",
    "\n",
    "facets = MeshFunction(\"size_t\", mesh, 1)\n",
    "AutoSubDomain(border).mark(facets, 1)\n",
    "AutoSubDomain(reinforcement).mark(facets, 2)\n",
    "ds = Measure(\"ds\", subdomain_data=facets, domain=mesh)\n",
    "dS = Measure(\"dS\", subdomain_data=facets, domain=mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then define the loading, the soil Mohr-Coulomb strength criterion and the reinforcement strength $N_0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = Constant((0.0, -1.0))\n",
    "\n",
    "c, phi = 1.0, 30 * pi / 180\n",
    "mat = la.MohrCoulomb2D(c, phi)\n",
    "\n",
    "N0 = 1.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then define the various function spaces which will be used to define the variables and constraint Lagrange multipliers. We use `DG 0` spaces for the stress and the normal force and a `CG 1` space for the displacement Lagrange multiplier. Note that since $N$ lives on mesh facets only, we use a `Discontinuous Lagrange Trace` space. Although $\\Gamma$ corresponds only to a subspace of the mesh facets, the use of such a space will define $N$ everywhere on the mesh facets. As a result, a drawback of this approach is  that dummy variables corresponding to $N$ outside $\\Gamma$ are created. This is however not an issue as the above integral will be performed only on the measure `dS(2)` corresponding to $\\Gamma$ and because solvers such as Mosek are able to identify such unused variables and remove them from the final optimization problem. Finally, we use the variable lower bound `lx` and upper bound `ux` keyword to enforce the uniaxial strength criterion $|N|\\leq N_0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "R = FunctionSpace(mesh, \"R\", 0)\n",
    "Wsig = VectorFunctionSpace(mesh, \"DG\", 0, dim=3)\n",
    "WN = FunctionSpace(mesh, \"Discontinuous Lagrange Trace\", 0)\n",
    "V = VectorFunctionSpace(mesh, \"Lagrange\", 1)\n",
    "\n",
    "prob = MosekProblem(\"Limit analysis with reinforcements\")\n",
    "\n",
    "lamb, Sig, N = prob.add_var([R, Wsig, WN], lx=[None, None, -N0], ux=[None, None, N0])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now express the above equilibrium weak form. Note that internal facet integrals must be restricted. In the present case, the integrand is only well defined on the facet itself so that the choice of the restriction operator has no influence. For the facet tagent vector, we use UFL's `perp` function on the mesh `FacetNormal`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "sig = to_mat(Sig)\n",
    "n = FacetNormal(mesh)\n",
    "tang = perp(n)\n",
    "\n",
    "def equilibrium(v):\n",
    "    return (\n",
    "        inner(sig, sym(grad(v))) * dx\n",
    "        + inner((N * outer(tang, tang))(\"+\"), grad(v)(\"+\")) * dS(2)\n",
    "        - lamb * dot(v, f) * dx\n",
    "    )\n",
    "\n",
    "\n",
    "bc = DirichletBC(V, Constant((0.0, 0.0)), border)\n",
    "prob.add_eq_constraint(V, A=equilibrium, name=\"Pseudo-mechanism\", bc=bc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We finish with defining the objective function $\\lambda$, applying the Mohr-Coulomb criterion to the bulk stress and calling the solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Matrix shape: (160402, 300201)\n",
      "Number of cones: 40000\n",
      "Problem\n",
      "  Name                   :                 \n",
      "  Objective sense        : max             \n",
      "  Type                   : CONIC (conic optimization problem)\n",
      "  Constraints            : 160402          \n",
      "  Cones                  : 40000           \n",
      "  Scalar variables       : 300201          \n",
      "  Matrix variables       : 0               \n",
      "  Integer variables      : 0               \n",
      "\n",
      "Optimizer started.\n",
      "Presolve started.\n",
      "Eliminator started.\n",
      "Freed constraints in eliminator : 80000\n",
      "Eliminator terminated.\n",
      "Eliminator started.\n",
      "Freed constraints in eliminator : 0\n",
      "Eliminator terminated.\n",
      "Eliminator - tries                  : 2                 time                   : 0.00            \n",
      "Lin. dep.  - tries                  : 0                 time                   : 0.00            \n",
      "Lin. dep.  - number                 : 0               \n",
      "Presolve terminated. Time: 0.41    \n",
      "GP based matrix reordering started.\n",
      "GP based matrix reordering terminated.\n",
      "Problem\n",
      "  Name                   :                 \n",
      "  Objective sense        : max             \n",
      "  Type                   : CONIC (conic optimization problem)\n",
      "  Constraints            : 160402          \n",
      "  Cones                  : 40000           \n",
      "  Scalar variables       : 300201          \n",
      "  Matrix variables       : 0               \n",
      "  Integer variables      : 0               \n",
      "\n",
      "Optimizer  - threads                : 2               \n",
      "Optimizer  - solved problem         : the primal      \n",
      "Optimizer  - Constraints            : 40000\n",
      "Optimizer  - Cones                  : 40001\n",
      "Optimizer  - Scalar variables       : 120200            conic                  : 120002          \n",
      "Optimizer  - Semi-definite variables: 0                 scalarized             : 0               \n",
      "Factor     - setup time             : 0.54              dense det. time        : 0.06            \n",
      "Factor     - ML order time          : 0.03              GP order time          : 0.21            \n",
      "Factor     - nonzeros before factor : 3.18e+05          after factor           : 1.58e+06        \n",
      "Factor     - dense dim.             : 3                 flops                  : 1.96e+08        \n",
      "Factor     - GP saved nzs           : 6.64e+04          GP saved flops         : 5.87e+07        \n",
      "ITE PFEAS    DFEAS    GFEAS    PRSTATUS   POBJ              DOBJ              MU       TIME  \n",
      "0   2.2e+00  1.0e+00  1.0e+00  0.00e+00   0.000000000e+00   0.000000000e+00   1.0e+00  1.13  \n",
      "1   1.6e-02  7.1e-03  2.6e-03  1.60e-01   -1.509808514e-02  -1.403595471e-01  7.1e-03  1.32  \n",
      "2   1.3e-03  5.6e-04  6.0e-05  9.82e-01   4.918123109e-02   3.854400084e-02   5.6e-04  1.49  \n",
      "3   2.9e-04  1.3e-04  7.1e-06  8.72e-01   7.138467507e-01   7.109820659e-01   1.3e-04  1.64  \n",
      "4   1.8e-04  8.2e-05  4.7e-06  2.94e-01   2.684262616e+00   2.681212927e+00   8.2e-05  1.79  \n",
      "5   9.9e-05  4.4e-05  2.6e-06  2.62e-03   6.636029143e+00   6.632656524e+00   4.4e-05  1.92  \n",
      "6   5.0e-05  2.2e-05  1.1e-06  1.06e-01   1.022119237e+01   1.021893544e+01   2.2e-05  2.04  \n",
      "7   2.2e-05  9.9e-06  3.4e-07  4.14e-01   1.338418404e+01   1.338307805e+01   9.9e-06  2.16  \n",
      "8   1.2e-05  5.4e-06  1.3e-07  6.91e-01   1.478708499e+01   1.478651457e+01   5.4e-06  2.28  \n",
      "9   6.2e-06  2.8e-06  4.7e-08  8.32e-01   1.558059633e+01   1.558033238e+01   2.8e-06  2.40  \n",
      "10  1.9e-06  8.3e-07  7.1e-09  9.22e-01   1.610883868e+01   1.610877313e+01   8.3e-07  2.51  \n",
      "11  6.0e-07  2.7e-07  1.2e-09  9.79e-01   1.626576268e+01   1.626574448e+01   2.7e-07  2.66  \n",
      "12  2.1e-07  9.2e-08  2.3e-10  9.94e-01   1.631459476e+01   1.631458899e+01   9.2e-08  2.78  \n",
      "13  5.4e-08  2.4e-08  2.9e-11  9.98e-01   1.633333451e+01   1.633333317e+01   2.4e-08  2.90  \n",
      "14  1.8e-08  8.1e-09  5.8e-12  9.99e-01   1.633740925e+01   1.633740881e+01   8.1e-09  3.05  \n",
      "15  5.1e-09  2.3e-09  8.4e-13  1.00e+00   1.633890289e+01   1.633890278e+01   2.3e-09  3.19  \n",
      "Optimizer terminated. Time: 3.30    \n",
      "\n",
      "\n",
      "Interior-point solution summary\n",
      "  Problem status  : PRIMAL_AND_DUAL_FEASIBLE\n",
      "  Solution status : OPTIMAL\n",
      "  Primal.  obj: 1.6338902894e+01    nrm: 4e+01    Viol.  con: 2e-10    var: 0e+00    cones: 0e+00  \n",
      "  Dual.    obj: 1.6338902775e+01    nrm: 1e+03    Viol.  con: 0e+00    var: 3e-08    cones: 0e+00  \n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "16.338902894084487"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "prob.add_obj_func([1, None])\n",
    "\n",
    "crit = mat.criterion(Sig)\n",
    "prob.add_convex_term(crit)\n",
    "\n",
    "prob.optimize(sense=\"maximize\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The collapse mechanism $\\bu$ clearly shows the restraining effect of the reinforcements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling FFC just-in-time (JIT) compiler, this may take some time.\n",
      "  Ignoring precision in integral metadata compiled using quadrature representation. Not implemented.\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAU0AAAEACAYAAAA3NiR2AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8li6FKAAAgAElEQVR4nO29W6wlWXrn9fvWith7n0tmZWX1xTa+dFfbM8Pg0aDstPwCD4OrBUg8oeq2hISAB1e/GKGxZ6oxIyGeZqgCM0IeIVUCmgd4QN2NEC/w0GUPwiPkYbqKsYYB26KyL3ZXd7ur8lKZ57J3RKyPh7VWxIo4+5zMU73PLfP7Sbv32RFr7xMneue/vm99N1FVDMMwjKfDXfQFGIZhXCVMNA3DME6BiaZhGMYpMNE0DMM4BSaahmEYp8BE0zAM4xRsVDRF5JaIvPMU614WkddF5JX0fGOT12EYhnFWyKbyNEXkFeAe8I6qyhPWfkNVv5B+fhn4iqp+eSMXYhiGcYZsTDT7DxTRk0QzieRbWTTTsfuq+uJGL8QwDOMMuIg9zVvAg+nBJKaGYRiXmosQzZtrjt0DbF/TMIxLT3XRF3ASIvIa8BqAx39+m+sXfEXGs4ZIsZMkkx9G59YdG5/7hV/86bO4RGMDvPPOOx+o6ic38VkXIZrrrMqbrHHZVfUOcAfgutzUX5ZfOfurM559xKUnAXHxOR93gngfhdB7SOekruMa5+LDu0FAq/TPaM/3v0Kr5MT5eEy99OvzOfWDoxfqo8d+73f/ww39wYaIfGdTn3URovkua1x0Vb17AddiPI9oAHFoUMQTn7NwBgUP5ABp0CiiIQwiGUIUTVVwPj27+F6I5wJx80t1bJ2OroPCup2cqoS/9q++EX+dl/5Yfz4dC378Ov5M/75/9N/9xlPeFONpORfRTEGee6r6QFXvli5ROvfV87gOwziChvSDH8S066JQdh14j3YdqAzi6Rx0SThDB65KQprUqgtQ+bFwIoUQp1/piMLpBAmKTp6B8bFOR+IIIAoqHCvAv/xv/xdoMl61jGDI+Ng3/xsT16dl03mat4A3gDeBb6jq2+nc19LrO+n1y8CrRKvzlqq++aTPN/fc2Cjiih8nbnoSP8lud3LHRVwUQxhc8uymS3LZ83onqHNRGF1c0wuepHNeUJG0Np7K7rk66cO0vaXph7U5qW+wOPNnRyEchDK9dyqcE9E8dl3i//qv/voJN/PyIyLvqOrtjXzWVWlCbKJpnAl5fzMLpRsEUFxSjiSQkvc4875mfz4Jad4LTaIJa/Y2RSDtb073NtVLIYbDsSiE6Vy5xpEEOImoxHPZPX8a4YzvS9dVvO5/TphoDljtuWFA76Zr3pfswnCu6+K59ExI51THj3wM4v6mSHTFRdav6Yq1gHTau+VSHCtf0ymSfpR8idnu0fye8fnhM4fjotNnLT4nrUvPV10wN42JpmFk8v6mBtCAdt0glF0XrcdeDEtRDcMaGM71x7U/JvlcKM7BSFQHURwEUyUJXCloEt83FsCnE07KvdD0WlT7zyGkY8WfaURMNI3nm6mFqYVKZIHMlmHTxuepSOa1o+dBJEWHY+pcb32KKrgsVoWolVYp0XWWMLY289pSKJmK7RrhPGphkgRzneXKsdH95xkTTcPQMHmphYgWz86NXfTSTc/PGgoXPUQRVR2EcmJhZvcb1lmDhdt+nGufLMajLvhR4VwnsEdc8qC9xRn/hnU37PnGRNMwEkesTY2ip2FsWWrXDRblun3O/FzubxbnRi66ahTA6f5msnIHK5XBIsxCd5xQHiOcY2EcW5zl+WHdCTfrOcZE0zAK1gonROGcBnSmwlm67RMxlWxtToVTCysykwUzi165ZhKkke7jCSfTzwuTZx1/jjFgomkY0Ad/4o86eu7FUPX4SPpUODPFnqhMrdBA7/5KuUaSS95pFNB1kfTChT6tcB67rhRk5aiYG4CJpmGsZZ1w6iTyrVqIqXPrU5DWRdITx0XS+31H1rjiec3UQpxaoKcVzun7s3AGE84pJpqGsQZxciQFKf442bucuugwCGQ4QTh78VsT4JkIb0wvksJlnoirxt8lnY7ShJ5aOHuBZPx+hW/+t1ZeOcVE0zBKTkpBSqKnR/YwQ3p0R8W0TFtyMuxP+qO16DkFCUqBK136dYI52Zt8kqt+7B7m+vPGUUw0DWPKmr3NI/ub5d6mk6HrUY6Iq8ZmHqP3hKNu+roUpGMqfSj2O/P5nEZURsFPL5zjPVKLnp+MiaZhrKMUzkkk/YhwNu04WDR6nqQXled6cQtDN6Qj79dxYntZcpmS4qN7PRHGqXCWe51ZIPV4Ie0F2ziCiaZhPA2lm+5kfQrSkXLKMiBU7FOGIoWoi+67rNnnlBCieE32MntBKwSuFM61rrrq+mAPZTrSsD7vcxpHMdE0jONYl4JUiKcW+Zh6jBUaU4aKn+NioNjDLK3QSaVQL6Bl1LwQ3uMszr5PZymQIwtzIsDHCK9xFBNNwziJaWCoP15akUUKkstBHzcutQzTvM6U9F5Ey6eR9GmHo2m60XEWZy+gKc/zSFT9yF7p+PMsGHQyJpqG8RSMUpBKC7MvhZykGa1t5rFGOKdCeUKNOQwie5LFOXp/aXHK2MJcG0SaCKdxFBNNw3gKehe964pIejdeVEbYc/XQkf6aOt4LzfuZMI6k50h8et0LYhJION7iLJtz9BYnR6PpR/pwHpPwbowx0TSMJ1G46KP9TXFjF1112NscBYGm+52F3ztxxQd3HkZdkdYFeU5jcXY66p/ZW5ilK67j83/w31ti+zpMNA3jaSij51lEj6lDVw2FeE4riLr4/klrOS1r1CfvkRCiiE4tziN7lOk50PfgLM9L0fV99PlTl9wszBMx0TSMj4OGuM9ZCGeMpoexRTmtRYfeMkRkCAgVwti/zmtZs9eZo+FrLE44GjQ6UsN+xMLM78vX+GPcm2ccE03DeFqO7YJUBn0KwXRFqWS5t3nEdU7CWaYgUVQH5ej601ick5ZyfXpR+hlOiIpP9jaN9ZhoGsZpWNPlvd/bLPcwRcbzhfLx3oIMSUDHUffR3mYWSjjZ4izfN23qUVq45fo+TWkSBEr7mjKJcRkDJpqG8TEoE93LTu69UOrk9SiSnt32yX5nn7bE6PhJFmdcPxVSPbZGvVw/HdI22u+02UDHYqJpGJtijdstUys0pRmNuiBNSjFzFLxsALLW4nTjvc9RjfoRCzS9PmlvM39OEUU3jmKiaRinpUxBmkbQNc0UKlz0tW46HE2IP7L/yej1ERf82KBRfl+Kok9LMI9x0Ye/zxTzJKpNfpiIvAy8CrwL3ALuqOqDE9a+AtwDXj5prWFcOjTEvUyiYIr38dlVR8WPZFn6FG33PgqnczEFyfleZIEklop4Bx1oJaNEePVSvJ9kcSrqYpqROk3PsX+nyuBrS9bMMr1I8rWO1xnr2ahoAm+p6hcAROQu8Abw5WPWvqqqb+YXIvIG8JUNX49hnDm5xFK8T+JY9NaEaHG6gKgMwghJ+Hwqr/RAem96T+/Kd0lwJ0I5uPrx7RKioEoA9cN5UUWRaHkWAkvQdO30ie/56v7h13/zfG7eFWRj7nmyHHtU9S7wpRPe8oXJ6xubuhbDOBemKUjTju6jiPq6YI0MKUh5pMY0r3Nag15WDDF2zbXcK53sTUrpuoscb0lmATWOZZN7mreAI+71VEwn574hIjdE5BXgaxu8FsM4H0rhFNfvYa7by1y7t1mK5BGhLBLf8/F1pZUJCaFwtctrTOcn6Uh5tlDfFq7/nFPfheeKTYrmzTXH7nG8BfnF9J5vAbdU9e0NXothnDu6rrNRZl3J5Wg0RnpMUpb6KPdkcuUR4exr1cd5l0eDSoUlObEo+1p0szRPZNN7mqfhNnEP82XgLRGh3OMEEJHXgNcAFmyf/xUaxmkpAkRxf5MoXGm/U7wfhNK5o1YhDMGi4n14NwSB8nv7IFBA82cl4VSfgkY5iES59xn3PGOwqAgOJczSPJlNWprrrMqbHO+y31LVt1X1DvA54LdEZPR+Vb2jqrdV9XbNfIOXahgbpOzmnvYnj6Qila/LUspscZaWJxSt5CbpRMftbRauvabyzSyIo/PQu/DZ4uwtz3TM9jRPZpOi+S5rXPQUEJryCvD2ZM2dde83jCtBMRt9dAyOVAX1XZCyULrin2HQoy3kSkEsoupHSioLF7zkiHD2eZpHrdy+B6dxLBsTzak4Jmvyq+XrwpJ8myicJ36GYVxZNESrcxpZD6kL0tTaLINBToYu79PoeybvbeYo/DHBn96iXGtxSjrGUEYpZmY+iU3vaX5RRF4nJberapmj+QbwDWIS+10RuZvW3iVamG9t+FoM40LQPv8xbSJO043Kn0OKvoQQ9y8himrli/eHYW/TkfYmj+Zx5nzMnNCe9yyPv9C81xmFWnPk3YTzRESvSMnUdbmpvyy/ctGXYRhPJgWCxMnwc53sExHEuSiCInF+T5XOVcMaRGKiu7j0LP2zikTRTOKmVapMyqLr0xoXz4cqiaDLx2O1kAoxsESqOiIe//3/6W+c2a25KETkHVW9vYnPstpzw9g06yZYHkklKurOs1tepiyVvTjXjclIn6U5ip73OCf/ontLUwbBHC8YHr2laZyIiaZhnAV9hU8RIJpOsJxSpiD1QaCimXHau5QiEb7fozxuXvqafM2yFh0ZHuXMION4TDQN4ww50uUdonsO69OLsqBlF13ceILlmv1GUe33OMtZQ9OuR9OGxH2ZZVF/bulGT8ZE0zDOmKlwatcN4nfEBZ+48WssTZ0Ip5ZC6zgxkHOkkzuMRvxaYvuTMdE0jDNGnPQPRGJVUA7aQMzZzMJYVPYcceGTJSrHWJxH8jnXMD0+TjfCSiifAhNNwzhn+j1NKUQUBiHN7ntVZAT26UXj5h0jRq49x7vyEN3xnGbUpxtt4q979rnI2nPDeObp047ExfQi70cVQL1gVtUgck5in02RQUjTs1ZjYdXKDW3jMjn/UrVPKepPZUFlvH8p+oScTqPHRNMwzgJZo0A5vSgEqKs4P8jJIJiVH4SzKrq5V1kwU76mc7EhRxsbdQiBMK+QZIVqamQc6mNUsA8MDULZW5rmnj8R+2+LYZwFGnorU1xMTO8T3PNzKZJlYCh3OHIO6gp1Dp3X4D06r9G6EFFH6nAUxTLMq14swyyua7cr1DtW1ytCLSxvVIS54+ATFWHmaLZzt3hoF45/8df/7rncoquKiaZhnAXiYjllGfDJlmPQYUplshzjsx/2MSs/jL4oXey8J+kcYVGj3tHt1IS5p92t6RYenBBqh7oonKLQzR0SoFs4Zh+1NFuOxYOO1a7Dr5S9T1e0i/j7tn8UTDhPwNxzwzgjxMVplFJXMTczN8iYBn4gRc8DBHfUHc+pRrP4zzXMfN+hKJdQxhfEUvLaRZFUaObDaIt+D3PhCDOhweGXymrX4Tql2RHqvWF/NAvnP/l7f32zN+aKY7XnhnEGZGGUqo7PdTVYld7FenNJ+5l+EFTquF7nQx26et/nX2rtURG0dlEE0/vCLIpnNy+aFRPrztVBqGUU6Onm8Xy7EPxS8at4PNQQ0nu7SQvbqyyem6w9N0vTMM6AcaejghCQWR3d7KoY95sDR2VXdkBnvt9E6/cy055lnCo5vO5/dxa9WXpeFOslHs9WZ3TZhW4ObTEcYXU9Plf78bl+/HHvxLOH7WkaxlnTdzRySRCLMsc+kT2ppEsRdS9QuTQsrXDRcxK8Klq5ZHEKoRLCLLrkoZa+s1Goh7lB7VxoFxLH+wq0W0KooNmGbjGuBnJNjK53W7C6AXs/Db/wn9o+J5ilaRhnT9dFqzKlGgFFbblA28F8NqwvIul9uzeRGCUnNuUI8yLAlE2foIRahr3NwqLsio8P9TivyHXxM1SieGo17H92W8P2neVxRkw0DeOsKSt9ugCj4I2mnMz07MYNO0SVUPnxNMk6phjlCDmp4XDep8yiKC1QFf00SXucqVyym9HnZbbbUShdlzRXx0JrgjlgomkY50W2NCfCSFCoh7ZwWlqatY+WZR0FVR29ay6ajuexvZqCOLl50iyuVzcEhCAGeCREgQwewnwQWHXxmPrB+ozXci536Epg//0wjHNA27YYWTGxNL0bAkClYFZuJJCQ6sWdRMszEEso8z6mjyWSrlNCDgLVUfwkaD+61zXxV3SzKJjRWoVQDdZl3vc0jmKiaRhngLix4oj3cW/TyXgeUCmYuVY8WZP9uN5R0+F0yAtaCa5TtC5yMV0MAuXIet9UWKMoTvc4paW3RiEFg5y1iDsJE03DOAM06Kj+XEfD0VwSznJ0b5ou2SWxdC4115AhCBSSqObZQBLdcQnxuXenk5uuAtJBV0TTs7WZe2dqVbSFIz5Lx9quR+/9jd84m5t1xTDRNIwzoM/RLKt+yjG+eZTvZO6PivRueVk+Kapx+FlIzymxXTTuV0pu9Va61TIWyOyCI3mfkzT+gkEgNbvzDH02jREmmoZxBvSWZteNB6Zl8r6mX/NPsCvc9dwCzkmq9ImJ79mSVBmS1iUo0g3VP1kM1cdKHzSlE61xxXtXXpLIKrgWUDHhnGCiaRhnwKgaSFKqUTk4LdOFoXdmxqd9TzcEgPoUpKCjh1bj6p4ojtrvU5YCqT663tIVbeGSRek6QIu9zJAFmSicwaJCGRNNwzgDRuN7Ne1fdt1Ry9I7CN1YNLti3zOEuLdZfJ46wTVxf1M6jfuVKfdS0jhf12WhTPuffrAoY9oSw/TJFIkfJlfGJ5kKqQGYaBrGmTK46Wl/swt9SSQwWJo59QgGSxPAOVzTDbmdEHtoVpLc60Egc8oRlAEf7UUSHaxMGAI+5eheSTsJpaueP8+IbDS5XUReBl4F3gVuAXdU9cEJ618FbgJ3AVT17U1ej2FcJLGfZnrhZCin7IrIebY0Kz+48NPP8SkAVLtCFLXvRqQy5GIG54bX3fAMQ6Rck/XYJ64XrvvoOYzfb0Q2bWm+papvJvH7OvDGcQuTYL6sqneIonnsWsO4cpTdjbRoujF10bsQXe8cUYfBPU/EFCQZtXwrGRLX055n4XpLdq+lsCKz9ahpL7M8Nn2dnv/kb13dtnCbZmOimazMHlW9C3zphLe8oapv5rWq+vlNXYthXDgpR7NMcteuG1z0LoyS1oEY+GnaUSAIBksThjzOMB2Y5pK4KpPE+HQ5pdU5iUVNBbMPCk0E1Ihs0tK8BRxxxadimo71a9PPhvFskWYEadcNApqqgo7MOS/xbhQIWmdZqo+VQDmPM+9pZqJbLYW1mAU3LxgLoqwRSBPK49mkaN5cc+wecGPN8ZeBe8lFvysir4vIKxu8FsO4WNKMIMRF4cxWZbYsczS8L63Mkyo1/qvsumh59hVCRaJ7l/pqdgrdUC7Zi1+RlD644oN5+SRXPH9G/tn2NMdcVJejm8ArqvoFABG5A3wLeLFcJCKvAa8BLNiefoZhXF40RAuz2NvUrotjLoKCJ7rgOTCUxlwAw95lm0byzv0QCNIcOdcYhB8NXcvvH6p61A/djPpz1Thvs3wmJbznVCNrCXeUTd6SdVblTda47Gntu/lFirDfWLMvekdVb6vq7Zr59DMM43KjYUg5CrFDuybLUrOYtm0MELVtfEz3OdN8c4Q057zocASDUGYXPGjaj8yvhyi56wY3vXTJc9PiqYufhfOP/hMLApVsUjTfZY2LngJC69ZOeUAUU8N4tsgC2YU4lbILwwhf74euR+lnLWYE5VLKYV8yIF2I5Y9ldJxCFIugERy3hrSG4bWMxdTKJ9ezMdGcimOyGr9avhaRG8Xae/l1Wnv3pJxOw7jSpOBPDAylZxgsy7ZNbrtH2i5amLkdXNfFqiBNgiiCtJpqywuLMie657rxxBGBnFT6jJLbc/nkJIJuDGx6T/OLIvI6KbldVb9cnHsD+AZwJ68FfktE3gM+B9h8XuPZpk9u72IH9yyWeZ8z9dGk8kgb4hygPkgTcz0VQbqAVnFWuTqBVkfJ7EPWempI7GXY1yxc8X7fs2gXl4NKKiaYx7FR0UwW5Jvp5duTc1+cvH4AfGWTv98wLiuxOsgPQZ8uBYr6mnSf3HgPbYfWFZKs0TzyIgZsckd3Yr042qcXlcIH0doMdRLOVHbZVw5V4+5F0g2t4iiCSMZRLDZmGGdM37xDo1mnTVNU/6T9TE0BI83ud9f/3NO72Wv2Lifdk/p2cW1+TV8BVHZzz8PUEHAx3jRy5//Z37Eg0BQTTcM4S3IQSMOQ6J7nlucczTKCnpt5ZBHs8zrj50jK55RUUVRGyaWNP7tUGSS9WMfPyWI4tHwbC+j0ufDyjQITTcM4S1I10CjRXSSOv4A+FalPcm+7+BDpLUhJ+ZrZXR+lF5ES13Myeqvr15Ru+7Q8Mgtl0UuzjLgbY0w0DeMsSS45METMc+qRKqohTqqUJJihGzoiUbjibaxFlzZZmK0WbnRhYVKmF03yMRmi43ntcQJpVubxmGgaxjlRdnPXJm025i5HObEdCne9mCkEsUKocMn7Jh2UVicQFNeMG3bEevNiDUk4i/lAo+T2zizN4zDRNIxzYhQQytZk1w1R9TxsDQoBnQhnpzFnk8IVnwpnKEotsyC2RJe/qDMPXtZ3PUr/809/24JA6zDRNIzzQCdmWzFsTct56DA080i5mdKG6EYX75Fi5G+MkmsaWTEun8wD10aWZiiOw7jnZpHwbqzHRNMwzhntOjRoei7c9RxBLyuE+qj5ILoSkojq2N2WacR9ioxFNT6ncRgyFk5zzY/HRNMwzpG+gQdEa1O1tzp11cTjvcWZLMu260f5DhH0sVXapxdR7lmOX+eZQOU+Zz8jXRmqisQCQSdhomkY50WRs9n/XPTVlLoqgj5dPNcVJl+XE99DSlMa72nmcRcqpL3PnNc5sRyLyZV9TbvS52ZaEOhkTDQN45zJASENGpPe2xhJ1ya55NniLGael+65Suq16cZ7m1IEifo9TBGmDYmzpVnud44i6AJ/+F9aEOg4TDQN4zzJKUelcDLkcGqXJlPCkQh6v4cZAlRulHqkLorp0BlJ+wh6/iwVKMsyc/lk2RZu2lfTOIqJpmGcN9Nk9+noCzfUoA/J7jFHs3ffNVmVuXJIsmueLMvkdpft45hYlnHvUuKawgq1/cyTMdE0jAtAnAzJ7nnIGqR9zBzBkfFjSrIuB+EE147NxDzaN+doapF6NF4ziKWlG52MiaZhXCDii/5r3kcB9Wt6sqWczeGNMhK/UMXhbKOZQSngk63NkGcDlUPact164Z4bJ2OiaRjnTZpUmQVTqioOXIPBVYfYnFgcOqtj02IYrM403lfKjkhOBnc7dTUqR/kCfU5mObGy73qU+Ce/Y0GgkzDRNIzzJs9ET+Mt+pEVsxqZzyCLZBhyMnNUXGs/eg6zwiotBFdr17vi5UTJMpIefAoeFR2QjCdjt8owzhNxY0szaBRJX0TMU2s4nEe35qhz6FZNmMcxv+qlT3ZXL2jlaLcrwszT7Po46pcokP1Ii3UWJ0NFUD+10vYzn8hFzT03jOcWyXuKGoa5596lPc2kZsldzylEqgppvzIskngyCGceaVE/7mh2Pb5Rullc381Tg4+iRVwO+qiTFFCyVKOnxUTTMC6K5Jr3+5kQAz6zOlqczsV5QC7mZIaF74NBYeYIVRTNbitZlk0UTtcpXS0EL2gFfql0c0E9dDPh8KZQ7UE3h+og/lrRNHjNeCLmnhvGOSJF5JpynG+VrMw8pXJWR+sTYtAnCaSKEGo3VP5A3yKu3XY01zztwrG84QgzQdpoaXa10M7jY3FPaXfAL+Hgk9DVsNqFdis+fvFv/l0++zu/fW735KphomkY54gGHZp25MYduS1c20ZXvar6fU31Hp1FS1SrlJLEsBcZagEntPPxP2XXQjeDZkc4fNHR7GZXHdotoTqAdie67M1uFNDVjfG1fvZ3ftvEcw2ix7WRumRcl5v6y2Kj0Y0rjrje2pT5PD4v5sPrWdqv3N6K630SzrlHvUO90C2qPpCjXpKrnqLrEoUSoN2Wfh9TfRTM4KMr3uyCPwCfyty7Oq4Jc6gex2N7P3t0k/Nb//5vbvR2nBci8o6q3t7EZ9mepmGcMxoUVxcBIGKuZnTXk4DmPpt1HdOLAoStuAfq2kCoHGHuUtpQTC1qdtyRbu3qAYFmOx5zXRRO10K3Bd0CmmvxPf5A8Idw8JOKPxDqjxzN9bFwZsvzqornJjDRNIxzRlzcx3Rbs/6Yti2yWKRa8xjw0cpBCDFPs3J98w3V5JYHTZaopLQhRZ3QzWRUPx7qKJLqo2C2O4wqf6QT1CvdlrJ8ScEr3TbMPnmA7tdHr/9BzWd/57f5mb/8A/73X/nPzuo2XVpMNA3jnMmWpjYtspgj4qLF2baQXHUAaUPMzUxVQOqkL4Hs04P6OelCqGKE3K9iulGo475mFtB2O+5huhZCFa3MUA/bc+0LHTQCnUAd6BqPq+IvqmbRz3/p2h58Gmpf5C89Z2xUNEXkZeBV4F3gFnBHVR88xfveUtUvb/JaDOOyMrI0u67PyYwBoBbm8xgdrxwSAl0drT3XBELt6OYutoJLrd/Clhv6YXbQLmKqkbookG3aHs0t4NpFtD5jz81kZS40CmYduxH7aw2zeTO67peu7XFttiQkFd6tV2d/sy4hm46ev6Wqb6rq28DXgTee9AYRuQW8tuHrMIxLS64G0qYF79Hcxb1tU7J7HnOR3PLcuSgooXa9lakuWpeS+mJCTC8alUrWeW3c42wX8bVfAQphroSZglPwQBAWLx1Qz1pWy5rVMn7Azd19VIWPlguc6HMrmLBB0UxWZo+q3gW+9BRvfRl4ojVqGM8K2dLEO7RpxsntbRsfALWPteO165tsuDbEzkU+djnKbd2CF1w3dHBHowuen8MMtIqRc1Fot5SwUNSluvdrDWy3sN1yuDcjBMFXHTdfeMwL24csqoZF1fCp7cdsV82Rv+l5YpOW5i3WiN9UTCfnXlXVr2/wGgzjUjNObk8R8raNIy8KN12WgyUnXUC6MEpq98tAqIeAj+s07l86iUntGq3M7KLHJPe4NtRRON0KdB7iY79CV3lLpawAAB7CSURBVA4RRUSZz1pu7B4wr1sq37HsKnbrVe+aP89sUjRvrjl2D7ix5ngW07sb/P2GcekZJbcXw9XEe2gKC857aIrZ6JXHpcofFZJbnueZQ1cXOZn9vHP6meahJu575l7Hs/iQ5SABfrul3mrZvhb3LfeWMyoJVBLYSYJponmxFUG3VPXdkxaIyGsi8k0R+WbD8ryuyzDOlL5je9EWTptkaZajLyo3tI1ruz6BHaIbHirpczSHeT/aB3zUxaT14ME1UVz7Du2BYcTFPCDbLdoJzUGFE8WJspg1LLuKZVf1gumsrftGRXOdVXmT9S77K8DbT/pAVb2jqrdV9XbN/EnLDeNKoIUwSm7a4X2cQpmF0/sYCMr9M5OlGepiMFqr/bHSuizdcpeFskp7miG1gauUsBXQWuHAo13cN/WzjlUbf+eqqeiCw7vAsotbB2Zpbjbl6F3WuOgpILSOL8nQdOCGiLwGvH3CesO4+mgY+mlWLuZq5l6aVXLRtxbRwqwc0nSxd2ayOF0Tn7vtmAAfrUilXZRiGp+bZGWS+2X6IS/THzg6omhqHfpc967xzGYtXZB+uoaq4F1HQHDWcHNzoqmqd6XsvBL3LL86eX1PVR+klKRy7VuqemdT12IYlxoNMeWo65BZHbsYaYAmxM7tqxgEksr33Y2kCYStZO3VEl/Pk3BWg1C2W5IaeYALENJ4XzQGgxxCqFOaUUIaB7MWDalmvfU4p8zqlk4Fr8JhWz/3UfPMpvc0vygiryf3+9VJwvobTFKQROSGiLyefn7jpEi7YTwzZEvTSUwvyh3cQ+raHjS66Xk/UzXmazZDHbh6ie55E6L1KbH6R1IUPeOa9GjzGwEV3Comtfd7m23sROx8IARBgWVT4UVRFXZnS1bB40Sfexd9oxVBybV+M718e3Lui2vWP0jr35yeM4xnnZFwkrocdV3sq+ncxC3v6LYqRBVRQdJeZqiP2j0udy5a5F+ULM8G2ll8rbXi2tTQeK5oJ4hTQuOpZh0ahKrqWLae7RQQmvuW/bbuU4+e16CQ9dM0jIsipRxpbka8Wo0HrTVtFNBkabomjNKGgHQs7VM2+Vjcv8xWZk5qz8dwGtOTuhR9XwniFYLgZh1t45Gc9C5w0MSqoGUXI+urzhNUaMPzKR/P519tGBeJhkEwcyRdU8S8acgjevEOWbWDxdlbnYHgpa/ugWG6pATwjUZ3PI2wcKtywBrRNRfAx5ZwOlNYudg9qXWIQLuKTuiqiS75YRLOVWczMUw0DeMiSPuawJCb6SRaljnJPe1zStMhyxhFl6BoJfgmJKEcas19E0W0m8VEd1H6fE2XCoykGzokySrlKXWAKJLc9a5xIEqbUo/azkULs02BKIRV8PzP/9LfO+ObdDkx0TSMiyDNPkcDOEFz0+GmiRZn18UouirSdeAFt2qHmvMuu+SaxvNq7LepKYndDdMnXTMIZQ4IuVZiMnwOELUx6k6bZhGFeL5NlmVMQYpBoefd2jTRNIwLYpTk7hx0YSinbLtYatkm5evSvuYq4FYhimcb5wT5VYhd2ovZ5nlfMyezj8S0XyMpCb7o0Ska+2kShVODjIRy2cWE97zP+TxiomkYF0iuQ9emJU+m1NwarmliwnuniCrusEW6KJhuFXoB7EVwFVOPXKe9S+5a+vp0GCLr0g4VRJr6agJxbzOJZdc6nA+sWk8Ijv1VTfecBn9K7A4YxkWRXHTtuuimty2smtjJvY0iyuEy/ivtuhgcInZ0Vye4RnHLgEosqUTijHNNwum6HDgiimnOTU+16b2rvhIkCKTkdvGKtlE821XVJ71n9psZ/9uv/Odnf38uKSaahnFRFMEgDTpOPXJxPlBsE9ciyyii0nSg4JZdTDeS2CZOfXTNQxUDQn4VZ6S7bFHqZJ9zNbZSEUWaJJwrB0HQdpCHpvW2n5kw0TSMiyKnHpUpSG3KzVyl1KOmGdKQAERwTdd3c5egBC/4ZegnUfbNOzRZncmi9EkoXYqyq4C0UWxz5Dw/x3ymZACvolA2rScE4f/81/72+d2jS4iJpmFcMGVACEiWpvTR89j5KInkqo3dj2Ye14QYSU+pRbnfpm9SqzjikDXR5JrrEC2PLjwjwXRNEswgsIxCGVoX3XVih6NlY7MYTTQN4xIwytkUQVdNDA4BHBwCRNdcJJZStl1yt9O+5SqmHrlVquQJ2s8B6tOK0sgMCeBTe9ocKBr112xTXXqqUxdRutakImN3wjAumrKcMndzh2hlZtf8MKlc1/UWqVt10WJcxff4wxwoYph7vk4oUzpSKZSujeN/pZG+47u2Ak5plxUaHO2q4o//zf/47O7DFcFE0zAuA6VwwkgcYyekuM8pB6lt3DKe84ep2UdKM/JNcsdTGWUplH0H91yA1KZ69BwUSm4+AjQpot7GuUGjms3nHBNNw7gk9LOD2jbua5LGYKhGS1MkuueHDXhJqUcuBoZ0mELpl1GAXauoj/ua6BApV6KY5qAQOXCUrcwwHIvXJXRLz3u/+rfO/Z5cRkw0DeMSUSa398KZczaXK2gadFYjB01Meg+xBt0fJJc99db0q9jJyK8G69C1SSjDEGGHZGXmGUPtEAyS3EApSN/1yDDRNIzLQ5l61MUyyiycqMZoelBkFbPUZZVc8xQU6tOOGqXsEyyBIdG9HY7lph45dxMlBYCGcswsomqBoB67E4ZxyRjaxSUBPSwmsSYrVFYNOFJppSKpIsil7u59RZAOqUi5NVzpqkM6RiGo7XAs8+1/9ysb/RuvMiaahnEJ6YWzbRHn0OUKXa5ivubhMnY/ygnuaT66dIpK6rPZDu508JIadgxllb1rniuEUlBoRONSUMhkosTuhmFcNpKFGVKepq5Wg5vexlxNVrnnZiqhLEQ0d0FCh+mV/YygglyLXp7rOx91w7Fv/9rf3Nzf9gxgomkYl5Fib7MPDDWDUMZHYW2uWqbTdfsKoRQMisPYGERRButSXdH5KH+OU2gFY4yJpmFcVjSMK4W6tBHZtlBXw6jf5J5rFevScYOF2X+UG0Q0W5hZKEcUUXLXyNAyzugx0TSMy0wxIx0nEzfdDW66cykYJP2o3xzMKYM6eSY6UpzrOx2Vx6Kb/q1f/80z/OOuJiaahnHJ6Uf9dh0ym6FZKCsPszrWqvuUGuSEMPMx0JM6I+Vgj6Zqn1IoRQcR7SPqaeKlWZnrMdE0jMuMuCiY3kfB7Lo4EmM+6zsf5SmVOME1OTCU3y9DL81CKPuSyT4fM31UFk8Hd/+D3zifv/GKYaJpGJeZ3MCj69AQkN2dKJiJsFWj3qM+jvnt5mluuirBA6q0C+kj5DlPs6ujUIoSVSC57a6NUXcCxjFstDmeiLwMvAq8C9wC7qjqg2PW3gJeSS9/Cfi149YaxnOLuGhtztIgs6aBuoZVg97YRZoOrT1uv6F9YU637ZEm0C0crlVClaZMShrt2wJVMjJ9Gr7WQpjRd34/kq9pjNh0R9G3VPULACJyF3gD+PJ0kYjcAG6r6pvp9avA7wKf3/D1GMbVJqcerZoonFtb8VhdR4vSe9oXFjFx3QnVo4b2Wo3rokjGNKMonqJRFENOM/LR8sxjMNTHMkoJ8N5vmmt+HBtzz5OV2aOqd4EvHbP8NlDWZb0N3EpiahhGRhw4QRbzNAZjRR7tG7bmaO1jJVAKBDUvRIs0VMJq16EO2q0hoKPJuizHYKiPrnnwRcMO41g2uad5CzjiXk/FFEBV3wa+WBx6OR0399wwCsQJ2sRWcTKro3D6+JCmi8PTDlq6maPd8gQvtPNB+LqZUO8poYZ2a0g/kgDdLPXTzDXnqWGHWZkns0nRvLnm2D1grfWoqu8WL38VeHOD12IYzw4axqlGyxW6qAnbNVo5wlYVI94pYi4BVtcc3Uz6R/BC8NDNk2Auho/P4gkpEGScyIVPSUou+a28Fzo59xrwGsCC7fO+NMO4WNKIX/EeXa1wuzvx+GKOLFvcMpqIQSraF+s+jajZibaQBKXZEVbXBASqQ2AJzU4UTX8I7QyaFwLVY4dW1jPzadikaK6zKm+yxmWf8AZjV71HVe8AdwCuy037f9R4vkjVQKWlKfN5PLWoUecICx/7Za6UMBOkU1yrrHYd3Yw0pTIGgNoFNNejWFZ70O6AVlA/jCLb7sZ/Yp/9nd/mEz//YX8Z//hff75H9k7ZpGi+yxoXPQWE1iIirwNfUdUHInLD9jQNoyBZmsCRRHY5bGAxwx12NDfmaBUj5dkdR6E6UNqtODCtuZ7av0m0MlfXY5S8fiysXtAUPde1VUC/9L/+R/3PJqAbFE1VvSsy3PAUAPrq5PW9LIwpzejrhVC+Anx9U9djGFeelG4k3qPLZbQyU+I6gDtc0b64jV/mPm4OTaMuVtccy+tCmEWh9IdRLCVAu6O4Rgi10uwqYZGmWe45upst8tjzwf/30sjazJQCCs+niIrq5rzeaXJ7zsNM574GfENV76R1703efldVP3fcZ1+Xm/rL8isbu1bDuBKIQ7xHZjXiHHL9Wjy+FSM5zU/FHbFu4VneqGK1z1xoF1Ewg4+J66GC1QuktbHLe3etRRoXq38qhUXA1VGAu4NoT/3ln/9efykf7O0ce5n/zmf/AIBf/0u/t8m/fmOIyDuqensTn7XRQFByxbNQvj0598XJOgvTGcaT0AD4uJ+5sx3bwm1vgyq6NcPvN3TbNf6wwy8d3dzhV8rhzfjPK9TQXEvJ6y664P5Q6BbRFddZiK78bkO38oTW4aqA32r51M1H3D+IAdgXt/b5xM5ef1l/9UYU05+dD9boT1TPx+7ahUfPDcN4An0gaIXMZxA6mC+QNtBuVbHmfCv+U1ZPHOO7ioEf6dLM8zpGz9uF0LzQofPQT5/UStFOqBctIUSxvXF9n0XV9Jdw0NT8/AsfADBzLZ+oHwGwH2Z8soo/P+iOt0SfJUw0DeMKoKsVbmsrNiIOCnsH6I1d/N6KsKhBod2OUfBmW3ArRbfzON4ooC4FexBw+56w3aGVsrhxiKahQQ548do+APvN0Bhku17x6dlH/ev7zQ4/OYuW5aNuwc1qj1qKGRnPMCaahnEFyLmaeI903dArs+lgXlPtNVTbuVYlpRBtSczHXMVHN4s9MnOppMwCs+2GdlVRzWLO5ydvPCaosDsbJmD+3O59AH64us7NOrron5p9RKMVN6vHADTqTTQNw7g85NQjAXT/AHnhGtJ0hN3opne7sUnHajdZm1sxoT341D+zhXA9phYhELY7BFjt19SLFlXhpWt77M6WOBmCw3/lhfdH1/FiPViUc9fQpbrM+sjcjGcXE03DuOREwYzNh7Vpopu+dxAj6G0gXJshbUCagIShMloFcKkRe0VMLeqArUC9s8L5KI7zuuUTu9FivH+wjXeB6/NDfm7nPg+aGAi6Ue/3gnnD768VS/+cNOE00TSMy860MqjrYkBo2eCWDZIS39vtXWYPW/Y/XTN7pHQLoUoBbxXY/o7n8JMKS0e7cjALVDsrQiX8+aNr3Ng+oPYdP737kIVv2PJxHtGnZjHQky3MlVbMpO1fe9HnRjDBRNMwrgR5zIXu7yPzOdo0iHOws42sWsLuAr+MwjV/OCS7R5c9zQlKzYghVf+0wmzWcm0R9y9XreczL9wnpKDQg2abv7T7/f4abvj9/ue5xMj6TDqchOdKNG3chWFcBXJJpfdo28YIehrjqy7+M64er9BK8EulmzuqA6U6UPwqllB2i5R+tAhIANluOdibI6KIKJ/aecx+W/PJxSN2qiVbfkUTKppQsSjc8KlgzqSL1qY8H+0hTDQN4yrQd3Bf9ZFz/egxtB2iittbopXDH3R9z8x6T+lqwa9inqY68Icxei4rh3bpodKnHEF0w2vp+OTsMY16tv2SWlocylwavCg7Lh6bFRFzR+B/+dYvnt89uSDMPTeMq4IGkDr21VSNbvr+AXJ9FwB30MTRFwFmH3Uc3vRUh8ryhlAdRNFcvqS4pdBd75CHcVjQvZ0Y7AkqOFFuzvdHv/anZ/eAnFbUspC415n3NGfS0iX7q+bZTzsyS9MwrhDaNmgXhUmXyxggerQHj/aQVYs0gfrhim7hqPcVdTD7SGNN+gLmH0ZLs7pX4VaC3FzSLj2LWcOq81ybH9Kq4+ZsKJk81JpDrbnmD1m4hg5HLR1eArPktnsCNfHYs46JpmFcNTQQDmPwRtsiP/LRIHSLHx7iVoH5w4BLxt/ig9Q/cw/8Mv7cHVRUs45VU3G4rNlvZhx2NY16GvW8WEer85o75DDUzKRjxy3xElhIg0NxKIvkqnue/X1NE03DuKLoKrrJ+ngPPTgA76ju71Hdj+I5u7/CN1HEFg8UHMw+An+QatRFkTrQrjyrw4rFvGHmW67Xh+y1c16s9mlCFM+SRqs+Wu5TIAhizubzkORuomkYVxENaFDC3n5sUNy0sFz1p6v7e4RZ/Oe9uB+DQ/MHSrWfhqspVB85OPSExzW+Duztx67w3318g+8+vsG3D17i2wcvAXCv2+GaP2CV9jUh7l/WdDii1elRszQNw7jEaIiu+v5+DA5VFTzeh8f7dC9s4Q9b5j86AGDrw47Zo8DqWpo+6WJfTf+Rh1ppfrSguT/no+WCj5YLdusVh13NJ2aPWYaaa+6Qe+0u99pdFtJQS8fCtf0jR9xr6fjD7/7Mxd6XM8ZE0zCuMDkoBBA+uBf7bc5n+A8e9cfrjxq6WlhedyzuxeBQtQ/zB3EUxuz7NX7fMbt5yKO9BYuq5XEzY7daMnctD9stDjXuc97w+31gqFFHUMETeiuztkCQYRiXHe26weq8/yAKZ13hHx7gHx7QbldUB4GdH0S3evYYFh/G0krXQqiV9sWW5cM53gceL2fc29/mg8Md7jU7vFjvsR9mXPOHSSwrFtJwqDULaXuxrCWK5+IZ73ZkomkYzwBhtSKsVrE2/d4D9Hs/gGWDzmsW34/NOLqFY/EgpA4e4IYtUOp7FeKV5UHNo4+2mFddzNms97jf7NCEiu8sP8FCGrZlSaeOhTR0yKgrUi1K/YxXBllyu2E8Q3SPH8cuSAAffQQ7c8LWnNmHhwAc/uQW1UEUtf2fEOYfxEqgw08r/l5NmFW4Tx7m2W38vx/9BAAv737A3EVL9V4Xk+l33LIP/HQIO9I9F1bY8/A3GsZzRTg4QNLgNb7/I1zqhtTt1NQftYjGsRfXvzVYhIsfCvVHgjpFgzCrOn7w+Bo/eHyN7arhQbPNrl/yKCx4FBZsuyUfdrsspGNGYEZgIcIsPX70/j93QX/92WOiaRjPIN29+zENqWmRDx8iHz7ErTq6hWfrRy3Xv9sgCrvfV0Sh24LVzYBrBecD9/e2+PDBLrOq44PDbXaqJbVr+VF7jW234lBrbrh9DtVzqJ75yEV/tmcmmmgaxjNK9+gR3aMhil69fw9/0OIPWlbXPP5Q8YdR7Kp92P6zNCbj4Zz9H+3gfOCH96OlCfCnhzfZdisedQvutbvs64wVnoV0LFVwDII5Z5wQ/yxhomkYzzjt+9+Pie91zez9h8zef0i1DLRp8Nr1bw9pQrN7URLcgaOqAttbK1p1fOvxS2z7hvvtDt9vbsQ6dGlZSEuH0KVGnZ0qNY7wDCe5m2gaxnNA++GHsH+Azit0XrF99wGL+x3VMrD3Uw6/gu0/j4095NCRO8WtmopvffASC9/yw8NrvLf3SR53C97d+wwA97ptftTtsC2BWhy1OLakZkvqi/tjzxgTTcN4Tmjf/z5y/xE6r9F5zdb39tj63h4/8Q8fUu3D8oYwfwjX33MgsPzeDsvv7eBc4HuPXuCP73+KnWrJe48/wQ8Pr/HnXXwspOGH3YKAshBPS0dLR/jBX7joP/lM2KhoisjLIvK6iLySnm9sYq1hGJuhff/7uIf7uIf7cV56QbUfH49/Rtn6vmPrB1E8D/7sGj98/wbeBVr1fO/RC/zV63/Kd1afYMct+TDssKdxRvq+xrSkLZmf+992Xmw6T/MtVf0CgIjcBd4AvryBtYZhbIj2vW8B0P3s5+nmMWDzwnvDnPO9n5qzvBn3JN0y7Xt+5hH7q5p//IOf4d/42X/GH+39JBBbxm27Jf/C/H2anNz5bAfPN2dpisjL5WtVvQt86cddaxjG2eD/wTvU9w6o7x2Mjv/U7y+pH0n/6K51PHz/Oo//+EW8U/6PDz7b99H87vIlbvh9PMqPOk8twr62fBj2+DDsHfObrzabdM9vAQ+mB6cC+THWGoZxRoQ//H/46J+/zsGn69HjxT8J/WPxfsW1n3jM9i885N6fvcDMd3xn70Wu14fMXUvA8e32Ja65lr0Q2AuB6zLn+jPqom9SNG+uOXYPWLdXeZq1hmGcIbv/wx+w9cOmf0x58U8Cjz7c4dGHO3zmcz9k2Va8//AFAD7qtvj9R38RR+AH3Rbvd1vccBUH2nCgRz/rWeBS156LyGvAawALti/4agzj2cX/g3fo/trnAY4I58Gnh4DRt7/7qf7nb+4MfTOv+wNeqPb5V3b+iD9NTY7+ys/82Rle8cWxSdFcZyneZI0b/rRrVfUOcAfg9u3b+o1vfm0zV2oYxo/FZ/7+m72AfuZn/5zf++Ff4LWf+33+6TLWnP9bP/+PLvLyzpRNiua7rHG7U5Dnx1lrGMYl49v/3utHjn3m77/J3/6X/8dnWjBhg6KpqnelKNRPQZ2vTl7fU9UHT1prGMbVIwrpUTF91tj0nuYXReR1oiV5S1XLvMs3gG+Q3O0nrDUMw7iUiOrVKKy/ffu2fvOb37zoyzAM4woiIu+o6u1NfJbVnhuGYZwCE03DMIxTYKJpGIZxCkw0DcMwToGJpmEYxikw0TQMwzgFJpqGYRinwETTMAzjFJhoGoZhnAITTcMwjFNgomkYhnEKTDQNwzBOgYmmYRjGKTDRNAzDOAUmmoZhGKfARNMwDOMUmGgahmGcAhNNwzCMU2CiaRiGcQpMNA3DME6BiaZhGMYpMNE0DMM4BSaahmEYp8BE0zAM4xSYaBqGYZwCE03DMIxTUG3qg0TkZeBV4F3gFnBHVR8cs/YW8Ep6+UvArx231jAM4zKxMdEE3lLVLwCIyF3gDeDL00UicgO4rapvptevAr8LfH6D12IYhnEmbMQ9T1Zmj6reBb50zPLbwFeK128Dt5KYGoZhXGo2tad5CzjiXk/FFEBV3wa+WBx6OR0399wwjEvPptzzm2uO3QPWWo+q+m7x8leBN9etE5HXgNfSy6WI/N8/zkVeEJ8APrjoizglds3nx1W87qt4zX9xUx+0yT3NU5Nc8lt5L3SKqt4B7qS131TV2+d5fZvgKl63XfP5cRWv+6pe86Y+60TRTJbe505Y8o3kbq+zKm+yxmWf8AZjV90wDONSc6JoJkvvaXiXNS56CgitRUReB76iqg9E5IbtaRqGcRXYSCBoKo4pAPTV8nUZHU9pRl8vhPIVnszTCvhl4ypet13z+XEVr/u5vmZR1c180CS5PedhpnNfI7ryd9K69yZvv6uqJ20DGIZhXAo2JpqGYRjPAxcdPT9N6eWxa0/zORdw3ceWjIrIG0Sr+6vEYoB7qvr1S3DNx17Xed7rU17z1zimHPc873P6fbeA/1pVT6xyu2Tf6ae95kvxfT7lNW/2+6yqF/Yguuz555eJpZinXnuazznP6yZmFLxWvH4VeKd4/QagwH3g9ctwzU+6rvO816e8Zl3zeP0C7vMr6R+f/jh/3znf56e65kv2fT7Nfd7o9/nM/qin+ENeLi84Hbt/2rWn+ZwLuO5XgPcmXzoFbuQv3WW71ydd13ne64/x/Xhlcmz0j/s87vPk9+vH/fvO+zt9imu+FN/n01zzSdf1ce/zRbaGe+rSyyesPc3nbIKNl4wmN+Ms+Vj3aM11nee9Ps3vupfudV7zKkX2RnH8rO/zabhM3+mn4hJ9nz8Wm/o+X6Ronqb08qS1pyrh3ACbLBl9WUReAe6KyBtn+GU77T067rrO814/9e8q/9Gm1Labk3/I53WfT8Nl+k4/NZfk+3xaNvp9vtBA0PPEupJRHadlvQV8g5MrsM6Fy3pdT8lvAX+nPHDF/55LyfP8fb5IS/M0pZcnrf24JZwfl42VjJYJ/xoLBM7K/TrVNZ9wXed5rz/u73pljbt4Xvf5NFym7/TH4SK/z6di09/nixTN05RenrT21CWcPyY/dsloOvYKsfnyefDU1/yE6zrPe/1x7vMrxH8I02PndZ9Pw2X6Tp+KS/B9fmrO4vt8YaI5vbCTSi9PWvukz9k0p7nu9Pq4ktFvUriRed0luOZjr+s87/Vp73Ni3cb+ud3nJ3FZv9MncRm/z0/irL/PF1oR9LSll0+x9thzF3ndTyoZLRKFHwCfU9WvcEac8l4fe13nea9Pc83p2GvE9Jc3J59znvc55w++QQyS5E5gl/Y7/bTXfMm+z6e5zxv9PlsZpWEYximwEb6GYRinwETTMAzjFJhoGoZhnAITTcMwjFNgomkYhnEKTDQNwzBOgYmmYRjGKTDRNAzDOAUmmoZhGKfg/wd42HuSt7kxHgAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "u = prob.get_lagrange_multiplier(\"Pseudo-mechanism\")\n",
    "plot(-0.004 * u, mode=\"displacement\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
  },
  "kernelspec": {
   "display_name": "Python 3.8.10 64-bit",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
