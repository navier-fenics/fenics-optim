Wout = 50.;
Win = 17.5;

nx = 40;
ny = 20;

Point(1) = {-Wout/2, -Wout/2, 0, 1};
Point(2) = {Wout/2, -Wout/2, 0, 1};
Point(3) = {Wout/2, Wout/2, 0, 1};
Point(4) = {-Wout/2, Wout/2, 0, 1};

Point(11) = {-Win/2, -Win/2, 0, 1};
Point(12) = {Win/2, -Win/2, 0, 1};
Point(13) = {Win/2, Win/2, 0, 1};
Point(14) = {-Win/2, Win/2, 0, 1};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line(5) = {11, 12};
//+
Line(6) = {12, 13};
//+
Line(7) = {13, 14};
//+
Line(8) = {14, 11};
//+
Line(9) = {1, 11};
//+
Line(10) = {2, 12};
//+
Line(11) = {3, 13};
//+
Line(12) = {4, 14};
//+
Curve Loop(1) = {1, 10, -5, -9};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {10, 6, -11, -2};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {3, 12, -7, -11};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {12, 8, -9, -4};
//+
Plane Surface(4) = {4};
//+
Transfinite Curve {1, 5, 6, 2, 7, 3, 4, 8} = nx+1 Using Progression 1;
//+
Transfinite Curve {9, 10, 11, 12} = ny+1 Using Progression 1;
//+
Transfinite Surface {1,2,3,4} Alternated;
//+
//+
Physical Curve(1) = {1, 2, 3, 4};
//+
Physical Curve(2) = {5, 6, 7, 8};

Physical Surface(1) = {1,2,3,4};