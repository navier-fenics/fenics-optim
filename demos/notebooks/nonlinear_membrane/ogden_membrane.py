#!/usr/bin/env python
# coding: utf-8
"""Implementation of nonlinear membrane behaviours."""
from ufl import as_vector, Identity, shape
from fenics_optim import (
    ConvexFunction,
    Pow,
    Quad,
    SDP,
    to_mat,
    to_vect,
    block_matrix,
)


class OgdenMembrane(ConvexFunction):
    """
    Ogden model of a thin membrane.

    The 2D potential is given by:

       :math:`\\psi(\\lambda_1,\\lambda_2) = \\frac{2\\mu}{\\alpha^2}\\left(
           \\lambda_1^\\alpha + \\lambda_2^\\alpha +
           \\lambda_1^{-\\alpha}\\lambda_2^{-\\alpha}
       \right)`

    Parameters
    ----------
    expr : UFL expression
        displacement gradient
    mu : float
        shear modulus
    alpha : float
        Ogden exponent, should be > 2
    metric : matrix, optional
        metric tensor for embedded membranes in 3D
    """

    def __init__(self, expr, mu, alpha, metric=None, **kwargs):
        ConvexFunction.__init__(self, expr, parameters=(mu, alpha, metric), **kwargs)

    def conic_repr(self, X, mu, alpha, metric):
        assert float(alpha) > 2, "Exponent should be larger than 2 for convexity."
        beta = alpha / 2
        t = self.add_var(name="t")
        s = self.add_var(dim=3, cone=Quad(3), name="s")
        w = self.add_var(dim=3, cone=Quad(3))
        x = self.add_var(dim=3, cone=Pow(3, 1 / beta))
        y = self.add_var(dim=3, cone=Pow(3, 1 / beta))
        z = self.add_var(dim=3, cone=Pow(3, 1 / (1 + 2 * beta)))

        self.add_eq_constraint(x[1], b=1)
        self.add_eq_constraint(y[1], b=1)
        self.add_eq_constraint(z[2], b=1)
        self.add_eq_constraint(x[2] - t - s[0])
        self.add_eq_constraint(y[2] - t + s[0])
        self.add_eq_constraint(w[0] - t)
        self.add_eq_constraint(w[2] - s[0])
        self.add_eq_constraint(w[1] - z[1])

        Cel = to_mat(as_vector([t + s[1], t - s[1], s[2]]))
        self.set_linear_term(2 * mu / alpha ** 2 * (x[0] + y[0] + z[0]))

        if metric is None:
            d1, d2 = 2, 2
            metric = Identity(2)
        else:
            d2, d1 = shape(metric)
        dtot = d1 + d2
        Z = to_mat(self.add_var(dim=dtot * (dtot + 1) // 2, cone=SDP(dtot)))
        Id2 = Identity(d2)
        Id1 = Identity(d1)
        A = Z + block_matrix([[-Cel, X.T], [X, 0 * Id2]])
        B = block_matrix([[0 * Id1, -metric.T], [-metric, Id2]])
        self.add_eq_constraint(to_vect(A), b=to_vect(B), name="Stress")
