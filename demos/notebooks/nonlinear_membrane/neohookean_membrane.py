#!/usr/bin/env python
# coding: utf-8
"""Implementation of nonlinear membrane behaviours."""
from ufl import as_vector, Identity, shape
from fenics_optim import (
    ConvexFunction,
    Pow,
    Quad,
    SDP,
    to_mat,
    to_vect,
    block_matrix,
)


class NeoHookeanMembrane(ConvexFunction):
    """
    Neo-Hookean model of a thin membrane.

    This should recover the special case of a Ogden material with :math:`\alpha=2`.
    The present implementation benefits from some simplifications in this specific case.

    The 2D potential is given by:

       :math:`\\psi(\\lambda_1,\\lambda_2) = \\frac{\\mu}{2}\\left(
           \\lambda_1^2 + \\lambda_2^2 +
           \\lambda_1^{-2}\\lambda_2^{-2}
       \right)`

    Parameters
    ----------
    expr : UFL expression
        displacement gradient
    mu : float
        shear modulus
    metric : matrix, optional
        metric tensor for embedded membranes in 3D
    """

    def __init__(self, expr, mu, metric=None, **kwargs):
        ConvexFunction.__init__(self, expr, parameters=(mu, metric), **kwargs)

    def conic_repr(self, X, mu, metric):
        t = self.add_var(name="t")
        s = self.add_var(dim=3, cone=Quad(3), name="s")
        w = self.add_var(dim=3, cone=Quad(3))
        z = self.add_var(dim=3, cone=Pow(3, 1 / 3))

        self.add_eq_constraint(z[2], b=1)
        self.add_eq_constraint(w[0] - t)
        self.add_eq_constraint(w[2] - s[0])
        self.add_eq_constraint(w[1] - z[1])

        Cel = to_mat(as_vector([t + s[1], t - s[1], s[2]]))
        self.set_linear_term(mu / 2 * (2 * t + z[0]))

        if metric is None:
            d1, d2 = 2, 2
            metric = Identity(2)
        else:
            d2, d1 = shape(metric)
        dtot = d1 + d2
        Z = to_mat(self.add_var(dim=dtot * (dtot + 1) // 2, cone=SDP(dtot)))
        Id2 = Identity(d2)
        Id1 = Identity(d1)
        A = Z + block_matrix([[-Cel, X.T], [X, 0 * Id2]])
        B = block_matrix([[0 * Id1, -metric.T], [-metric, Id2]])
        self.add_eq_constraint(to_vect(A), b=to_vect(B), name="Stress")
