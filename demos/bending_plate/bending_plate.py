#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis of a thin plate in bending.

Square plate with simple or clamped supports with a von Mises criterion
(bending strength m) and uniformly distributed loading.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    UnitSquareMesh,
    MeshFunction,
    FacetNormal,
    FunctionSpace,
    DirichletBC,
    Measure,
    AutoSubDomain,
    dx,
    dS,
    plot,
)
from ufl import sqrt, sym, grad, dot, as_matrix, as_vector, jump
from fenics_optim import MosekProblem, L2Norm, AbsValue
import matplotlib.pyplot as plt

# Plate bending strength
m = Constant(1.0)
# Plate uniform load
load = Constant(-1.0)

# Support type:
# - "ss": simple supports
# - "clamped": clamped supports
supports = "ss"

N = 50
mesh = UnitSquareMesh(N, N, "crossed")

facets = MeshFunction("size_t", mesh, 1)
facets.set_all(0)
n = FacetNormal(mesh)


# Define boundary conditions
def boundary(x, on_boundary):  # noqa
    return on_boundary


AutoSubDomain(boundary).mark(facets, 1)
ds = Measure("ds", subdomain_data=facets)

prob = MosekProblem("Bending plate limit analysis")

V = FunctionSpace(mesh, "CG", 2)
bc = DirichletBC(V, Constant(0.0), boundary)
u = prob.add_var(V, bc=bc)

R = FunctionSpace(mesh, "R", 0)


def Pext(lamb):
    """Power of external loads."""
    return lamb * dot(load, u) * dx


prob.add_eq_constraint(R, A=Pext, b=1)

J = as_matrix([[2.0, 1.0, 0.0], [0, sqrt(3.0), 0.0], [0, 0, 1]])


def Chi(v):
    """Curvature tensor."""
    chi = sym(grad(grad(v)))
    return as_vector([chi[0, 0], chi[1, 1], 2 * chi[0, 1]])


t = m / sqrt(3) * dot(J, Chi(u))
pi_c = L2Norm(t, quadrature_scheme="vertex", degree=1)
prob.add_convex_term(pi_c)

if supports == "ss":
    pi_h = AbsValue([jump(grad(u), n)], k=2 / sqrt(3) * m, on_facet=True)
#    pi_h.set_term(, k=2/sqrt(3)*m)
elif supports == "clamped":
    pi_h = AbsValue(
        [jump(grad(u), n), dot(grad(u), n)],
        k=2 / sqrt(3) * m,
        measure=[dS, ds(1)],
        on_facet=True,
    )
else:
    raise (NotImplementedError)
prob.add_convex_term(pi_h)

prob.optimize()

p = plot(u)
plt.colorbar(p)
plt.show()

p = plot(dot(dot(J, Chi(u)), dot(J, Chi(u))) ** 0.5, cmap="Blues")
plt.colorbar(p)
plt.show()
