#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Convex optimization formulation of the Cheeger problem.

The Cheeger problem consists in finding the subset of a domain
:math:`Omega` which minimizes the ratio of perimeter over area.

Geometry can be either a unit square or a star-shaped polynom.
Various discretization are available (DG0, DG1, CG1, CG2,...).
The default norm for the classical Cheeger problem is the L2-norm of
the gradient, other anisotropic norms like L1 or Linf are available.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    UnitSquareMesh,
    sqrt,
    dot,
    grad,
    dx,
    FacetNormal,
    jump,
    plot,
    XDMFFile,
    Point,
    DirichletBC,
    FunctionSpace,
    Constant,
)
from fenics_optim import MosekProblem, L2Norm, L1Norm, LinfNorm
from mshr import Polygon, generate_mesh
import matplotlib.pyplot as plt


def Cheeger_pb(
    N: int, interp: str, degree: int, geometry: str = "square", norm_type: str = "l2"
):
    """Cheeger problem.

    Parameters
    ----------
    N : int
        mesh resolution
    interp : str
        interpolation type
    degree : int
        interpolation degree
    geometry : {"square", "star"}
        domain geometry
    norm_type : {"l2", "l1", "linf"}
        norm used in the Cheeger problem objective
    """
    # Define geometry
    if geometry == "square":
        mesh = UnitSquareMesh(N, N, "crossed")
    elif geometry == "star":
        domain = Polygon(
            (
                Point(-1, -1),
                Point(0, -0.5),
                Point(1, -1),
                Point(0.5, 0.0),
                Point(1, 1),
                Point(0, 0.5),
                Point(-1, 1),
                Point(-0.5, 0.0),
            )
        )
        mesh = generate_mesh(domain, N)
    else:
        raise (NotImplementedError)

    V = FunctionSpace(mesh, interp, degree)

    def border(x, on_boundary):
        return on_boundary

    bc = DirichletBC(V, Constant(0), border)

    # Define variational problem
    prob = MosekProblem("Cheeger problem")
    u = prob.add_var(V, bc=bc)

    # Choose norm type using predefined functions
    if norm_type == "l2":
        Norm_type = L2Norm

        def norm(n):
            return sqrt(dot(n, n))

    elif norm_type == "l1":
        Norm_type = L1Norm

        def norm(n):
            return abs(n[0]) + abs(n[1])

    elif norm_type == "linf":
        Norm_type = LinfNorm
        # Max ufl operator is not working anymore with quadrature representation
        p = 100

        def norm(n):
            return (abs(n[0]) ** p + abs(n[1]) ** p) ** (1 / p)

    else:
        raise (NotImplementedError)

    # Choose quadrature scheme depending on chosen interpolation degree
    if degree == 1:
        F = Norm_type(grad(u))
    elif degree == 2:
        F = Norm_type(grad(u), quadrature_scheme="vertex")
    else:
        F = Norm_type(grad(u), degree=degree)

    # Adds gradient term only if not DG0
    if degree > 0:
        prob.add_convex_term(F)

    # Adds normalization constraint (f, u)=1
    f = Constant(1.0)
    R = FunctionSpace(mesh, "Real", 0)

    def constraint(lamb):
        return lamb * f * u * dx

    prob.add_eq_constraint(R, A=constraint, b=1)

    # Adds jumps terms for DG interpolation
    # Note that for the isotropic l2-norm, jump terms are just |[u]|
    # but for anisotropic l1 or linf norms, one must account for normal orientation
    # |[u]|.||n||_p
    if interp == "DG":
        n = FacetNormal(mesh)
        G = Norm_type(
            [jump(u) * norm(n)("-"), u * norm(n)], degree=degree + 1, on_facet=True
        )
        prob.add_convex_term(G)

    prob.parameters["presolve"] = True
    prob.optimize()

    plot(u)
    plt.show()

    with XDMFFile("results/Cheeger_pb_{}_{}.xdmf".format(interp, degree)) as ffile:
        ffile.write_checkpoint(u, "disp", 0)
    return prob.pobj


Cheeger_pb(50, "DG", degree=2, geometry="square", norm_type="l2")
