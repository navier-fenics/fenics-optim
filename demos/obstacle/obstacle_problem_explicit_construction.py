#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Obstacle problem solved with explicit construction of auxiliary variables.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, grad, as_vector
from dolfin import (
    UnitSquareMesh,
    VectorElement,
    FunctionSpace,
    Function,
    DirichletBC,
    Constant,
    Expression,
    Measure,
    dx,
    plot,
)
from fenics_optim import MosekProblem, RQuad
import numpy as np
import matplotlib.pyplot as plt

N = 100
mesh = UnitSquareMesh(N, N, "crossed")
V = FunctionSpace(mesh, "CG", 1)


bc = DirichletBC(V, Constant(0), "on_boundary")

load = Constant(-5)
hmean = -0.1
ampl = 0.01
k1, k2 = 2, 8
obstacle = Function(V)
obstacle.interpolate(
    Expression(
        "h+a*sin(2*pi*k1*x[0])*cos(2*pi*k1*x[1])*sin(2*pi*k2*x[0])*cos(2*pi*k2*x[1])",
        h=hmean,
        a=ampl,
        k1=k1,
        k2=k2,
        degree=2,
    )
)

prob = MosekProblem("Obstacle problem")
u = prob.add_var(V, bc=bc, lx=obstacle)


def quad_element(degree=0, dim=1):
    return VectorElement(
        "Quadrature", mesh.ufl_cell(), degree=degree, dim=dim, quad_scheme="default"
    )


V2 = FunctionSpace(mesh, quad_element(degree=0, dim=4))
W = FunctionSpace(mesh, quad_element(degree=0, dim=3))
y = prob.add_var(V2, cone=RQuad(4))

dxq = Measure("dx", metadata={"quadrature_scheme": "default", "quadrature_degree": 0})


def constraint(z):
    g = grad(u)
    a21 = dot(z, as_vector([0, g[0], g[1]])) * dxq
    a22 = -dot(z, as_vector([y[1], y[2], y[3]])) * dxq
    return a21 + a22


def rhs(z):
    return -z[0] * dxq


prob.add_eq_constraint(W, A=constraint, b=rhs)
prob.add_obj_func(-dot(load, u) * dx + y[0] * dxq)

prob.parameters["presolve"] = True
prob.optimize()

x = np.linspace(0, 1, 200)
y = 0.5
U = [u(xi, y) for xi in x]
plt.figure(1)
plt.plot(
    x,
    hmean
    + ampl
    * np.sin(2 * np.pi * k1 * x)
    * np.sin(2 * np.pi * k2 * x)
    * np.cos(2 * np.pi * k1 * y)
    * np.cos(2 * np.pi * k2 * y),
    linewidth=2,
    label="obstacle",
)
plt.plot(x, U, linewidth=1.0, label="membrane")
plt.legend()
plt.xlabel("$x$ coordinate")
plt.ylabel("Displacement")
plt.show()

plot(u)
plt.show()
