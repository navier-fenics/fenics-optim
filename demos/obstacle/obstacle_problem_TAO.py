#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Obstacle problem solved with PETSc's TAO bound-constrained solver.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, grad, dx, derivative
from dolfin import (
    UnitSquareMesh,
    FunctionSpace,
    DirichletBC,
    Constant,
    Function,
    Expression,
    OptimisationProblem,
    assemble,
    PETScTAOSolver,
    TestFunction,
    TrialFunction,
    plot,
)
from time import time

N = 100
mesh = UnitSquareMesh(N, N, "crossed")
V = FunctionSpace(mesh, "CG", 1)


bc = DirichletBC(V, Constant(0), "on_boundary")

load = Constant(-5)
hmean = -0.1
ampl = 0.01
k1, k2 = 2, 8
obstacle = Function(V)
obstacle.interpolate(
    Expression(
        "h+a*sin(2*pi*k1*x[0])*cos(2*pi*k1*x[1])*sin(2*pi*k2*x[0])*cos(2*pi*k2*x[1])",
        h=hmean,
        a=ampl,
        k1=k1,
        k2=k2,
        degree=2,
    )
)

z = Function(V)
u = Function(V)
energy = 0.5 * dot(grad(u), grad(u)) * dx - dot(load, u) * dx
u_ = TestFunction(V)
du = TrialFunction(V)
F = derivative(energy, u, u_)
J = derivative(F, u, du)


class Obstacle(OptimisationProblem):
    def __init__(self, total_energy, Dalpha_total_energy, J_alpha, alpha):
        OptimisationProblem.__init__(self)
        self.total_energy = total_energy
        self.Dalpha_total_energy = Dalpha_total_energy
        self.J_alpha = J_alpha
        self.alpha = alpha

    def f(self, x):
        self.alpha.vector()[:] = x
        return assemble(self.total_energy)

    def F(self, b, x):
        self.alpha.vector()[:] = x
        assemble(self.Dalpha_total_energy, b)

    def J(self, A, x):
        self.alpha.vector()[:] = x
        assemble(self.J_alpha, A)


solver = PETScTAOSolver()
solver.parameters["method"] = "tron"
solver.parameters["line_search"] = "gpcg"
solver.parameters["linear_solver"] = "cg"
solver.parameters["preconditioner"] = "ilu"
solver.parameters["monitor_convergence"] = True
solver.parameters["maximum_iterations"] = 1000
solver.parameters["report"] = False

bc.apply(obstacle.vector())


tic = time()
solver.solve(Obstacle(energy, F, J, u), u.vector(), obstacle.vector(), z.vector())
print("Solving time", time() - tic)

plot(u)
