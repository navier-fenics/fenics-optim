from dolfin import (
    dot,
    BoxMesh,
    FunctionSpace,
    VectorFunctionSpace,
    assemble,
    Point,
    dx,
    Constant,
)
from fenics_optim import MosekProblem, Exp, ConvexFunction

import numpy as np

L = 10.0
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(L, L, L), 2, 2, 2)
vol = assemble(Constant(1) * dx(domain=mesh))

V = FunctionSpace(mesh, "CG", 1)
R = FunctionSpace(mesh, "Real", 0)
R3 = VectorFunctionSpace(mesh, "Real", 0, dim=3)

x_list = np.linspace(0.5, 3, 10)
res = []
for x in x_list:

    prob = MosekProblem()

    # create
    # 3 scalars to express -log(y) in conic form and impose y  = int(u)*dx
    # + the u field which will be imposed to u=cte=x for the test
    y, u = prob.add_var([R3, V], cone=[Exp(3), None])

    # min -Log(y[0])  <=>  min -y[2] s.t. y = (y[0], y[1], y[2]) \in Exp(3) and y[1] == 1
    # second constraint y[1] = 1 (constraint is scalar, function space for the Lagrange multiplier mu is therefore "R")
    prob.add_eq_constraint(R, A=lambda mu: [mu * y[1] * dx], b=1)
    # add objective -y[2]
    prob.add_obj_func(-y[2] * dx)

    # constraint y[0] = int(u)*dx (constraint is scalar, function space for the Lagrange multiplier mu is therefore "R")
    prob.add_eq_constraint(R, A=lambda mu: [mu * y[0] * dx, -mu * u * dx])

    # u=cte=x
    prob.add_eq_constraint(
        V, A=lambda v: [None, v * u * dx], b=lambda v: v * Constant(x) * dx
    )

    prob.optimize()

    res.append(prob.pobj)

import matplotlib.pyplot as plt

# we check that the optimal solution is -log(vol*x)
plt.plot(x_list, -np.log(vol * x_list))
plt.plot(x_list, res, "o")
plt.show()