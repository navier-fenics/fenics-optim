#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
MBB example.

Supplementary file to the paper:

    Mourad L., Bleyer J., Mesnil R., Nseir J., Sab K., Raphael W.,
    Topoology optimization of load-bearing capacity,
    submitted to Structural and Multidisciplinary Optimization, 2020

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    Point,
    near,
    AutoSubDomain,
    MeshFunction,
    Measure,
    DirichletBC,
    dot,
)
from fenics_optim.topology_optimization import (
    BimaterialVolumeMinimization,
)
import fenics_optim.limit_analysis as la
from mshr import Rectangle, generate_mesh

# loading
f = Constant((0.0, -20))
# compressive strength
fc = 40.0
# tensile strength
ft = 40.0
# volume fraction
frac = 0.2

L = 7.5
H = 4.75
s = 0.5

Lo = 1.5
Ho = 1.5
Lf = 4.75
N = 50
domain = Rectangle(Point(0.0, 0.0), Point(L, H)) - Rectangle(
    Point(s, s), Point(s + Lo, s + Ho)
)
mesh = generate_mesh(domain, N)


def gauche(x, on_boundary):  # noqa
    return near(x[1], 0) and x[0] <= 0.5


def top(x, on_boundary):  # noqa
    return (Lf - s / 2 <= x[0] <= Lf + s / 2) and near(x[1], H)


def droite(x, on_boundary):  # noqa
    return near(x[1], 0) and L - s <= x[0] <= L


def point(x, on_boundary):  # noqa
    return near(x[0], 0) and near(x[1], 0)


Bottom = AutoSubDomain(gauche)
Top = AutoSubDomain(top)
facets = MeshFunction("size_t", mesh, 1)
Bottom.mark(facets, 1)
Top.mark(facets, 2)
ds = Measure("ds", subdomain_data=facets)


alp = 0.99

materials = [
    la.L1Rankine2D(alp * fc, (1 - alp) * ft),
    la.L1Rankine2D((1 - alp) * fc, alp * ft),
    # la.OrthotropicL1Rankine2D(fcx, ftx, fcy, fty, f_tau),
]


prob = BimaterialVolumeMinimization(mesh, materials)

prob.parameters.update({"alpha": alp})

bc0 = DirichletBC(prob.V.sub(1), Constant(0.0), gauche, method="pointwise")
bc1 = DirichletBC(prob.V.sub(1), Constant(0), droite, method="pointwise")
bc2 = DirichletBC(prob.V.sub(0), Constant(0), point, method="pointwise")
prob.bc = [bc0, bc1, bc2]
prob.Pext = lambda u: dot(f, u) * ds(2)

prob.cost = [0.5, 0.5]

prob.solve_problem(
    penalization=True,
    output="deep_beam/fc_{}_ft_{}_alp_{}".format(fc, ft, alp),
    pmax=2,
    nitermax=20,
    length_control=0.08,
    plots=False,
)
