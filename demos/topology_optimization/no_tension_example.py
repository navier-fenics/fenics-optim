#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
MBB example.

Supplementary file to the paper:

    Mourad L., Bleyer J., Mesnil R., Nseir J., Sab K., Raphael W.,
    Topoology optimization of load-bearing capacity,
    submitted to Structural and Multidisciplinary Optimization, 2020

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    Point,
    near,
    AutoSubDomain,
    MeshFunction,
    Measure,
    DirichletBC,
    dot,
    plot,
)
import fenics_optim.limit_analysis as la
from fenics_optim.topology_optimization import LoadMaximization
from mshr import Rectangle, generate_mesh
import matplotlib.pyplot as plt

# loading
f = Constant((0.0, -1.0))

# compressive strength
fc = 1.0
# tensile strength
ft = 0.2
# volume fraction
frac = 0.2
# Geometry
N = 50
offset = 0.0
domain = (
    Rectangle(Point(0.0, 0.0), Point(5, 2.5))
    - Rectangle(Point(1.8 - offset, 0.0), Point(3.2 + offset, 1.0))
    - Rectangle(Point(0.0, 1.5), Point(1.8, 2.5))
    - Rectangle(Point(3.2, 1.5), Point(5, 2.5))
)
mesh = generate_mesh(domain, N)
plot(mesh)
plt.show()


# Define boundary conditions
def left(x, on_boundary):  # noqa
    return near(x[0], 0.0) and on_boundary


def right(x, on_boundary):  # noqa
    return near(x[0], 5) and on_boundary


def bottom(x, on_boundary):  # noqa
    return near(x[1], 0.0) and on_boundary


def top(x, on_boundary):  # noqa
    return near(x[1], 2.5) and on_boundary


Top = AutoSubDomain(top)
facets = MeshFunction("size_t", mesh, 1)
Top.mark(facets, 1)
ds = Measure("ds", subdomain_data=facets)

frac = 0.2
mat = la.L1Rankine2D(fc, ft)
problem = LoadMaximization(mesh, frac, mat)
problem.Pext = lambda u: dot(f, u) * ds(1)
problem.bc = [
    DirichletBC(problem.V, Constant((0.0, 0.0)), left),
    DirichletBC(problem.V, Constant((0.0, 0.0)), bottom),
    DirichletBC(problem.V, Constant((0.0, 0.0)), right),
]


problem.solve_problem(
    penalization=False,
    output="notension",
    length_control=0.2,
    plots=True,
)
