#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
MBB example.

Supplementary file to the paper:

    Mourad L., Bleyer J., Mesnil R., Nseir J., Sab K., Raphael W.,
    Topoology optimization of load-bearing capacity,
    submitted to Structural and Multidisciplinary Optimization, 2020

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Constant,
    Point,
    near,
    AutoSubDomain,
    MeshFunction,
    Measure,
    DirichletBC,
    dot,
    as_vector,
)
from fenics_optim.topology_optimization import (
    ReinforcementLoadMaximization,
)
from ufl import pi
import fenics_optim.limit_analysis as la
from mshr import Rectangle, generate_mesh

# loading
f = Constant((0.0, -0.5))
# compressive strength
fc = 1.0
# tensile strength
ft = 5
# volume fraction
frac = 0.2

# Geometry
L = 36
H = 6
s = 0.5
# Mesh size (paper N=150)
N = 50
domain = Rectangle(Point(0.0, 0.0), Point(L / 2, H))
mesh = generate_mesh(domain, N)


def support(x, on_boundary):
    return near(x[1], 0) and x[0] <= s


def top(x, on_boundary):
    return L / 2 - s <= x[0] <= L / 2 and near(x[1], H)


def sym_plane(x, on_boundary):
    return 0 <= x[1] <= H and near(x[0], L / 2)


Top = AutoSubDomain(top)
facets = MeshFunction("size_t", mesh, 1)
Top.mark(facets, 1)
ds = Measure("ds", subdomain_data=facets)


mat = la.MultiaxialConvHull(ft, ft, angles=[0, pi / 4, -pi / 4])
materials = [
    la.Rankine2D(fc, 0.05 * fc),
    mat,
]


prob = ReinforcementLoadMaximization(mesh, frac, materials, additive=True)


bc0 = DirichletBC(prob.V.sub(1), Constant(0.0), support, method="pointwise")
bc1 = DirichletBC(prob.V.sub(0), Constant(0.0), sym_plane, method="pointwise")
prob.bc = [bc0, bc1]
prob.Pext = lambda u: dot(f, u) * ds(1)

prob.cost = [0.5, 0.5]

prob.solve_problem(
    penalization=True,
    output="MBB_bimaterial/reinforcement_fc_{}_ft_{}".format(fc, ft),
    pmax=2,
    nitermax=20,
    length_control=0.5,
    plots=False,
)
