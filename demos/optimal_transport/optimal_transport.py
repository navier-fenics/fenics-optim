#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Optimal transport problem using space-time finite element discretization.

Formulation follows (Benamou and Brenier, 2000) dynamic formulation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    BoxMesh,
    Point,
    near,
    Constant,
    Expression,
    VectorFunctionSpace,
    DirichletBC,
    XDMFFile,
)
from ufl import as_vector
from fenics_optim import MosekProblem, InequalityConstraint, ConvexFunction, RQuad

Nx = 10
NT = 10
mesh = BoxMesh(Point(0, 0, 0), Point(1, 1, 1), Nx, Nx, NT)

V = VectorFunctionSpace(mesh, "CG", 2, dim=3)

rho0 = Expression("exp(-(pow(x[0]-0.5,2)+pow(x[1]-0.5,2))/0.04)", degree=2)
rho1 = Expression(
    "exp(-(pow(x[0]-0.8,2)+pow(x[1]-0.8,2))/0.01) + \
                   exp(-(pow(x[0]-0.2,2)+pow(x[1]-0.2,2))/0.01) + \
                   exp(-(pow(x[0]-0.8,2)+pow(x[1]-0.2,2))/0.01) + \
                   exp(-(pow(x[0]-0.2,2)+pow(x[1]-0.8,2))/0.01)",
    degree=2,
)


def tini(x, on_boundary):
    """Surface at initial time."""
    return near(x[2], 0)


def tfin(x, on_boundary):
    """Surface at final time."""
    return near(x[2], 1)


def border_x(x, on_boundary):
    """Surface of normal ex."""
    return near(x[0], 0) or near(x[0], 1)


def border_y(x, on_boundary):
    """Surface of normal ey."""
    return near(x[1], 0) or near(x[1], 1)


bc = [
    DirichletBC(V.sub(0), rho0, tini),
    DirichletBC(V.sub(0), rho1, tfin),
    DirichletBC(V.sub(1), Constant(0), border_x),
    DirichletBC(V.sub(2), Constant(0), border_y),
]

prob = MosekProblem("Optimal transport")
u = prob.add_var(V, bc=bc)

rho, mx, my = u[0], u[1], u[2]
eps = 1e-3
conserv = InequalityConstraint(
    rho.dx(2) + mx.dx(0) + my.dx(1), bl=-eps, bu=eps, degree=2
)
prob.add_convex_term(conserv)


class CostFunction(ConvexFunction):
    """Optimal transport cost function :math:`\\|m\\|_2^2/2/\\rho`."""

    def conic_repr(self, X):  # noqa
        Y = self.add_var(dim=4, cone=RQuad(4))
        Ybar = as_vector([Y[i] for i in range(1, 4)])
        self.add_eq_constraint(X - Ybar)
        self.set_linear_term(Y[0])


c = CostFunction(u, degree=2)
prob.add_convex_term(c)

prob.optimize()

ffile = XDMFFile("optimal_transport.xdmf")
rho = u.sub(0, True)
rho.rename("rho", "rho")
ffile.write(rho, 0.0)
ffile.close()
