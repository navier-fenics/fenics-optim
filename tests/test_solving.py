#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test the resolution of a linear PDE using linear (in)equality constraints.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import grad, dot
from dolfin import (
    UnitSquareMesh,
    FunctionSpace,
    Constant,
    DirichletBC,
    DomainBoundary,
    dx,
)
from fenics_optim import MosekProblem

mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, "Lagrange", 1)
f = Constant(100.0)

bc = DirichletBC(V, 0.0, DomainBoundary())


def a(v, u):
    """Bilinear form."""
    return dot(grad(u), grad(v)) * dx + u * v * dx


def L(v):
    """Linear form."""
    return f * v * dx


def test_linear_equality():
    """Solve PDE with linear equality constraint."""
    prob = MosekProblem()
    u = prob.add_var(V, bc=bc)

    prob.add_eq_constraint(V, A=lambda v: [a(v, u)], b=L, bc=bc)

    prob.optimize()

    assert round(u.vector().norm("l2") - 14.9362601686, 10) == 0


def test_linear_inequality():
    """Solve PDE with linear inequality constraint."""
    prob = MosekProblem()
    u = prob.add_var(V, bc=bc)

    prob.add_ineq_constraint(V, A=lambda v: [a(v, u)], bu=L, bl=L, bc=bc)

    prob.optimize()

    assert round(u.vector().norm("l2") - 14.9362601686, 10) == 0
