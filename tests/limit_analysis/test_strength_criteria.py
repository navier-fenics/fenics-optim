#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test limit analysis strength criteria against analytical expression.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    UnitIntervalMesh,
    VectorFunctionSpace,
    FunctionSpace,
    Constant,
    dx,
    inner,
)
from fenics_optim import MosekProblem, to_mat
import fenics_optim.limit_analysis as la
import numpy as np
import pytest


def _check_limit_load(mat, Sig, lamb):
    lamb_ex = 1.0 / mat.gauge_function(Sig)
    if np.isnan(lamb):  # handle the case of unbounded criterion in direction Sig
        return lamb_ex < 0  # criterion is usually bounded in the other direction
    else:
        return np.isclose(lamb, lamb_ex, rtol=1e-5, atol=1e-5)


def _check_criterion(mat, Sig):
    mesh = UnitIntervalMesh(1)

    V = FunctionSpace(mesh, "R", 0)

    prob = MosekProblem("criterion")
    lamb = prob.add_var(V)

    crit = mat.criterion(lamb * Constant(tuple(Sig)))
    prob.add_convex_term(crit)

    prob.add_obj_func([1, None])
    prob.parameters["log_level"] = 0
    prob.optimize(sense="max")

    return _check_limit_load(mat, Sig, prob.pobj)


def _check_support_function(mat, Sig):
    mesh = UnitIntervalMesh(1)

    V = VectorFunctionSpace(mesh, "R", 0, dim=len(Sig))
    R = FunctionSpace(mesh, "R", 0)

    prob = MosekProblem("criterion")
    Eps = prob.add_var(V)

    pi = mat.support_function(Eps)
    prob.add_convex_term(pi)
    prob.add_eq_constraint(
        R,
        lambda lamb: lamb * inner(to_mat(Constant(tuple(Sig))), to_mat(Eps)) * dx,
        b=1,
    )
    prob.parameters["log_level"] = 0
    prob.optimize()

    return _check_limit_load(mat, Sig, prob.pobj)


plane_strain_crit = [
    la.vonMises(1.0),
    la.DruckerPrager(10.0, np.pi / 2.9),
    la.MohrCoulomb2D(1.0, np.pi / 4.5),
    la.MohrCoulomb2D(1.0, np.pi / 4.5, 0.1),
    la.Rankine2D(1.0, 1.0),
    la.OrthotropicRankine2D(1.0, 0.5, 0.1, 0.2),
    la.L1Rankine2D(1.0, 1.0),
    la.L1Rankine2D(1.0, 0.0),
    la.Hosford_plane_stress(1.0, 1.1),
    la.Hosford_plane_stress(1.0, 6.0),
    la.Gurson(1.0, 0.5),
]
tridimensional_crit = [
    la.vonMises(1.0),
    la.DruckerPrager(10.0, np.pi / 2.9),
    la.MohrCoulomb3D(1.0, 0 * np.pi / 5.1),
    la.MohrCoulomb3D(1.0, np.pi / 5.1),
    la.MohrCoulomb3D(10.0, np.pi / 5.1, ft=0.0),
    la.Rankine3D(1.0, 10.0),
    la.Rankine3D(30.0, 0.0),
    la.Gurson(10.0, 0.3),
]


@pytest.mark.parametrize("mat", plane_strain_crit)
def test_plane_strain(mat, N=20):
    """Test in 2D plane strain conditions."""
    Sig = 2 * np.random.rand(N, 3) - 1
    for sig in Sig:
        assert _check_criterion(mat, sig)
        if hasattr(mat, "support_function"):
            assert _check_support_function(mat, sig)


@pytest.mark.parametrize("mat", tridimensional_crit)
def test_tridimensional(mat, N=20):
    """Test in 3 dimensions."""
    Sig = 2 * np.random.rand(N, 6) - 1
    for sig in Sig:
        assert _check_criterion(mat, sig)
        if hasattr(mat, "support_function"):
            assert _check_support_function(mat, sig)


approx_levels = [(3, 0), (6, 0), (9, 0)]
types = ["inner", "outer"]


@pytest.mark.parametrize("approx_level", approx_levels)
@pytest.mark.parametrize("type", types)
def test_sdp_approximations(approx_level, type):
    """SDP approximations are exact in the 2D planes."""
    Sig = 2 * np.random.rand(3, 6) - 1
    Sig[0, [2, 4, 5]] = 0.0  # x-y plane stress
    Sig[1, [1, 3, 4]] = 0.0  # x-z plane stress
    Sig[2, [0, 3, 5]] = 0.0  # y-z plane stress
    mat = la.MohrCoulomb3D_approx(1.0, np.pi / 6, approx_level=approx_level, type=type)
    for sig in Sig:
        assert _check_criterion(mat, sig)
    mat = la.Rankine3D_approx(1.0, 0.5, approx_level=approx_level, type=type)
    for sig in Sig:
        assert _check_criterion(mat, sig)
