#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
2D Vertical slope stability test.

This test implements a limit analysis problem using a plane strain
Mohr-Coulomb criterion. Both lower bound and
upper bound discretization are performed.
Bounding status against an analytical solution is verified.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import grad, dot, tr, cos, sym, diag, as_matrix, div, avg, jump, as_vector, dev
from dolfin import (
    RectangleMesh,
    FacetNormal,
    MeshFunction,
    CompiledSubDomain,
    Measure,
    Point,
    DirichletBC,
    Constant,
    VectorFunctionSpace,
    FunctionSpace,
    dx,
)
from fenics_optim import MosekProblem, ConvexFunction, Quad
from math import pi, ceil, tan, sin
import pytest

N = 10
mesh = RectangleMesh(Point(0, 0), Point(1.2, 1), N, N, "crossed")

n = FacetNormal(mesh)


facets = MeshFunction("size_t", mesh, 1)
CompiledSubDomain("near(x[0], 0) || near(x[1], 0)").mark(facets, 1)
ds = Measure("ds", subdomain_data=facets, domain=mesh)
dS = Measure("dS", subdomain_data=facets, domain=mesh)
f = Constant((0, -1))


def lower_bound(phi):
    """Lower bound with piecewise affine stress."""
    Vsig = VectorFunctionSpace(mesh, "DG", 1, dim=3)

    R = FunctionSpace(mesh, "R", 0)
    prob = MosekProblem("lower bound")

    lamb, Sig = prob.add_var([R, Vsig])

    sig = as_matrix([[Sig[0], Sig[2]], [Sig[2], Sig[1]]])

    V_eq = VectorFunctionSpace(mesh, "DG", 0)
    V_jump = VectorFunctionSpace(mesh, "Discontinuous Lagrange Trace", 1)

    def equilibrium(u):
        return dot(u, div(sig) + lamb * f) * dx

    prob.add_eq_constraint(V_eq, A=equilibrium)

    def continuity(u):
        return dot(avg(u), dot(jump(sig), n("-"))) * dS + dot(u, dot(sig, n)) * ds(0)

    prob.add_eq_constraint(V_jump, A=continuity)

    prob.add_obj_func([1, None])

    class MohrCoulomb(ConvexFunction):
        def __init__(self, x, c=1, phi=0, **kwargs):
            ConvexFunction.__init__(self, x, parameters=(c, phi), **kwargs)

        def conic_repr(self, X, c, phi):
            d = self.dim_x
            Y = self.add_var(d, cone=Quad(d))
            Q = diag(as_vector([sin(phi), -1, -1]))
            self.add_eq_constraint(dot(Q, X) + Y, b=as_vector([c * cos(phi), 0, 0]))

    s = dev(sig)
    S_tilde = as_vector([tr(sig) / 2, s[0, 0], s[0, 1]])
    crit = MohrCoulomb(S_tilde, c=1, phi=phi, quadrature_scheme="vertex")
    prob.add_convex_term(crit)

    prob.parameters["log_level"] = 0
    prob.optimize(sense="max")

    return prob.pobj


def upper_bound(phi):
    """Upper bound with quadratic velocity."""
    deg_vel = 2
    V = VectorFunctionSpace(mesh, "CG", deg_vel)

    bc0 = DirichletBC(V, Constant((0.0, 0.0)), facets, 1)

    prob = MosekProblem("upper bound")

    u = prob.add_var(V, bc=bc0)

    R = FunctionSpace(mesh, "R", 0)

    def Pext(lamb):
        return lamb * dot(f, u) * dx

    prob.add_ineq_constraint(R, A=Pext, bl=1)

    class MohrCoulomb(ConvexFunction):
        def __init__(self, x, c=1, phi=0, **kwargs):
            ConvexFunction.__init__(self, x, parameters=(c, phi), **kwargs)

        def conic_repr(self, X, c, phi):
            d = self.dim_x
            Y = self.add_var(d, cone=Quad(d))
            Q = as_matrix([[1, 1, 0], [1, -1, 0], [0, 0, 1]])
            Q2 = diag(as_vector([sin(phi), 1, 1]))
            self.add_eq_constraint(dot(Q, X) - dot(Q2, Y))
            self.set_linear_term(c * cos(phi) * Y[0])

    def Eps(v):
        E = sym(grad(v))
        return as_vector([E[0, 0], E[1, 1], 2 * E[0, 1]])

    crit = MohrCoulomb(Eps(u), c=1, phi=phi, quadrature_scheme="vertex")
    prob.add_convex_term(crit)

    prob.parameters["log_level"] = 0
    prob.optimize(sense="min")
    return prob.pobj


@pytest.mark.parametrize("phi", [pi / 10, pi / 6, pi / 3])
def test_bounds(phi):
    """Check lower <= exact <= upper."""
    lb = lower_bound(phi)
    ub = upper_bound(phi)
    # a good approximation to the exact solution
    ex = 3.83 * tan(phi / 2 + pi / 4)
    assert lb < ex < ub, "Bounding theorems are not respected"
    print("phi = {} lb: {} | {} | {} : ub".format(ceil(phi * 180 / pi), lb, ex, ub))
