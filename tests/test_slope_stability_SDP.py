#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
3D vertical slope stability test.

This test implements a limit analysis problem using a 3D
Mohr-Coulomb criterion which has an SDP representation.
Mesh is coarse so only SDP implementation is checked and
not accuracy against a known solution.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import grad, dot, tr, cos, sym
from dolfin import (
    BoxMesh,
    Point,
    DirichletBC,
    Constant,
    VectorFunctionSpace,
    FunctionSpace,
    dx,
)
from fenics_optim import MosekProblem, ConvexFunction, SDP, to_mat, to_vect
from math import sin, pi

mesh = BoxMesh(Point(0, 0, 0), Point(1.2, 1.0, 1.0), 10, 1, 10)


f = Constant((0, 0, -1.0))

deg_vel = 2
V = VectorFunctionSpace(mesh, "CG", deg_vel)

bc0 = [
    DirichletBC(V, Constant((0.0, 0.0, 0.0)), "near(x[0], 0) || near(x[2], 0)"),
    DirichletBC(V.sub(1), Constant(0.0), "near(x[1], 0) || near(x[1], 1.0)"),
]

prob = MosekProblem("upper bound")

u = prob.add_var(V, bc=bc0, name="Mechanism")

R = FunctionSpace(mesh, "R", 0)

prob.add_eq_constraint(R, A=lambda lamb: lamb * dot(f, u) * dx, b=1)


class MohrCoulomb(ConvexFunction):
    """Direct 3D Mohr-Coulomb criterion implementation."""

    def __init__(self, x, c=1, phi=0, **kwargs):
        ConvexFunction.__init__(self, x, parameters=(c, phi), **kwargs)

    def conic_repr(self, X, c, phi):  # noqa
        Yp = self.add_var(6, cone=SDP(3))
        Ym = self.add_var(6, cone=SDP(3))
        a = (1 - sin(phi)) / (1 + sin(phi))
        self.add_eq_constraint(X - Yp + Ym)
        self.add_eq_constraint(-a * tr(to_mat(Yp)) + tr(to_mat(Ym)))
        self.set_linear_term(2 * c * cos(phi) / (1 + sin(phi)) * tr(to_mat(Yp)))


phi = pi / 6.0
crit = MohrCoulomb(to_vect(sym(grad(u))), c=1, phi=phi, quadrature_scheme="vertex")
prob.add_convex_term(crit)

prob.optimize()

assert 9.0 < prob.pobj < 10.0
