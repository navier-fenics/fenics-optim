#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Cheeger set problem on the unit square.

This test checks the finite element implementation and upper bound status
against the known analytical solution.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import grad, jump
from dolfin import UnitSquareMesh, DirichletBC, Constant, FunctionSpace, dx, ds, dS
from fenics_optim import MosekProblem, L2Norm
from math import pi, sqrt


def Cheeger_pb(N, interp, degree):
    """Primal (upper bound) Cheeger problem."""
    mesh = UnitSquareMesh(N, N, "crossed")

    V = FunctionSpace(mesh, interp, degree)

    bc = DirichletBC(V, Constant(0), "on_boundary")

    prob = MosekProblem("test_Cheeger")
    u = prob.add_var(V, bc=bc)

    R = FunctionSpace(mesh, "R", 0)

    prob.add_eq_constraint(R, A=lambda lamb: lamb * u * dx, b=1)

    if degree > 0:
        if degree == 2:
            F = L2Norm(grad(u), quadrature_scheme="vertex")
        else:
            F = L2Norm(grad(u), degree=degree)

        prob.add_convex_term(F)

    if interp == "DG":
        Fj = L2Norm([u, jump(u)], degree=degree + 1, on_facet=True, measure=[ds, dS])
        prob.add_convex_term(Fj)

    prob.optimize()
    analytical = (4 - pi) / (2 - sqrt(pi))
    print(prob.pobj, analytical)
    assert 0 < (prob.pobj / analytical - 1) < 0.25


N_list = [10, 15, 20]


def test_Cheeger_CG1():
    """CG1 discretization."""
    for N in N_list:
        Cheeger_pb(N, "CG", 1)


def test_Cheeger_CG2():
    """CG2 discretization."""
    for N in N_list:
        Cheeger_pb(N, "CG", 2)


def test_Cheeger_DG0():
    """DG0 discretization."""
    for N in N_list:
        Cheeger_pb(N, "DG", 0)


def test_Cheeger_DG1():
    """DG1 discretization."""
    for N in N_list:
        Cheeger_pb(N, "DG", 1)
