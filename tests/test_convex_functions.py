#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Verify pre-implemented convex functions and associated transformations.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, as_vector, as_matrix
from dolfin import (
    UnitIntervalMesh,
    UnitSquareMesh,
    UnitCubeMesh,
    VectorFunctionSpace,
    FunctionSpace,
    Function,
    dx,
    Constant,
)
from fenics_optim import (
    MosekProblem,
    L2Ball,
    L1Ball,
    LinfBall,
    L2Norm,
    L1Norm,
    LinfNorm,
    QuadraticTerm,
    QuadOverLin,
    Epigraph,
    Perspective,
    InfConvolution,
    Sum,
    SumExpr,
    Marginal,
    Marginals,
    dummy_variable,
)
import pytest
import numpy as np


def my_norm(p):
    """p-norm of a vector."""
    return lambda x: np.linalg.norm(x, ord=p)


functions = [
    L2Norm,
    L1Norm,
    LinfNorm,
    QuadraticTerm,
    QuadOverLin,
    lambda x, **kwargs: L1Ball(x, k=10, **kwargs),
]
p = [2, 1, np.inf]
fun_eval = list(map(my_norm, p)) + [
    lambda x: np.dot(x, x) / 2,
    lambda x: np.dot(x[1:], x[1:]) / x[0],
    lambda x: 0,
]


@pytest.mark.parametrize("fun, value", zip(functions, fun_eval))
@pytest.mark.parametrize("dim", [2, 3])
def test_convex_function(fun, value, dim):
    """Test convex function in different dimension."""
    if dim == 1:
        mesh = UnitIntervalMesh(1)
    elif dim == 2:
        mesh = UnitSquareMesh(1, 1)
    elif dim == 3:
        mesh = UnitCubeMesh(1, 1, 1)

    x0 = np.random.rand(dim)
    if dim == 1:
        V = FunctionSpace(mesh, "CG", 1)
        u0 = Constant(x0[0])
    else:
        V = VectorFunctionSpace(mesh, "CG", 1)
        u0 = as_vector(x0)

    prob = MosekProblem()

    u = prob.add_var(V)
    f = fun(u, quadrature_scheme="vertex")
    prob.add_convex_term(f)
    prob.add_eq_constraint(V, A=lambda v: dot(u, v) * dx, b=lambda v: dot(u0, v) * dx)

    prob.optimize()
    assert np.isclose(prob.pobj, value(x0))


@pytest.mark.parametrize("dim", [1, 2, 3])
def test_quadratic_term(dim):
    """Test convex quadratic term."""
    if dim == 1:
        mesh = UnitIntervalMesh(1)
    elif dim == 2:
        mesh = UnitSquareMesh(1, 1)
    elif dim == 3:
        mesh = UnitCubeMesh(1, 1, 1)

    if dim == 1:
        V = FunctionSpace(mesh, "CG", 1)
        u0 = Constant(1)
    else:
        V = VectorFunctionSpace(mesh, "CG", 1)
        u0 = as_vector([1] * dim)

    prob = MosekProblem()

    u = prob.add_var(V)
    f = QuadraticTerm(u, x0=u0, quadrature_scheme="vertex")
    prob.add_convex_term(f)

    prob.optimize()
    assert np.isclose(prob.pobj, 0.0, atol=1e-6)


norms = [L2Ball, L1Ball, LinfBall, L2Norm, L1Norm, LinfNorm, QuadraticTerm]
value = [np.sqrt(2), 2, 1, np.sqrt(2), 2, 1, 2]


@pytest.mark.parametrize("fun, value", zip(norms, value))
def test_perspective(fun, value):
    """Test Persective of convex function."""
    dim = 2
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, "DG", 0)
    Vt = FunctionSpace(mesh, "DG", 0)

    prob = MosekProblem()

    u, t = prob.add_var([V, Vt], lx=[None, 0], name=["u", "t"])

    f = fun(u, degree=0)
    persp = Perspective(f, t)
    prob.add_convex_term(persp)

    u0 = as_vector([1] * dim)
    prob.add_eq_constraint(V, A=lambda v: dot(u, v) * dx, b=lambda v: dot(u0, v) * dx)
    prob.add_obj_func(t * dx)
    prob.optimize()
    assert np.isclose(prob.pobj, value)


norms = [L2Norm, L1Norm, LinfNorm]
value = [np.sqrt(2), 2, 1]
t0 = [0.36, 1.5, 0.5]


@pytest.mark.parametrize("fun, value, t0", zip(norms, value, t0))
def test_epigraph(fun, value, t0):
    """Test Epigraph of convex function."""
    dim = 2
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, "DG", 0)
    Vt = FunctionSpace(mesh, "DG", 0)

    prob = MosekProblem()

    u, t = prob.add_var([V, Vt], lx=[None, 0], name=["u", "t"])

    f = fun(u, degree=0)
    epi = Epigraph(f, t, t0=t0)
    prob.add_convex_term(epi)

    u0 = as_vector([1] * dim)
    prob.add_eq_constraint(V, A=lambda v: dot(u, v) * dx, b=lambda v: dot(u0, v) * dx)
    prob.add_obj_func(t * dx)
    prob.optimize()
    assert np.isclose(prob.pobj, value - t0)


norm1 = [L2Norm, L1Norm, LinfNorm]
norm2 = [L2Norm] * 3
value = [np.sqrt(2), np.sqrt(2), 1]


@pytest.mark.parametrize("fun1, fun2, value", zip(norm1, norm2, value))
def test_infconvolution(fun1, fun2, value):
    """Test Inf-Convolution between two convex functions."""
    dim = 2
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, "DG", 0)

    prob = MosekProblem()

    u = prob.add_var(V)

    f1 = fun1(u, degree=0)
    f2 = fun2(u, degree=0)
    infconv = InfConvolution(f1, f2)
    prob.add_convex_term(infconv)

    u0 = as_vector([1] * dim)
    prob.add_eq_constraint(V, A=lambda v: dot(u, v) * dx, b=lambda v: dot(u0, v) * dx)
    prob.optimize()
    assert np.isclose(prob.pobj, value)


value = [3 * np.sqrt(2), 2 + 2 * np.sqrt(2), 1 + 2 * np.sqrt(2)]


@pytest.mark.parametrize("fun1, fun2, value", zip(norm1, norm2, value))
def test_sum(fun1, fun2, value):
    """Test Sum between two convex functions."""
    dim = 2
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, "DG", 0)

    prob = MosekProblem()

    u = prob.add_var(V)

    f1 = fun1(u, degree=0)
    f2 = fun2(-Constant(2) * u, degree=0)
    prob.add_convex_term(Sum(f1, f2))

    u0 = as_vector([1] * dim)
    prob.add_eq_constraint(V, A=lambda v: dot(u, v) * dx, b=lambda v: dot(u0, v) * dx)
    prob.optimize()
    assert np.isclose(prob.pobj, value)


def test_least_squares():
    """
    Test a least square problem with non zero constant term.

        :math:`\\min_x \\frac{1}{2}\\|A x - x_0\\|_2`

    with solution :math:`x = (A^TA)^{-1}A^Tx_0`.
    """
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, "R", 0, dim=2)

    prob = MosekProblem()

    u = prob.add_var(V)

    A = np.array([[3.0, 1.0], [0.0, 2.0], [0.1, 0.0]])
    u0 = np.array([0.5, -1, 0.0])
    expr = SumExpr(dot(as_matrix(A), u), -as_vector(u0))
    prob.add_convex_term(QuadraticTerm(expr))

    value = np.dot(np.linalg.inv(np.dot(A.T, A)), np.dot(A.T, u0))

    prob.optimize()
    assert np.allclose(u.vector()[:], value)


def test_marginal():
    """Test Marginal of a convex functions."""
    mesh = UnitIntervalMesh(1)

    A = np.array([[3.0, 1.0, 0.0], [0.0, 2.0, 1.0]])
    u0 = np.array([1.0, -2.0])
    V = VectorFunctionSpace(mesh, "DG", 0, dim=2)
    uimp = Function(V)
    uimp.interpolate(Constant(tuple(u0)))

    prob = MosekProblem()
    u = prob.add_var(V, lx=uimp, ux=uimp)
    v = dummy_variable(3, mesh)
    f = Marginal(QuadraticTerm(v, degree=0), as_matrix(A), u)
    prob.add_convex_term(f)

    prob.optimize()

    y_ex = np.dot(A.T, np.dot(np.linalg.inv(np.dot(A, A.T)), u0))
    assert np.allclose(prob.var[1].vector()[:3], y_ex)


def test_marginals():
    """Test Marginals of a convex functions."""
    mesh = UnitIntervalMesh(1)

    A = np.array([[3.0, 1.0, 0.0], [0.0, 2.0, 1.0]])
    u0 = np.array([1.0, -2.0])
    V = VectorFunctionSpace(mesh, "DG", 0, dim=2)
    uimp = Function(V)
    uimp.interpolate(Constant(tuple(u0)))

    prob = MosekProblem()
    u = prob.add_var(V, lx=uimp, ux=uimp)
    v = dummy_variable(3, mesh)

    f = Marginals(QuadraticTerm(v, degree=0), [as_matrix(A), 0 * as_matrix(A)], u)
    prob.add_convex_term(f)

    prob.optimize()
    y_ex = np.dot(A.T, np.dot(np.linalg.inv(np.dot(A, A.T)), u0))
    assert np.allclose(prob.var[1].vector()[:3], y_ex)
