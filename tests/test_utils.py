#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check implementation of utility functions.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import as_vector, as_matrix, Identity, shape
from fenics_optim import (
    to_vect,
    to_mat,
    get_slice,
    tail,
    hstack,
    vstack,
    block_matrix,
    concatenate,
)


def test_vect_to_mat():
    """Verify symmetric/unsymmetric vector <-> matrix transformations."""
    X = as_matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    assert to_vect(X) == as_vector([1, 5, 9, 2, 6, 3])
    assert to_vect(X, symmetric=False) == as_vector([1, 5, 9, 2, 4, 6, 8, 3, 7])
    assert to_mat(to_vect(X)) == as_matrix([[1, 2, 3], [2, 5, 6], [3, 6, 9]])
    assert to_mat(to_vect(X, symmetric=False), symmetric=False)


def test_slice():
    """Test UFL vector slicing."""
    X = as_vector([1, 2, 3, 4])
    assert tail(X) == as_vector([2, 3, 4])
    assert get_slice(X, end=2) == as_vector([1, 2])
    assert get_slice(X, end=-2) == as_vector([1, 2])
    assert get_slice(X, step=2) == as_vector([1, 3])


def test_stacking():
    """Test vertical and horizontal stacking."""
    Id = Identity(2)
    A = as_matrix([[1, 2], [3, 4]])
    B = as_matrix([[1, 2, 3], [4, 5, 6]])
    IAAI = as_matrix([[1, 0, 1, 2], [0, 1, 3, 4], [1, 2, 1, 0], [3, 4, 0, 1]])
    assert hstack([as_vector([0, 0]), A]) == as_matrix([[0, 1, 2], [0, 3, 4]])
    assert vstack([as_vector([0, 0]), A]) == as_matrix([[0, 0], [1, 2], [3, 4]])
    assert hstack([Id, A]) == as_matrix([[1, 0, 1, 2], [0, 1, 3, 4]])
    assert vstack([Id, A]) == as_matrix([[1, 0], [0, 1], [1, 2], [3, 4]])
    assert block_matrix([[Id, A], [A, Id]]) == IAAI
    assert shape(hstack([Id, B])) == (2, 5)
    assert shape(block_matrix([[Id, B], [B.T, Identity(3)]])) == (5, 5)


def test_concatenate():
    """Test vector concatenation."""
    x = [1, 2]
    assert concatenate(x) == as_vector(x)
    assert concatenate(x + [as_vector(x)]) == as_vector(x + x)
