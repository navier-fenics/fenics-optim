#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis test for periodic masonry.

A test implementing a limit analysis problem using an anisotropic
criterion of periodic masonry (de Buhan, de Felice, 1997)
The criterion has many possible implementations using LP or SOCP.
This test amounts to check that all formulation give identical results.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import sym, grad, as_vector, pi, tan, as_matrix, dot, div
from dolfin import (
    RectangleMesh,
    Constant,
    Point,
    FunctionSpace,
    VectorFunctionSpace,
    VectorElement,
    DirichletBC,
    dx,
)
from fenics_optim import MosekProblem, Quad, ConvexFunction
import numpy as np


def Eps(v):
    """Voigt notation for strain."""
    E = sym(grad(v))
    return as_vector([E[0, 0], E[1, 1], 2 * E[0, 1]])


load = Constant((0.1, -0.2))

# Material parameters
phi, c, a, b = pi / 6.0, 1, 1.0, 2.0
m = 2 * a / b
f = tan(phi)

Q = as_matrix([[2.0 / b, 0, 0, 0], [0, 0, 0, 1.0 / a], [0, 2.0 / b, 1.0 / a, 0]])
Q1 = as_matrix([[1, 0, 0, 0], [0, f, 0, 0]])
Q2 = as_matrix([[0, -1, 0, 1], [f, 0, -f, 0]])
Q3 = as_matrix([[0, 1, 0, 1], [f, 0, f, 0]])
QQ = as_matrix(
    [
        [1, 0, 0, 0],
        [0, f, 0, 0],
        [0, -1, 0, 1],
        [f, 0, -f, 0],
        [0, 1, 0, 1],
        [f, 0, f, 0],
    ]
)

N = 5
mesh = RectangleMesh(Point(0.0, 0.0), Point(0.5, 1.0), N, 2 * N, "crossed")

# Create function space
deg_vel = 2
V = VectorFunctionSpace(mesh, "CG", deg_vel)


bc0 = [DirichletBC(V, Constant((0.0, 0.0)), "near(x[1], 0.0) && on_boundary")]

R = FunctionSpace(mesh, "R", 0)


def test_full_implementation():
    """A full implementatino without relying on `ConvexFunction`."""
    Vae = VectorElement(
        "Quadrature", mesh.ufl_cell(), degree=2, dim=4, quad_scheme="default"
    )
    Valp = FunctionSpace(mesh, Vae)
    Vxe = VectorElement(
        "Quadrature", mesh.ufl_cell(), degree=2, dim=2, quad_scheme="default"
    )
    VX = FunctionSpace(mesh, Vxe)
    Vde = VectorElement(
        "Quadrature", mesh.ufl_cell(), degree=2, dim=3, quad_scheme="default"
    )
    Vd = FunctionSpace(mesh, Vde)
    dxq = dx(metadata={"quadrature_scheme": "default", "quadrature_degree": 2})

    prob = MosekProblem("masonry_full_implementation")
    u, alp = prob.add_var([V, Valp], bc=[bc0, None])
    X1, X2, X3 = prob.add_var([VX, VX, VX], cone=[Quad(2), Quad(2), Quad(2)])

    def Pext(lamb):
        """External load power."""
        return lamb * dot(load, u) * dx

    prob.add_eq_constraint(R, A=Pext, b=1)

    def E_alp(Y):
        """Eps - Q*alp == 0."""
        return dot(Y, Eps(u) - dot(Q, alp)) * dxq

    prob.add_eq_constraint(Vd, A=E_alp)

    def cons1(Y):
        """Q1*alp - X1 == 0."""
        return dot(Y, dot(Q1, alp) - X1) * dxq

    def cons2(Y):
        """Q2*alp - X2 == 0."""
        return dot(Y, dot(Q2, alp) - X2) * dxq

    def cons3(Y):
        """Q3*alp - X3 == 0."""
        return dot(Y, dot(Q3, alp) - X3) * dxq

    prob.add_eq_constraint(VX, A=cons1)
    prob.add_eq_constraint(VX, A=cons2)
    prob.add_eq_constraint(VX, A=cons3)

    prob.add_obj_func(c / f * div(u) * dxq)
    prob.optimize(sense="min")

    return prob.pobj


def test_cvx_fun_implementation():
    """Implementation relying on `ConvexFunction`."""
    prob = MosekProblem("masonry_cvx_fun_implementation")
    u = prob.add_var(V, bc=bc0)

    def Pext(lamb):
        """Power of external loads."""
        return lamb * dot(load, u) * dx

    prob.add_eq_constraint(R, A=Pext, b=1)

    class SupportFunction(ConvexFunction):
        """Masonry criterion support function."""

        def conic_repr(self, X):
            alp = self.add_var(4)
            self.add_eq_constraint(X - dot(Q, alp))
            Y1, Y2, Y3 = self.add_var([2] * 3, cone=Quad(2))
            self.add_eq_constraint(dot(Q1, alp) - Y1)
            self.add_eq_constraint(dot(Q2, alp) - Y2)
            self.add_eq_constraint(dot(Q3, alp) - Y3)
            self.set_linear_term(c / f * (X[0] + X[1]))

    pi = SupportFunction(Eps(u), degree=2)

    prob.add_convex_term(pi)

    prob.optimize(sense="min")

    return prob.pobj


def test_LP_implementation():
    """A reformulation using linear programming."""
    prob = MosekProblem("masonry_reduced_implementation")
    u = prob.add_var(V, bc=bc0)

    def Pext(lamb):
        return lamb * dot(load, u) * dx

    prob.add_eq_constraint(R, A=Pext, b=1)

    class SupportFunction(ConvexFunction):
        def conic_repr(self, X):
            if m <= 1 / f:
                M = as_matrix(
                    [[-1, 0, 0], [f, -m, 0], [-f, -1 / f, 1], [-f, -1 / f, -1]]
                )
            else:
                M = as_matrix(
                    [
                        [-1, 0, 0],
                        [f, -m, 0],
                        [-1 / m, -m, 1],
                        [-1 / m, -m, -1],
                        [(-1 / f + 1 / m / f ** 2 - 1 / m), -1 / f, 1],
                        [(-1 / f + 1 / m / f ** 2 - 1 / m), -1 / f, -1],
                    ]
                )
            self.add_ineq_constraint(dot(M, X), bu=0)
            self.set_linear_term(c / f * (X[0] + X[1]))

    pi = SupportFunction(Eps(u), degree=2)

    prob.add_convex_term(pi)

    prob.optimize(sense="min")

    return prob.pobj


def test_final():
    """Comparison of the three implementations."""
    p1 = test_full_implementation()
    p2 = test_cvx_fun_implementation()
    p3 = test_LP_implementation()
    assert np.isclose(p1 / p2, 1) and np.isclose(p2 / p3, 1)
