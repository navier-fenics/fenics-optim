#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test limit analysis homogenization formulation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, grad
from dolfin import (
    UnitSquareMesh,
    FunctionSpace,
    VectorFunctionSpace,
    DirichletBC,
    Constant,
    Expression,
    dx,
)
from fenics_optim import MosekProblem, L2Norm


N = 50
mesh = UnitSquareMesh(N, N, "crossed")

prob = MosekProblem("Homogenization")

V = FunctionSpace(mesh, "CG", 2)
Rv = VectorFunctionSpace(mesh, "R", 0)

bc = DirichletBC(V, Constant(0.0), "on_boundary")
D, u = prob.add_var([Rv, V], bc=[None, bc])
Sig = Constant((1.0, 0.0))

R = FunctionSpace(mesh, "R", 0)

prob.add_eq_constraint(R, A=lambda lamb: lamb * dot(Sig, D) * dx, b=1)

k = Expression("fabs(x[0]-0.5) <= 0.1 ? 10: 1.", degree=0)

pi = L2Norm(D + grad(u), k=k, quadrature_scheme="vertex")
prob.add_convex_term(pi)

prob.optimize()
