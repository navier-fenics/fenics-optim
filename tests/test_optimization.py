#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mosek-based optimization tests.

These tests are intended to check the low-level interface with Mosek.
They are purely discrete problems formulated in R^n. We therefore use
Real FunctionSpaces to represent variables and constraints (through Lagrange
multipliers) in R^n.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""

from ufl import dot, as_matrix, as_vector, tr, inner
from dolfin import (
    UnitIntervalMesh,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    Function,
    dx,
)
from fenics_optim import MosekProblem, Quad, RQuad, SDP, Pow, Exp, to_mat
import numpy as np

inf = 1e30

mesh = UnitIntervalMesh(2)


def R(d=0):
    """Generate Real function spaces of dimension d."""
    if d == 0:
        return FunctionSpace(mesh, "R", 0)
    else:
        return VectorFunctionSpace(mesh, "R", 0, dim=d)


def test_LP():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-lo-shared.html#example-lo1."""  # noqa
    prob = MosekProblem("LO1")
    ux = Function(R(4))
    ux.vector().set_local([inf, 10, inf, inf])
    x = prob.add_var(R(4), lx=0, ux=ux)

    M = np.array([[3, 1, 2, 0], [2, 1, 3, 1], [0, 2, 0, 3]], dtype="float")

    def A(y):
        return dot(y, dot(as_matrix(M), x)) * dx

    def bu(y):
        return dot(y, as_vector([30, inf, 25])) * dx

    def bl(y):
        return dot(y, as_vector([30, 15, -inf])) * dx

    prob.add_ineq_constraint(R(3), A=A, bu=bu, bl=bl)
    prob.add_obj_func([dot(as_vector([3, 1, 5, 1]), x) * dx])

    prob.optimize(sense="max")

    assert np.allclose(x.vector().get_local(), np.array([0, 0, 15, 8.33333333]))


def test_QP():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-qo-shared.html#example-quadratic-objective."""  # noqa
    prob = MosekProblem("QO1")
    x = prob.add_var(R(3), lx=0)

    A = np.array([[2, 0, -1], [0, 0.2, 0], [-1, 0, 2]], dtype="float")
    C = np.linalg.cholesky(A).T

    r = prob.add_var(R(5), cone=RQuad(5))

    def A(y):
        return dot(y, dot(as_matrix(C), x) - as_vector([r[2], r[3], r[4]])) * dx

    prob.add_eq_constraint(R(3), A=A)

    def A2(y):
        return y * r[1] * dx

    prob.add_eq_constraint(R(), A=A2, b=1)

    def A3(y):
        return y * sum(x) * dx

    prob.add_ineq_constraint(R(), A=A3, bl=1)
    prob.add_obj_func((r[0] - x[1]) * dx)

    prob.optimize(sense="min")

    assert np.allclose(x.vector().get_local(), np.array([0, 5, 0]), atol=1e-3)


def test_QP2():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-qo-shared.html#example-quadratic-objective."""  # noqa
    prob = MosekProblem("QO1")
    x = prob.add_var(R(3), lx=0)

    A = np.array([[2, 0, -1], [0, 0.2, 0], [-1, 0, 2]], dtype="float")
    C = np.linalg.cholesky(A).T

    r = prob.add_var(R(5), cone=RQuad(5))

    def A(y):
        expr = dot(y, dot(as_matrix(C), x) - as_vector([r[2], r[3], r[4]])) * dx
        return expr

    prob.add_eq_constraint(R(3), A=A)

    def A2(y):
        return y * r[1] * dx

    prob.add_eq_constraint(R(), A=A2, b=1)

    def A3(y):
        return y * sum(x) * dx

    prob.add_ineq_constraint(R(), A=A3, bl=1)
    prob.add_obj_func((r[0] - x[1]) * dx)

    prob.optimize(sense="min")

    assert np.allclose(x.vector().get_local(), np.array([0, 5, 0]), atol=1e-3)


def test_SOCP():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-cqo-shared.html#example-cqo1."""  # noqa
    prob = MosekProblem("CQO1")
    lx1 = Function(R(3))
    lx2 = Function(R(3))
    lx1.vector().set_local([-inf, 0, 0])
    lx2.vector().set_local([-inf, -inf, 0])
    x1, x2 = prob.add_var([R(3)] * 2, lx=[lx1, lx2], cone=[Quad(3), RQuad(3)])

    def A(y):
        return y * (x1[1] + x1[2] + 2 * x2[2]) * dx

    prob.add_eq_constraint(R(), A=A, b=1)

    prob.add_obj_func((x1[0] + x2[0] + x2[1]) * dx)

    prob.optimize(sense="min")

    sol1 = np.array([0.3689972, 0.2609204, 0.2609204])
    sol2 = np.array([0.1690548, 0.1690548, 0.23907959])
    assert np.allclose(x1.vector().get_local(), sol1, atol=1e-3)
    assert np.allclose(x2.vector().get_local(), sol2, atol=1e-3)


def test_SDP():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-sdo-shared.html#example-sdo1."""  # noqa
    prob = MosekProblem("SDO1")
    x, Xv = prob.add_var([R(3), R(6)], cone=[Quad(3), SDP(3)])
    X = to_mat(Xv)

    def A(y):
        return y * (x[0] + tr(X)) * dx

    prob.add_eq_constraint(R(), A=A, b=1)

    def A2(y):
        return y * (x[1] + x[2] + inner(as_matrix(np.ones((3, 3))), X)) * dx

    prob.add_eq_constraint(R(), A=A2, b=1 / 2.0)

    C = as_matrix(np.array([[2, 1, 0], [1, 2, 1], [0, 1, 2]], dtype="float"))
    prob.add_obj_func((x[0] + inner(C, X)) * dx)

    prob.optimize(sense="min")

    solx = np.array([0.25440969934889024, 0.1798948233519128, 0.1798948233519128])
    solbarx = np.array(
        [
            0.2172499319348291,
            -0.2599699526998759,
            0.21724993162712838,
            0.3110904376697469,
            -0.25996995269987394,
            0.21724993193482584,
        ]
    )
    assert np.allclose(x.vector().get_local(), solx, atol=1e-3)
    assert np.allclose(prob.barx, solbarx, atol=1e-3)


def test_SDP2():
    """Example https://docs.mosek.com/8.1/pythonapi/tutorial-sdo-shared.html#example-sdo1."""  # noqa
    prob = MosekProblem("SDO1")
    x, Xv, Yv = prob.add_var([R(3), R(6), R(10)], cone=[Quad(3), SDP(3), SDP(4)])
    X = to_mat(Xv)
    Y = to_mat(Yv)

    def A(y):
        return (
            dot(
                y,
                as_vector([x[0], x[1] + x[2]])
                + as_vector([tr(X), inner(as_matrix(np.ones((3, 3))), X)])
                + as_vector([tr(Y), 0]),
            )
            * dx
        )

    def b(y):
        return (y[0] + y[1] / 2) * dx

    prob.add_eq_constraint(R(2), A=A, b=b)

    C = as_matrix(np.array([[2, 1, 0], [1, 2, 1], [0, 1, 2]], dtype="float"))
    prob.add_obj_func([x[0] * dx, inner(C, X) * dx, tr(Y) * dx])

    prob.optimize(sense="min")

    solx = np.array([0.25440969934889024, 0.1798948233519128, 0.1798948233519128])
    solbarx = np.array(
        [
            0.2172499319348291,
            -0.2599699526998759,
            0.21724993162712838,
            0.3110904376697469,
            -0.25996995269987394,
            0.21724993193482584,
        ]
    )
    assert np.allclose(x.vector().get_local(), solx, atol=1e-3)
    assert np.allclose(prob.barx[0], solbarx, atol=1e-3)
    assert np.linalg.norm(prob.barx[1]) < 1e-6


def test_POW1():
    """Example https://docs.mosek.com/9.1/pythonapi/tutorial-pow-shared.html."""
    prob = MosekProblem("POW1")
    x, y = prob.add_var([R(3), R(3)], cone=[Pow(3, 0.2), Pow(3, 0.4)])

    def A(z):
        return z[0] * (x[0] + x[1] + y[0] / 2) * dx + z[1] * y[1] * dx

    def b(z):
        return (Constant(2) * z[0] + Constant(1) * z[1]) * dx

    prob.add_eq_constraint(R(2), A=A, b=b)

    prob.add_obj_func((x[2] - x[0] + y[2]) * dx)

    prob.optimize(sense="max")
    sol = np.array([0.06389298, 0.78308564, 2.30604283])
    xyz = x.vector().get_local()
    xyz[2] = y.vector().get_local()[0]
    assert np.allclose(xyz, sol, atol=1e-3)


def test_CEO1():
    """Example https://docs.mosek.com/9.1/pythonapi/tutorial-ceo-shared.html#example-ceo1."""  # noqa
    prob = MosekProblem("CEO1")
    x = prob.add_var(R(3), cone=Exp(3))

    def A(z):
        return z * (x[0] + x[1] + x[2]) * dx

    def b(z):
        return Constant(1) * z * dx

    prob.add_eq_constraint(R(), A=A, b=b)

    prob.add_obj_func((x[0] + x[1]) * dx)

    prob.optimize()
    sol = np.array([0.6117882543880888, 0.17040004803741837, 0.2178116988575844])
    assert np.allclose(x.vector().get_local(), sol, atol=1e-3)
