sphinx==4.2
nbsphinx
nbsphinx-link
docutils==0.17.1
jupyter
nbconvert
sphinx-book-theme
sphinx-panels
sphinx-tabs
sphinx-togglebutton
mock
scipy
mosek
myst-parser
matplotlib
numpy
