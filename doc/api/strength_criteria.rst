.. fenics_optim documentation master file, created by
   sphinx-quickstart on Wed Apr 14 16:48:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:mod:`limit_analysis.materials`
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Strength criteria
------------------

.. automodule:: fenics_optim.limit_analysis.materials.strength_criterion
    :members:

von Mises
---------

.. automodule:: fenics_optim.limit_analysis.materials.von_mises
    :members:
    :show-inheritance:

Drucker-Prager 
--------------

.. automodule:: fenics_optim.limit_analysis.materials.drucker_prager
    :members:
    :show-inheritance:

Gurson
-------

.. automodule:: fenics_optim.limit_analysis.materials.gurson
    :members:
    :show-inheritance:
    
Hosford
--------------

.. automodule:: fenics_optim.limit_analysis.materials.hosford
    :members:   
    :show-inheritance:

Masonry
-------

.. automodule:: fenics_optim.limit_analysis.materials.masonry
    :members:
    :show-inheritance:
    
Mohr-Coulomb
--------------

.. automodule:: fenics_optim.limit_analysis.materials.mohr_coulomb
    :members:
    :show-inheritance:

Rankine
-------

.. automodule:: fenics_optim.limit_analysis.materials.rankine
    :members:
    :show-inheritance:

Tsai-Wu
-------

.. automodule:: fenics_optim.limit_analysis.materials.tsai_wu
    :members:
    :show-inheritance:


