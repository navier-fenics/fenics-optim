.. fenics_optim documentation master file, created by
   sphinx-quickstart on Wed Apr 14 16:48:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:mod:`convex_function`
======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
:mod:`base_convex_functions`
----------------------------

.. autoclass:: fenics_optim.convex_function.base_convex_function.ConvexFunction
   :members:
   :inherited-members:

.. autoclass:: fenics_optim.convex_function.base_convex_function.SumExpr


:mod:`simple_terms`
-------------------

.. automodule:: fenics_optim.convex_function.simple_terms
   :members:
   :no-inherited-members:
   
:mod:`norms`
------------

.. automodule:: fenics_optim.convex_function.norms
   :members:
   :exclude-members: conic_repr


:mod:`perspective`
------------------

.. autoclass:: fenics_optim.convex_function.perspective.Perspective

:mod:`epigraph`
------------------

.. autoclass:: fenics_optim.convex_function.epigraph.Epigraph

:mod:`infconvolution`
---------------------

.. autoclass:: fenics_optim.convex_function.infconvolution.InfConvolution

:mod:`marginal`
---------------------

.. autoclass:: fenics_optim.convex_function.marginal.Marginal

.. autoclass:: fenics_optim.convex_function.marginal.Marginals
