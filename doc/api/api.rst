.. fenics_optim documentation master file, created by
   sphinx-quickstart on Wed Apr 14 16:48:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Public API
==========

.. toctree::
   :maxdepth: 2
   
   cones
   convex_function
   mosek_io
   strength_criteria
   topology_optimization
   utils
   mesh_utils
