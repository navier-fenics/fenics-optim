Cheeger problems
================

.. math:: 
   \renewcommand{\div}{\operatorname{div}}
   \newcommand{\bsig}{\boldsymbol{\sigma}}

Dual problem
---------------

The Cheeger problem dual formulation reads as:

.. math::
    \begin{equation}
    \begin{array}{rl} \displaystyle{\sup_{\lambda\in \mathbb{R}, \bsig\in W}} & \displaystyle{\lambda} \\
    \text{s.t.}  & \lambda f = \div\bsig \quad \text{in }\Omega \\
    & \|\bsig\|_2 \leq 1
    \end{array} \label{Cheeger-dual}
    \end{equation}

A natural discretization strategy for such a problem is to use :math:`H(\div)`-conforming elements such as 
the Raviart-Thomas element :math:`RT_1`. Two minimization variables are therefore defined: :math:`\lambda`
belonging to a scalar :code:`Real` function space and :math:`\bsig \in RT_1`.
Since for :math:`\bsig \in RT_1`, :math:`\div \bsig \in \mathbb{P}^0`, we write the constraint equation using :math:`\mathbb{P}^0`
Lagrange multipliers:

Implementation
--------------

.. literalinclude:: ../../../demos/Cheeger_sets/Cheeger_dual.py
  :language: python

.. image:: ../../../paper/convex_optimization/results/Cheeger_RT.png
  :width: 400
  :align: center
