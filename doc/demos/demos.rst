.. _demos:

.. fenics_optim documentation master file, created by
   sphinx-quickstart on Wed Apr 14 16:48:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documented demos
================

.. toctree::
   :maxdepth: 2
   
   obstacle_problem
   Cheeger_sets/Cheeger_dual
   limit_analysis_3D_SDP
   mixed_modelling
   elastoplasticity/elastoplasticity_exponential_hardening
   viscoplasticity/bingham_hele_shaw
   nonlinear_membrane/nonlinear_membrane
   minimal_crack_surfaces/minimal_crack_surfaces
