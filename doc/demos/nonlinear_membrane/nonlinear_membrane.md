# Nonlinear tension-field elastic membrane

$\newcommand{\bA}{\boldsymbol{A}}
\newcommand{\bB}{\boldsymbol{B}}
\newcommand{\bC}{\boldsymbol{C}}
\newcommand{\bE}{\boldsymbol{E}}
\newcommand{\bCel}{\boldsymbol{C}_\text{el}}
\newcommand{\bF}{\boldsymbol{F}}
\newcommand{\bG}{\boldsymbol{G}}
\newcommand{\bI}{\boldsymbol{I}}
\newcommand{\bS}{\boldsymbol{S}}
\newcommand{\bT}{\boldsymbol{T}}
\newcommand{\bU}{\boldsymbol{U}}
\newcommand{\bV}{\boldsymbol{V}}
\newcommand{\bW}{\boldsymbol{W}}
\newcommand{\bX}{\boldsymbol{X}}
\newcommand{\bY}{\boldsymbol{Y}}
\newcommand{\bZ}{\boldsymbol{Z}}
\newcommand{\be}{\boldsymbol{e}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\by}{\boldsymbol{y}}
\newcommand{\bz}{\boldsymbol{z}}
\newcommand{\T}{^\text{T}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\Pp}{\mathcal{P}}
\newcommand{\Qq}{\mathcal{Q}}
\newcommand{\dOm}{\text{d}\Omega}
\newcommand{\dS}{\text{d}S}$

This demo explores a nonlinear elastic membrane formulation using the so-called "tension-field" theory developed by [[REI38], [PIP86]](#References). This theory enables to account for wrinkles occuring in compressed regions of a thin elastic membrane in an *effective* manner. Indeed, a unilateral constitutive law is formulated which assumes to stiffness of the elastic membrane in compressed regions.

## Contents
```{toctree}
---
maxdepth: 1
---
material_point_response
annular_square_membrane
```

## Hyperelastic membrane

We consider the computation of the displacement field $\bu$ of an hyperelastic membrane subject to imposed tractions $\bT$ on the Neumann part of the boundary $\partial\Omega_\text{N}$ and to fixed displacement $\bu = 0$ on the Dirichlet part of the boundary $\partial \Omega_\text{D}$. In the finite-strain setting, the hyperelastic potential of a 3D material can be expressed as a function of some nonlinear strain measures. For instance, let us consider the Cauchy-Green strain tensor $\bC=\bF\T\bF$ where $\bF=\bI+\nabla \bu$ is the deformation gradient. The free energy hyperelastic potential is then $\psi(\bC)$ and the displacement field can be obtained as the solution to the following minimum principle:

$$
\begin{array}{rl} \displaystyle{\inf_{\bu,\bC}} & \displaystyle{\int_\Omega \psi(\bC) \dOm - \int_{\partial \Omega_\text{N}} \bT\cdot\bu \dS}\\
\text{s.t.} & \bC = \bI + \nabla \bu + \nabla \bu\T +\nabla \bu\T\nabla \bu\\
& \bu = 0 \text{ on } \partial\Omega_\text{D}
\end{array}
$$ (hyperelastic-min-principle)

In the general case, such a problem is not convex which makes the analysis of hyperelastic materials rather complex. In particular, some equilibrium positions can be unstable and lead to buckling phenomena.

## Tension-field elastic membrane formulation

As regards thin hyperelastic membranes, local buckling (or wrinkling) will occur at very low load levels in compressed regions. In the limit of infinitely thin membranes, compressed stress states cannot be supported at all. Tension field theory [[REI38]](#References) has been proposed in order to simplify the analysis of thin membranes.

In the finite-deformation case, the tension-field theory has been first formalized by [[PIP86]](#References) by introducing a relaxed strain energy functional. More precisely, introducing the following quasi-convexification of $\psi$:
\begin{equation*}
\begin{array}{rl}\psi_\text{memb}(\bC) =  \displaystyle{\inf_{\bCel}} & \psi(\bCel) \\
 \text{s.t. } &\bC_\text{el} \succeq \bC \end{array} 
 \end{equation*} (relaxed-hyperelastic-potential)
the tension field variational principle is obtained when replacing $\psi$ with $\psi_\text{memb}$ in {eq}`hyperelastic-min-principle`:

$$
\begin{array}{rl} \displaystyle{\inf_{\bu,\bC}} & \displaystyle{\int_\Omega \psi_\text{memb}(\bCel) \dOm - \int_{\partial \Omega_\text{N}} \bT\cdot\bu \dS}\\
\text{s.t.} & \bCel \succeq \bI + \nabla \bu + \nabla \bu\T +\nabla \bu\T\nabla \bu\\
& \bu = 0 \text{ on } \partial\Omega_\text{D}
\end{array}
$$ (relaxed-hyperelastic-min-principle)

The above relaxed potential then provides a tension-field constitutive equation in terms of the second Piola-Kirchhoff stress $\bS$ as follows:

\begin{align*}
\bS &= 2\dfrac{\partial \psi}{\partial \bC}(\bC_\text{el})\\
\bC &= \bC_\text{el} + \bC_\text{w} \\
\bC_\text{w} &\preceq 0,\quad \bS \succeq 0, \quad \bS:\bC_\text{w}=0
\end{align*}

where $\bC_\text{w}$ can be seen as an inelastic *wrinkling* strain accounting for the occurrence of wrinkles in compressed regions. As a consequence, the resulting stress is always tensile.


## Conic reformulation

Finally, Pipkin showed that when $\psi$ is a convex function of $\bC$, $\psi_\text{memb}$ turns out to be a convex function of $\bF$ or, equivalently, of the displacement gradient $\bG=\nabla \bu$. Indeed, if $\psi(\bCel)$ is convex, the relaxed minimum principle {eq}`relaxed-hyperelastic-min-principle` is a convex program due to the following conic reformulation of the SDP constraint (see also [[KAN11]](#References)):
\begin{equation*}
\bCel \succeq \bC = \bI + \bG + \bG\T +\bG\T\bG
\end{equation*}

Let us first recall the *Schur complement lemma for a PSD block-matrix:
> A symmetric block-matrix
> \begin{equation*}
\bZ = \begin{bmatrix}
\bU & \bV \\ \bV\T & \bW
\end{bmatrix}
\end{equation*}
> is positive semi-definite if and only if $\bW\succeq 0$ and $\bU-\bV\bW^{-1}\bV\T \succeq 0.$


Let us then consider the following symmetric matrix:
\begin{equation*}
  \bZ = 
\begin{bmatrix}
\bCel & \bI+\bG\T \\
\bI+\bG & \bI
\end{bmatrix} \succeq 0
\end{equation*}
we have $\bW=\bI \succ 0$ and $\bU-\bV\bW^{-1}\bV\T = \bCel - (\bI+\bG\T)(\bI+\bG) = \bCel - \bI - \bG - \bG\T - \bG\T\bG=\bC$. As a result, using the Schur complement lemma, $\bZ\succeq 0$ if and only if $\bCel \succeq \bC$. 

In conclusion, if $\psi(\bC)$ admits a convex conic representation in terms of $\bC$, problem {eq}`relaxed-hyperelastic-min-principle` is a convex conic program.

## Ogden-type hyperelastic materials
In the following, we consider an incompressible Ogden-type material (with only one term for simplicity) for which the 3D potential reads:
\begin{equation*}
\psi(\lambda_1,\lambda_2,\lambda_3) = \dfrac{2\mu}{\alpha^2}\sum_{i=1}^3 \lambda_i^\alpha
\end{equation*}
where $\mu$ is the shear modulus, $\alpha$ is some power-law exponent (which we assume in the following to be larger than 2) and $\lambda_1,\lambda_2,\lambda_3$ denote the principal stretches. The latter are the positive square roots of the Cauchy-Green tensor eigenvalues. Note that the case $\alpha=2$ corresponds to a neo-Hookean material.

In the incompressible case, we have $J=\lambda_1\lambda_2\lambda_3=1$ which yields a reduced energy:
\begin{equation*}
\hat{\psi}(\lambda_1,\lambda_2)=\psi(\lambda_1,\lambda_2,\lambda_1^{-1}\lambda_2^{-1}) = \dfrac{2\mu}{\alpha^2}\left(\lambda_1^\alpha + \lambda_2^\alpha + \dfrac{1}{\lambda_1^\alpha\lambda_2^\alpha}\right)
\end{equation*}

From the following relation for the principal stretches in 2D:
\begin{align*}
\lambda_1^2 = \dfrac{1}{2}\left(C_{11}+C_{22} + \sqrt{(C_{11}-C_{22})^2 + 4C_{12}^2}\right)  \\
\lambda_2^2 = \dfrac{1}{2}\left(C_{11}+C_{22} - \sqrt{(C_{11}-C_{22})^2 + 4C_{12}^2}\right)  \\
\end{align*}
one can see that the reduced energy can be equivalently expressed as a convex function of $\bC$ as follows:
\begin{equation*}
\begin{array}{rl} \displaystyle{\hat{\psi}(\bC) = \min_{s,t}} & \dfrac{\mu}{2\beta^2}\left((t+s)^{\beta}+(t-s)^{\beta} + (t^2-s^2)^{-\beta}\right) \\
\text{s.t.} & t = \dfrac{1}{2}(C_{11}+C_{22}) \\
& \dfrac{1}{2}\sqrt{(C_{11}-C_{22})^2 + 4C_{12}^2} \leq s 
\end{array}
\end{equation*}
where we assume that $\beta=\alpha/2 \geq 1$.

The above expression can further be re-expressed introducing additional auxiliary variables as:
\begin{equation*}
\begin{array}{rl} \displaystyle{\hat{\psi}(\bC) = \min_{r,s,t,\by}} & \dfrac{\mu}{2\beta^2}(x_0+y_0+z_0) \\
\text{s.t.} & \begin{Bmatrix} (C_{11}-C_{22})/2 \\ C_{12} \end{Bmatrix} = \begin{Bmatrix} y_1 \\ y_2 \end{Bmatrix} \\
& \sqrt{y_1^2+y_2^2} \leq s \\
& (t+s)^\beta \leq x_0 \\
& (t-s)^\beta \leq y_0 \\
& (t^2-s^2) \geq u^2 \\
& u^{-2\beta} \leq z_0
\end{array}
\end{equation*}
and where the last constraints can be formulated using a suitable quadratic and power cones as follows:

\begin{align*}
(t+s)^\beta \leq x_0 & \Leftrightarrow \begin{cases} x_2 = t+s \\ x_1 = 1 \\ |x_2|\leq x_0^{1/\beta} \text{ that is } \bx \in \Pp^{1/\beta}_3 \end{cases}\\
(t-s)^\beta \leq y_0 & \Leftrightarrow \begin{cases} y_2 = t-s \\ y_1 = 1 \\ |y_2|\leq y_0^{1/\beta} \text{ that is } \by \in \Pp^{1/\beta}_3 \end{cases}\\
(t^2-s^2) \geq u^2 & \Leftrightarrow \sqrt{s^2+u^2}\leq t \text{ that is } (t,s,u)\in \Qq_3\\
u^{-2\beta}\leq z_0 & \Leftrightarrow 1 \leq  u z_0^{2\beta} \Leftrightarrow 1\leq u^{\frac{1}{1+2\beta}} z_0^{\frac{2\beta}{1+2\beta}} \text{ that is } (u,z_0,1) \in \Pp_3^{\frac{1}{1+2\beta}}
\end{align*}

## Surfaces embedded in $\mathbb{R}^3$

The above presentation essentially considered planar surfaces. In general, membranes are embedded in a 3D space. The only modification to account for is the expression of the deformation gradient $\bF$.
In this setting, since we consider out-of-plane displacements, the displacement and deformation gradients $\bG$ and $\bF$ are in fact $3\times 2$ matrices such that:
\begin{equation*}
\bF = \overline{\bI} + \bG\quad \text{where } \overline{\bI} = \begin{bmatrix}
\be_1 & \be_2
\end{bmatrix}\in \mathbb{R}^{3\times 2}
\end{equation*}
where $\be_1$, $\be_2$ denotes orthonormal basis vectors of the membrane initial reference plane. In the following implementation, $\overline{\bI}$ will be refered to as the `metric` in the reference configuration. 

The strain tensors $\bC,\bCel$ are still $2\times 2$ matrices. The only modification to apply to section \ref{sec:membrane-conic-reformulation} concerns the SDP constraint. We now have:
\begin{equation*}
\bCel \succeq \bI_2 + \overline{\bI}\T\bG + \bG\T\overline{\bI} + \bG\T\bG
\end{equation*}
where $\bI_n$ is the identity matrix of dimension $n$. The latter constraint can now be equivalently reformulated as follows:
\begin{equation*}
\bZ = \begin{bmatrix}
\bCel & \overline{\bI}\T+\bG\T \\
\overline{\bI} + \bG & \bI_3
\end{bmatrix} \succeq 0
\end{equation*}


## Implementation
```{literalinclude} ../../../demos/notebooks/nonlinear_membrane/ogden_membrane.py
:language: python
```

## Acknowledgements

We acknowledge [Alexander Niewiarowski (Princeton University)](mailto:aan2@princeton.edu) for stimulating discussions on this topic.


## References

[REI38] E.  Reissner,  in Proceedings  of  the  Fifth  International Congress on Applied Mechanics (1938) pp. 88–92.

[PIP86] A. C. Pipkin, IMA Journal of Applied Mathematics, **36**, 85 (1986)

[KAN11] Kanno, Y. (2011). Nonsmooth mechanics and convex optimization. Crc Press.
