Obstacle problem
================

.. math:: 
   \renewcommand{\div}{\operatorname{div}}
   \newcommand{\bsig}{\boldsymbol{\sigma}}

   
Implementation
--------------

.. literalinclude:: ../../demos/obstacle/obstacle_problem.py
  :language: python

.. image:: ../../paper/convex_optimization/results/contact_area.png
  :width: 400
  :align: center
