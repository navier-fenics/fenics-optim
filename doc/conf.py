# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath(".."))


# -- Project information -----------------------------------------------------

project = "fenics_optim"
copyright = "2021, Jeremy Bleyer"
author = "Jeremy Bleyer"


# -- General configuration ---------------------------------------------------
autodoc_mock_imports = ["dolfin", "ffc", "ufl", "FIAT", "meshio"]

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "nbsphinx",
    "nbsphinx_link",
    "sphinx_rtd_theme",
    "myst_parser",
]
myst_enable_extensions = [
    "amsmath",
    "dollarmath",
]

# The full version, including alpha/beta/rc tags
# # reads version from file
with open(
    os.path.join(os.path.dirname(__file__), "../fenics_optim/VERSION.txt")
) as version_file:
    release = version_file.read().strip()

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# Add Markdown source files
source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}
# Autostructify config for Markdown parser
github_doc_root = "https://github.com/rtfd/recommonmark/tree/master/doc/"

mathjax_path = (
    "https://cdn.jsdelivr.net/npm/mathjax@2/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
)
mathjax3_config = {"tex": {"tags": "ams", "useLabelIds": True}}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = "sphinxdoc"
html_theme = "sphinx_book_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

pygments_style = "sphinx"

# html_theme_options = {
#     "analytics_id": "UA-XXXXXXX-1",  #  Provided by Google in your dashboard
#     "analytics_anonymize_ip": False,
#     "logo_only": False,
#     "display_version": True,
#     "prev_next_buttons_location": "bottom",
#     "style_external_links": False,
#     "vcs_pageview_mode": "",
#     "style_nav_header_background": "white",
#     # Toc options
#     "collapse_navigation": True,
#     "sticky_navigation": True,
#     "navigation_depth": 4,
#     "includehidden": True,
#     "titles_only": False,
# }


def remove_module_docstring(app, what, name, obj, options, lines):
    if what == "module":
        del lines[:]


def setup(app):
    app.connect("autodoc-process-docstring", remove_module_docstring)


autosummary_generate = True
