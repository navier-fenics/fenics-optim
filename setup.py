#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Setup file for the fenics_optim package.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import setuptools
import os


def readme():
    """Get Readme file."""
    try:
        with open("README.md") as f:
            return f.read()
    except Exception:
        return ""


with open(
    os.path.join(os.path.dirname(__file__), "fenics_optim/VERSION.txt")
) as version_file:
    __version__ = version_file.read().strip()

setuptools.setup(
    name="fenics_optim",
    version=__version__,
    url="https://gitlab.enpc.fr/navier-fenics/fenics-optim/",
    author="Jeremy Bleyer",
    author_email="jeremy.bleyer@enpc.fr",
    packages=setuptools.find_packages(),
    package_data={"fenics_optim": ["VERSION.txt"]},
    description="Convex optimization interface in FEniCS",
    long_description=readme(),
    test_suite="tests",
)
