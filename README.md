# fenics_optim -- Convex optimization interface in FEniCS

[![Documentation Status](https://readthedocs.org/projects/fenics-optim/badge/?version=latest)](https://fenics-optim.readthedocs.io/en/latest/)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3604085.svg)](https://doi.org/10.5281/zenodo.3604085)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Introduction
**fenics_optim** is a Python library that aims at providing a simple user interface in FEniCS for solving convex optimization problems. In particular, it relies on the **Mosek** mathematical programming library. In particular, Mosek is a state-of-the art solver for linear programming (LP), convex quadratic programming (QP), second-order conic programming (SOCP) and semi-definite programming (SDP).

## Documentation

Documentation is available at https://fenics-optim.readthedocs.io.

## Prerequisites
**fenics_optim** requires: 
* **FEniCS** (>= 2018.1.0, Python 3), see [installation instructions here](https://fenics.readthedocs.io/en/latest/installation.html).
* **MOSEK** (>= version 9 with its Python interface), see [installation instructions here](https://www.mosek.com/downloads/). The Python interface can be simply installed via `pip`:
```
pip install -f https://download.mosek.com/stable/wheel/index.html Mosek
```
Mosek is a commercial software so users need a valid Mosek license. Free unlimited licenses are available for education and research purposes, see the [Academic License section](https://www.mosek.com/products/academic-licenses/).

## Installation and usage
Simply clone the **fenics-optim** public repository
```
git clone https://gitlab.enpc.fr/navier-fenics/fenics-optim.git
```
and install the package by typing
```
cd fenics-optim

python3 setup.py install --user
```

## Features
Current version supports variational problem formulations resulting in Linear Programs, Second-Order Cone Programs, Semi-Definite Programs and Power or Exponential cone programs.

## License
Like all core **FEniCS** components, **fenics_optim** is freely available under the GNU LGPL, version 3.

## Citing
The source code can be cited from the following Zenodo repository: 
```
@Misc{fenics_optim_2020,
  author       = {Jérémy Bleyer},
  title        = {{fenics\_optim -- Convex optimization interface in FEniCS. Version 2.0}},
  howpublished = {Zenodo, https://doi.org/10.5281/zenodo.3604085},
  month        = jan,
  year         = {2020},
  doi          = {10.5281/zenodo.3604085},
}
```
The project is also presented in the following paper:

> Bleyer, J. (2020). Automating the formulation and resolution of convex variational problems: applications from image processing to computational mechanics. ACM Transactions on Mathematical Software (TOMS), 46(3), 1-33.

and in the following one for the limit analysis module:

> Bleyer, J., & Hassen, G. (2021). Automated formulation and resolution of limit analysis problems. Computers & Structures, 243, 106341.
