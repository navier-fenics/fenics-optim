# CHANGELOG 

## Version 2.0.3
* Better support of rectangular tensor expressions
* Add mesh utility function for `Gmsh`-generated meshes
* Fix bug in `hstack` function
* Correctly ships version number with package
* Add various demos

## Version 2.0.2

* Add Bingham Hele-Shaw demo

## Version 2.0.1

### Implementation of `Marginal`
* `Marginal(f, A, x)` for a convex function $f(y)$ through a linear operator $A$ defined as:
\begin{equation}
f \setminus A(x) = \inf_{y \text{ s.t. } x = Ay} f(y)
\end{equation}

## Version 2.0

### Combined UFL expression support (fixes #34)
Support for UFL expression combining different variables in convex function, linear constraint matrix or objective function. Block-like expressions e.g.
```python
 u, v = prob.add_var([V, V])
 prob.add_eq_constraint(R, A=lambda lamb: [lamb * u * dx, -lamb * v*dx])
 
 F1 = L2Norm(grad(u))
 F2 = L2Norm(grad(v))
 
 prob.add_convex_term(F1)
 prob.add_convex_term(F2)
```
can now be written as follows:
```python
 u, v = prob.add_var([V, V])
 prob.add_eq_constraint(R, A=lambda lamb: lamb * (u-v) * dx)
 
 F = L2Norm(grad(u+v))
 
 prob.add_convex_term(F)
```

Support of block-like expressions will probably be deprecated in next releases.

### Implementation of `Epigraph`, `Perpective` and `InfConvolution`
Addresses #35 and #36 thanks to the support of combined UFL expressions.
* `Epigraph(f, t)` for a `ConvexFunction` $f(x)$ defined as:
\begin{equation}
(x, t) \in \text{epi} f \Leftrightarrow f(x) \leq t
\end{equation}
* `Perspective(f, t)` for a `ConvexFunction` $f(x)$ defined as:
\begin{equation}\text{persp}_f(x, t) = t f(x/t)
\end{equation}
* `InfConvolution(f1, f2)` for two `ConvexFunction` $f_1(x)$ and $f_2(x)$ defined as:
\begin{equation}
f_1 \square f_2(x) = \inf_{x=y_1+y_2} f_1(y_1)+f_2(y_2)
\end{equation}

### Add `topology_optimization` module
Adds a convenience module for single and multi-material limit-analysis based topology optimization along with some demos.

### Style change

Coding style changed to be PEP8-compliant.

### New strength criteria

`RobustMohrCoulomb2D` and `PlaneCoulomb2D` accounting for cohesion and friction angle uncertainty.

## Version 1.2

Support for power and exponential cones (requires `mosek` >= v9.0)

## Version 1.1

Includes the `limit_analysis` module along with corresponding demos
