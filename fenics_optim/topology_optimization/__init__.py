#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
fenics_optim submodule for solving limit analysis-based topology optimization problems.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .single_material import (
    LimitAnalysis,
    VolumeMinimization,
    LoadMaximization,
)
from .multi_materials import (
    ReinforcementLoadMaximization,
    ReinforcementVolumeMinimization,
    BimaterialVolumeMinimization,
    BimaterialLoadMaximization,
)
