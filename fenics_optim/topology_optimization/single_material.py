#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Single-material limit analysis-based topology optimization models.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from fenics_optim import MosekProblem
from ufl import dot
from .topology_optimization import TopologyOptimization, Eps


class LoadMaximization(TopologyOptimization):
    """
    Single-material load maximization problem.

    The material volume fraction is set to a maximum value.
    The loading is amplified by a load factor, we look for
    its maximum value under the constraints of equilibrium,
    density-dependent strength conditions and volume constraints.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    frac : float
        maximum volume fraction
    material : :class:`StrengthCriterion`
        material strength criterion
    """

    def __init__(self, mesh, frac, material, **kwargs):
        TopologyOptimization.__init__(self, 1, 1, mesh, material, **kwargs)
        self.frac = frac
        self.optimization_sense = "max"
        self.parameters.update({"volume_fraction": frac})

    def set_up_problem(self):
        """
        Load-maximization problem.

        Optimization variables are :math:`\\lambda, \\rho, \\sigma`.
        """
        self.prob = MosekProblem("load maximization")
        self.lamb, self.rho, self.sig = self.prob.add_var(
            [self.VR, self.Vrho, self.Vsig],
            lx=[None, self.rhomin, None],
            ux=[None, 1, None],
            name=["Load factor", "Density", "Stress"],
        )

        def volume_frac(lamb):
            return lamb * self.rho * self.dx

        self.prob.add_ineq_constraint(self.VR, volume_frac, bu=self.frac * self.volume)

        def equilibrium(u):
            return (
                self.lamb * self.Pext(u)
                + self.rho_Pext0(u, self.rho)
                - dot(self.sig, Eps(u)) * self.dx
            )

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, bc=self.bc, name="Displacement"
        )

        self.prob.add_obj_func([1, 0, 0])

        self.apply_slope_control()

        self.apply_strength_conditions()


class LimitAnalysis(TopologyOptimization):
    """
    Standard limit analysis problem (no density field).

    The loading is amplified by a load factor, we look for
    its maximum value under the constraints of equilibrium and
    strength conditions.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    material : :class:`StrengthCriterion`
        material strength criterion
    """

    def __init__(self, mesh, material, **kwargs):
        TopologyOptimization.__init__(self, 0, 1, mesh, material, **kwargs)
        self.optimization_sense = "max"

    def set_up_problem(self):
        """
        Limit analysis problem.

        Optimization variables are :math:`\\lambda, \\sigma`.
        """
        self.prob = MosekProblem("limit analysis")
        self.lamb, self.sig = self.prob.add_var(
            [self.VR, self.Vsig], name=["Load factor", "Stress"]
        )

        def equilibrium(u):
            return self.lamb * self.Pext(u) - dot(self.sig, Eps(u)) * self.dx

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, bc=self.bc, name="Displacement"
        )

        self.prob.add_obj_func([1, 0])

        self.apply_strength_conditions()

    def apply_strength_conditions(self):
        """No density-dependent strength condition."""
        self.prob.add_convex_term(
            self.material.criterion(self.sig, quadrature_scheme="vertex")
        )


class VolumeMinimization(TopologyOptimization):
    """
    Single-material volume minimization problem.

    The loading is fixed to a reference value, we look for
    the minimum material volume such that there exists
    a stress field in equilibrium and complying with the
    density-dependent strength conditions.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    material : :class:`StrengthCriterion`
        material strength criterion
    """

    def __init__(self, mesh, material, **kwargs):
        TopologyOptimization.__init__(self, 1, 1, mesh, material, **kwargs)

    def set_up_problem(self):
        """
        Volume-minimization problem.

        Optimization variables are :math:`\\rho, \\sigma`.
        """
        self.prob = MosekProblem("volume minimization")
        self.rho, self.sig = self.prob.add_var(
            [self.Vrho, self.Vsig],
            lx=[self.rhomin, None],
            ux=[1, None],
            name=["Density", "Stress"],
        )

        def equilibrium(u):
            return dot(self.sig, Eps(u)) * self.dx

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, b=self.Pext, bc=self.bc, name="Displacement"
        )

        self.prob.add_obj_func(self.rho / self.volume * self.dx)

        self.apply_slope_control()

        self.apply_strength_conditions()
