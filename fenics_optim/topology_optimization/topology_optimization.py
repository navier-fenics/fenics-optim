#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Limit analysis-based topoology optimization module.

Supplementary file to the paper:

    Mourad L., Bleyer J., Mesnil R., Nseir J., Sab K., Raphael W.,
    Topoology optimization of load-bearing capacity,
    submitted to Structural and Multidisciplinary Optimization, 2020

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import as_matrix, sqrt, sym, grad, as_vector, jump, Max
from dolfin import (
    Measure,
    assemble,
    Constant,
    FunctionSpace,
    VectorFunctionSpace,
    Function,
    XDMFFile,
    plot,
)
from fenics_optim import L1Ball, L2Ball, Perspective
from fenics_optim.utils import local_project
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import json


def deviator(dim):
    """Deviatoric matrix operator."""
    if dim == 3:
        Q = as_matrix(
            [
                [2 / 3.0, -1 / 3.0, -1 / 3.0, 0, 0, 0],
                [-1 / 3.0, 2 / 3.0, -1 / 3.0, 0, 0, 0],
                [-1 / 3.0, -1 / 3.0, 2 / 3.0, 0, 0, 0],
                [0, 0, 0, sqrt(2), 0, 0],
                [0, 0, 0, 0, sqrt(2), 0],
                [0, 0, 0, 0, 0, sqrt(2)],
            ]
        )
    elif dim == 2:
        Q = as_matrix([[1 / 2.0, -1 / 2.0, 0], [-1 / 2.0, 1 / 2.0, 0], [0, 0, sqrt(2)]])
    return Q


def Eps(v):
    """Strain vector notation."""
    E = sym(grad(v))
    d = v.geometric_dimension()
    if d == 2:
        return as_vector([E[0, 0], E[1, 1], 2 * E[0, 1]])
    elif d == 3:
        return as_vector(
            [E[0, 0], E[1, 1], E[2, 2], 2 * E[0, 1], 2 * E[0, 2], 2 * E[1, 2]]
        )


def Heav(x):
    """Heaviside function."""
    return (1 + x / np.abs(x)) / 2


class TopologyOptimization:
    """
    Base class for limit analysis-based topology optimization problems.

    Parameters
    ----------
    nrho : int
        number of density variables
    nsig : int
        number of stress variables
    mesh : dolfin.Mesh
        mesh object
    material : StrengthCriterion
        material strength criterion
    facets : dolfin.MeshFunction, optional
        facets MeshFunction, by default None
    domains : MeshFunction, optional
        domains MeshFunction, by default None
    rho_interp : tuple, optional
        interpolation choice for rho field, by default ("CG", 1)
    user_parameters : dict, optional
        user parameters to be saved, by default None
    """

    def __init__(
        self,
        nrho,
        nsig,
        mesh,
        material,
        facets=None,
        domains=None,
        rho_interp=("CG", 1),
        user_parameters={},
        **kwargs
    ):
        self.mesh = mesh
        self.dim = self.mesh.geometric_dimension()
        self.facets = facets
        self.domains = domains
        self.optimization_sense = "min"
        self.ds = Measure("ds", subdomain_data=self.facets, domain=self.mesh)
        self.dx = Measure("dx", subdomain_data=self.domains, domain=self.mesh)
        self.volume = assemble(Constant(1) * self.dx)
        self.material = material
        if isinstance(material, list):
            self.criterion = "Composite {}/{}".format(
                *(mat.name() for mat in self.material)
            )
        else:
            self.criterion = material.name()
        self.nrho = nrho
        self.nsig = nsig
        self.rho = None
        self.sig = None

        self.rho_interp = rho_interp
        self.Vrho = FunctionSpace(mesh, *self.rho_interp)
        self.Vrho_out = FunctionSpace(mesh, "CG", 1)
        self.VR = FunctionSpace(self.mesh, "R", 0)
        if self.rho_interp == ("CG", 1):
            self.V = VectorFunctionSpace(mesh, "CG", 2)
        elif self.rho_interp == ("DG", 0):
            self.V = VectorFunctionSpace(mesh, "CG", 1)
        if self.dim == 2:
            dsig = 3
        elif self.dim == 3:
            dsig = 6
        self.Vsig = VectorFunctionSpace(self.mesh, "DG", 1, dim=dsig)

        self.Vsig0 = VectorFunctionSpace(self.mesh, "DG", 0, dim=dsig)
        self.Vvec = VectorFunctionSpace(mesh, "DG", 0)

        self.length_control = None

        self.set_functions()

        self.Pext = lambda u: 0
        self.rho_Pext0 = lambda u, rho: 0
        self.bc = []

        self.parameters = {
            "problem_type": self.__class__.__name__,
            "criterion": self.criterion,
            "rho_interp": self.rho_interp,
            "rho_min": self.rhomin,
            "length_control": None,
        }
        if isinstance(user_parameters, dict):
            self.parameters.update(user_parameters)
        self.parameters.update(**kwargs)

    def set_functions(self):
        """Initialize functions."""
        if self.nsig == 1:
            self.Sig = Function(self.Vsig0)
            self.eI = Function(self.Vvec, name="Scaled minor principal direction")
            self.eII = Function(self.Vvec, name="Scaled major principal direction")
        else:
            self.Sig = [
                Function(self.Vsig0, name="Stress phase {}".format(i + 1))
                for i in range(self.nsig)
            ]
            self.eI = [
                Function(
                    self.Vvec, name="Scaled minor principal direction {}".format(i + 1)
                )
                for i in range(self.nsig)
            ]
            self.eII = [
                Function(
                    self.Vvec, name="Scaled major principal direction {}".format(i + 1)
                )
                for i in range(self.nsig)
            ]

        self.rhomin = 0.0
        if self.nrho > 1:
            self.rho_old = []
            self.coeff = []
            for i in range(self.nrho):
                self.rho_old.append(
                    Function(self.Vrho, name="Previous density - {}".format(i + 1))
                )
                self.coeff.append(Function(self.Vrho, name="Coeff - {}".format(i + 1)))
                self.coeff[i].vector()[:] = 1
        else:
            self.rho_old = Function(self.Vrho, name="Density")
            self.coeff = Function(self.Vrho, name="Coeff")
            self.coeff.vector()[:] = 1

        self.rho_out = Function(self.Vrho_out, name="Density")

        self.p = Constant(1.0)
        self.exponent_counter = 0

    def apply_slope_control(self):
        """Slope control constraint."""
        if self.length_control is not None:
            if self.rho_interp == ("CG", 1):
                slope = L2Ball(grad(self.rho), k=1 / self.length_control)
            elif self.rho_interp == ("DG", 0):
                slope = L1Ball(
                    [jump(self.rho)],
                    k=self.mesh.hmin() / self.length_control,
                    on_facet=True,
                )
            self.prob.add_convex_term(slope)

    def apply_strength_conditions(self):
        """Strength condition constraints."""
        alp = (1 - self.p) * self.coeff * self.rho_old
        bet = self.p * self.coeff * self.rho
        crit = Perspective(
            self.material.criterion(self.sig, quadrature_scheme="vertex"), bet, t0=alp
        )
        self.prob.add_convex_term(crit)

    def set_up_problem(self):
        """Optimization problem definition."""
        self.prob = None

    def solve_problem(
        self,
        penalization=False,
        plots=True,
        output=None,
        verbose=1,
        pmax=2,
        nitermax=20,
        length_control=None,
        time_stamp=False,
        solver_parameters={},
    ):
        """Solves the topology optimization problem.

        Parameters
        ----------
        penalization : bool, optional
            activate the penalization procedure, by default False
        plots : bool, optional
            show density and stress field plots, by default True
        output : str, optional
            folder name for results output, by default None
        verbose : int, optional
            verbosity level of Mosek, by default 1
        pmax : int, optional
            maximum penalization exponent, by default 2
        nitermax : int, optional
            maximum penalization iteration number, by default 20
        length_control : float, optional
            length scale parameter for the slope control constraint, by default None
        time_stamp : bool, optional
            appends a time stamp subfolder to output path, by default False
        """
        self.pmax = pmax
        self.parameters.update({"penalization": penalization})
        self.parameters.update({"pmax": self.pmax})
        self.parameters.update({"niter_max": nitermax})
        if penalization:
            self.length_control = length_control
            self.parameters.update({"length_control": length_control})
        self.results = {"pobj": [], "p": [], "gray_level": []}
        if output:
            import os

            output_path = output + "/"
            if time_stamp:
                now = datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
                output_path += now + "/"
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            ffile = XDMFFile(output_path + "results.xdmf")
            ffile.parameters["functions_share_mesh"] = True
            ffile.parameters["flush_output"] = True
            with open(output_path + "parameters.json", "w") as fp:
                json.dump(self.parameters, fp)

        if penalization:
            print("Iteration | Objective | Gray level | SIMP exponent |")
        for it in range(nitermax):
            self.set_up_problem()
            self.prob.parameters["log_level"] = verbose
            self.prob.parameters["solve_form"] = "primal"
            self.prob.parameters["tol_rel_gap"] = 1e-4
            self.prob.parameters.update(solver_parameters)
            self.prob.optimize(sense=self.optimization_sense)

            self.u = self.prob.get_lagrange_multiplier("Displacement")
            self.compute_principal_stresses()
            self.results["pobj"].append(self.prob.pobj)
            if plots:
                # plt.figure()
                # plot(self.u)
                # plt.show()

                if self.nrho >= 2:
                    plt.figure(figsize=(12, 6))
                    for (i, rho) in enumerate(self.rho):
                        plt.subplot(1, self.nrho, i + 1)
                        pp = plot(rho, cmap="bone_r", vmin=0, vmax=1)
                        plt.colorbar(pp)
                elif self.nrho == 1:
                    plt.figure()
                    pp = plot(self.rho, cmap="bone_r", vmin=0, vmax=1)
                    plt.colorbar(pp)
                plt.title("Density")
                plt.show()

                if self.nsig >= 2:
                    plt.figure(figsize=(12, 6))
                    for (i, (eI, eII)) in enumerate(zip(self.eI, self.eII)):
                        plt.subplot(1, self.nsig, i + 1)
                        plot(eI)
                        plot(eII)
                else:
                    plt.figure()
                    plot(self.eI)
                    plot(self.eII)
                plt.title("Principal stresses")
                plt.show()

            if output:
                if self.nrho >= 2:
                    for rho in self.rho:
                        ffile.write(rho, it + 1)
                elif self.nrho == 1:
                    ffile.write(self.rho, it + 1)
                if self.nsig >= 2:
                    for eI, eII, Sig in zip(self.eI, self.eII, self.Sig):
                        ffile.write(eI, it + 1)
                        ffile.write(eII, it + 1)
                        ffile.write(Sig, it + 1)
                else:
                    ffile.write(self.eI, it + 1)
                    ffile.write(self.eII, it + 1)
                    ffile.write(self.Sig, it + 1)

                ffile.write(self.u, it + 1)
                with open(output_path + "results.json", "w") as fp:
                    json.dump(self.results, fp)

            if penalization:
                self.update_exponent()
                print(
                    "{:2d} | {} | {} | {}".format(
                        it + 1, self.prob.pobj, self.avg_gray_level, float(self.p)
                    )
                )
                self.results["p"].append(float(self.p))
                self.results["gray_level"].append(self.avg_gray_level)
                # self.rho.vector().set_local(Heav(self.rho.vector().get_local() - 0.5))
                # pp = plot(self.rho, cmap="bone_r", vmin=0, vmax=1)
                # plt.colorbar(pp)
                # plt.show()
            else:
                print("Objective:", self.prob.pobj)
                break

    def _extract_eigen_directions(self, Sigv):
        if self.dim == 2:
            Sigxx = Sigv[::3]
            Sigyy = Sigv[1::3]
            Sigxy = Sigv[2::3]
            n = Sigxx.shape[0]
            dI = np.zeros((2 * n,))
            dII = np.zeros((2 * n,))
            sI = np.zeros((n,))
            sII = np.zeros((n,))
            for i in range(n):
                S = np.array([[Sigxx[i], Sigxy[i]], [Sigxy[i], Sigyy[i]]])
                v, d = np.linalg.eig(S)
                idx = np.argsort(v)
                d = d[:, idx]
                v = v[idx]
                dI[2 * i : 2 * i + 2] = v[0] * d[:, 0]
                dII[2 * i : 2 * i + 2] = v[1] * d[:, 1]
                sI[i] = v[0]
                sII[i] = v[1]
        else:
            Sigxx = Sigv[::6]
            Sigyy = Sigv[1::6]
            Sigzz = Sigv[2::6]
            Sigxy = Sigv[3::6]
            Sigyz = Sigv[4::6]
            Sigxz = Sigv[5::6]
            n = Sigxx.shape[0]
            dI = np.zeros((3 * n,))
            dII = np.zeros((3 * n,))
            sI = np.zeros((n,))
            sII = np.zeros((n,))
            for i in range(n):
                S = np.array(
                    [
                        [Sigxx[i], Sigxy[i], Sigxz[i]],
                        [Sigxy[i], Sigyy[i], Sigyz[i]],
                        [Sigxz[i], Sigyz[i], Sigzz[i]],
                    ]
                )
                v, d = np.linalg.eig(S)
                idx = np.argsort(v)
                d = d[:, idx]
                v = v[idx]
                dI[3 * i : 3 * i + 3] = v[0] * d[:, 0]
                dII[3 * i : 3 * i + 3] = v[2] * d[:, 2]
                sI[i] = v[0]
                sII[i] = v[2]
        return dI, dII

    def compute_principal_stresses(self):
        """Extract stress fields eigenvalues and eigendirections."""
        if self.nsig >= 2:
            for (i, sig) in enumerate(self.sig):
                self.Sig[i].assign(local_project(sig, self.Vsig0))
                dI, dII = self._extract_eigen_directions(self.Sig[i].vector()[:])
                self.eI[i].vector()[:] = dI
                self.eII[i].vector()[:] = dII
        else:
            self.Sig.assign(local_project(self.sig, self.Vsig0))
            dI, dII = self._extract_eigen_directions(self.Sig.vector()[:])
            self.eI.vector().set_local(dI)
            self.eII.vector().set_local(dII)

    def update_exponent(self):
        """Penalization exponent update procedure."""
        self.exponent_counter += 1
        if self.nrho > 1:
            rho = Max(self.rho[0], self.rho[1])
        else:
            rho = self.rho
        self.avg_gray_level = (
            assemble((rho - self.rhomin) * (1.0 - rho) * self.dx) * 4 / self.volume
        )
        self.p.assign(
            Constant(min(float(self.p) * 1.1 ** (0.5 + self.avg_gray_level), self.pmax))
        )
        if self.nrho > 1:
            for i in range(self.nrho):
                self.rho_old[i].assign(self.rho[i])
                self.coeff[i].vector().set_local(
                    np.abs(self.rho_old[i].vector().get_local()) ** (float(self.p) - 1)
                )
        else:
            self.rho_old.assign(self.rho)
            self.coeff.vector().set_local(
                np.abs(self.rho_old.vector().get_local()) ** (float(self.p) - 1)
            )
