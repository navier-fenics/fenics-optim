#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multi-materials limit analysis-based topology optimization models.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import Constant
from fenics_optim import MosekProblem, Perspective, L2Ball, L1Ball, InequalityConstraint
from ufl import dot, grad, jump
from .topology_optimization import TopologyOptimization, Eps


class ReinforcementLoadMaximization(TopologyOptimization):
    """
    Reinforcement load maximization problem.

    We assume that the total stress field decomposes as the
    sum of two stress fields, one for each material. The first
    material is assumed to exist everywhere and we optimize
    the second material pseudo-density (reinforcement phase).

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    frac : float
        maximum volume fraction for the second material
    materials : list of :class:`StrengthCriterion`
        a list of two material strength criteria
    additive : bool, optional
        If true, both matrix and reinforcement stresses can coexist
        i.e. :math:`\\sigma \\in G_1 \\oplus \\rho G_2`
        else, only one material can exist at a time
        i.e. :math:`\\sigma \\in (1-\\rho)G_1 \\oplus \\rho G_2`
    """

    def __init__(self, mesh, frac, materials, additive=True, **kwargs):
        TopologyOptimization.__init__(
            self, 1, 2, mesh, materials, additive=additive, **kwargs
        )
        self.frac = frac
        self.optimization_sense = "max"
        self.parameters.update({"volume_fraction": frac})

    def set_up_problem(self):
        """
        Load-maximization problem.

        Optimization variables are :math:`\\lambda, \\rho, \\sigma_1, \\sigma_2`.
        with the total stress :math:`\\sigma= \\sigma_1 + \\sigma_2` being the
        sum of a matrix and reinforcement stress respectively.
        """
        self.prob = MosekProblem(self.__class__.__name__)
        self.lamb, self.rho, self.sig1, self.sig2 = self.prob.add_var(
            [self.VR, self.Vrho, self.Vsig, self.Vsig],
            lx=[None, self.rhomin, None, None],
            ux=[None, 1, None, None],
            name=["Load factor", "Density", "Stress Matrix", "Stress Reinforcement"],
        )
        self.sig = [self.sig1, self.sig2]

        def volume_frac(lamb):
            return lamb * self.rho * self.dx

        self.prob.add_ineq_constraint(self.VR, volume_frac, bu=self.frac * self.volume)

        def equilibrium(u):
            return (
                self.lamb * self.Pext(u)
                + self.rho_Pext0(u, self.rho)
                - dot(sum(self.sig), Eps(u)) * self.dx
            )

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, bc=self.bc, name="Displacement"
        )

        self.prob.add_obj_func([1, 0, 0, 0])

        self.apply_slope_control()

        self.apply_strength_conditions()

    def apply_strength_conditions(self):
        """Define the total strength conditions from partial strength criteria."""
        # FIXME : penalization strategy
        alp = (1 - self.p) * self.coeff * self.rho_old
        bet = self.p * self.coeff * self.rho
        alp2 = (1 - self.p) * (1 - self.rho_old) ** self.p
        bet2 = self.p * (1 - self.rho_old) ** (self.p - 1)
        # (y)^p = y0^p + p*y0^(p-1)*(y-yo) = (1-p)*y0^p
        assert len(self.material) == 2, "Two materials should be defined."
        crit1 = self.material[0].criterion(self.sig1, quadrature_scheme="vertex")
        crit2 = self.material[1].criterion(self.sig2, quadrature_scheme="vertex")
        if self.parameters.get("additive", True):
            self.prob.add_convex_term(crit1)
            self.prob.add_convex_term(Perspective(crit2, bet, t0=alp))
        else:
            self.prob.add_convex_term(
                Perspective(crit1, -bet2 * self.rho, t0=alp2 + bet2)
            )
            self.prob.add_convex_term(Perspective(crit2, bet, t0=alp))


class ReinforcementVolumeMinimization(TopologyOptimization):
    """
    Reinforcement volume minimization problem.

    We assume that the total stress field decomposes as the
    sum of two stress fields, one for each material. The first
    material is assumed to exist everywhere and we optimize
    the second material pseudo-density (reinforcement phase).

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    materials : list of :class:`StrengthCriterion`
        a list of two material strength criteria
    additive : bool, optional
        If true, both matrix and reinforcement stresses can coexist
        i.e. :math:`\\sigma \\in G_1 \\oplus \\rho G_2`
        else, only one material can exist at a time
        i.e. :math:`\\sigma \\in (1-\\rho)G_1 \\oplus \\rho G_2`
    """

    def __init__(self, mesh, materials, additive=True, **kwargs):
        TopologyOptimization.__init__(
            self, 1, 2, mesh, materials, additive=additive, **kwargs
        )

    def set_up_problem(self):
        """
        Volume minimization problem.

        Optimization variables are :math:`\\rho, \\sigma_1, \\sigma_2`.
        with the total stress :math:`\\sigma= \\sigma_1 + \\sigma_2` being the
        sum of a matrix and reinforcement stress respectively.
        """
        self.prob = MosekProblem(self.__class__.__name__)
        self.rho, self.sig1, self.sig2 = self.prob.add_var(
            [self.Vrho, self.Vsig, self.Vsig],
            lx=[self.rhomin, None, None],
            ux=[1, None, None],
            name=["Density", "Stress Matrix", "Stress Reinforcement"],
        )
        self.sig = [self.sig1, self.sig2]

        def equilibrium(u):
            return dot(sum(self.sig), Eps(u)) * self.dx

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, b=self.Pext, bc=self.bc, name="Displacement"
        )

        self.prob.add_obj_func(self.rho / self.volume * self.dx)

        self.apply_slope_control()

        self.apply_strength_conditions()

    def apply_strength_conditions(self):
        """Define the total strength conditions from partial strength criteria."""
        ReinforcementLoadMaximization.apply_strength_conditions(self)


class BimaterialVolumeMinimization(TopologyOptimization):
    """
    Bimaterial volume minimization problem.

    We assume that the total stress field decomposes as the
    sum of two stress fields, one for each material. We optimize
    the pseudo-density of both materials.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    materials : list of :class:`StrengthCriterion`
        a list of two material strength criteria
    """

    def __init__(self, mesh, material, **kwargs):
        TopologyOptimization.__init__(self, 2, 2, mesh, material, **kwargs)
        self.cost = [1, 1]

    def set_up_problem(self):
        """
        Volume minimization problem.

        Optimization variables are :math:`\\rho_1, \\sigma_1, \\rho_2 \\sigma_2`.
        with the total stress :math:`\\sigma= \\sigma_1 + \\sigma_2` being the
        sum of a two stresses with density-dependent strengths.
        """
        self.prob = MosekProblem(self.__class__.__name__)
        self.rho1, self.sig1, self.rho2, self.sig2 = self.prob.add_var(
            [self.Vrho, self.Vsig, self.Vrho, self.Vsig],
            lx=[self.rhomin, None, self.rhomin, None],
            ux=[1, None, 1, None],
            name=["Density-1", "Stress-1", "Density-2", "Stress-2"],
        )

        self.sig = [self.sig1, self.sig2]
        self.rho = [self.rho1, self.rho2]

        # equilibrium condition
        def equilibrium(u):
            return dot(self.sig1 + self.sig2, Eps(u)) * self.dx

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, b=self.Pext, bc=self.bc, name="Displacement"
        )

        # rho1 + rho2 <= 1 condition
        ineq = InequalityConstraint(
            self.rho1 + self.rho2, bu=1, quadrature_scheme="vertex"
        )
        self.prob.add_convex_term(ineq)

        self.prob.add_obj_func(
            (Constant(self.cost[0]) * self.rho1 + Constant(self.cost[1]) * self.rho2)
            / self.volume
            * self.dx
        )

        for rho in [self.rho1, self.rho2]:
            if self.length_control is not None:
                if self.rho_interp == ("CG", 1):
                    slope = L2Ball(grad(rho), k=1 / self.length_control)
                elif self.rho_interp == ("DG", 0):
                    slope = L1Ball(
                        [jump(rho)],
                        k=self.mesh.hmin() / self.length_control,
                        on_facet=True,
                    )
                self.prob.add_convex_term(slope)

        self.apply_strength_conditions()

        self.parameters.update({"cost": self.cost})

    def apply_strength_conditions(self):
        """Define the total strength conditions from partial strength criteria."""
        alp1 = (1 - self.p) * self.coeff[0] * self.rho_old[0]
        alp2 = (1 - self.p) * self.coeff[1] * self.rho_old[1]
        bet1 = self.p * self.coeff[0] * self.rho1
        bet2 = self.p * self.coeff[1] * self.rho2
        assert len(self.material) == 2
        crit1 = self.material[0].criterion(self.sig1, quadrature_scheme="vertex")
        crit2 = self.material[1].criterion(self.sig2, quadrature_scheme="vertex")
        self.prob.add_convex_term(Perspective(crit1, bet1, t0=alp1))
        self.prob.add_convex_term(Perspective(crit2, bet2, t0=alp2))


class BimaterialLoadMaximization(TopologyOptimization):
    """
    Bimaterial load maximization problem.

    We assume that the total stress field decomposes as the
    sum of two stress fields, one for each material. We optimize
    the pseudo-density of both materials.

    Parameters
    ----------
    mesh : dolfin.Mesh
        mesh object
    materials : list of :class:`StrengthCriterion`
        a list of two material strength criteria
    """

    def __init__(self, mesh, frac, material, **kwargs):
        TopologyOptimization.__init__(self, 2, 2, mesh, material, **kwargs)
        self.cost = [1, 1]
        self.frac = frac
        self.optimization_sense = "max"
        self.parameters.update({"volume_fraction": frac})

    def set_up_problem(self):
        """
        Load maximization problem.

        Optimization variables are :math:`\\rho_1, \\sigma_1, \\rho_2 \\sigma_2`.
        with the total stress :math:`\\sigma= \\sigma_1 + \\sigma_2` being the
        sum of a two stresses with density-dependent strengths.
        """
        self.prob = MosekProblem(self.__class__.__name__)
        self.lamb, self.rho1, self.sig1, self.rho2, self.sig2 = self.prob.add_var(
            [self.VR, self.Vrho, self.Vsig, self.Vrho, self.Vsig],
            lx=[None, self.rhomin, None, self.rhomin, None],
            ux=[None, 1, None, 1, None],
            name=["Load factor", "Density-1", "Stress-1", "Density-2", "Stress-2"],
        )

        self.sig = [self.sig1, self.sig2]
        self.rho = [self.rho1, self.rho2]

        def volume_frac(lamb):
            return (
                lamb
                * (
                    Constant(self.cost[0]) * self.rho1
                    + Constant(self.cost[1]) * self.rho2
                )
                * self.dx
            )

        self.prob.add_ineq_constraint(self.VR, volume_frac, bu=self.frac * self.volume)

        # equilibrium condition
        def equilibrium(u):
            return (
                self.lamb * self.Pext(u) - dot(self.sig1 + self.sig2, Eps(u)) * self.dx
            )

        self.prob.add_eq_constraint(
            self.V, A=equilibrium, bc=self.bc, name="Displacement"
        )

        # rho1 + rho2 <= 1 condition
        ineq = InequalityConstraint(
            self.rho1 + self.rho2, bu=1, quadrature_scheme="vertex"
        )
        self.prob.add_convex_term(ineq)

        self.prob.add_obj_func([1, 0, 0, 0, 0])

        for rho in [self.rho1, self.rho2]:
            if self.length_control is not None:
                if self.rho_interp == ("CG", 1):
                    slope = L2Ball(grad(rho), k=1 / self.length_control)
                elif self.rho_interp == ("DG", 0):
                    slope = L1Ball(
                        [jump(rho)],
                        k=self.mesh.hmin() / self.length_control,
                        on_facet=True,
                    )
                self.prob.add_convex_term(slope)

        self.apply_strength_conditions()

        self.parameters.update({"cost": self.cost})

    def apply_strength_conditions(self):
        """Define the total strength conditions from partial strength criteria."""
        alp1 = (1 - self.p) * self.coeff[0] * self.rho_old[0]
        alp2 = (1 - self.p) * self.coeff[1] * self.rho_old[1]
        bet1 = self.p * self.coeff[0] * self.rho1
        bet2 = self.p * self.coeff[1] * self.rho2
        assert len(self.material) == 2
        crit1 = self.material[0].criterion(self.sig1, quadrature_scheme="vertex")
        crit2 = self.material[1].criterion(self.sig2, quadrature_scheme="vertex")
        self.prob.add_convex_term(Perspective(crit1, bet1, t0=alp1))
        self.prob.add_convex_term(Perspective(crit2, bet2, t0=alp2))
