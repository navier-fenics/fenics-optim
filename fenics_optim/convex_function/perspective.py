#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Perspective class of a convex function.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import shape, replace
from fenics_optim.convex_function.base_convex_function import (
    ConvexFunction,
    reshape,
    my_sum,
)
from fenics_optim.utils import concatenate, get_slice


class Perspective(ConvexFunction):
    """
    Perspective :math:`\\text{persp}_f` of a convex function :math:`f(x)`.

    :math:`\\text{persp}_f(x, t) = t\\cdot f(x/t)`

    Optional: if a constant :math:`t_0` is given, then we have:
        :math:`\\text{persp}_f(x, t+t_0) = (t+t_0)\\cdot f(x/(t+t_0))`
    """

    def __init__(self, f, t, t0=0):
        self.f_function = f
        self.t = t
        self.t0 = t0
        x = f.expr
        if len(shape(x)) <= 1:
            xt = concatenate([x, t])
        else:
            raise NotImplementedError("Variable of f must be a scalar or a vector.")

        ConvexFunction.__init__(
            self,
            xt,
            parameters=f.parameters,
            interp_type=f.interp_type,
            quadrature_scheme=f.quadrature_scheme,
            degree=f.degree,
            measure=f.dx,
            on_facet=f.on_facet,
        )

    def _apply_on_problem(self, prob):
        self.f_function.set_term(self.f_function.expr)
        if self.parameters is None:
            self.f_function.conic_repr(self.f_function.X)
        elif isinstance(self.parameters, tuple):
            self.f_function.conic_repr(self.f_function.X, *self.parameters)
        else:
            self.f_function.conic_repr(self.f_function.X, self.parameters)

        self.set_term(self.expr)

        d = self.f_function.dim_x
        if d == 0:
            x = self.X[0]
        else:
            x = get_slice(self.X, end=d)
        t = self.X[-1]
        for cons in self.f_function.constraints:
            dim = cons["dim"]
            A = replace(my_sum(cons["A"]), {self.f_function.X: x})
            if cons["bu"] is not None:
                self.constraints.append(
                    {
                        "A": A - reshape(cons["bu"], dim) * t,
                        "bu": reshape(cons["bu"], dim) * self.t0,
                        "bl": None,
                        "dim": dim,
                        "V": cons["V"],
                        "name": cons["name"],
                    }
                )
            if cons["bl"] is not None:
                self.constraints.append(
                    {
                        "A": A - reshape(cons["bl"], dim) * self.t,
                        "bu": None,
                        "bl": reshape(cons["bl"], dim) * self.t0,
                        "dim": dim,
                        "V": cons["V"],
                        "name": cons["name"],
                    }
                )
        self.c = my_sum(self.f_function.c)

        prob.var += self.f_function.additional_variables
        prob.Vx += [v.function_space() for v in self.f_function.additional_variables]
        prob.cones += self.f_function.cones
        prob.ux += self.f_function.ux
        prob.lx += self.f_function.lx

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)
