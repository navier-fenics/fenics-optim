#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Marginal class of a convex function.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import shape, dot
from dolfin import Constant
from .base_convex_function import ConvexFunction


class Marginals(ConvexFunction):
    """
    Generalized marginal operator.

    :math:`f \\setminus  \\{{A_i,c_i\\}}(x) = \\inf_{{y_i}} \\sum_i c_i f(y_i)`

    such that :math:`x=\\sum_i A_i y_i`.

    Parameters
    ----------
    f : :class:`ConvexFunction`
        the original convex function. It can be defined using a dummy variable if
        needed.
    A_list : list
        a list of linear operators
    expr : UFL expression
        the UFL expression for the variable :math:`x`
    c_list : list, optional
        a list of positive coefficients
    lagrange_multiplier_name : str, optional
        the Lagrange multiplier name corresponding to the coupling constraint, by
        default None
    """

    def __init__(self, f, A_list, expr, c_list=None, lagrange_multiplier_name=None):
        self.f = f
        self.operators = A_list
        self.sum_coefficients = c_list
        self.lagrange_mulitplier_name = lagrange_multiplier_name

        ConvexFunction.__init__(
            self,
            expr,
            parameters=f.parameters,
            interp_type=f.interp_type,
            quadrature_scheme=f.quadrature_scheme,
            degree=f.degree,
            measure=f.dx,
        )

    def _apply_on_problem(self, prob):
        self.set_term(self.expr)
        self.f_list = []
        Y = []
        for op in self.operators:
            Yi = self.add_var(shape(op)[1])
            assert shape(op)[0] == len(self.expr)
            Y.append(Yi)

        prob.var += self.additional_variables
        prob.Vx += [v.function_space() for v in self.additional_variables]
        prob.cones += self.cones
        prob.ux += self.ux
        prob.lx += self.lx

        self.add_eq_constraint(
            self.expr - sum([dot(op, Yi) for Yi, op in zip(Y, self.operators)]),
            name=self.lagrange_mulitplier_name,
        )
        for i, (Yi, op) in enumerate(zip(Y, self.operators)):
            self.f.expr = Yi
            if self.sum_coefficients is not None:
                self.f.scale_factor = Constant(self.sum_coefficients[i])
            self.f._apply_on_problem(prob)
            self.f.additional_variables = []
            self.f.ux = []
            self.f.lx = []
            self.f.cones = []
            self.f.constraints = []
            self.c = Constant(0.0)
            # self.f.set_term(Yi)
            # if self.f.parameters is None:
            #     self.f.conic_repr(Yi)
            # elif isinstance(self.f.parameters, tuple):
            #     self.f.conic_repr(Yi, *self.f.parameters)
            # else:
            #     self.f.conic_repr(Yi, self.f.parameters)

            # for cons in self.f.constraints:
            #     self.constraints.append(
            #         {
            #             "A": cons["A"],
            #             "bu": cons["bu"],
            #             "bl": cons["bl"],
            #             "dim": cons["dim"],
            #             "V": cons["V"],
            #         }
            #     )

            # self.c += my_sum(self.f.c)

            # prob.var += self.f.additional_variables
            # prob.Vx += [v.function_space() for v in self.f.additional_variables]
            # prob.cones += self.f.cones
            # prob.ux += self.f.ux
            # prob.lx += self.f.lx

            # self.f._apply_constraints(prob)
            # self.f._apply_linear_terms(prob)

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)


class Marginal(Marginals):
    """
    Marginal :math:`f \\setminus A` of :math:`f(y)` through a linear operator :math:`A`.

    :math:`f \\setminus A(x) = \\inf_{{y}} f(y)`

    such that :math:`x=Ay`.
    """

    def __init__(self, f, A, expr):
        Marginals.__init__(self, f, [A], expr)
