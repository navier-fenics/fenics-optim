#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simple convex terms.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, as_vector, Identity, shape
from .base_convex_function import ConvexFunction
from fenics_optim.cones import RQuad
from fenics_optim.utils import tail


class EqualityConstraint(ConvexFunction):
    """Impose a linear equality constraint :math:`X = b`."""

    def __init__(self, x, b=0, **kwargs):
        ConvexFunction.__init__(self, x, parameters=b, **kwargs)

    def conic_repr(self, X, b):
        self.add_eq_constraint(X, b=b)


class InequalityConstraint(ConvexFunction):
    """Impose a linear inequality constraint :math:`bl <= X <= bu`."""

    def __init__(self, x, bl=None, bu=None, **kwargs):
        ConvexFunction.__init__(self, x, parameters=(bl, bu), **kwargs)

    def conic_repr(self, X, bl, bu):
        self.add_ineq_constraint(X, bl=bl, bu=bu)


class LinearTerm(ConvexFunction):
    """Define the linear function :math:`c^TX`."""

    def __init__(self, x, c, **kwargs):
        ConvexFunction.__init__(self, x, parameters=c, **kwargs)

    def conic_repr(self, X, c):
        self.set_linear_term(dot(c, X))


class QuadraticTerm(ConvexFunction):
    """Define the quadratic function :math:`\\frac{1}{2}(X-x_0)^TQ^TQ(X-x_0)`.

    Parameters
    ----------
    x : UFL expression
        optimization variable
    Q : matrix
        Cholesky matrix
    x0 : UFL expression
        constant term
    """

    def __init__(self, x, Q=1, x0=0, **kwargs):
        ConvexFunction.__init__(self, x, parameters=(Q, x0), **kwargs)

    def conic_repr(self, X, Q, x0):
        d = self.dim_x
        if d == 0:
            Y = self.add_var(3, cone=RQuad(3))
            self.add_eq_constraint(Q * X - Y[2], b=Q * x0)
        else:
            Y = self.add_var(d + 2, cone=RQuad(d + 2))
            if type(Q) in [float, int] or shape(Q) == ():
                Q = Q * Identity(d)
            if x0 == 0:
                b = 0
            else:
                b = dot(Q, x0)
            self.add_eq_constraint(
                dot(Q, X) - as_vector([Y[i] for i in range(2, d + 2)]), b=b
            )
        self.add_eq_constraint(Y[1], b=1)
        self.set_linear_term(Y[0])


class QuadOverLin(ConvexFunction):
    """Define the quadratic over linear function :math:`X=(t,y) \to y^T y/t`.

    Parameters
    ----------
    X : UFL expression
        optimization variable
    """

    def conic_repr(self, X):
        d = len(X)
        Y = self.add_var(d + 1, cone=RQuad(d + 1))
        self.add_eq_constraint(X - tail(Y))
        self.set_linear_term(2 * Y[0])
