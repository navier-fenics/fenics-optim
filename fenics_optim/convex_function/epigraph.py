#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Epigraph class of a convex function.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import shape, replace
from .base_convex_function import ConvexFunction, my_sum
from fenics_optim.utils import concatenate, get_slice


class Epigraph(ConvexFunction):
    """
    Epigraph constraint :math:`(x,t)\\in \\text{epi} f` of :math:`f(x)`.

    :math:`(x, t) \\in \\text{epi} f \\Leftrightarrow f(x) \\leq t`

    Optional: if a constant :math:`t_0` is given, then we have:
        :math:`(x, t+t_0) \\in \\text{epi} f`
    """

    def __init__(self, f, t, t0=0):
        self.f_function = f
        self.t = t
        self.t0 = t0
        x = f.expr
        if len(shape(x)) <= 1:
            xt = concatenate([x, t])
        else:
            raise NotImplementedError("Variable of f must be a scalar or a vector.")

        ConvexFunction.__init__(
            self,
            xt,
            parameters=f.parameters,
            interp_type=f.interp_type,
            quadrature_scheme=f.quadrature_scheme,
            degree=f.degree,
            measure=f.dx,
            on_facet=f.on_facet,
        )

    def _apply_on_problem(self, prob):
        self.f_function.set_term(self.f_function.expr)
        if self.parameters is None:
            self.f_function.conic_repr(self.f_function.X)
        elif isinstance(self.parameters, tuple):
            self.f_function.conic_repr(self.f_function.X, *self.parameters)
        else:
            self.f_function.conic_repr(self.f_function.X, self.parameters)

        self.set_term(self.expr)

        d = self.f_function.dim_x
        x = get_slice(self.X, end=d)
        t = self.X[-1]

        for cons in self.f_function.constraints:
            A = replace(my_sum(cons["A"]), {self.f_function.X: x})
            self.constraints.append(
                {
                    "A": A,
                    "bu": cons["bu"],
                    "bl": cons["bl"],
                    "dim": cons["dim"],
                    "V": cons["V"],
                    "name": cons["name"],
                }
            )

        self.add_ineq_constraint(my_sum(self.f_function.c) - t, bu=self.t0)

        prob.var += self.f_function.additional_variables
        prob.Vx += [v.function_space() for v in self.f_function.additional_variables]
        prob.cones += self.f_function.cones
        prob.ux += self.f_function.ux
        prob.lx += self.f_function.lx

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)
