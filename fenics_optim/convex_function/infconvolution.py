#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
InfConvolution class of a convex function.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import as_vector
from .base_convex_function import ConvexFunction, my_sum


class InfConvolution(ConvexFunction):
    """
    Inf-convolution :math:`f_1 \\square f_2` of :math:`f_1(x)` and :math:`f_2(x)`.

    :math:`f_1 \\square f_2(x) = \\inf_{{y_1,y_2}} f_1(y_1)+f_2(y_2)`

    such that :math:`x=y_1+y_2`

    Optional: if a list of indices :math:`I` is given, the inf-convolution is done only
    partially on the corresponding indices for the first function

        :math:`f_1 \\square f_2(x) = \\inf_{{y_1,y_2}} f_1(y_1, x_j)+f_2(y_2)`

    where :math:`x=(x_i, x_j)=(y_1+y_2, x_j)` with :math:`i\\in I, j\\not \\in I`
    """

    def __init__(self, f1, f2, indices=None):
        self.f1 = f1
        self.f2 = f2
        self.indices = indices
        if indices is None:
            assert f1.expr == f2.expr, "Both functions should have the same argument"
        else:
            assert len(f2.expr) == len(
                indices
            ), "The second function must have the same length as the indices list."
        assert (
            f1.interp_type == f2.interp_type
        ), "Both functions should have the same interpolation type."
        assert (
            f1.quadrature_scheme == f2.quadrature_scheme
        ), "Both functions should have the same quadrature scheme."
        assert (
            f1.degree == f2.degree
        ), "Both functions should have the same quadrature degree."
        assert f1.dx == f2.dx, "Both functions should have the same measure."
        assert f1.on_facet == f2.on_facet, "Both functions should have the same type."
        x = f1.expr

        ConvexFunction.__init__(
            self,
            x,
            parameters=f1.parameters,
            interp_type=f1.interp_type,
            quadrature_scheme=f1.quadrature_scheme,
            degree=f1.degree,
            measure=f1.dx,
            on_facet=f1.on_facet,
        )

    def _apply_on_problem(self, prob):
        self.set_term(self.expr)
        Y1 = self.add_var(len(self.f1.expr))
        Y2 = self.add_var(len(self.f2.expr))

        prob.var += self.additional_variables
        prob.Vx += [v.function_space() for v in self.additional_variables]
        prob.cones += self.cones
        prob.ux += self.ux
        prob.lx += self.lx
        if self.indices is None:
            self.add_eq_constraint(self.expr - Y1 - Y2)
        else:
            self.add_eq_constraint(
                as_vector([self.expr[i] - Y1[i] for i in self.indices]) - Y2
            )
            self.add_eq_constraint(
                as_vector(
                    [
                        self.expr[i] - Y1[i]
                        for i in range(len(Y1))
                        if i not in self.indices
                    ]
                )
            )

        for f, Y in [(self.f1, Y1), (self.f2, Y2)]:
            f.set_term(Y)
            if self.parameters is None:
                f.conic_repr(Y)
            elif isinstance(self.parameters, tuple):
                f.conic_repr(Y, *self.parameters)
            else:
                f.conic_repr(Y, self.parameters)

            for cons in f.constraints:
                self.constraints.append(
                    {
                        "A": cons["A"],
                        "bu": cons["bu"],
                        "bl": cons["bl"],
                        "dim": cons["dim"],
                        "V": cons["V"],
                        "name": cons["name"],
                    }
                )

            self.c += my_sum(f.c)

            prob.var += f.additional_variables
            prob.Vx += [v.function_space() for v in f.additional_variables]
            prob.cones += f.cones
            prob.ux += f.ux
            prob.lx += f.lx

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)
