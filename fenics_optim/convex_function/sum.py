#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sum class of a convex function.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .base_convex_function import ConvexFunction


class Sum(ConvexFunction):
    """
    Sum :math:`(f_1 + f_2)(x)` of :math:`f_1(x)` and :math:`f_2(x)`.

    This is equivalent to `add_convex_term(f1)` and `add_convex_term(f2)`, however,
    `Sum` yields a `ConvexFunction` object which can be further combined with other
    supported operations (e.g. `InfConvolution`, `Perspective`, etc.).
    """

    def __init__(self, f1, f2):
        self.f1 = f1
        self.f2 = f2
        assert (
            f1.interp_type == f2.interp_type
        ), "Both functions should have the same interpolation type."
        assert (
            f1.quadrature_scheme == f2.quadrature_scheme
        ), "Both functions should have the same quadrature scheme."
        assert (
            f1.degree == f2.degree
        ), "Both functions should have the same quadrature degree."
        assert f1.dx == f2.dx, "Both functions should have the same measure."
        assert f1.on_facet == f2.on_facet, "Both functions should have the same type."
        x = f1.expr

        ConvexFunction.__init__(
            self,
            x,
            parameters=f1.parameters,
            interp_type=f1.interp_type,
            quadrature_scheme=f1.quadrature_scheme,
            degree=f1.degree,
            measure=f1.dx,
            on_facet=f1.on_facet,
        )

    def _apply_on_problem(self, prob):
        self.set_term(self.expr)

        prob.var += self.additional_variables
        prob.Vx += [v.function_space() for v in self.additional_variables]
        prob.cones += self.cones
        prob.ux += self.ux
        prob.lx += self.lx

        for f in [self.f1, self.f2]:
            # f._apply_on_problem(prob)
            f.set_term(f.expr)
            if f.parameters is None:
                f.conic_repr(f.X)
            elif isinstance(f.parameters, tuple):
                f.conic_repr(f.X, *f.parameters)
            else:
                f.conic_repr(f.X, f.parameters)

            # for cons in f.constraints:
            #     self.constraints.append(
            #         {
            #             "A": cons["A"],
            #             "bu": cons["bu"],
            #             "bl": cons["bl"],
            #             "dim": cons["dim"],
            #             "V": cons["V"],
            #             "name": cons["name"],
            #         }
            #     )

            # self.c += my_sum(f.c)

            prob.var += f.additional_variables
            prob.Vx += [v.function_space() for v in f.additional_variables]
            prob.cones += f.cones
            prob.ux += f.ux
            prob.lx += f.lx

            f._apply_constraints(prob)
            f._apply_linear_terms(prob)

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)
