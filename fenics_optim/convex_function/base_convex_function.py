#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Base class for convex functions and mesh/facet children.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import ufl
from ufl import dot, as_vector, avg, shape, replace, derivative
from dolfin import (
    Measure,
    Constant,
    FacetNormal,
    Function,
    FunctionSpace,
    VectorFunctionSpace,
    TensorElement,
    VectorElement,
    FiniteElement,
    assemble,
    TestFunction,
)
from fenics_optim.utils import to_list
import warnings


class SumExpr(object):
    """
    Handle the sum of a variable and a constant expression.

    Parameters
    ----------
    X : UFL expression
        variable expression
    X0 : UFL expression
        constant expression
    """

    def __init__(self, X, X0):
        assert shape(X) == shape(X0), "Both expressions must have the same shape."
        self.X = X
        self.X0 = X0


class BaseConvexFunction(object):
    """Base class for generic convex functions.

    Parameters
    ----------
    expr : UFL expression
        UFL linear expression on which the convex function acts
    parameters : dict, optional
        convex function parameters (e.g. radius of a norm ball), by default None
    symmetric : bool, optional
        specify if tensors must be considered symmetric or not, by default True
    interp_type : str, optional
        interpolation type of auxiliary variables, by default "quadrature"
    quadrature_scheme : {"default", "vertex", "lobatto"}
        quadrature scheme for numerical quadrature and auxiliary variable
        discretization, by default "default", "vertex" scheme available for degree
        1 only.
    degree : int, optional
        quadrature scheme degree, by default 1
    measure : dolfin.Measure, optional
        integration measure, by default Measure("dx")
    """

    def __init__(
        self,
        expr,
        parameters=None,
        interp_type="quadrature",
        quadrature_scheme="default",
        degree=1,
        measure=Measure("dx"),
    ):

        if isinstance(expr, SumExpr):
            self.expr = expr.X
            self.expr_0 = expr.X0
        else:
            self.expr = expr
            self.expr_0 = None
        self.mesh = self._get_mesh_from_expr(self.expr)
        self.n = FacetNormal(self.mesh)
        self.parameters = parameters
        self.degree = degree
        self.quadrature_scheme = quadrature_scheme
        self.interp_type = interp_type
        metadata = {
            "quadrature_degree": self.degree,
            "quadrature_scheme": self.quadrature_scheme,
            "representation": "quadrature",
        }
        if isinstance(measure, list):
            self.dx = [
                m(domain=self.mesh, scheme=self.quadrature_scheme, degree=degree)
                for m in measure
            ]
        else:
            self.dx = measure(
                domain=self.mesh, scheme=self.quadrature_scheme, metadata=metadata
            )
        self.constraints = []
        self.cones = []
        self.additional_variables = []
        self.ux = []
        self.lx = []
        self.c = Constant(0)
        self.scale_factor = 1

    def _get_mesh_from_expr(self, expr):
        expr = to_list(expr)
        for e in expr:
            coeffs = ufl.algorithms.analysis.extract_coefficients(e)
            for c in coeffs:
                if hasattr(c, "function_space"):
                    return c.function_space().mesh()
        raise ValueError("Unable to extract mesh from UFL expression")

    def _set_dummy_function(self):
        """
        Declare the expression as a function unkown (in self.X).

        The corresponding UFL expression is replaced by a dummy
        Function living on the corresponding subspace in order
        to handle replacements or derivatives.

        Returns
        -------
        X : the dummy function
        """
        self.X = Function(self._generate_function_space(self.dim_x))

    def get_dimension(self, x):
        """Compute the dimension of the abstract expression, stored in dim_x."""
        if isinstance(x, list):
            assert (
                len({shape(xi) for xi in x}) == 1
            ), "Non matching shapes for variables list"
            self.dim_x = shape(x[0])
        else:
            self.dim_x = shape(x)
        if len(self.dim_x) == 0:
            # X is a scalar variable
            self.dim_x = 0
        elif len(self.dim_x) == 1:  # vector case, we flatten the dimension
            self.dim_x = self.dim_x[0]

    def _generate_function_space(self, d):
        pass

    def set_term(self, expr, *args):
        """
        Define on which expression of :math:`x` operates the convex function.

        A fictitious variable :math:`X` is then substituted to the expression
        when defining the function conic representation.

        Parameters
        ----------
        expr
            expression of the optimization variable :math:`x` on which acts
            the convex function.
            `expr` must be a (list of) linear operation on :math:`x`
            or a :class:`SumExpr` of :math:`x` and a constant term :math:`x_0`.

        Optional parameters can be defined.
        """
        if isinstance(expr, SumExpr):
            X = expr.X
            self.X_expr = X
        elif isinstance(expr, list):
            X = expr[0]
            self.X_expr = expr
        else:
            X = expr
            self.X_expr = X
        self.get_dimension(X)
        self._set_dummy_function()

    def conic_repr(self, X, *args):
        """Conic representation of the function f(X)."""
        pass

    def add_var(self, dim=0, cone=None, ux=None, lx=None, name=None):
        """
        Add a (list of) auxiliary optimization variable.

        These variables are local and their interpolation is defined through the chosen
        quadrature scheme. They are added to the block structure of the optimization
        problem by following their order of declaration. Inside a ConvexFunction, the
        block structure is :math:`z=[X, Y_0, Y_1, \\ldots, Y_n]` where :math:`X` is the
        global declared variable and each :math:`Y_i` are the additional variables.

        Parameters
        ----------
        dim : int, list of int
            dimension of each variable (0 for a scalar)
        cone : `Cone`, list of `Cone`
            cone in which each variable belongs (None if no constraint)
        ux : float, Function
            upper bound on variable :math:`x\\leq u_x`
        lx : float, Function
            lower bound on variable :math:`x\\leq l_x`
        name : str
            variable name
        """
        dim_list = to_list(dim)
        nlist = len(dim_list)
        new_V_add_var = [self._generate_function_space(d) for d in dim_list]
        self.cones += to_list(cone, nlist)
        self.ux += to_list(ux, nlist)
        self.lx += to_list(lx, nlist)
        if not isinstance(dim, list):
            new_Y = Function(new_V_add_var[0], name=name)
            self.additional_variables.append(new_Y)
            return new_Y
        else:
            new_Y = [
                Function(v, name=n)
                for (v, n) in zip(new_V_add_var, to_list(name, nlist))
            ]
            self.additional_variables += to_list(new_Y, nlist)
            return tuple(new_Y)

    def add_global_var(self, V, cone=None, ux=None, lx=None):
        """
        Add a (list of) global optimization variable.

        These variables are added to the block structure of the optimization
        problem by following their order of declaration.

        Parameters
        ----------
        V : (list of) `FunctionSpace`
            variable FunctionSpace
        cone : `Cone`, list of `Cone`
            cone in which each variable belongs (None if no constraint)
        ux : float, Function
            upper bound on variable :math:`x\\leq u_x`
        lx : float, Function
            lower bound on variable :math:`l_x\\leq x`
        """
        if isinstance(V, list):
            nlist = len(V)
        else:
            nlist = 1
            V = [V]
        self.cones += to_list(cone, nlist)
        self.ux += to_list(ux, nlist)
        self.lx += to_list(lx, nlist)
        if nlist == 1:
            new_Y = Function(V[0])
        else:
            new_Y = [Function(v) for v in V]
        self.additional_variables += to_list(new_Y, nlist)
        return new_Y

    def add_eq_constraint(self, Az, b=0, name=None):
        """
        Add an equality constraint :math:`Az=b`.

        `z` can contain a linear combination of X and local variables.

        Parameters
        ----------
        Az : UFL expression
            a UFL linear combination of X and local variables defining the
            linear constraint. We still support expressing Az as a list of
            linear expressions of X and local variable blocks. Use 0 or None for
            an empty block.
        b : float, expression
            corresponding right-hand side
        name : str, optional
            Lagrange-multiplier name for later retrieval.
        """
        self.add_ineq_constraint(Az, b, b, name)

    def add_ineq_constraint(self, Az, bu=None, bl=None, name=None):
        """
        Add an inequality constraint :math:`b_l \\leq Az \\leq b_u`.

        `z` can contain a linear combination of X and local variables.

        Parameters
        ----------
        Az : UFL expression
            a UFL linear combination of X and local variables defining the
            linear constraint. We still support expressing Az as a list of
            linear expressions of X and local variable blocks. Use 0 or None for
            an empty block.
        b_l : float, expression
            corresponding lower bound. Ignored if None.
        b_u : float, expression
            corresponding upper bound. Ignored if None
        name : str, optional
            Lagrange-multiplier name for later retrieval.
        """
        if isinstance(Az, list):
            shapes = list(
                filter(
                    lambda x: x is not None,
                    [shape(a) if a is not None else None for a in Az],
                )
            )
        else:
            shapes = [shape(Az)]
        if len(set(shapes)) != 1:
            raise ValueError("Shapes of constraint are not equal: " + str(shapes))
        if len(shapes[0]) == 0:  # scalar constraint
            dim = 0
        else:
            dim = shapes[0][0]
        self.constraints.append(
            {
                "A": Az,
                "bu": bu,
                "bl": bl,
                "dim": dim,
                "V": self._generate_function_space(dim),
                "name": name,
            }
        )

    def set_linear_term(self, cz):
        """
        Add a linear combination term of X and local variables.

        Parameters
        ----------
        cz : UFL expression
            a UFL linear combination of X and local variables defining the
            linear objective. We still support expressing Az as a list of
            linear expressions of :math:`z`-blocks. Use 0 or None for an empty block.
        """
        self.c = cz

    def __rmul__(self, alpha):
        """Allow multiplication by a scalar."""
        if type(alpha) in [float, int, Constant] or isinstance(
            alpha, ufl.core.expr.Expr
        ):
            self.scale_factor = alpha
        return self

    def _apply_on_problem(self, prob):
        """Append function variables, constraints and objective to global problem."""
        self.set_term(self.expr)
        if self.parameters is None:
            self.conic_repr(self.X)
        elif isinstance(self.parameters, tuple):
            self.conic_repr(self.X, *self.parameters)
        else:
            self.conic_repr(self.X, self.parameters)
        self._update_rhs(self.expr_0)

    def _update_rhs(self, X0):
        """Update constraint rhs with constant term."""
        if X0 is not None:
            for cons in self.constraints:
                rhs = derivative(cons["A"], self.X, self.expr_0)
                if len(shape(rhs)) == 0:
                    size = 0
                elif len(shape(rhs)) == 1:
                    size = shape(rhs)[0]
                if cons["bl"] is not None:
                    cons["bl"] = reshape(cons["bl"], size) - rhs
                if cons["bu"] is not None:
                    cons["bu"] = reshape(cons["bu"], size) - rhs


class MeshConvexFunction(BaseConvexFunction):
    """A convex function defined on element cells."""

    def _generate_function_space(self, d):
        if self.interp_type == "quadrature":
            if isinstance(d, tuple):
                element = TensorElement(
                    "Quadrature",
                    self.mesh.ufl_cell(),
                    degree=self.degree,
                    shape=d,
                    quad_scheme=self.dx.metadata()["quadrature_scheme"],
                )
            else:
                if d > 0:
                    element = VectorElement(
                        "Quadrature",
                        self.mesh.ufl_cell(),
                        degree=self.degree,
                        dim=d,
                        quad_scheme=self.dx.metadata()["quadrature_scheme"],
                    )
                else:
                    element = FiniteElement(
                        "Quadrature",
                        self.mesh.ufl_cell(),
                        degree=self.degree,
                        quad_scheme=self.dx.metadata()["quadrature_scheme"],
                    )
            return FunctionSpace(self.mesh, element)
        elif self.interp_type == "vertex":
            if d > 0:
                element = VectorElement(
                    "DG", self.mesh.ufl_cell(), degree=self.degree, dim=d
                )
            else:
                element = FiniteElement("DG", self.mesh.ufl_cell(), degree=self.degree)
            return FunctionSpace(self.mesh, element)

    def _apply_on_problem(self, prob):
        BaseConvexFunction._apply_on_problem(self, prob)

        prob.var += self.additional_variables
        prob.Vx += [v.function_space() for v in self.additional_variables]
        prob.cones += self.cones
        prob.ux += self.ux
        prob.lx += self.lx
        self._apply_constraints(prob)
        self._apply_linear_terms(prob)

    def _apply_constraints(self, prob):
        for constraint in self.constraints:

            def constraint_func(Z):
                c_var = Constant(0) * self.dx
                if isinstance(constraint["A"], list):
                    warnings.warn(
                        "Block constraints will no longer be supported in next release",
                        DeprecationWarning,
                    )
                    if constraint["A"][0] not in [None, 0]:
                        c_var += (
                            dot(
                                Z,
                                replace(constraint["A"][0], {self.X: self.X_expr}),
                            )
                            * self.dx
                        )
                    for Ai in constraint["A"][1:]:
                        if Ai is not None:
                            c_var += dot(Z, Ai) * self.dx
                else:
                    c_var = (
                        dot(Z, replace(constraint["A"], {self.X: self.X_expr}))
                        * self.dx
                    )

                return c_var

            def constraint_rhs(b):
                if b is None:
                    return None
                elif b == 0:
                    return 0
                elif type(b) == list:
                    return lambda Z: dot(Z, as_vector(b)) * self.dx
                else:
                    return lambda Z: dot(Z, b) * self.dx

            bu, bl = constraint["bu"], constraint["bl"]
            prob.add_ineq_constraint(
                constraint["V"],
                A=constraint_func,
                bu=constraint_rhs(bu),
                bl=constraint_rhs(bl),
                name=constraint["name"],
            )

    def _apply_linear_terms(self, prob):
        nvar = len(prob.var)
        self.c_list = [None] * nvar
        if isinstance(self.c, list):
            warnings.warn(
                "Block linear terms will no longer be supported in next release",
                DeprecationWarning,
            )
            if self.c[0] is not None:
                for p in range(nvar):
                    c0 = replace(self.c[0] * self.dx, {self.X: self.X_expr})
                    self.c_list[p] = c0
            for (i, ci) in enumerate(self.c[1:]):
                if ci is not None:
                    self.c_list[nvar + i] = ci * self.dx
            prob.add_obj_func(
                [self.scale_factor * c if c is not None else None for c in self.c_list]
            )
        else:
            c = replace(self.scale_factor * self.c, {self.X: self.X_expr}) * self.dx
            prob.add_obj_func(c)

    def compute_cellwise(self):
        """Compute the value of int f*dx per cell."""
        V0 = FunctionSpace(self.mesh, "DG", 0)
        y_ = TestFunction(V0)
        y0 = Function(V0)
        if self.c[0] is None:
            f0 = []
        else:
            f0 = [y_ * replace(self.c[0], {self.X: self.X_expr}) * self.dx]
        f = f0 + [y_ * c * self.dx for c in self.c[1:] if c is not None]
        y0.vector().set_local(assemble(sum(f)).get_local())
        return y0


def reshape(v, n):
    """Reshape constant term to vector of size n."""
    if shape(v) == () and n > 0:
        return as_vector([v] * n)
    else:
        return v


def my_sum(x):
    """Sum over list with possible None."""
    if isinstance(x, list):
        if x == [None]:
            return Constant(0)
        else:
            return sum([xi for xi in x if xi is not None])
    else:
        return x


def my_restrict(X, measure):
    """Restrict the function on an interior facet."""
    if measure.integral_type() == "interior_facet":
        return avg(X)
    else:
        return X


class FacetConvexFunction(BaseConvexFunction):
    """
    A convex function defined on mesh facets.

    Parameters
    ----------
    x
        the block variable on which acts the convex function
    quadrature_scheme : {"default", "vertex", "lobatto"}
        quadrature scheme used for the integral computation and additional variables
        discretization. "vertex" scheme is used only with degree=1
    degree : int
        quadrature degree
    measure : list of `Measure`
        list of measures used for the integral. The first measure corresponds
        to **internal edges** (`dS`). The second measure correspond to
        **external edges** (`ds`).
    """

    def _generate_function_space(self, d):
        if d > 0:
            return VectorFunctionSpace(
                self.mesh, "Discontinuous Lagrange Trace", self.degree, dim=d
            )
        else:
            return FunctionSpace(self.mesh, "Discontinuous Lagrange Trace", self.degree)

    def _apply_on_problem(self, prob):
        BaseConvexFunction._apply_on_problem(self, prob)
        prob.var += self.additional_variables
        prob.Vx += [v.function_space() for v in self.additional_variables]
        prob.cones += self.cones
        prob.ux += self.ux
        prob.lx += self.lx

        self._apply_constraints(prob)
        self._apply_linear_terms(prob)

    def _apply_constraints(self, prob):
        nvar = len(prob.var)
        for constraint in self.constraints:
            c_var = [None] * nvar

            def constraint_func(Z):
                if isinstance(constraint["A"], list):
                    warnings.warn(
                        "Block constraints will no longer be supported in next release",
                        DeprecationWarning,
                    )
                    if constraint["A"][0] not in [None, 0]:
                        for p in range(nvar):
                            c_var[p] = sum(
                                [
                                    dot(
                                        my_restrict(Z, self.dx[i]),
                                        replace(constraint["A"][0], {self.X: Xi}),
                                    )
                                    * self.dx[i]
                                    for (i, Xi) in enumerate(self.X_expr)
                                ]
                            )
                    return c_var + [
                        sum(
                            [
                                dot(
                                    my_restrict(Z, dx),
                                    replace(Ai, {Yi: my_restrict(Yi, dx)}),
                                )
                                * dx
                                for dx in self.dx
                            ]
                        )
                        if Ai is not None
                        else None
                        for (Yi, Ai) in zip(
                            self.additional_variables, constraint["A"][1:]
                        )
                    ]
                else:
                    ret = []
                    for (dx, Xi) in zip(self.dx, self.X_expr):
                        cons = replace(constraint["A"], {self.X: Xi})
                        for Yi in self.additional_variables:
                            cons = replace(cons, {Yi: my_restrict(Yi, dx)})
                        ret.append(dot(my_restrict(Z, dx), cons) * dx)
                    return sum(ret)

            def constraint_rhs(b):
                if b is None:
                    return None
                elif b == 0:
                    return 0
                elif type(b) == list:
                    b = as_vector(b)
                else:
                    b
                    return lambda Z: sum(
                        [
                            dot(my_restrict(Z, dx), my_restrict(as_vector(b), dx)) * dx
                            for dx in self.dx
                        ]
                    )

            bu, bl = constraint["bu"], constraint["bl"]
            prob.add_ineq_constraint(
                constraint["V"],
                A=constraint_func,
                bu=constraint_rhs(bu),
                bl=constraint_rhs(bl),
                name=constraint["name"],
            )

    def _apply_linear_terms(self, prob):
        nvar = len(prob.var)
        c_list = [None] * nvar
        if isinstance(self.c, list):
            warnings.warn(
                "Block linear terms will no longer be supported in next release",
                DeprecationWarning,
            )
            if self.c[0] is not None:
                for p in range(nvar):
                    c_list[p] = sum(
                        [
                            replace(self.c[0] * self.dx[i], {self.X: Xi})
                            for (i, Xi) in enumerate(self.X_expr)
                        ]
                    )
            for (i, ci) in enumerate(self.c[1:]):
                if ci is not None:
                    c_list[nvar + i] = sum(
                        [
                            replace(
                                ci * dx,
                                {
                                    prob.var[nvar + i]: my_restrict(
                                        prob.var[nvar + i], dx
                                    )
                                },
                            )
                            for dx in self.dx
                        ]
                    )
            prob.add_obj_func(
                [self.scale_factor * c if c is not None else None for c in c_list]
            )
        else:
            c_list = []
            for (dx, Xi) in zip(self.dx, self.X_expr):
                c = replace(self.c, {self.X: Xi}) * dx
                for Yi in self.additional_variables:
                    c = replace(c, {Yi: my_restrict(Yi, dx)})
                c_list.append(c)
            prob.add_obj_func(self.scale_factor * sum(c_list))

    def compute_cellwise(self):
        """Compute the value of int f*dx per cell."""
        V0 = FunctionSpace(self.mesh, "DG", 0)
        y_ = TestFunction(V0)
        y0 = Function(V0)
        if self.c[0] is None:
            f0 = []
        else:
            f0 = [
                replace(avg(y_) * self.c[0] * self.dx[i], {self.X: Xi})
                for (i, Xi) in enumerate(self.X_expr)
            ]
        for (i, ci) in enumerate(self.c[1:]):
            yi = self.additional_variables[i]
            if ci is not None:
                f0 += [
                    replace(avg(y_) * ci * self.dx[0], {yi: avg(yi)}),
                    y_ * ci * self.dx[1],
                ]
        y0.vector().set_local(assemble(sum(f0)).get_local())
        return y0


class ConvexFunction(BaseConvexFunction):
    """
    A composite convex function which acts by default as a `MeshConvexFunction`.

    Parameters
    ----------
    expr : UFL expression
        UFL linear expression on which the convex function acts
    parameters : dict, optional
        convex function parameters (e.g. radius of a norm ball), by default None
    symmetric : bool, optional
        specify if tensors must be considered symmetric or not, by default True
    interp_type : str, optional
        interpolation type of auxiliary variables, by default "quadrature"
    quadrature_scheme : {"default", "vertex", "lobatto"}
        quadrature scheme for numerical quadrature and auxiliary variable
        discretization, by default "default", "vertex" scheme available for degree
        1 only.
    degree : int, optional
        quadrature scheme degree, by default 1
    measure : dolfin.Measure, optional
        integration measure, by default Measure("dx")
    on_facet : bool, optional
        specify whether the function lives on facets, by default False
    """

    def __init__(
        self,
        expr,
        parameters=None,
        interp_type="quadrature",
        quadrature_scheme=None,
        degree=None,
        measure=None,
        on_facet=False,
    ):
        self.on_facet = on_facet
        if self.on_facet:
            if measure is None:
                measure = [Measure("dS"), Measure("ds")]
            if degree is None:
                degree = 2
            if quadrature_scheme is None:
                quadrature_scheme = "lobatto"
            FacetConvexFunction.__init__(
                self,
                expr,
                parameters=parameters,
                interp_type=interp_type,
                quadrature_scheme=quadrature_scheme,
                degree=degree,
                measure=measure,
            )
        else:
            if measure is None:
                measure = Measure("dx")
            if degree is None:
                degree = 1
            if quadrature_scheme is None:
                quadrature_scheme = "default"
            MeshConvexFunction.__init__(
                self,
                expr,
                parameters=parameters,
                interp_type=interp_type,
                quadrature_scheme=quadrature_scheme,
                degree=degree,
                measure=measure,
            )
        if self.on_facet:
            self._base_class = FacetConvexFunction
        else:
            self._base_class = MeshConvexFunction

    def _generate_function_space(self, d):
        return self._base_class._generate_function_space(self, d)

    def _apply_on_problem(self, prob):
        return self._base_class._apply_on_problem(self, prob)

    def _apply_constraints(self, prob):
        return self._base_class._apply_constraints(self, prob)

    def _apply_linear_terms(self, prob):
        return self._base_class._apply_linear_terms(self, prob)

    def compute_cellwise(self):
        """Compute the value of int f*dx per cell."""
        return self._base_class.compute_cellwise(self)
