#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Pre-implemented useful norms and balls.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import as_vector
from .base_convex_function import ConvexFunction
from fenics_optim.cones import Quad


class L2Norm(ConvexFunction):
    """Define the scaled L2-norm function :math:`k||X||_2`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k):
        d = self.dim_x
        if d == 0:
            Y = self.add_var(2, cone=Quad(2))
            self.add_eq_constraint(X - Y[1])
        else:
            Y = self.add_var(d + 1, cone=Quad(d + 1))
            self.add_eq_constraint(X - as_vector([Y[i] for i in range(1, d + 1)]))
        self.set_linear_term(k * Y[0])


class L1Norm(ConvexFunction):
    """Define the scaled L1-norm function :math:`k||X||_1`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k):
        d = self.dim_x
        Y = self.add_var(d)
        self.add_ineq_constraint(X - Y, bu=0)
        self.add_ineq_constraint(-X - Y, bu=0)
        if d == 0:
            self.set_linear_term(k * Y)
        else:
            self.set_linear_term(k * sum(Y))


class AbsValue(L1Norm):
    """Define the scaled absolute value function :math:`k|X|`."""

    pass


class LinfNorm(ConvexFunction):
    """Define the scaled Linf-norm function :math:`k||X||_\\infty`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k):
        d = self.dim_x
        Y = self.add_var()
        if d == 0:
            e = 1
        else:
            e = as_vector(
                [
                    1,
                ]
                * d
            )
        self.add_ineq_constraint(X - Y * e, bu=0)
        self.add_ineq_constraint(-X - Y * e, bu=0)
        self.set_linear_term(k * Y)


class L2Ball(ConvexFunction):
    """Define the scaled L2-ball constraint :math:`||X||_2 \\leq k`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k=1):
        d = self.dim_x
        Y = self.add_var(d + 1, cone=Quad(d + 1))
        self.add_eq_constraint(X - as_vector([Y[i] for i in range(1, d + 1)]))
        self.add_eq_constraint(Y[0], b=k)


class L1Ball(ConvexFunction):
    """.Defines the scaled L1-ball constraint :math:`||X||_1 \\leq k`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k=1):
        d = self.dim_x
        Y = self.add_var(d)
        self.add_ineq_constraint(X - Y, bu=0)
        self.add_ineq_constraint(-X - Y, bu=0)
        if d == 0:
            self.add_eq_constraint(Y, b=k)
        else:
            self.add_eq_constraint(sum(Y), b=k)


class LinfBall(ConvexFunction):
    """Defines the scaled Linf-ball constraint :math:`||X||_\\infty \\leq k`."""

    def __init__(self, x, k=1, **kwargs):
        ConvexFunction.__init__(self, x, parameters=k, **kwargs)

    def conic_repr(self, X, k=1):
        d = self.dim_x
        Y = self.add_var()
        if d == 0:
            e = 1
        else:
            e = as_vector(
                [
                    1,
                ]
                * d
            )
        self.add_ineq_constraint(X - Y * e, bu=0)
        self.add_ineq_constraint(-X - Y * e, bu=0)
        self.add_eq_constraint(Y, b=k)
