#!/usr/bin/env python
# coding: utf-8
"""Utility functions for mesh conversion from GMSH to dolfin."""
import meshio
import numpy as np
from dolfin import Mesh, MeshValueCollection, MeshFunction, XDMFFile


# from J. Dokken website
def create_mesh(mesh, cell_type, prune_z=False):
    """Generate a proper mesh from a meshio data structure."""
    cells = np.vstack([cell.data for cell in mesh.cells if cell.type == cell_type])
    data = np.hstack(
        [
            mesh.cell_data_dict["gmsh:physical"][key]
            for key in mesh.cell_data_dict["gmsh:physical"].keys()
            if key == cell_type
        ]
    )
    if prune_z:
        points = mesh.points[:, :2]
    else:
        points = mesh.points
    mesh = meshio.Mesh(
        points=points, cells={cell_type: cells}, cell_data={"name_to_read": [data]}
    )
    return mesh


def import_msh(filename):
    """Import a .msh file and return a dolfin mesh, domains and facets meshfunctions."""
    xdmf_filename = filename.replace(".msh", ".xdmf")
    facets_xdmf_filename = filename.replace(".msh", "_facets.xdmf")
    msh_mesh = meshio.read(filename)

    if "tetra" in msh_mesh.cells_dict.keys():
        dim = 3
        domain_mesh = create_mesh(msh_mesh, "tetra")
        facets_mesh = create_mesh(msh_mesh, "triangle")
    elif "triangle" in msh_mesh.cells_dict.keys():
        dim = 2
        domain_mesh = create_mesh(msh_mesh, "triangle", True)
        facets_mesh = create_mesh(msh_mesh, "line", True)

    meshio.write(xdmf_filename, domain_mesh)
    mesh = Mesh()
    mvc = MeshValueCollection("size_t", mesh, dim)
    with XDMFFile(xdmf_filename) as infile:
        infile.read(mesh)
        infile.read(mvc, "name_to_read")
    domains = MeshFunction("size_t", mesh, mvc)

    meshio.write(facets_xdmf_filename, facets_mesh)
    mvc = MeshValueCollection("size_t", mesh, dim - 1)
    with XDMFFile(facets_xdmf_filename) as infile:
        infile.read(mvc, "name_to_read")
    facets = MeshFunction("size_t", mesh, mvc)

    return mesh, domains, facets
