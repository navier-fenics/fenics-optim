#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Automated formulation and resolution of convex variational problems.

fenics_optim is an open-source library that provides an interface
between FEniCS and convex optimization solvers like Mosek. It enables to
define and solve easily convex variational problems discretized using
finite elements.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import dolfin as df
import os
import warnings
from ffc.quadrature.deprecation import QuadratureRepresentationDeprecationWarning

try:  # exception in order to handle mock import by sphinx
    df.parameters["form_compiler"]["representation"] = "quadrature"
    warnings.simplefilter("ignore", QuadratureRepresentationDeprecationWarning)
except:
    pass

from .cones import Quad, RQuad, Product, SDP, Pow, Exp
from .mosek_io import MosekProblem

from .convex_function.base_convex_function import ConvexFunction, SumExpr
from .convex_function.epigraph import Epigraph
from .convex_function.infconvolution import InfConvolution
from .convex_function.sum import Sum
from .convex_function.perspective import Perspective
from .convex_function.marginal import Marginal, Marginals
from .convex_function.norms import (
    L1Ball,
    L1Norm,
    L2Ball,
    L2Norm,
    LinfBall,
    LinfNorm,
    AbsValue,
)
from .convex_function.simple_terms import (
    EqualityConstraint,
    InequalityConstraint,
    LinearTerm,
    QuadraticTerm,
    QuadOverLin,
)

from .utils import (
    to_list,
    to_vect,
    to_mat,
    concatenate,
    hstack,
    vstack,
    block_matrix,
    tail,
    get_slice,
    local_frame,
    facet_project,
    cell_project,
    dummy_variable,
)

from .custom_quadrature_schemes import monkey_patch_fiat_quadrature_scheme

monkey_patch_fiat_quadrature_scheme()

# reads version from file
with open(os.path.join(os.path.dirname(__file__), "VERSION.txt")) as version_file:
    __version__ = version_file.read().strip()
