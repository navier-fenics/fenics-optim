#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
mosek_io provides interface to the Mosek optimization solver.

All types of conic programming (LP, SOCP, SDP, power and exponential cones)
are supported.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from dolfin import (
    Function,
    FunctionSpace,
    TestFunction,
    TrialFunction,
    assemble,
    as_backend_type,
    derivative,
    __version__
)
import scipy.sparse as sp
import numpy as np
import sys
import mosek
import ufl
from fenics_optim.utils import (
    to_list,
    subk_list,
    subl_list,
    half_vect2subk,
    half_vect2subl,
)
from fenics_optim import Product, SDP
from itertools import compress

# unimportant value to denote infinite bounds
inf = 1e30

MOSEK_CONE_TYPES = {"quad": mosek.conetype.quad, "rquad": mosek.conetype.rquad}
version = mosek.Env().getversion()
if version >= (9, 0, 0):
    MOSEK_CONE_TYPES.update(
        {
            "ppow": mosek.conetype.ppow,
            "dpow": mosek.conetype.dpow,
            "pexp": mosek.conetype.pexp,
            "dexp": mosek.conetype.dexp,
        }
    )


class MosekProblem:
    """A generic optimization problem using the Mosek optimization solver."""

    def __init__(self, name=""):
        self.name = name
        self.Vx = []
        self.Vy = []
        self.lagrange_multiplier_names = []
        self.cones = []
        self.ux = []
        self.lx = []
        self.var = []
        self.int_var = []
        self.A = []
        self.bu = []
        self.bl = []
        self.bc_dual = []
        self.bc_prim = []
        self.c = []
        self.parameters = self._default_parameters()

    def _default_parameters(self):
        return {
            "presolve": True,
            "presolve_lindep": False,
            "log_level": 10,
            "tol_rel_gap": 1e-7,
            "solve_form": "free",
            "num_threads": 0,
            "dump_file": None,
        }

    def add_var(
        self, V, cone=None, lx=None, ux=None, bc=None, name=None, int_var=False
    ):
        """Add a (list of) optimization variable.

        The added variables belong to the corresponding FunctionSpace V.

        Parameters
        ----------
        V : (list of) `FunctionSpace`
            variable FunctionSpace
        cone : (list of) `Cone`
            cone in which each variable belongs (None if no constraint)
        ux : (list of) float, Function
            upper bound on variable :math:`x \\leq u_x`
        lx : (list of) float, Function
            lower bound on variable :math:`l_x \\leq x`
        bc : (list of) `DirichletBC`
            boundary conditions applied to the variables (None if no bcs)
        name : (list of) str
            name of the associated functions
        int_var : (list of) bool
            True if variable is an integer, False if it is continuous (default)

        Returns
        -------
        x : Function tuple
            optimization variables
        """
        if not isinstance(V, list):
            V_list = [V]
            bc_list = [bc]
        else:
            V_list = V
            bc_list = bc
        nlist = len(V_list)

        self.lx += to_list(lx, nlist)
        self.ux += to_list(ux, nlist)
        self.cones += to_list(cone, nlist)
        self.bc_prim += to_list(bc_list, nlist)
        name_list = to_list(name, nlist)
        self.int_var += to_list(int_var, nlist)

        new_var = [Function(v, name=n) for (v, n) in zip(V_list, name_list)]
        self.var += new_var
        self.Vx += V_list

        if nlist == 1:
            return new_var[0]

        return tuple(new_var)

    def _update_names(self):
        """Check for similar variable names and appends indices at the end."""
        names = [v.name() for v in self.var]
        for (i, name) in enumerate(names):
            j = 0
            while name in names[i + 1 :]:
                k = names.index(name)
                names[k] = name + "-" + str(j)
                j += 1
        for (v, name) in zip(self.var, names):
            v.rename(name, v.name())

    def _get_field(self, function_list, name):
        name_list = [v.name() for v in function_list]
        try:
            i = name_list.index(name)
            return function_list[i]
        except ValueError:
            raise ValueError(
                "'{}' not in list. Available names are:\n{}".format(name, name_list)
            )

    def get_var(self, name):
        """Return the corresponding variable from its name."""
        return self._get_field(self.var, name)

    def get_lagrange_multiplier(self, name):
        """Return the corresponding Lagangrage multiplier from its name."""
        return self._get_field(self.y, name)

    def add_eq_constraint(self, Vy, A=None, b=0.0, bc=None, name=None):
        """
        Add a linear equality constraint :math:`Ax = b`.

        The constraint matrix A is expressed through a bilinear form involving
        the corresponding Lagrange multiplier defined on the space Vy.
        The right-hand side is a linear form involving the same Lagrange multiplier.
        Naming the constraint enables to retrieve the corresponding Lagrange multiplier
        optimal value.

        Parameters
        ----------
        Vy : `FunctionSpace`
            FunctionSpace of the corresponding Lagrange multiplier
        A : function
            A function of signature `y -> bilinear_form` where the function
            argument `y` is the constraint Lagrange multiplier.
        b : float, function
            A float or a function of signature `y -> linear_form` where the function
            argument `y` is the constraint Lagrange multiplier (default is 0.0)
        bc : DirichletBC
            boundary conditions to apply on the Lagrange multiplier (will be
            applied to all columns of the constraint when possible)
        name : str
            Lagrange multiplier name
        """
        self.add_ineq_constraint(Vy, A, b, b, bc, name)

    def add_ineq_constraint(self, Vy, A=None, bu=None, bl=None, bc=None, name=None):
        """
        Add a linear inequality constraint :math:`b_l \\leq Ax \\leq b_u`.

        The constraint matrix A is expressed through a bilinear form involving
        the corresponding Lagrange multiplier defined on the space Vy.
        The right-hand sides are linear forms involving the same Lagrange multiplier.
        Naming the constraint enables to retrieve the corresponding Lagrange multiplier
        optimal value.

        Parameters
        ----------
        Vy : `FunctionSpace`
            FunctionSpace of the corresponding Lagrange multiplier
        A : function
            A function of signature `y -> bilinear_form` where the function
            argument `y` is the constraint Lagrange multiplier.
        bl : float, function
            A float or a function of signature `y -> linear_form` where the function
            argument `y` is the constraint Lagrange multiplier (default is 0.0)
        bu : float, function
            same as bl
        bc : DirichletBC
            boundary conditions to apply on the Lagrange multiplier (will be
            applied to all columns of the constraint when possible)
        name : str
            Lagrange multiplier name
        """
        self.Vy.append(Vy)
        self.lagrange_multiplier_names.append(name)
        v = TestFunction(Vy)
        if callable(bu):
            self.bu.append(bu(v))
        else:
            self.bu.append(bu)
        if callable(bl):
            self.bl.append(bl(v))
        else:
            self.bl.append(bl)
        self.A.append(A(v))
        if isinstance(bc, list):
            self.bc_dual.append(bc)
        else:
            self.bc_dual.append([bc])

    def add_obj_func(self, obj):
        """
        Add an objective function.

        Parameters
        ----------
        obj : list of float, function
            objective function described either as a list of floats
            or as a linear form.
        """
        vtest = [TestFunction(V) for V in self.Vx]
        if isinstance(obj, list):
            self.c.append(
                [
                    ufl.replace(c, {v: v_}) if isinstance(c, ufl.form.Form) else c
                    for (c, v, v_) in zip(obj, self.var, vtest)
                ]
            )
        else:
            self.c.append(
                [
                    ufl.algorithms.expand_derivatives(derivative(obj, v, v_))
                    if obj is not None
                    else None
                    for (v, v_) in zip(self.var, vtest)
                ]
            )

    def add_convex_term(self, conv_fun):
        """Add the convex term `conv_fun` to the problem."""
        conv_fun._apply_on_problem(self)

    def _write_problem(self):
        self._update_names()

        sdp_cones = [isinstance(c, SDP) for c in self.cones]
        not_sdp_cones = [not s for s in sdp_cones]

        def sdp_filter(x):
            """Filter sdp/non sdp variables."""
            return (
                list(compress(x, not_sdp_cones)),
                list(compress(x, sdp_cones)),
            )

        self.nvar, self.nvar_sdp = sdp_filter([v.dim() for v in self.Vx])
        self.ncon = [v.dim() for v in self.Vy]
        lvar = len(self.nvar) + len(self.nvar_sdp)
        lcon = len(self.ncon)
        Nvar = sum(self.nvar)
        Ncon = sum(self.ncon)

        self.A_array, self.sdp_A_list = _block_mat_to_sparse(
            self.A, self.var, self.ncon, self.bc_dual, sdp_cones
        )
        assert len(self.A) == lcon
        assert max([len(a) if isinstance(a, list) else 0 for a in self.A]) <= lvar
        assert len(self.bu) == lcon or self.bu is None
        assert len(self.bl) == lcon or self.bl is None
        assert self.ux is None or len(self.ux) == lvar
        assert self.lx is None or len(self.lx) == lvar

        sum_c = [None] * lvar
        for c_row in self.c:
            for (i, ci) in enumerate(c_row):
                if ci is not None and ci != 0:
                    if (
                        not isinstance(ci, ufl.form.Form)
                        or len(ufl.algorithms.extract_arguments(ci)) == 1
                    ):
                        if sum_c[i] is None or sum_c[i] == 0:
                            sum_c[i] = ci
                        else:
                            sum_c[i] += ci
        cx, cx_sdp = sdp_filter(sum_c)
        self.carray = _block_vect_to_array(cx, self.nvar)
        self.carray_sdp = _block_vect_to_array(cx_sdp, self.nvar_sdp)

        self.bound_arrays = [
            None,
        ] * 4
        # no bounds on SDP variables
        inputs = [self.bu, self.bl, sdp_filter(self.ux)[0], sdp_filter(self.lx)[0]]
        default_values = [inf, -inf, inf, -inf]
        bc_duals = [self.bc_dual, self.bc_dual, None, None]
        sizes = [(self.ncon, Ncon)] * 2 + [(self.nvar, Nvar)] * 2

        for (i, (inp, deflt, size, bc_dual)) in enumerate(
            zip(inputs, default_values, sizes, bc_duals)
        ):
            if inp is not None:
                self.bound_arrays[i] = _block_vect_to_array(
                    inp, size[0], default_value=deflt, bc_dual=bc_dual
                )
            else:
                self.bound_arrays[i] = [deflt] * size[1]

        A_bc, b_bc = _bcs_to_block_mat(self.bc_prim, self.nvar)
        if A_bc != []:
            self.bound_arrays[0] += b_bc.tolist()
            self.bound_arrays[1] += b_bc.tolist()
            self.A_array = sp.vstack((self.A_array, A_bc))
            Ncon += len(b_bc)

        # SDP cones are specified separately from the other
        cone_list, cone_type, cone_alp = _block_cones_to_list(
            sdp_filter(self.cones)[0], self.nvar
        )
        sdp_cone_list, _, _ = _block_cones_to_list(
            sdp_filter(self.cones)[1], self.nvar_sdp
        )
        self.sdp_cones = sdp_cone_list

        assert self.A_array.shape == (Ncon, Nvar)

        self.cone_dict = {"list": cone_list, "type": cone_type, "alpha": cone_alp}
        self.num_cones = len(cone_list)
        if self.parameters["log_level"] > 0:
            print("Matrix shape:", self.A_array.shape)
            print("Number of cones:", self.num_cones)

    def _call_mosek(self, readfile=False):
        env = mosek.Env()
        self.task = env.Task()
        self.task.set_Stream(mosek.streamtype.log, _streamprinter)
        if readfile:
            try:
                print("Reading problem file...")
                self.task.readdata("mosekio.jtask")
            except Exception:
                print("Problem reading the file")
        else:

            Ncon, Nvar = self.A_array.shape
            self.task.appendcons(Ncon)
            self.task.appendvars(Nvar)

            self.task.putaijlist(self.A_array.row, self.A_array.col, self.A_array.data)
            self.task.putclist(range(Nvar), self.carray)

            bu, bl, ux, lx = self.bound_arrays
            bk = map(_get_boundkey, zip(bl, bu))
            xk = map(_get_boundkey, zip(lx, ux))
            self.task.putconboundlist(range(Ncon), list(bk), bl, bu)
            self.task.putvarboundlist(range(Nvar), list(xk), lx, ux)

            # SDP variables and constraints
            if len(self.sdp_cones) > 0:
                self.barvardim = [
                    int(-1 + (1 + 8 * len(c)) ** 0.5) // 2 for c in self.sdp_cones
                ]
                self.task.appendbarvars(self.barvardim)
                nvar_sdp = len(self.sdp_cones)
                clist = [
                    [self.carray_sdp[s] for s in self.sdp_cones[i]]
                    for i in range(nvar_sdp)
                ]
                subk = [s for d in self.barvardim for s in subk_list(d)]
                subl = [s for d in self.barvardim for s in subl_list(d)]
                subj = [i for i in range(nvar_sdp) for j in self.sdp_cones[i]]
                vals = []
                for i in range(nvar_sdp):
                    vals += clist[i]
                num = len(self.carray_sdp)
                vals = [v if k == l else v / 2 for (k, l, v) in zip(subk, subl, vals)]
                self.task.putbarcblocktriplet(num, subj, subk, subl, vals)

                subi = []
                subj = []
                subk = []
                subl = []
                vals = []
                buffi = 0
                nrow = 0
                for (i, Arow) in enumerate(self.sdp_A_list):
                    buffj = 0
                    for (j, A) in enumerate(Arow):
                        d = self.barvardim[j]
                        d2 = d * (d + 1) // 2
                        if A is not None:
                            nrow, ncol = A.shape
                            assert nrow == self.ncon[i]
                            row, col, data = A.row, A.col, A.data
                            subi += (buffi + row).tolist()
                            subj += (buffj + np.floor_divide(col, d2)).tolist()
                            lmod = np.remainder(col, d2).tolist()
                            subk += half_vect2subk(lmod, d)
                            subl += half_vect2subl(lmod, d)
                            vals += data.tolist()
                        buffj += self.nvar_sdp[j] // d2
                    buffi += self.ncon[i]
                vals = [v if k == l else v / 2 for (k, l, v) in zip(subk, subl, vals)]
                self.task.putbarablocktriplet(len(subi), subi, subj, subk, subl, vals)

            # Integer variables
            var_cumdim = np.cumsum(np.array(self.nvar))
            for (i, int_var) in enumerate(self.int_var):
                if int_var:
                    self.task.putvartypelist(
                        range(var_cumdim[i] - self.nvar[i], var_cumdim[i]),
                        [mosek.variabletype.type_int] * self.nvar[i],
                    )

            if self.sense == "max":
                self.task.putobjsense(mosek.objsense.maximize)

            for k in range(self.num_cones):
                self.task.appendcone(
                    MOSEK_CONE_TYPES[self.cone_dict["type"][k]],
                    self.cone_dict["alpha"][k],
                    self.cone_dict["list"][k],
                )

        self._set_task_parameters()
        if self.parameters["dump_file"] is not None:
            self.task.writedata(self.parameters["dump_file"])
        self.task.optimize()
        self.task.solutionsummary(mosek.streamtype.msg)

    def _set_task_parameters(self):
        assert all(
            [p in self._default_parameters().keys() for p in self.parameters.keys()]
        ), "Available parameters are:\n{}".format(self._default_parameters())
        self.task.putintparam(mosek.iparam.log, self.parameters["log_level"])
        assert type(self.parameters["presolve"]) in [
            bool,
            mosek.presolvemode,
        ], "Presolve parameter must be a bool or of `mosek.presolvemode` type"
        self.task.putintparam(mosek.iparam.presolve_use, self.parameters["presolve"])
        self.task.putintparam(
            mosek.iparam.presolve_lindep_use, self.parameters["presolve_lindep"]
        )
        #     ... without basis identification (integer parameter)
        self.task.putintparam(mosek.iparam.intpnt_basis, mosek.basindtype.never)
        #     Set relative gap tolerance (double parameter)
        self.task.putdouparam(
            mosek.dparam.intpnt_co_tol_rel_gap, self.parameters["tol_rel_gap"]
        )
        # Controls whether primal or dual form is solved
        mapping = {
            "free": mosek.solveform.free,
            "primal": mosek.solveform.primal,
            "dual": mosek.solveform.dual,
        }
        self.task.putintparam(
            mosek.iparam.intpnt_solve_form, mapping[self.parameters["solve_form"]]
        )
        self.task.putintparam(
            mosek.iparam.sim_solve_form, mapping[self.parameters["solve_form"]]
        )
        self.task.putintparam(mosek.iparam.num_threads, self.parameters["num_threads"])
        self.task.putintparam(mosek.iparam.auto_update_sol_info, mosek.onoffkey.on)

    def get_solution_info(self, output=True):
        """
        Return information dictionary on the solution.

        If output=True, it gets printed out.
        """
        int_info = ["intpnt_iter", "opt_numcon", "opt_numvar", "ana_pro_num_var"]
        double_info = [
            "optimizer_time",
            "presolve_eli_time",
            "presolve_lindep_time",
            "presolve_time",
            "intpnt_time",
            "intpnt_order_time",
            "sol_itr_primal_obj",
            "sol_itr_dual_obj",
        ]
        int_value = [self.task.getintinf(getattr(mosek.iinfitem, k)) for k in int_info]
        double_value = [
            self.task.getdouinf(getattr(mosek.dinfitem, k)) for k in double_info
        ]
        info = dict(zip(int_info + double_info, int_value + double_value))
        info.update(
            {
                "solution_status": str(self.task.getsolsta(mosek.soltype.itr)).split(
                    "."
                )[1]
            }
        )
        if output:
            print("Solver information:\n{}".format(info))
        return info

    def _read_solution(self, xsol, ysol, slxsol, suxsol):
        solsta = self.task.getsolsta(mosek.soltype.itr)
        status = [mosek.solsta.optimal, mosek.solsta.unknown]
        if hasattr(
            mosek.solsta, "near_optimal"
        ):  # removed near_optimal status in version 9
            status.append(mosek.solsta.near_optimal)
        if solsta in status:
            numvar = self.task.getnumvar()
            numcon = self.task.getnumcon()
            self.xx = np.zeros((numvar,))
            self.yy = np.zeros((numcon,))
            self.task.getxx(mosek.soltype.itr, self.xx)
            self.task.gety(mosek.soltype.itr, self.yy)
            _populate_sol(xsol, self.xx)
            _populate_sol(ysol, self.yy)

            if len(self.sdp_cones) > 0:
                self.barx = [[0.0] * ((d * (d + 1)) // 2) for d in self.barvardim]
                for (j, b) in enumerate(self.barx):
                    self.task.getbarxj(mosek.soltype.itr, j, b)
                    # TODO: populate sol with barx

            if slxsol is not None:
                slx = np.zeros((numvar,))
                self.task.getslx(mosek.soltype.itr, slx)
                _populate_sol(slxsol, slx)
            if suxsol is not None:
                sux = np.zeros((numvar,))
                self.task.getsux(mosek.soltype.itr, sux)
                _populate_sol(suxsol, sux)

            if solsta == mosek.solsta.unknown:
                print(
                    "Warning: Solver finished with UNKNOWN type. \
                    Solution might be inaccurate..."
                )
            return self.task.getprimalobj(mosek.soltype.itr)
        else:
            return np.nan

    def optimize(self, sense="min", get_bound_dual=False):
        """
        Write the problem in Mosek format and solves.

        Parameters
        ----------
        sense : {"min[imize]", "max[imize]"}
            sense of optimization
        get_bound_dual : bool
            if True, optimal dual variable bounds will be stored in `self.sux` and
            `self.slx`

        Returns
        -------
        pobj : float
            the computed optimal value
        """
        if sense == "minimize":
            sense = "min"
        elif sense == "maximize":
            sense = "max"
        self.sense = sense
        self._write_problem()
        self._call_mosek()

        self.y = [
            Function(v, name=name)
            for (v, name) in zip(self.Vy, self.lagrange_multiplier_names)
        ]
        if get_bound_dual:
            self.slx = [Function(v) for v in self.Vx]
            self.sux = [Function(v) for v in self.Vx]
        else:
            self.slx = None
            self.sux = None
        self.pobj = self._read_solution(self.var, self.y, self.slx, self.sux)

        return self.pobj


def _streamprinter(text):
    """Write log message."""
    sys.stdout.write(text)
    sys.stdout.flush()


def _keep_rows_csr(mat, indices):
    """Keep the rows denoted by ``indices`` form the CSR sparse matrix ``mat``."""
    if not isinstance(mat, sp.csr_matrix):
        raise ValueError("works only for CSR format -- use .tocsr() first")
    indices = list(indices)
    mask = np.zeros(mat.shape[0], dtype=bool)
    mask[indices] = True
    return mat[mask]


def _block_mat_to_sparse(A, var, ncon, bc_dual, sdp_cones):
    rows = []
    sdp_rows = []
    for (i, nc) in enumerate(ncon):
        cols = []
        sdp_cols = []
        for (j, v) in enumerate(var):
            nv = v.function_space().dim()
            if not isinstance(A[i], list):
                Ab = A[i]
            elif j >= len(A[i]):
                Ab = None
            else:
                Ab = A[i][j]
            if Ab == 0.0 or Ab is None:
                As = sp.coo_matrix((nc, nv))
            else:
                V = v.function_space()
                if __version__ >= "2019.2":
                    Ve = V.ufl_element()
                    Ve._quad_scheme = "default"
                    V = FunctionSpace(v.function_space().mesh(), Ve)
                v_ = TrialFunction(V)
                f = ufl.replace(Ab, {v: v_})
                if len(ufl.algorithms.extract_arguments(f)) == 2:
                    AAb = assemble(
                        ufl.derivative(Ab, v, v_),
                        keep_diagonal=True,
                    )
                    if bc_dual[i] is not None:
                        for bc in bc_dual[i]:
                            if bc is not None:
                                try:
                                    bc.zero(AAb)
                                except Exception:
                                    pass
                    Aa = as_backend_type(AAb).mat()
                    row, col, data = Aa.getValuesCSR()
                    As = sp.csr_matrix((data, col, row), shape=(nc, nv))
                    As.eliminate_zeros()
                else:
                    As = sp.coo_matrix((nc, nv))

            if sdp_cones[j]:
                if Ab == 0.0 or Ab is None:
                    sdp_cols.append(None)
                else:
                    sdp_cols.append(As.tocoo())
            else:
                cols.append(As)

        rows.append(cols)
        sdp_rows.append(sdp_cols)

    AA = sp.bmat(rows, format="coo")
    return AA, sdp_rows


def _block_vect_to_array(r, ndim, bcs=None, default_value=0, bc_dual=None):
    rarray = np.zeros((0,))
    for (i, ri) in enumerate(r):
        if ri is not None:
            if isinstance(ri, ufl.form.Form):
                if len(ufl.algorithms.extract_arguments(ri)) == 1:
                    riv = assemble(ri)
                    if bc_dual is not None:
                        for bc in bc_dual[i]:
                            if bc is not None:
                                bc.apply(riv)
                    rarray = np.concatenate((rarray, riv.get_local()))
                else:
                    rarray = np.concatenate(
                        (rarray, default_value * np.ones((ndim[i],)))
                    )
            elif isinstance(ri, Function):
                rarray = np.concatenate((rarray, ri.vector().get_local()))
            elif type(ri) in [int, float]:
                rarray = np.concatenate((rarray, ri * np.ones((ndim[i],))))
            else:
                rarray = np.concatenate((rarray, ri.get_local()))
        else:
            rarray = np.concatenate((rarray, default_value * np.ones((ndim[i],))))
    return rarray.tolist()


def _bcs_to_block_mat(bcs, nvar):
    cols = []
    b_bc = []
    for (i, bc) in enumerate(bcs):
        bc_list = to_list(bc)
        for bci in bc_list:
            if bc is not None:
                nv = nvar[i]
                Al = []
                for (j, nvj) in enumerate(nvar):
                    if i == j:
                        Al.append(sp.eye(nvj))
                    else:
                        Al.append(sp.csr_matrix((nv, nvj)))
                As = sp.hstack(tuple(Al)).tocsr()
                bc_lines = list(bci.get_boundary_values().keys())
                bc_values = np.array(list(bci.get_boundary_values().values()))

                idx_bc = np.argsort(bc_lines)
                As = _keep_rows_csr(As, bc_lines)

                cols.append(As)
                b_bc.append(bc_values[idx_bc])

    if cols:
        A_bc = sp.vstack(tuple(cols))
        b_bc = np.concatenate(tuple(b_bc))
        return A_bc, b_bc

    return [], []


def _block_cones_to_list(cones, ndim):
    buff = 0
    cone_list = []
    cone_type = []
    cone_alp = []
    for (i, c) in enumerate(cones):
        if c is None:
            buff += ndim[i]
        elif isinstance(c, Product):
            for cc in c.cones:
                d = cc.dim
                ncones = ndim[i] // d // len(c.cones)
                for j in range(ncones):
                    cone_list.append(range(buff, buff + d))
                    cone_type.append(cc.type)
                    cone_alp.append(getattr(cc, "alp", 0))
                    buff += d
        elif isinstance(c, SDP):
            d = c.dim
            d2 = d * (d + 1) // 2
            ncones = ndim[i] // d2
            for j in range(ncones):
                cone_list.append(range(buff, buff + d2))
                buff += d2
        else:
            d = c.dim
            ncones = ndim[i] // d
            for j in range(ncones):
                cone_list.append(range(buff, buff + d))
                cone_type.append(c.type)
                cone_alp.append(getattr(c, "alp", 0))
                buff += d
    return cone_list, cone_type, cone_alp


def _get_boundkey(x):
    lb, ub = x
    if ub == inf and lb == -inf:
        return mosek.boundkey.fr
    elif ub == inf:
        return mosek.boundkey.lo
    elif lb == inf:
        return mosek.boundkey.up
    elif lb == ub:
        return mosek.boundkey.fx

    return mosek.boundkey.ra


def _populate_sol(xsol, array):
    buff = 0
    for x in xsol:
        x.vector().set_local(array[buff : x.vector().size() + buff])
        buff += x.vector().size()
