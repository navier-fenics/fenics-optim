#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Custom "lobatto" and "midside" quadrature scheme.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from numpy import array, kron
from FIAT.reference_element import (
    UFCInterval,
    UFCTriangle,
    UFCTetrahedron,
    TensorProductCell,
)
from FIAT.quadrature import (
    QuadratureRule,
    GaussJacobiQuadratureLineRule,
    GaussLobattoLegendreQuadratureLineRule,
)
from FIAT.quadrature_schemes import create_quadrature
from ffc.analysis import _autoselect_quadrature_rule


def create_quadrature_monkey_patched(ref_el, degree, scheme="default"):
    """Monkey patched FIAT.quadrature_schemes.create_quadrature()."""
    # Our "special" scheme
    if scheme == "lobatto":
        # We use Gauss-Lobatto points
        # https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss%E2%80%93Lobatto_rules # noqa
        if isinstance(ref_el, UFCInterval):
            if degree >= 2:
                return GaussLobattoLegendreQuadratureLineRule(ref_el, degree)
            else:
                return GaussJacobiQuadratureLineRule(ref_el, 1)

        if isinstance(ref_el, UFCTriangle):
            if degree == 2:
                x = array(
                    [
                        [0.0, 0.0],
                        [1.0, 0.0],
                        [0.0, 1.0],
                        [0.5, 0.0],
                        [0.5, 0.5],
                        [0.0, 0.5],
                    ]
                )
                w = array(
                    [
                        1 / 12.0,
                    ]
                    * 6
                )
            elif degree == 1:
                x = array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0]])
                w = array(
                    [
                        1 / 6.0,
                    ]
                    * 3
                )
            else:
                raise NotImplementedError(
                    "Lobatto rule for degree 1 and 2 only on a triangle"
                )
            return QuadratureRule(ref_el, x, w)
        if isinstance(ref_el, UFCTetrahedron):
            if degree == 2:
                x = array(
                    [
                        [0.0, 0.0, 0.0],
                        [1.0, 0.0, 0.0],
                        [0.0, 1.0, 0.0],
                        [0.5, 0.0, 0.0],
                        [0.5, 0.5, 0.0],
                        [0.0, 0.5, 0.0],
                        [0.5, 0.0, 0.5],
                        [0.5, 0.5, 0.5],
                        [0.0, 0.5, 0.5],
                        [0.0, 0.0, 1.0],
                    ]
                )
                w = array(
                    [
                        1 / 60.0,
                    ]
                    * 10
                )
            elif degree == 1:
                x = array(
                    [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]
                )
                w = array(
                    [
                        1 / 24.0,
                    ]
                    * 4
                )
            else:
                raise NotImplementedError(
                    "Lobatto rule for degree 1 and 2 only on a tetrahedra"
                )
            return QuadratureRule(ref_el, x, w)
        elif isinstance(ref_el, TensorProductCell):
            if degree == 1:
                x = array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]])
                w_1D = array([1 / 2.0, 1 / 2.0])
                w = kron(w_1D, w_1D)
                return QuadratureRule(ref_el, x, w)
            elif degree == 2:
                x = array(
                    [
                        [0.0, 0.0],
                        [0.5, 0.0],
                        [1.0, 0.0],
                        [0.0, 0.5],
                        [0.5, 0.5],
                        [1.0, 0.5],
                        [0.0, 1.0],
                        [0.5, 1.0],
                        [1.0, 1.0],
                    ]
                )
                w_1D = array([1 / 6.0, 2 / 3.0, 1 / 6.0])
                w = kron(w_1D, w_1D)
                return QuadratureRule(ref_el, x, w)
            else:
                raise NotImplementedError(
                    "Lobatto rule for degree 1 and 2 only on a tensor cell"
                )
        raise NotImplementedError(
            "Scheme {} of degree {} on {} not implemented".format(
                scheme, degree, ref_el
            )
        )
    elif scheme == "midside":
        # We use Gauss-Lobatto points
        # https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss%E2%80%93Lobatto_rules # noqa
        if isinstance(ref_el, UFCTriangle):
            x = array([[0.5, 0.0], [0.5, 0.5], [0.0, 0.5]])
            w = array(
                [
                    1 / 6.0,
                ]
                * 3
            )
            return QuadratureRule(ref_el, x, w)
        else:
            raise NotImplementedError(
                "Scheme {} on {} not implemented".format(scheme, ref_el)
            )
    # Fallback to FIAT's normal operation
    return create_quadrature(ref_el, degree, scheme=scheme)


def _autoselect_quadrature_rule_monkey_patched(*args, **kwargs):
    """
    Monkey patched ffc.analysis._autoselect_quadrature_rule().

    This prevents FFC to complain about a non-existing quadrature scheme.
    """
    try:
        return _autoselect_quadrature_rule(*args, **kwargs)
    except Exception:
        integral_metadata = args[0]
        qr = integral_metadata["quadrature_rule"]
        return qr


def monkey_patch_fiat_quadrature_scheme():
    """Monkey patch FIAT quadrature scheme generator and autoselection."""
    import FIAT
    import ffc.analysis

    FIAT.create_quadrature = create_quadrature_monkey_patched
    ffc.analysis._autoselect_quadrature_rule = (
        _autoselect_quadrature_rule_monkey_patched
    )
