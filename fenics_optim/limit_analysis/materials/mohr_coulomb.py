#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mohr-Coulomb strength criteria conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import sin, cos, tan, as_matrix, as_vector, dot, diag, tr, perp
from .strength_criterion import StrengthCriterion, to_Constant
from fenics_optim import Quad, SDP, to_mat, tail
import numpy as np


class MohrCoulomb2D(StrengthCriterion):
    """
    Plane strain Mohr-Coulomb criterion.

    Parameters
    ----------
    c : float
        cohesion
    phi : float
        friction angle [rad]
    ft : float, optional
        tension cut-off
    """

    def __init__(self, c, phi, ft=None):
        self.dim = 2
        self.c = to_Constant(c)
        self.phi = to_Constant(phi)
        if ft is None:
            self.tension_cut_off = False
            self.ft = 2 * self.c * cos(self.phi) / (1 + sin(self.phi))
        else:
            self.tension_cut_off = True
            self.ft = to_Constant(ft)
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Y = inst.add_var(3, cone=Quad(3), name="Y")
        Q = as_matrix([[1, -1, 0], [0, 0, 2]])
        inst.add_eq_constraint(dot(Q, X) + tail(Y), b=as_vector([0, 0]))
        inst.add_ineq_constraint(
            sin(self.phi) * (X[0] + X[1]) + Y[0], bu=2 * self.c * cos(self.phi)
        )
        if self.tension_cut_off:
            inst.add_ineq_constraint((X[0] + X[1]) + Y[0], bu=2 * self.ft)

    def _support_func_conic_repr(self, inst, X):
        Y = inst.add_var(3, cone=Quad(3), name="Auxiliary_MC")

        Q = as_matrix([[-1, 1, 0], [0, 0, 2]])
        inst.add_eq_constraint(dot(Q, X) - tail(Y))
        T = X[0] + X[1]
        inst.add_ineq_constraint(T - Y[0], bu=0)
        pic = self.c * cos(self.phi) * (Y[0] - T) / (1 - sin(self.phi))
        if self.tension_cut_off:
            inst.add_ineq_constraint(sin(self.phi) * Y[0] - T, bu=0)
            pit = self.ft * (T - sin(self.phi) * Y[0]) / (1 - sin(self.phi))
        else:
            inst.add_eq_constraint(sin(self.phi) * Y[0] - T)
            pit = 0
        inst.set_linear_term(pic + pit)

    def _disc_support_func_conic_repr(self, inst, X):
        """X is (V_n,V_t) in the local facet frame."""
        f = tan(self.phi)
        if inst.dim_x == 2:
            Qn = diag(as_vector([1.0, f]))
        elif inst.dim_x == 3:
            Qn = diag(as_vector([1.0, f, f]))
        Y = inst.add_var(inst.dim_x, cone=Quad(inst.dim_x))
        inst.add_eq_constraint(dot(Qn, X) - Y)
        inst.set_linear_term(self.c / tan(self.phi) * Y[0])

    def _compute_pi(self, d):
        return self.c / tan(self.phi) * tr(d)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = ((\\sigma_{xx}+\\sigma_{yy})\\sin\\phi + \
             \\sqrt{(\\sigma_{xx}-\\sigma_{yy})^2 + 4\\sigma_{xy}^2})/(2c\\cos\\phi)`

        In presence of a tension cut-off :math:`f_t`, the gauge function is given by:

            :math:`g_{TC}(\\sigma) = \\max\\{{ g(\\sigma) ; \\sigma_I/f_t \\}}`

        """
        assert len(Sig) == 3
        phi, c, ft = float(self.phi), float(self.c), float(self.ft)
        g1 = (
            (Sig[0] + Sig[1]) * np.sin(phi)
            + np.sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)
        ) / (2 * c * np.cos(phi))
        g2 = (
            (Sig[0] + Sig[1] + np.sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2))
            / 2
            / ft
        )
        if self.tension_cut_off:
            return np.maximum(g1, g2)
        else:
            return g1


class MohrCoulomb2D_plane_stress(StrengthCriterion):
    """
    Plane stress Mohr-Coulomb criterion.

    Parameters
    ----------
    fc : float
        compressive strength
    phi : float
        friction angle [rad]
    ft : float, optional
        tension cut-off
    """

    def __init__(self, fc, phi, ft=None):
        self.dim = 2
        self.fc = to_Constant(fc)
        self.phi = to_Constant(phi)
        self.a = (1 + sin(self.phi)) / (1 - sin(self.phi))
        if ft is None:
            self.tension_cut_off = False
            self.ft = self.fc / self.a
        else:
            self.tension_cut_off = True
            self.ft = to_Constant(ft)
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Y = inst.add_var(3, cone=Quad(3), name="Y")
        Q = as_matrix([[1, -1, 0], [0, 0, 2]])
        inst.add_eq_constraint(dot(Q, X) + tail(Y), b=as_vector([0, 0]))
        tr = X[0] + X[1]
        sig_M = inst.add_var(lx=0)
        sig_m = inst.add_var(ux=0)

        inst.add_ineq_constraint(
            sig_M * self.a - sig_m,
            bu=self.fc,
        )
        inst.add_ineq_constraint(sig_M - tr / 2 - Y[0] / 2, bl=0)
        inst.add_ineq_constraint(sig_m - tr / 2 + Y[0] / 2, bu=0)
        if self.tension_cut_off:
            inst.add_ineq_constraint(sig_M, bu=self.ft)

    def _support_func_conic_repr(self, inst, X):
        Y = inst.add_var(3, cone=Quad(3), name="Auxiliary_MC")

        Q = as_matrix([[-1, 1, 0], [0, 0, 2]])
        inst.add_eq_constraint(dot(Q, X) - tail(Y))
        T = X[0] + X[1]
        inst.add_ineq_constraint(T - Y[0], bu=0)
        pic = self.c * cos(self.phi) * (Y[0] - T) / (1 - sin(self.phi))
        if self.tension_cut_off:
            inst.add_ineq_constraint(sin(self.phi) * Y[0] - T, bu=0)
            pit = self.ft * (T - sin(self.phi) * Y[0]) / (1 - sin(self.phi))
        else:
            inst.add_eq_constraint(sin(self.phi) * Y[0] - T)
            pit = 0
        inst.set_linear_term(pic + pit)

    def _disc_support_func_conic_repr(self, inst, X):
        """X is (V_n,V_t) in the local facet frame."""
        f = tan(self.phi)
        if inst.dim_x == 2:
            Qn = diag(as_vector([1.0, f]))
        elif inst.dim_x == 3:
            Qn = diag(as_vector([1.0, f, f]))
        Y = inst.add_var(inst.dim_x, cone=Quad(inst.dim_x))
        inst.add_eq_constraint(dot(Qn, X) - Y)
        inst.set_linear_term(self.c / tan(self.phi) * Y[0])

    def _compute_pi(self, d):
        return self.c / tan(self.phi) * tr(d)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = ((\\sigma_{xx}+\\sigma_{yy})\\sin\\phi + \
             \\sqrt{(\\sigma_{xx}-\\sigma_{yy})^2 + 4\\sigma_{xy}^2})/(2c\\cos\\phi)`

        In presence of a tension cut-off :math:`f_t`, the gauge function is given by:

            :math:`g_{TC}(\\sigma) = \\max\\{{ g(\\sigma) ; \\sigma_I/f_t \\}}`

        """
        assert len(Sig) == 3
        phi, c, ft = float(self.phi), float(self.c), float(self.ft)
        g1 = (
            (Sig[0] + Sig[1]) * np.sin(phi)
            + np.sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)
        ) / (2 * c * np.cos(phi))
        g2 = (
            (Sig[0] + Sig[1] + np.sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2))
            / 2
            / ft
        )
        if self.tension_cut_off:
            return np.maximum(g1, g2)
        else:
            return g1


class PlaneCoulomb2D(StrengthCriterion):
    """Coulomb criterion on a plane of angle alpha.

    Parameters
    ----------
    c : float
        cohesion
    phi : float
        friction angle
    alpha : float
        plane normal orientation angle with respect to the horizontal
        direction
    dalpha : float, optional
        uncertainty on the plane orientation (robust version), by default 0
    """

    def __init__(self, c, phi, alpha, dalpha=0.0):
        self.dim = 2
        self.c = to_Constant(c)
        self.phi = to_Constant(phi)
        self.alpha = to_Constant(alpha)
        self.dalpha = to_Constant(abs(dalpha))
        self.robust = dalpha != 0

        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        n = as_vector([cos(self.alpha), sin(self.alpha)])
        t = perp(n)
        sig = to_mat(X)
        sig_n = dot(n, dot(sig, n))
        tau = dot(t, dot(sig, n))
        sig_t = dot(t, dot(sig, t))
        dsig = sig_t - sig_n
        inst.add_ineq_constraint(
            tau * (1 + 2 * tan(self.phi) * self.dalpha)
            + dsig * self.dalpha
            + tan(self.phi) * sig_n,
            bu=self.c,
        )
        inst.add_ineq_constraint(
            -tau * (1 - 2 * tan(self.phi) * self.dalpha)
            - dsig * self.dalpha
            + tan(self.phi) * sig_n,
            bu=self.c,
        )
        if self.robust:
            inst.add_ineq_constraint(
                tau * (1 - 2 * tan(self.phi) * self.dalpha)
                - dsig * self.dalpha
                + tan(self.phi) * sig_n,
                bu=self.c,
            )
            inst.add_ineq_constraint(
                -tau * (1 + 2 * tan(self.phi) * self.dalpha)
                + dsig * self.dalpha
                + tan(self.phi) * sig_n,
                bu=self.c,
            )


class RobustMohrCoulomb2D(StrengthCriterion):
    """
    Uncertain version of the plane strain MohrCoulomb criterion.

    Cohesion varies between :math:`c-\\Delta c` and :math:`c+\\Delta c`.
    Friction angle varies between :math:`\\phi-\\Delta\\phi` and
    :math:`\\phi+\\Delta\\phi`.

    Parameters
    ----------
    c : float
        nominal cohesion
    phi : float
        nominal friction angle
    dc : float
        cohesion variability
    dphi : float
        friction angle variability
    """

    def __init__(self, c, phi, dc, dphi):
        self.dim = 2
        self.c = to_Constant(c)
        self.phi = to_Constant(phi)
        self.dc = to_Constant(dc)
        self.dphi = to_Constant(dphi)

        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Y1 = inst.add_var(3, cone=Quad(3), name="Y1")
        Q1 = as_matrix(
            [
                [sin(self.phi + self.dphi), sin(self.phi + self.dphi), 0],
                [1, -1, 0],
                [0, 0, 2],
            ]
        )
        rhs1 = 2 * self.c * cos(self.phi + self.dphi) - 2 * self.dc * cos(self.phi)
        inst.add_eq_constraint(dot(Q1, X) + Y1, b=as_vector([rhs1, 0, 0]))
        Y2 = inst.add_var(3, cone=Quad(3), name="Y2")
        Q2 = as_matrix(
            [
                [sin(self.phi - self.dphi), sin(self.phi - self.dphi), 0],
                [1, -1, 0],
                [0, 0, 2],
            ]
        )
        rhs2 = 2 * self.c * cos(self.phi - self.dphi) - 2 * self.dc * cos(self.phi)
        inst.add_eq_constraint(dot(Q2, X) + Y2, b=as_vector([rhs2, 0, 0]))


class MohrCoulomb3D(StrengthCriterion):
    """
    Tridimensional Mohr-Coulomb criterion.

    Parameters
    ----------
    c : float
        cohesion
    phi : float
        friction angle [rad]
    ft : float, optional
        tension cut-off
    """

    def __init__(self, c, phi, ft=None):
        self.c = c
        self.phi = phi
        if ft is None:
            self.tension_cut_off = False
            self.ft = 2 * c * cos(phi) / (1 + sin(phi))
        else:
            self.tension_cut_off = True
            self.ft = ft
        self.fc = 2 * c * cos(phi) / (1 - sin(phi))
        self.a = (1 - sin(phi)) / (1 + sin(phi))
        self.dim = 3
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        t = inst.add_var()
        Yp = inst.add_var(6, cone=SDP(3))
        Ym = inst.add_var(6, cone=SDP(3))
        Id = as_vector([1] * 3 + [0] * 3)
        if self.tension_cut_off:
            alp = inst.add_var(ux=self.ft)
            inst.add_eq_constraint(X + Yp - alp * Id)
            inst.add_eq_constraint(-X + t * Id + Ym)
            inst.add_ineq_constraint(-self.a * t + alp, bu=self.a * self.fc)
        else:
            inst.add_eq_constraint(X - self.a * t * Id + Yp, b=self.a * self.fc * Id)
            inst.add_eq_constraint(-X + t * Id + Ym)

    def _support_func_conic_repr(self, inst, X):
        Yp = inst.add_var(6, cone=SDP(3))
        Ym = inst.add_var(6, cone=SDP(3))
        inst.add_eq_constraint(X - Yp + Ym)
        if self.tension_cut_off:
            inst.add_ineq_constraint(-self.a * tr(to_mat(Yp)) + tr(to_mat(Ym)), bu=0)
        else:
            inst.add_eq_constraint(-self.a * tr(to_mat(Yp)) + tr(to_mat(Ym)))
        if self.ft == 0:
            pit = 0
        else:
            pit = self.ft * tr(to_mat(Yp))
        if self.tension_cut_off:
            pic = (self.fc - self.ft / self.a) * tr(to_mat(Ym))
        else:
            pic = 0
        inst.set_linear_term(pit + pic)

    def _compute_pi(self, d):
        return self.c / tan(self.phi) * tr(d)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = ((1+\\sin\\phi)\\sigma_I - \
             (1-\\sin\\phi)\\sigma_{III})/(2c\\cos\\phi)`

        where :math:`\\sigma_I,\\sigma_{III}` are the major and minor principal
        stresses respectively.

        In presence of a tension cut-off :math:`f_t`, the gauge function is given by:

            :math:`g_{TC}(\\sigma) = \\max\\{{ g(\\sigma) ; \\sigma_I/f_t \\}}`
        """
        assert len(Sig) == 6
        phi, c = float(self.phi), float(self.c)
        sig = np.array(
            [
                [Sig[0], Sig[3], Sig[5]],
                [Sig[3], Sig[1], Sig[4]],
                [Sig[5], Sig[4], Sig[2]],
            ]
        )
        sig_princ = np.sort(np.linalg.eig(sig)[0])
        g = (
            (sig_princ[-1] * (1 + sin(phi)) - sig_princ[0] * (1 - sin(phi)))
            / 2
            / c
            / cos(phi)
        )
        if self.tension_cut_off:
            return max(g, sig_princ[-1] / float(self.ft))
        else:
            return g
