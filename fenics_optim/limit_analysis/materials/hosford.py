#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Hosford strength criterion conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sqrt
from ufl import as_matrix, as_vector, dot
from .strength_criterion import StrengthCriterion
from fenics_optim import Quad, Pow, tail


class Hosford_plane_stress(StrengthCriterion):
    """Hosford strength criterion in 2D plane stress conditions.

       :math:`|\\sigma_I|^n+|\\sigma_{{II}}|^n+|\\sigma_{II}-\\sigma_I|^n\\leq\\sigma_0`

    Parameters
    ----------
    sig0 : float, Constant
        tensile strength
    n : float
        Hosford exponent
    """

    def __init__(self, sig0, n):
        self.sig0 = sig0
        self.n = n
        self.dim = 2
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        assert inst.dim_x == 3, "Plane stress criterion available only in 2D"
        P = Pow(3, 1 / self.n)
        Y = inst.add_var(3, cone=Quad(3))
        Z1 = inst.add_var(3, cone=P)
        Z2 = inst.add_var(3, cone=P)
        Z3 = inst.add_var(3, cone=P)
        A = as_matrix([[1, -1, 0], [0, 0, 2]])
        t = as_vector([1, 1, 0])
        inst.add_eq_constraint(dot(A, X) - tail(Y))
        inst.add_eq_constraint(Z1[1], b=1)
        inst.add_eq_constraint(dot(t, X) / 2 + Y[0] / 2 - Z1[2])
        inst.add_eq_constraint(Z2[1], b=1)
        inst.add_eq_constraint(-dot(t, X) / 2 + Y[0] / 2 - Z2[2])
        inst.add_eq_constraint(Z3[1], b=1)
        inst.add_eq_constraint(Y[0] - Z3[2])
        inst.add_ineq_constraint(Z1[0] + Z2[0] + Z3[0], bu=2 * self.sig0 ** self.n)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`(|\\sigma_I|^n+|\\sigma_{{II}}|^n+|\\sigma_{II}-\\sigma_I|^n)/\\sigma_0`

        where :math:`\\sigma_I,\\sigma_{{II}}` are the major and minor principal
        stresses respectively
        """
        assert len(Sig) == 3
        S1 = (Sig[0] + Sig[1]) / 2 + sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2) / 2
        S2 = (Sig[0] + Sig[1]) / 2 - sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2) / 2

        return (
            (abs(S1) ** self.n + abs(S2) ** self.n + abs(S1 - S2) ** self.n) / 2
        ) ** (1 / self.n) / (self.sig0)
