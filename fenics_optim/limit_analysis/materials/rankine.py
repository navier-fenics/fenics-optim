#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Rankine strength criteria conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .strength_criterion import StrengthCriterion, to_Constant
from ufl import as_vector, tr, dot, diag, sqrt, as_matrix
from fenics_optim import Quad, RQuad, SDP, to_mat, tail
import numpy as np


class OrthotropicRankine2D(StrengthCriterion):
    """Orthotropic Rankine criterion in 2D.

    Parameters
    ----------
    fcx : float
        compressive strength in :math:`x` direction
    fcy : float
        compressive strength in :math:`y` direction
    ftx : float
        tensile strength in :math:`x` direction
    fty : float
        tensile strength in :math:`x` direction
    """

    def __init__(self, fcx, fcy, ftx, fty):
        self.fcx = to_Constant(fcx)
        self.fcy = to_Constant(fcy)
        self.ftx = to_Constant(ftx)
        self.fty = to_Constant(fty)
        self.dim = 2
        self.set_material_frame()
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        # FIXME : wrong implementation ?
        Xr = dot(self.rotation_matrix, X)
        Yp = inst.add_var(3, cone=RQuad(3))
        Ym = inst.add_var(3, cone=RQuad(3))
        bt = as_vector([self.ftx, self.fty, 0])
        bc = as_vector([self.fcx, self.fcy, 0])
        Q = diag(as_vector([1, 1, sqrt(2)]))
        inst.add_eq_constraint(dot(Q, Xr) + Yp, b=bt)
        inst.add_eq_constraint(dot(Q, Xr) - Ym, b=-bc)

    def _support_func_conic_repr(self, inst, X):
        Xr = dot(self.rotation_matrix, X)
        Yp = inst.add_var(3, cone=RQuad(3))
        Ym = inst.add_var(3, cone=RQuad(3))
        Q = diag(as_vector([1, 1, sqrt(2)]))
        inst.add_eq_constraint(dot(Q, Xr) - Yp + Ym)
        if self.ftx == 0 and self.fty == 0:
            pit = 0
        else:
            pit = self.ftx * Yp[0] + self.fty * Yp[1]
        inst.set_linear_term(pit + self.fcx * Ym[0] + self.fcy * Ym[1])

    def _disc_support_func_conic_repr(self, inst, X):
        pass

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) =\\max\\{{ -\\sigma_{III}/f_c  ; \\sigma_I/f_t \\}}``

        where :math:`\\sigma_I,\\sigma_{III}` are the major and minor principal
        stresses respectively.
        """
        # FIXME : wrong implementation ?
        assert len(Sig) == 3
        ftx = float(self.ftx)
        fty = float(self.fty)
        fcx = float(self.fcx)
        fcy = float(self.fcy)
        gt = max(
            (Sig[0] / ftx + Sig[1] / fty) + (Sig[2] ** 2 - Sig[0] * Sig[1]) / ftx / fty,
            max(Sig[0] / ftx, Sig[1] / fty),
        )
        gc = max(
            -(Sig[0] / fcx + Sig[1] / fcy)
            + (Sig[2] ** 2 - Sig[0] * Sig[1]) / fcx / fcy,
            max(-Sig[0] / fcx, -Sig[1] / fcy),
        )
        return max(gt, gc)


class Rankine2D(OrthotropicRankine2D):
    """Isotropic Rankine criterion in 2D.

    Parameters
    ----------
    fc : float
        compressive strength
    ft : float
        tensile strength
    """

    def __init__(self, fc=1.0, ft=1.0):
        OrthotropicRankine2D.__init__(self, fc, fc, ft, ft)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) =\\max\\{{ -\\sigma_{III}/f_c  ; \\sigma_I/f_t \\}}``

        where :math:`\\sigma_I,\\sigma_{III}` are the major and minor principal
        stresses respectively
        """
        assert len(Sig) == 3
        sigI = ((Sig[0] + Sig[1]) + sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)) / 2
        sigII = ((Sig[0] + Sig[1]) - sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)) / 2
        return max(-sigII / float(self.fcx), sigI / float(self.ftx))

    def _disc_support_func_conic_repr(self, inst, X):
        """X is (V_n,V_t) in the local facet frame."""
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(X - tail(Y))
        inst.set_linear_term(
            0.5 * ((self.fcx + self.ftx) * Y[0] + (self.ftx - self.fcx) * X[0])
        )


class L1Rankine2D(StrengthCriterion):
    """L1-Rankine criterion in 2D.

    Parameters
    ----------
    fc : float
        compressive strength
    ft : float
        tensile strength
    """

    def __init__(self, fc, ft):
        self.fc = to_Constant(fc)
        self.ft = to_Constant(ft)
        self.dim = 2
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        a = (self.fc - self.ft) / (self.fc + self.ft)
        b = as_vector([2 * self.fc * self.ft / (self.fc + self.ft), 0, 0])
        A = as_matrix([[a, a, 0], [1, -1, 0], [0, 0, 2]])
        tr = X[0] + X[1]
        Y = inst.add_var(3, cone=Quad(3))
        inst.add_eq_constraint(dot(A, X) + Y, b=b)
        inst.add_ineq_constraint(tr, bu=self.ft, bl=-self.fc)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) =\\max\\{{ -\\sigma_{III}/f_c  ; \\sigma_{III}/f_t \\}}
            + \\max\\{{ -\\sigma_{I}/f_c  ; \\sigma_{I}/f_t \\}}`

        where :math:`\\sigma_I,\\sigma_{III}` are the major and minor principal
        stresses respectively.
        """
        assert len(Sig) == 3
        sigI = ((Sig[0] + Sig[1]) + sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)) / 2
        sigII = ((Sig[0] + Sig[1]) - sqrt((Sig[0] - Sig[1]) ** 2 + 4 * Sig[2] ** 2)) / 2
        return max(-sigI / float(self.fc), sigI / float(self.ft)) + max(
            -sigII / float(self.fc), sigII / float(self.ft)
        )


class OrthotropicL1Rankine2D(Rankine2D):
    """Orthotropic L1-Rankine criterion in 2D.

    Parameters
    ----------
    fcx : float
        compressive strength in :math:`x` direction
    fcy : float
        compressive strength in :math:`y` direction
    ftx : float
        tensile strength in :math:`x` direction
    fty : float
        tensile strength in :math:`x` direction
    ftau : float
        shear strength
    """

    def __init__(self, fcx, ftx, fcy, fty, ftau):
        self.fcx = to_Constant(fcx)
        self.ftx = to_Constant(ftx)
        self.fcy = to_Constant(fcy)
        self.fty = to_Constant(fty)
        self.ftau = to_Constant(ftau)
        self.dim = 2
        self.set_material_frame()
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Xr = dot(self.rotation_matrix, X)
        Y = inst.add_var(2)
        inst.add_ineq_constraint(Xr[0] - self.ftx * Y[0], bu=0)
        inst.add_ineq_constraint(-Xr[0] - self.fcx * Y[0], bu=0)
        inst.add_ineq_constraint(Xr[1] - self.fty * Y[1], bu=0)
        inst.add_ineq_constraint(-Xr[1] - self.fcy * Y[1], bu=0)
        inst.add_ineq_constraint(sum(Y), bu=1)
        inst.add_ineq_constraint(Xr[2], bu=self.ftau, bl=-self.ftau)


class Rankine3D(StrengthCriterion):
    """Isotropic Rankine criterion in 3D.

    Parameters
    ----------
    fc : float
        compressive strength
    ft : float
        tensile strength
    """

    def __init__(self, fc, ft):
        self.fc = fc
        self.ft = ft
        self.dim = 3
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Yp = inst.add_var(6, cone=SDP(3))
        Ym = inst.add_var(6, cone=SDP(3))
        Id = as_vector([1] * 3 + [0] * 3)
        inst.add_eq_constraint(X + Yp, b=self.ft * Id)
        inst.add_eq_constraint(-X + Ym, b=self.fc * Id)

    def _support_func_conic_repr(self, inst, X):
        Yp = inst.add_var(6, cone=SDP(3))
        Ym = inst.add_var(6, cone=SDP(3))
        inst.add_eq_constraint(X - Yp + Ym)
        if self.ft == 0:
            pit = 0
        else:
            pit = self.ft * tr(to_mat(Yp))
        inst.set_linear_term(pit + self.fc * tr(to_mat(Ym)))

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) =\\max\\{{ -\\sigma_{III}/f_c  ; \\sigma_I/f_t \\}}``

        where :math:`\\sigma_I,\\sigma_{III}` are the major and minor principal
        stresses respectively.
        """
        assert len(Sig) == 6
        sig = np.array(
            [
                [Sig[0], Sig[3], Sig[5]],
                [Sig[3], Sig[1], Sig[4]],
                [Sig[5], Sig[4], Sig[2]],
            ]
        )
        sig_princ = np.sort(np.linalg.eig(sig)[0])
        return max(-sig_princ[0] / float(self.fc), sig_princ[-1] / float(self.ft))


class L1Rankine3D(Rankine3D):
    """Isotropic L1-Rankine criterion in 2D.

    Parameters
    ----------
    fc : float
        compressive strength
    ft : float
        tensile strength
    """

    def _criterion_conic_repr(self, inst, X):
        # FIXME : wrong implementation ?
        u = inst.add_var(3)
        Yp = inst.add_var(6, cone=SDP(3))
        Ym = inst.add_var(6, cone=SDP(3))
        U = as_vector([u[i] for i in range(3)] + [0] * 3)
        inst.add_ineq_constraint(u[0] + u[1] + u[2], bu=1)
        inst.add_eq_constraint(X - self.ft * U + Yp)
        inst.add_eq_constraint(-X - self.fc * U + Ym)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) =\\sum_J\\max\\{{ -\\sigma_{J}/f_c  ; \\sigma_J/f_t \\}}``

        where :math:`\\sigma_J` are the principal stresses.
        """
        assert len(Sig) == 6
        sig = np.array(
            [
                [Sig[0], Sig[3], Sig[5]],
                [Sig[3], Sig[1], Sig[4]],
                [Sig[5], Sig[4], Sig[2]],
            ]
        )
        sig_princ = np.sort(np.linalg.eig(sig)[0])
        return sum([max(-s / float(self.fc), s / float(self.ft)) for s in sig_princ])
