#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Gurson strength criterion.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sqrt
from ufl import dot
from .strength_criterion import StrengthCriterion, deviator
from fenics_optim import RQuad, Exp, get_slice
import numpy as np
from scipy.optimize import root


class Gurson(StrengthCriterion):
    """
    Gurson criterion in 2D or 3D.

    Parameters
    ----------
    sig0 : float
        Uniaxial yield strength.
    f : float
        Porosity.
    """

    def __init__(self, sig0, f):
        self.sig0 = sig0
        self.f = f
        self.dim = [2, 3]
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        # seq^2/sig0^2 + 2f*cosh(sigkk/2/sig0) \leq 1+f^2
        # seq^2/sig0^2 + f*(exp(sigkk/2/sig0) + exp(-sigkk/2sig0)) \leq 1+f^2
        # x0 + f*(y0+z0) <= 1+f^2

        # x = sqrt(3/2)*dev(sig)
        # x^2 <= 2*x0*x1
        # y1 = 1
        # y2 = sigkk/2/sig0
        # z1 = 1
        # z2 = -sigkk/2/sig0

        if inst.dim_x == 6:
            d = 3
        elif inst.dim_x == 3:
            d = 2
        else:
            raise (ValueError, "Wrong dimension for X, must be 3 (2D) or 6 (3D)")
        Q = deviator(d)
        Y = inst.add_var(inst.dim_x + 2, cone=RQuad(inst.dim_x + 2))
        inst.add_eq_constraint(sqrt(3 / 2) * dot(Q, X) - get_slice(Y, 2))
        inst.add_eq_constraint(Y[1], b=1 / 2.0)

        T = sum(get_slice(X, end=d))
        Z1 = inst.add_var(3, cone=Exp(3))
        inst.add_eq_constraint(Z1[1], b=1)
        inst.add_eq_constraint(Z1[2] - T / 2 / self.sig0)
        Z2 = inst.add_var(3, cone=Exp(3))
        inst.add_eq_constraint(Z2[1], b=1)
        inst.add_eq_constraint(Z2[2] + T / 2 / self.sig0)

        inst.add_ineq_constraint(
            Y[0] / self.sig0 ** 2 + self.f * (Z1[0] + Z2[0]), bu=1 + self.f ** 2
        )

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = \\right(\\frac{3}{2}dev(\\sigma):dev(\\sigma)}/
            \\sigma_0\\left)^2 + 2 f \\cosh(\\sigma_m/\\sigma_0) \\leq 1+f^2`
        """
        if len(Sig) == 3:
            d = 2
        elif len(Sig) == 6:
            d = 3
        # trace
        t = sum(Sig[:d])
        # identity
        Id = np.zeros_like(Sig)
        Id[:d] = 1
        # deviator
        s = Sig - t / d * Id
        # norm
        seq = sqrt(sum(s[:d] ** 2) + 2 * sum(s[d:] ** 2))
        sigeq = sqrt(3 / 2) * seq

        def f(x):
            return x ** 2 * sigeq ** 2 / float(self.sig0 ** 2) + 2 * float(
                self.f
            ) * np.cosh(x * t / 2 / float(self.sig0))

        return 1 / root(lambda x: f(x) - (1 + float(self.f) ** 2), 1.0)["x"][0]
