#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Drucker-Prager strength criterion conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sqrt, sin, tan
from .strength_criterion import StrengthCriterion, deviator
from ufl import as_vector, tr, diag, dot
from fenics_optim import Quad, to_mat, tail


class DruckerPrager(StrengthCriterion):
    """Drucker-Prager criterion in 2D or 3D.

        :math:`||dev(\\sigma)||_2 + \\alpha tr(\\sigma) \\leq \\alpha h`

    where :math:`\\alpha = \\dfrac{{\\sqrt{{6}}\\sin(\\phi)}}
    {{\\sqrt{{3+\\sin^2(\\phi)}}}}`

    Parameters
    ----------
    h : float, Constant
        isotropic tension limit
    phi : float, Constant
        internal friction angle
    """

    def __init__(self, h, phi):
        self.h = h
        self.phi = phi
        self.dim = [2, 3]
        self.initialize()

    def _support_func_conic_repr(self, inst, X):
        if inst.dim_x == 6:
            Q = diag(as_vector([1.0, 1.0, 1.0, sqrt(2.0), sqrt(2.0), sqrt(2.0)]))
        elif inst.dim_x == 3:
            Q = diag(as_vector([1.0, 1.0, sqrt(2.0)]))
        else:
            raise (ValueError, "Wrong dimension for X, must be 3 (2D) or 6 (3D)")
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        Z0 = tr(to_mat(X))
        Z = sqrt(2) * sin(self.phi) / sqrt(1 + sin(self.phi) ** 2) * dot(Q, X)
        inst.add_eq_constraint(Z0 - Y[0])
        inst.add_eq_constraint(Z - tail(Y))
        inst.set_linear_term(self.h * Y[0])

    def _disc_support_func_conic_repr(self, inst, X):
        """X is (V_n,V_t) in the local facet frame."""
        f = tan(self.phi)
        if inst.dim_x == 2:
            Qn = diag(as_vector([1.0, f]))
        elif inst.dim_x == 3:
            Qn = diag(as_vector([1.0, f, f]))
        Y = inst.add_var(inst.dim_x, cone=Quad(inst.dim_x))
        inst.add_eq_constraint(dot(Qn, X) - Y)
        inst.set_linear_term(self.h * Y[0])

    def _criterion_conic_repr(self, inst, X):
        if inst.dim_x == 6:
            Q = deviator(3)
            T = as_vector([1.0 / 3, 1.0 / 3, 1.0 / 3, 0.0, 0.0, 0.0])
        elif inst.dim_x == 3:
            Q = deviator(2)
            T = as_vector([1.0 / 2, 1.0 / 2, 0.0])
        else:
            raise (ValueError, "Wrong dimension for X, must be 3 (2D) or 6 (3D)")
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(dot(Q, X) - tail(Y))
        alp = sqrt(6) * sin(self.phi) / sqrt(3 + sin(self.phi) ** 2)
        inst.add_eq_constraint(alp * dot(T, X) + Y[0], b=alp * self.h)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = (||dev(\\sigma)||_2 + \\alpha tr(\\sigma))/(\\alpha h)`
        """
        if len(Sig) == 3:
            d = 2
        elif len(Sig) == 6:
            d = 3
        # trace
        t = sum(Sig[:d])
        # identity
        Id = 0 * Sig
        Id[:d] = 1
        # deviator
        s = Sig - t / d * Id
        # norm
        seq = sqrt(sum(s[:d] ** 2) + 2 * sum(s[d:] ** 2))
        phi = float(self.phi)
        alp = sqrt(6) * sin(phi) / sqrt(3 + sin(phi) ** 2)
        return (seq + alp * t / d) / alp / float(self.h)
