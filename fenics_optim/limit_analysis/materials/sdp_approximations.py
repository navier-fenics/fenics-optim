#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SOC approximation strategies for SDP criteria in 3D.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .mohr_coulomb import MohrCoulomb3D
from .rankine import Rankine3D
from math import sqrt
from ufl import as_matrix, as_vector, dot, diag, tr, outer
from fenics_optim import RQuad, to_list, to_mat, to_vect


def MohrCoulomb3D_approx(
    c, phi, ft=None, approx_level=(3, 0), type="inner", directions=None
):
    """Approximate 3D Mohr-Coulomb criterion using SOC cones.

    Parameters
    ----------
    c : float
        cohesion
    phi : float
        friction angle
    ft : float, optional
        tension cut-off, by default None
    approx_level : tuple, optional
        (k, l) uses k SOC cones and l linear inequalities, by default (3, 0)
    type : {"inner", "outer"}, optional
        approximation status, by default "inner"
    directions : list, optional
        list of direction vectors along which adding linear inequalities
        for approximations with l>0, by default None
    """
    if type == "inner":
        return MohrCoulomb3D_inner_approx(
            c, phi, ft=ft, approx_level=approx_level, directions=directions
        )
    elif type == "outer":
        return MohrCoulomb3D_outer_approx(
            c, phi, ft=ft, approx_level=approx_level, directions=directions
        )
    else:
        raise ValueError("Wrong approximation type, must be 'inner' or 'outer'.")


class MohrCoulomb3D_inner_approx(MohrCoulomb3D):
    """Inner approximation of Mohr-Coulomb."""

    def __init__(self, c, phi, ft=None, approx_level=(3, 0), directions=None):
        self.approx_level = approx_level
        self.type = "inner"
        self.directions = directions
        MohrCoulomb3D.__init__(self, c, phi, ft)

    def _criterion_conic_repr(self, inst, X):
        n, m = self.approx_level
        t = inst.add_var()

        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform(wi)) for wi in generate_w(self.directions)]
        L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        iP = diag(as_vector([1, 1, 1 / sqrt(2)]))
        Ypt = [dot(dot(W[i], iP), Yp[i]) for i in range(n)] + [
            L[i] * Zp[i] for i in range(m)
        ]
        Ymt = [dot(dot(W[i], iP), Ym[i]) for i in range(n)] + [
            L[i] * Zm[i] for i in range(m)
        ]

        Id = as_vector([1] * 3 + [0] * 3)
        if self.tension_cut_off:
            alp = inst.add_var(ux=self.ft)
            inst.add_eq_constraint(X + sum(Ypt) - alp * Id)
            inst.add_eq_constraint(-X + t * Id + sum(Ymt))
            inst.add_ineq_constraint(alp - self.a * t, bu=self.a * self.fc)
        else:
            inst.add_eq_constraint(
                X - self.a * t * Id + sum(Ypt), b=self.a * self.fc * Id
            )
            inst.add_eq_constraint(-X + t * Id + sum(Ymt))

    def _support_func_conic_repr(self, inst, X):
        n, m = self.approx_level
        Yp0 = inst.add_var(6)
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zp = to_list(inst.add_var([0]*m, lx=0))
        Ym0 = inst.add_var(6)
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zm = to_list(inst.add_var([0]*m, lx=0))

        W = [as_matrix(_transform_str(wi)) for wi in generate_w(self.directions)]
        # L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        P = diag(as_vector([1, 1, sqrt(2)]))
        inst.add_eq_constraint(X - Yp0 + Ym0)
        cons = -self.a * tr(to_mat(Yp0)) + tr(to_mat(Ym0))
        for i in range(n):
            lp = [None] * (n + m)
            lp[i] = -Yp[i]
            inst.add_eq_constraint([None] + [dot(dot(P, W[i].T), Yp0)] + lp)
            lm = [None] * (n + m)
            lm[i] = -Ym[i]
            inst.add_eq_constraint(
                [None] * (1 + (n + m + 1)) + [dot(dot(P, W[i].T), Ym0)] + lm
            )
        if self.tension_cut_off:
            inst.add_ineq_constraint(cons, bu=0)
        else:
            inst.add_eq_constraint(cons)
        if self.ft == 0:
            pit = 0
        else:
            pit = self.ft * tr(to_mat(Yp0))
        if self.tension_cut_off:
            pic = (self.fc - self.ft / self.a) * tr(to_mat(Ym0))
        else:
            pic = 0
        inst.set_linear_term(pit + pic)


class MohrCoulomb3D_outer_approx(MohrCoulomb3D):
    """Outer approximation of Mohr-Coulomb."""

    def __init__(self, c, phi, ft=None, approx_level=(3, 0), directions=None):
        self.approx_level = approx_level
        self.type = "outer"
        self.directions = directions
        MohrCoulomb3D.__init__(self, c, phi, ft)

    def _support_func_conic_repr(self, inst, X):
        n, m = self.approx_level
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform(wi)) for wi in generate_w(self.directions)]
        L = [
            to_vect(outer(as_vector(li), as_vector(li)))
            for li in generate_l(self.directions)
        ]
        P = diag(as_vector([1, 1, 1 / sqrt(2)]))
        Ypt = [dot(dot(W[i], P), Yp[i]) for i in range(n)] + [
            L[i] * Zp[i] for i in range(m)
        ]
        Ymt = [dot(dot(W[i], P), Ym[i]) for i in range(n)] + [
            L[i] * Zm[i] for i in range(m)
        ]
        inst.add_eq_constraint([X] + [-y for y in Ypt] + Ymt)
        cons = (
            [0] + [-self.a * tr(to_mat(y)) for y in Ypt] + [tr(to_mat(y)) for y in Ymt]
        )
        if self.tension_cut_off:
            inst.add_ineq_constraint(cons, bu=0)
        else:
            inst.add_eq_constraint(cons)
        if self.ft == 0:
            pit = 0
        else:
            pit = sum([self.ft * tr(to_mat(y)) for y in Ypt])
        if self.tension_cut_off:
            pic = sum([(self.fc - self.ft / self.a) * tr(to_mat(y)) for y in Ymt])
        else:
            pic = 0
        inst.set_linear_term(pit + pic)

    def _criterion_conic_repr(self, inst, X):
        n, m = self.approx_level
        assert m == 0 and not self.tension_cut_off, "Not implemented"
        t = inst.add_var()
        Yp0 = inst.add_var(6)
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zp = to_list(inst.add_var([0]*m, lx=0))
        Ym0 = inst.add_var(6)
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zm = to_list(inst.add_var([0]*m, lx=0))

        W = [as_matrix(_transform_str(wi)) for wi in generate_w(self.directions)]
        # L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        P = diag(as_vector([1, 1, 1 / sqrt(2)]))

        Id = as_vector([1] * 3 + [0] * 3)
        if self.tension_cut_off:
            alp = inst.add_var(ux=self.ft)
            inst.add_eq_constraint(X + Yp0 - alp * Id)
            inst.add_eq_constraint(-X + t * Id + Ym0)
            inst.add_ineq_constraint(
                -self.a * t + alp,
                bu=self.a * self.fc,
            )
        else:
            inst.add_eq_constraint(X - self.a * t * Id + Yp0, b=self.a * self.fc * Id)
            inst.add_eq_constraint(-X + t * Id + Ym0)
            for i in range(n):
                inst.add_eq_constraint(dot(W[i].T, Yp0) - dot(P, Yp[i]))
                inst.add_eq_constraint(dot(W[i].T, Ym0) - dot(P, Ym[i]))


def Rankine3D_approx(fc, ft, approx_level=(3, 0), type="inner", directions=None):
    """Approximate 3D Rankine criterion using SOC cones.

    Parameters
    ----------
    fc : float
        compressive strength
    ft : float
        tensile strength
    approx_level : tuple, optional
        (k, l) uses k SOC cones and l linear inequalities, by default (3, 0)
    type : {"inner", "outer"}, optional
        approximation status, by default "inner"
    directions : list, optional
        list of direction vectors along which adding linear inequalities
        for approximations with l>0, by default None
    """
    if type == "inner":
        return Rankine3D_inner_approx(
            fc=fc, ft=ft, approx_level=approx_level, directions=directions
        )
    elif type == "outer":
        return Rankine3D_outer_approx(fc=fc, ft=ft, approx_level=approx_level)
    else:
        raise ValueError("Wrong approximation type, must be 'inner' or 'outer'.")


class Rankine3D_inner_approx(Rankine3D):
    """Inner approximation of Rankine."""

    def __init__(self, fc=1.0, ft=1.0, approx_level=(3, 0), directions=None):
        self.approx_level = approx_level
        self.directions = directions
        self.type = "inner"
        Rankine3D.__init__(self, fc, ft)

    def _criterion_conic_repr(self, inst, X):
        n, m = self.approx_level

        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform(wi)) for wi in generate_w(self.directions)]
        L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        iP = diag(as_vector([1, 1, 1 / sqrt(2)]))
        Ypt = [dot(dot(W[i], iP), Yp[i]) for i in range(n)] + [
            L[i] * Zp[i] for i in range(m)
        ]
        Ymt = [dot(dot(W[i], iP), Ym[i]) for i in range(n)] + [
            L[i] * Zm[i] for i in range(m)
        ]

        Id = as_vector([1] * 3 + [0] * 3)
        inst.add_eq_constraint(X + sum(Ypt), b=self.ft * Id)
        inst.add_eq_constraint(-X + sum(Ymt), b=self.fc * Id)

    def _support_func_conic_repr(self, inst, X):
        n, m = self.approx_level
        Yp0 = inst.add_var(6)
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym0 = inst.add_var(6)
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform_str(wi)) for wi in generate_w(self.directions)]
        # L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        P = diag(as_vector([1, 1, sqrt(2)]))
        inst.add_eq_constraint(X - Yp0 + Ym0)
        for i in range(n):
            inst.add_eq_constraint(dot(dot(P, W[i].T), Yp0) - Yp[i])
            inst.add_eq_constraint(dot(dot(P, W[i].T), Ym0) - Ym[i])
        inst.set_linear_term(self.ft * tr(to_mat(Yp0)) + self.fc * tr(to_mat(Ym0)))


class Rankine3D_outer_approx(Rankine3D):
    """Outer approximation of Rankine."""

    def __init__(self, fc=1, ft=1, approx_level=(3, 1), directions=None):
        self.approx_level = approx_level
        self.type = "outer"
        self.directions = directions
        Rankine3D.__init__(self, fc, ft)

    def _support_func_conic_repr(self, inst, X):
        n, m = self.approx_level
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform(wi)) for wi in generate_w(self.directions)]
        L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        P = diag(as_vector([1, 1, 1 / sqrt(2)]))
        Ypt = [dot(dot(W[i], P), Yp[i]) for i in range(n)] + [
            L[i] * Zp[i] for i in range(m)
        ]
        Ymt = [dot(dot(W[i], P), Ym[i]) for i in range(n)] + [
            L[i] * Zm[i] for i in range(m)
        ]
        inst.add_eq_constraint([X] + [-y for y in Ypt] + Ymt)
        pit = [self.ft * tr(to_mat(y)) for y in Ypt]
        pic = [self.fc * tr(to_mat(y)) for y in Ymt]
        inst.set_linear_term(sum(pit) + sum(pic))

    def _criterion_conic_repr(self, inst, X):
        n, m = self.approx_level

        Yp0 = inst.add_var(6)
        Yp = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zp = to_list(inst.add_var([0] * m, lx=0))
        Ym0 = inst.add_var(6)
        Ym = inst.add_var([3] * n, cone=[RQuad(3)] * n)
        # Zm = to_list(inst.add_var([0] * m, lx=0))

        W = [as_matrix(_transform_str(wi)) for wi in generate_w(self.directions)]
        # L = [to_vect(outer(as_vector(li), as_vector(li))) for li in generate_l()]
        P = diag(as_vector([1, 1, 1 / sqrt(2)]))

        Id = as_vector([1] * 3 + [0] * 3)
        inst.add_eq_constraint(X + Yp0, b=self.ft * Id)
        inst.add_eq_constraint(-X + Ym0, b=self.fc * Id)
        for i in range(n):
            inst.add_eq_constraint(dot(W[i].T, Yp0) - dot(P, Yp[i]))
            inst.add_eq_constraint(dot(W[i].T, Ym0) - dot(P, Ym[i]))
        inst.add_eq_constraint(X + Yp0, b=self.ft * Id)
        inst.add_eq_constraint(-X + Ym0, b=self.fc * Id)
        for i in range(n):
            inst.add_eq_constraint(
                -dot(W[i].T, X) - dot(P, Yp[i]), b=-dot(W[i].T, self.ft * Id)
            )
            inst.add_eq_constraint(
                dot(W[i].T, X) - dot(P, Ym[i]),
                b=-dot(W[i].T, self.fc * Id),
            )


def _transform(w):
    return [[w[i][0] ** 2, w[i][1] ** 2, 2 * w[i][0] * w[i][1]] for i in range(3)] + [
        [
            w[i][0] * w[(i + 1) % 3][0],
            w[i][1] * w[(i + 1) % 3][1],
            w[i][0] * w[(i + 1) % 3][1] + w[i][1] * w[(i + 1) % 3][0],
        ]
        for i in range(3)
    ]


def _transform_str(w):
    return [[w[i][0] ** 2, w[i][1] ** 2, w[i][0] * w[i][1]] for i in range(3)] + [
        [
            2 * w[i][0] * w[(i + 1) % 3][0],
            2 * w[i][1] * w[(i + 1) % 3][1],
            w[i][0] * w[(i + 1) % 3][1] + w[i][1] * w[(i + 1) % 3][0],
        ]
        for i in range(3)
    ]


def generate_w(directions=None):
    """Generate directions for approximations using SOC inequalities."""
    if directions is None:
        return [
            [[1, 0], [0, 1], [0, 0]],
            [[1, 0], [0, 0], [0, 1]],
            [[0, 0], [1, 0], [0, 1]],
            [[1, 0], [1, 0], [0, 1]],
            [[0, 1], [1, 0], [1, 0]],
            [[1, 0], [0, 1], [1, 0]],
            [[1, 0], [-1, 0], [0, 1]],
            [[1, 0], [0, 1], [-1, 0]],
            [[0, 1], [1, 0], [-1, 0]],
        ]
    else:
        other_dir = [directions[1], directions[2], directions[0]]
        return [
            [[dI[i], dII[i]] for i in range(3)]
            for (dI, dII) in zip(directions, other_dir)
        ]


def generate_l(directions=None):
    """Generate directions for approximations using linear inequalities."""
    if directions is None:
        return [[-1, -1, -1], [-1, 1, 1], [1, -1, 1], [1, 1, -1]]
    else:
        return directions
