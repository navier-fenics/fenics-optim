#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
von Mises strength criteria.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sqrt
from ufl import sqrt as ufl_sqrt
from ufl import as_matrix, as_vector, dot, diag, tr, inner
from .strength_criterion import StrengthCriterion, deviator
from fenics_optim import Quad, to_mat, tail
import numpy as np


class vonMises_plane_stress(StrengthCriterion):
    """Plane stress von Mises criterion."""

    def __init__(self, sig0):
        self.sig0 = sig0
        self.dim = 2
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        assert inst.dim_x == 3, "Plane stress criterion available only in 2D"

        J = as_matrix([[1.0, -1.0 / 2, 0.0], [0, sqrt(3.0) / 2, 0.0], [0, 0, sqrt(3)]])
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(dot(J, X) - tail(Y))
        inst.add_eq_constraint(Y[0], b=self.sig0)

    def _support_func_conic_repr(self, inst, X):
        assert inst.dim_x == 3, "Plane stress criterion available only in 2D"

        J = as_matrix([[2.0, 1.0, 0.0], [0, sqrt(3.0), 0.0], [0, 0, 2]])
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(dot(J, X) - tail(Y))
        inst.set_linear_term(self.sig0 / sqrt(3) * Y[0])


class vonMises(StrengthCriterion):
    """2D/3D von Mises criterion."""

    def __init__(self, k):
        self.k = k
        self.dim = [2, 3]
        self.initialize()

    def _support_func_conic_repr(self, inst, X):
        if inst.dim_x == 6:
            Q = diag(as_vector([1.0, 1.0, 1.0, sqrt(2.0), sqrt(2.0), sqrt(2.0)]))
        elif inst.dim_x == 3:
            Q = diag(as_vector([1.0, 1.0, sqrt(2.0)]))
        else:
            raise (ValueError, "Wrong dimension for X, must be 3 (2D) or 6 (3D)")
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(dot(Q, X) - tail(Y))
        inst.add_eq_constraint(tr(to_mat(X)))
        inst.set_linear_term(sqrt(2) * self.k * Y[0])

    def _disc_support_func_conic_repr(self, inst, X):
        Y = inst.add_var(inst.dim_x, cone=Quad(inst.dim_x))
        inst.add_eq_constraint(tail(X) - tail(Y))
        inst.add_eq_constraint(X[0])
        inst.set_linear_term(self.k * Y[0])

    def _criterion_conic_repr(self, inst, X):
        if inst.dim_x == 6:
            Q = deviator(3)
        elif inst.dim_x == 3:
            Q = deviator(2)
        else:
            raise (ValueError, "Wrong dimension for X, must be 3 (2D) or 6 (3D)")
        Y = inst.add_var(inst.dim_x + 1, cone=Quad(inst.dim_x + 1))
        inst.add_eq_constraint(dot(Q, X) - tail(Y))
        inst.add_eq_constraint(Y[0], b=sqrt(2.0) * self.k)

    def gauge_function(self, Sig):
        """
        Numerical evaluation of the gauge function.

            :math:`g(\\sigma) = \\sqrt{\\frac{1}{2}dev(\\sigma):dev(\\sigma)}/k`
        """
        if len(Sig) == 3:
            d = 2
        elif len(Sig) == 6:
            d = 3
        # trace
        t = sum(Sig[:d])
        # identity
        Id = np.zeros_like(Sig)
        Id[:d] = 1
        # deviator
        s = Sig - t / d * Id
        # norm
        seq = sqrt(sum(s[:d] ** 2) + 2 * sum(s[d:] ** 2))
        return seq / sqrt(2) / float(self.k)

    def _compute_pi(self, d):
        return ufl_sqrt(2 * inner(d, d)) * self.k


class vonMises_shell(StrengthCriterion):
    """von Mises shell criterion.

    Parameters
    ----------
    sig0 : float, Constant
        uniaxial yield strength
    thick : float
        shell thickness
    nz : int
        number of quadrature points in z direction
    quadrature : {"gauss", "trapezoidal", "piecewise"}
        quadrature rule for z direction
    """

    def __init__(self, sig0, thick, nz, quadrature):
        self.sig0 = sig0
        self.z_quadrature_rule = quadrature
        self.thick = thick
        self.zm = -thick / 2
        self.zp = thick / 2
        self.nz = nz
        zz, wz = self.z_quadrature()
        self.zz, self.wz = zz, wz
        self.ngz = len(self.zz)
        self.initialize()

    def L(self, z):  # noqa
        return as_matrix(
            [[1, 0, 0], [0, 1, 0], [0, 0, 1], [-z, 0, 0], [0, -z, 0], [0, 0, -z]]
        )

    def _criterion_conic_repr(self, inst, X):
        Yz = []
        for i in range(self.nz):
            Yz.append(inst.add_var(4, cone=Quad(4)))
        iJ = as_matrix([[1, 1 / sqrt(3), 0], [0, 2 / sqrt(3), 0], [0, 0, 1 / sqrt(3)]])
        equil = X - sum(
            [
                -dot(w * self.L(z), dot(iJ, tail(Y)))
                for (z, w, Y) in zip(self.zz, self.wz, Yz)
            ]
        )
        inst.add_eq_constraint(equil)
        for Y in Yz:
            inst.add_eq_constraint(Y[0], b=self.sig0)

    def _support_func_conic_repr(self, inst, X):
        Yz = inst.add_var([4] * self.nz, cone=Quad(4))
        iJ = as_matrix([[1, 1 / sqrt(3), 0], [0, 2 / sqrt(3), 0], [0, 0, 2 / sqrt(3)]])
        for k in range(self.nz):
            ll = [dot(iJ.T, dot(self.L(self.zz[k]).T, X))] + [None] * self.nz
            ll[k + 1] = -tail(Yz[k])
            inst.add_eq_constraint(ll)
        inst.set_linear_term(sum([w * Y[0] for (w, Y) in zip(self.wz, Yz)]))

    def z_quadrature(self):
        """Generate quadrature in z direction."""
        if self.z_quadrature_rule == "trapezoidal":
            assert self.nz >= 2
            zz = np.linspace(self.zm, self.zp, self.nz)
            wz = self.thick / (self.nz - 1) * np.ones((self.nz,))
            wz[0] /= 2
            wz[-1] /= 2
        elif self.z_quadrature_rule == "piecewise":
            z = np.linspace(self.zm, self.zp, self.nz + 1)
            zz = (z[:-1] + z[1:]) / 2
            wz = self.thick / self.nz * np.ones((self.nz,))
        elif self.z_quadrature_rule == "gauss":
            zz, wz = np.polynomial.legendre.leggauss(self.nz)
            zz *= self.thick / 2
            wz *= self.thick / 2
        else:
            raise (
                ValueError,
                "Wrong quadrature rule, must be 'trapezoidal', 'piecewise' or 'gauss'",
            )
        return zz, wz
