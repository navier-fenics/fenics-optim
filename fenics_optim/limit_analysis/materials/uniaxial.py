#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
von Mises strength criteria.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from ufl import dot, outer, as_vector, cos, sin
from .strength_criterion import StrengthCriterion, to_Constant
from fenics_optim import Quad, to_mat, to_vect, to_list

class Uniaxial1D(StrengthCriterion):
    """1D Uniaxial strength criterion."""

    def __init__(self, fc, ft):
        self.fc = to_Constant(fc)
        self.ft = to_Constant(ft)
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        inst.add_ineq_constraint(X, bu=self.ft, bl=-self.fc)


class Uniaxial(StrengthCriterion):
    """Uniaxial strength criterion."""

    def __init__(self, fc, ft, direction):
        self.fc = to_Constant(fc)
        self.ft = to_Constant(ft)
        self.direction = direction
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        y = inst.add_var()
        sig_nn = y * outer(self.direction, self.direction)
        inst.add_eq_constraint(X - to_vect(sig_nn))
        inst.add_ineq_constraint(y, bu=self.ft, bl=-self.fc)

    def _support_func_conic_repr(self, inst, X):

        y = inst.add_var(dim=2, cone=Quad(2))
        eps_nn = y[1] * outer(self.direction, self.direction)
        inst.add_eq_constraint(X - to_vect(eps_nn))
        inst.set_linear_term(self.sig0 * y[0])


class MultiaxialSum(StrengthCriterion):
    """Minkowski sum of multiple uniaxial strength criteria."""

    def __init__(self, fc, ft, directions=None, angles=None):
        self.fc = to_Constant(fc)
        self.ft = to_Constant(ft)
        if directions is None:
            self.fc = to_list(fc, len(angles))
            self.ft = to_list(ft, len(angles))
            self.directions = [as_vector([cos(a), sin(a)]) for a in angles]
        else:
            self.fc = to_list(fc, len(directions))
            self.ft = to_list(ft, len(directions))
            self.directions = directions

        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        y = inst.add_var(len(self.directions))
        sig_nn = sum(
            [
                y[i] * outer(direction, direction)
                for (i, direction) in enumerate(self.directions)
            ]
        )
        inst.add_eq_constraint(X - to_vect(sig_nn))
        for (i, direction) in enumerate(self.directions):
            inst.add_ineq_constraint(y[i], bu=self.ft[i], bl=-self.fc[i])


class MultiaxialConvHull(StrengthCriterion):
    """Convex hull of multiple uniaxial strength criteria."""

    def __init__(self, fc, ft, directions=None, angles=None):
        self.fc = to_Constant(fc)
        self.ft = to_Constant(ft)
        if directions is None:
            self.fc = to_list(fc, len(angles))
            self.ft = to_list(ft, len(angles))
            self.directions = [as_vector([cos(a), sin(a)]) for a in angles]
        else:
            self.fc = to_list(fc, len(directions))
            self.ft = to_list(ft, len(directions))
            self.directions = directions

        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        y = inst.add_var(len(self.directions))
        z = inst.add_var(len(self.directions), lx=0)
        sig_nn = sum(
            [
                y[i] * outer(direction, direction)
                for (i, direction) in enumerate(self.directions)
            ]
        )
        inst.add_eq_constraint(X - to_vect(sig_nn))
        for (i, direction) in enumerate(self.directions):
            inst.add_ineq_constraint(y[i] - z[i] * self.ft[i], bu=0)
            inst.add_ineq_constraint(-y[i] - z[i] * self.fc[i], bu=0)

        inst.add_ineq_constraint(sum(z), bu=1)
