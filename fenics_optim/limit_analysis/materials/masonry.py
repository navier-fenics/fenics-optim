#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Homogenized strength criteria of periodic masonry.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import tan
from .strength_criterion import StrengthCriterion
from ufl import as_matrix, dot


class deBuhan_deFelice(StrengthCriterion):
    """2D homogenized criterion of de Buhan and de Felice.

    `A homogenization approach to the ultimate strength of brick masonry, JMPS, 1997
    <https://doi.org/10.1016/S0022-5096(97)00002-1>`_.

    Blocks are rigid and arranged in a running bond pattern.
    Joints follow a Coulomb friction law.

    Parameters
    ----------
    width : float
        block width
    height : float
        block height
    phi : float
        joint friction angle [rad]
    c : float, optional
        joint cohesion, by default 0.
    """

    def __init__(self, width, height, phi, c=0.0):
        self.width = width
        self.height = height
        self.phi = phi
        self.c = c
        self.dim = 2
        self.initialize()

    def _support_func_conic_repr(self, inst, X):
        m = 2 * self.height / self.width
        f = tan(self.phi)
        if m <= 1 / f:
            M = as_matrix([[-1, 0, 0], [f, -m, 0], [-f, -1 / f, 2], [-f, -1 / f, -2]])
        else:
            M = as_matrix(
                [
                    [-1, 0, 0],
                    [f, -m, 0],
                    [-1 / m, -m, 2],
                    [-1 / m, -m, -2],
                    [(-1 / f + 1 / m / f ** 2 - 1 / m), -1 / f, 2],
                    [(-1 / f + 1 / m / f ** 2 - 1 / m), -1 / f, -2],
                ]
            )
        inst.add_ineq_constraint(dot(M, X), bu=0)
        inst.set_linear_term(self.c / f * (X[0] + X[1]))
