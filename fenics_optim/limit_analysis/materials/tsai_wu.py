#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tsai-Wu anisotropic strength criterion conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sqrt
from ufl import as_vector, dot, diag
from .strength_criterion import StrengthCriterion, rotation_matrix
from fenics_optim import Quad, tail


class TsaiWu(StrengthCriterion):
    """Tsai-Wu anisotropic strength criterion.

    The behaviour is transverse isotropic with axis given by direction 1.

    Parameters
    ----------
    fc1 : float
        compressive strength along direction 1
    ft1 : float
        tensile strength along direction 1
    fc2 : float
        compressive strength along direction 2/3
    ft2 : float
        tensile strength along direction 2/3
    tau12 : float
        shear strength in 1/(2,3) plane
    tau23 : float
        shear strength in 2/3 plane
    orient : float, optional
        fiber orientation with respect to x axis, by default 0
    dim : int, optional
        criterion dimension, by default 3
    """

    def __init__(self, fc1, ft1, fc2, ft2, tau12, tau23, orient=0, dim=3):
        self.fc1 = fc1
        self.ft1 = ft1
        self.fc2 = fc2
        self.ft2 = ft2
        self.tau12 = tau12
        self.tau23 = tau23
        self.dim = dim
        F1 = 1 / ft1 - 1 / fc1
        F2 = 1 / ft2 - 1 / fc2
        F3 = F2
        P11 = sqrt(1 / ft1 / fc1)
        P22 = sqrt(1 / ft2 / fc2)
        P33 = P22
        P44 = 1 / tau12
        P55 = 1 / tau23
        P66 = 1 / tau12
        if dim == 2:
            self.F = as_vector([F1, F2, 0])
            self.P = diag(as_vector([P11, P22, P44]))
            self.iP = diag(as_vector([1 / P11, 1 / P22, 1 / P44]))
        else:
            self.F = as_vector([F1, F2, F3, 0, 0, 0])
            self.P = diag(as_vector([P11, P22, P33, P44, P55, P66]))
            self.iP = diag(
                as_vector([1 / P11, 1 / P22, 1 / P33, 1 / P44, 1 / P55, 1 / P66])
            )

        self.cent = -1 / 2 * dot(self.iP, dot(self.iP, self.F))
        self.r0 = sqrt(1 - dot(self.F, self.cent) / 2)
        self.R = rotation_matrix(orient, dim)
        self.initialize()

    def _criterion_conic_repr(self, inst, X):
        Xr = dot(self.R, X)
        if self.dim == 2:
            Y = inst.add_var(4, cone=Quad(4))
        else:
            Y = inst.add_var(7, cone=Quad(7))
        inst.add_eq_constraint(dot(self.P, Xr) - tail(Y), b=dot(self.P, self.cent))
        inst.add_eq_constraint(Y[0], b=self.r0)

    def _support_func_conic_repr(self, inst, X):
        if self.dim == 2:
            Y = inst.add_var(4, cone=Quad(4), name="Auxiliary_TW")
            Q = diag(as_vector([1, 1, 2]))
        else:
            Y = inst.add_var(7, cone=Quad(7))
            Q = diag(as_vector([1, 1, 1, 2, 2, 2]))
        Xv = dot(Q, X)
        inst.add_eq_constraint(dot(self.R.T, dot(self.iP, Xv)) - tail(Y))
        inst.set_linear_term(dot(self.cent, Xv) + self.r0 * Y[0])
