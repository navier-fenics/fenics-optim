#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Utility functions and generic classes for strength criterion conic representation.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from math import sin, cos, sqrt
import ufl
import dolfin as df
from fenics_optim import ConvexFunction
import warnings


def to_Constant(p):
    """Wrap as a Constant."""
    if type(p) in [int, float]:
        return df.Constant(p)
    else:
        return p


def rotation_matrix(alpha=0, dim=3):
    """Rotation matrix for angle :math:`\\alpha` with respect to horizontal axis."""
    c = cos(alpha)
    s = sin(alpha)
    if dim == 2:
        return ufl.as_matrix(
            [
                [c ** 2, s ** 2, 2 * s * c],
                [s ** 2, c ** 2, -2 * s * c],
                [-c * s, c * s, c ** 2 - s ** 2],
            ]
        )
    else:
        return ufl.as_matrix(
            [
                [c ** 2, s ** 2, 0, 2 * s * c, 0, 0],
                [s ** 2, c ** 2, 0, -2 * s * c, 0, 0],
                [0, 0, 1, 0, 0, 0],
                [-c * s, c * s, 0, c ** 2 - s ** 2, 0, 0],
                [0, 0, 0, 0, -s, c],
                [0, 0, 0, 0, c, s],
            ]
        )


def deviator(dim):
    """Matrix for deviator operator."""
    if dim == 3:
        Q = ufl.as_matrix(
            [
                [2 / 3.0, -1 / 3.0, -1 / 3.0, 0, 0, 0],
                [-1 / 3.0, 2 / 3.0, -1 / 3.0, 0, 0, 0],
                [-1 / 3.0, -1 / 3.0, 2 / 3.0, 0, 0, 0],
                [0, 0, 0, sqrt(2), 0, 0],
                [0, 0, 0, 0, sqrt(2), 0],
                [0, 0, 0, 0, 0, sqrt(2)],
            ]
        )
    elif dim == 2:
        Q = ufl.as_matrix(
            [[1 / 2.0, -1 / 2.0, 0], [-1 / 2.0, 1 / 2.0, 0], [0, 0, sqrt(2)]]
        )
    return Q


class StrengthCriterion:
    """A generic object for limit analysis strength criteria.

    A :class:`~limit_analysis.materials.strength_criterion.StrengthCriterion` object
    provides three different :class:`ConvexFunction` instances, namely:

    * :attr:`criterion`: the strength criterion constraint :math:`\\boldsymbol{\\sigma}
      \\in G`
    * :attr:`support_function`: the corresponding support function
      :math:`\\pi(\\boldsymbol{d}) = \\displaystyle{\\sup_{\\boldsymbol{\\sigma}\\in G}
      \\{\\boldsymbol{\\sigma}:\\boldsymbol{d}\\}}`
    * :attr:`support_function_disc` : the support function associated with
      discontinuities :math:`\\Pi(\\boldsymbol{V}:\\boldsymbol{n}) =
      \\pi(\\boldsymbol{V}\\overset{s}{\\otimes} \\boldsymbol{n})`
    """

    def initialize(self):
        """Must be called before ending __init__."""
        func_list = [
            "_criterion_conic_repr",
            "_support_func_conic_repr",
            "_disc_support_func_conic_repr",
        ]
        string_list = ["criterion", "support_function", "disc_support_function"]
        for (f, s) in zip(func_list, string_list):
            try:
                func = getattr(self, f)
                setattr(self, s, self._generate_convex_func(func))
            except Exception:
                warnings.warn(
                    "\n-----------\n{} {} not implemented!\n-----------".format(
                        self.__class__.__name__, s
                    ),
                    UserWarning,
                )

    def set_material_frame(self, theta=0):
        """
        Define rotation matrix for material frame.

        theta is the angle with respect to the x direction.
        """
        self.theta = theta
        self.rotation_matrix = rotation_matrix(theta, dim=self.dim)

    def _generate_convex_func(self, conic_representation):
        def conic_repr(inst, X):
            return conic_representation(inst, X)

        class ThisConvexFunction(ConvexFunction):
            pass

        ThisConvexFunction.conic_repr = conic_repr
        return ThisConvexFunction

    def set_domain(self, i):
        """Specify the corresponding domain id."""
        self.domain_id = i

    def name(self):
        """Strength criterion name."""
        return self.__class__.__name__

    def _compute_pi(self, d):
        raise NotImplementedError(
            "Support function evaluation has not been implemented in \
            '{}' criterion.".format(
                self.__class__.__name__
            )
        )


class Rigid(StrengthCriterion):
    """A rigid material."""

    def __init__(self):
        self.dim = [2, 3]
        self.initialize()

    def _support_func_conic_repr(self, inst, X):
        inst.add_eq_constraint(X)
