#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 17:20:26 2019

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import dolfin as df
from fenics_optim import MosekProblem, to_list, to_mat, to_vect, local_frame
from ufl import sym, grad, dot, jump, div, avg
import numpy as np
from .adapt_fix import adapt


class KinematicApproach:
    def __init__(
        self,
        mesh,
        material,
        facets=None,
        domains=None,
        degree=2,
        interpolation="CG",
        user_callback=None,
    ):
        self.mesh = mesh
        self.facets = facets
        self.domains = domains
        self.ds = df.Measure("ds", subdomain_data=self.facets, domain=self.mesh)
        self.dS = df.Measure("dS", subdomain_data=self.facets, domain=self.mesh)
        self.dx = df.Measure("dx", subdomain_data=self.domains, domain=self.mesh)
        self.n = df.FacetNormal(self.mesh)
        self.measures = {"dx": self.dx, "ds": self.ds, "dS": self.dS}
        self.degree_u = degree
        self.interpolation = interpolation
        self.materials = to_list(material)
        self.bc = None
        self.loading = None
        self.fixed_loading = None
        self.V = df.VectorFunctionSpace(self.mesh, self.interpolation, self.degree_u)
        self.prob = MosekProblem("kinematic approach")
        self.user_callback = user_callback

    def set_problem(self):

        self.u = self.prob.add_var(self.V, bc=self.bc, name="Mechanism")

        R = df.FunctionSpace(self.mesh, "R", 0)

        if self.loading is not None:

            def Pext(lamb):
                return lamb * self.loading(self.u)

            self.prob.add_eq_constraint(R, A=Pext, b=1)
        if self.fixed_loading is not None:
            self.prob.add_obj_func([-self.fixed_loading(self.u)])

        self.strain = to_vect(sym(grad(self.u)))
        self.pi = []
        for mat in self.materials:
            if hasattr(mat, "domain_id"):
                dxi = self.measures["dx"](mat.domain_id)
            else:
                dxi = self.measures["dx"]
            if self.degree_u > 0:
                if self.degree_u == 1:
                    ppi = mat.support_function(self.strain, measure=dxi)
                elif self.degree_u == 2:
                    ppi = mat.support_function(
                        self.strain, quadrature_scheme="vertex", measure=dxi
                    )
                else:
                    ppi = mat.support_function(
                        self.strain, degree=self.degree_u + 1, measure=dxi
                    )
                self.prob.add_convex_term(ppi)
                self.pi.append(ppi)

        if self.interpolation == "DG":
            assert (
                len(self.materials) == 1
            ), "DG interpolation not supported for multiple materials"
            P_dS = local_frame(self.n("-"))
            P_ds = local_frame(self.n)
            self.discontinuities = [dot(P_dS, jump(self.u)), -dot(P_ds, self.u)]
            self.pi_disc = self.materials[0].support_function_disc(
                self.discontinuities,
                degree=self.degree_u + 1,
                on_facet=True,
                measure=[self.measures["dS"], self.measures["ds"]],
            )
            self.prob.add_convex_term(self.pi_disc)

    def post_process(self):
        Prm_d = 0
        if self.interpolation == "DG":
            Prm_d += sum(self.pi_disc.compute_cellwise().vector().get_local())
        print("Prm_disc:", Prm_d)
        Prm_b = 0
        if self.degree_u > 0:
            for (i, mat) in enumerate(self.materials):
                Prm_b += sum(self.pi[i].compute_cellwise().vector().get_local())
        print("Prm_bulk:", Prm_b)
        if self.user_callback is not None:
            self.user_callback(self)

    def optimize(self, refine_levels=1, refine_ratio=0.5):
        self.set_problem()
        self.prob.optimize()
        self.post_process()
        for k in range(refine_levels - 1):
            self.refine(refine_ratio)
            self.set_problem()
            self.prob.optimize()
            self.post_process()

    def refine(self, ratio):
        cell_markers = df.MeshFunction("bool", self.mesh, self.mesh.topology().dim())
        gamma = np.zeros((self.mesh.num_cells(),))
        if self.interpolation == "DG":
            gamma = self.pi_disc.compute_cellwise().vector().get_local()
        if self.degree_u > 0:
            for (i, mat) in enumerate(self.materials):
                gamma += self.pi[i].compute_cellwise().vector().get_local()

        index_sort = np.argsort(gamma)[::-1]
        cum_sum = np.cumsum(gamma[index_sort])
        for c in df.cells(self.mesh):
            cell_markers[c] = cum_sum[np.where(c.index() == index_sort)] < ratio * sum(
                gamma
            )
        self.mesh = df.refine(self.mesh, cell_markers)
        self.V = df.VectorFunctionSpace(self.mesh, self.interpolation, self.degree_u)
        if self.facets:
            self.facets = adapt(self.facets, self.mesh)
        if self.domains:
            self.domains = adapt(self.domains, self.mesh)
        self.bc = [
            adapt(bci, self.mesh, self.V._cpp_object) for bci in to_list(self.bc)
        ]
        self.ds = df.Measure("ds", subdomain_data=self.facets, domain=self.mesh)
        self.dS = df.Measure("dS", subdomain_data=self.facets, domain=self.mesh)
        self.dx = df.Measure("dx", subdomain_data=self.domains, domain=self.mesh)
        self.measures = {"dx": self.dx, "ds": self.ds(1), "dS": self.dS}
        self.n = df.FacetNormal(self.mesh)
        old_params = self.prob.parameters
        self.prob = MosekProblem("kinematic approach")
        self.prob.parameters = old_params


class StaticApproach:
    def __init__(self, mesh, material, facets=None, domains=None, user_callback=None):
        self.mesh = mesh
        self.facets = facets
        self.domains = domains
        self.ds = df.Measure("ds", subdomain_data=self.facets, domain=self.mesh)
        self.dS = df.Measure("dS", subdomain_data=self.facets, domain=self.mesh)
        self.dx = df.Measure("dx", subdomain_data=self.domains, domain=self.mesh)
        self.n = df.FacetNormal(self.mesh)
        self.measures = {"dx": self.dx, "ds": self.ds, "dS": self.dS}
        self.materials = to_list(material)
        self.bc = None
        self.loading = 0
        self.fixed_loading = 0
        self.dim = self.mesh.geometry().dim()
        self.Vsig = df.VectorFunctionSpace(
            self.mesh, "DG", 1, dim=self.dim * (self.dim + 1) // 2
        )
        self.V_eq = df.VectorFunctionSpace(self.mesh, "DG", 0)
        self.V = df.VectorFunctionSpace(self.mesh, "Discontinuous Lagrange Trace", 1)
        self.R = df.FunctionSpace(self.mesh, "R", 0)
        self.prob = MosekProblem("static approach")
        self.user_callback = user_callback

    def set_problem(self):

        self.lamb, self.Sig = self.prob.add_var([self.R, self.Vsig])

        sig = to_mat(self.Sig)

        def equilibrium(u):
            return self.lamb * self.loading(u), dot(u, div(sig)) * self.dx

        self.prob.add_eq_constraint(self.V_eq, A=equilibrium, b=self.fixed_loading)

        def continuity(u):
            return dot(avg(u), dot(jump(sig), self.n("-"))) * self.dS + dot(
                u, dot(sig, self.n)
            ) * self.ds(0)

        self.prob.add_eq_constraint(self.V, A=continuity)

        self.prob.add_obj_func([1, None])

        self.crit = []
        for mat in self.materials:
            if hasattr(mat, "domain_id"):
                dxi = self.measures["dx"](mat.domain_id)
            else:
                dxi = self.measures["dx"]
            ccrit = mat.criterion(self.Sig, quadrature_scheme="vertex", measure=dxi)
            self.prob.add_convex_term(ccrit)
            self.crit.append(ccrit)

    def optimize(self, refine_levels=1, refine_ratio=0.5):
        self.set_problem()
        self.prob.optimize(sense="max")
        self.post_process()
        for k in range(refine_levels - 1):
            self.refine(refine_ratio)
            self.set_problem()
            self.prob.optimize(sense="max")
            print("Objective function:", self.prob.pobj)
            self.post_process()

    def post_process(self):
        if self.user_callback is not None:
            self.user_callback(self)

    def refine(self, ratio):
        V0 = df.FunctionSpace(self.mesh, "DG", 0)
        V1 = df.VectorFunctionSpace(self.mesh, "CG", 1)
        cell_markers = df.MeshFunction("bool", self.mesh, self.mesh.topology().dim())
        gamma = np.zeros((V0.dim(),))
        for (i, mat) in enumerate(self.materials):
            # if hasattr(mat, "domain_id"):
            #     dxi = self.measures["dx"](mat.domain_id)
            # else:
            #     dxi = self.measures["dx"]

            d = sym(grad(df.project(self.prob.y[0], V1)))
            gamma += df.project(mat._compute_pi(d), V0).vector().get_local()
            # gamma += pi.compute_cellwise().vector().get_local()

        index_sort = np.argsort(gamma)[::-1]
        cum_sum = np.cumsum(gamma[index_sort])
        for c in df.cells(self.mesh):
            cell_markers[c] = cum_sum[np.where(c.index() == index_sort)] < ratio * sum(
                gamma
            )
        self.mesh = df.refine(self.mesh, cell_markers)
        self.Vsig = df.VectorFunctionSpace(
            self.mesh, "DG", 1, dim=self.dim * (self.dim + 1) // 2
        )
        self.V_eq = df.VectorFunctionSpace(self.mesh, "DG", 0)
        self.V = df.VectorFunctionSpace(self.mesh, "Discontinuous Lagrange Trace", 1)
        self.R = df.FunctionSpace(self.mesh, "R", 0)
        if self.facets:
            self.facets = adapt(self.facets, self.mesh)
        if self.domains:
            self.domains = adapt(self.domains, self.mesh)
        self.ds = df.Measure("ds", subdomain_data=self.facets, domain=self.mesh)
        self.dS = df.Measure("dS", subdomain_data=self.facets, domain=self.mesh)
        self.dx = df.Measure("dx", subdomain_data=self.domains, domain=self.mesh)
        self.measures = {"dx": self.dx, "ds": self.ds(1), "dS": self.dS}
        self.n = df.FacetNormal(self.mesh)
        old_params = self.prob.parameters
        self.prob = MosekProblem("static approach")
        self.prob.parameters = old_params
