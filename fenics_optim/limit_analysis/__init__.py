#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
fenics_optim submodule for solving limit analysis problems.

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC, Univ Gustave Eiffel, CNRS, UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .materials.strength_criterion import rotation_matrix
from .materials.uniaxial import Uniaxial, Uniaxial1D, MultiaxialConvHull, MultiaxialSum
from .materials.von_mises import vonMises, vonMises_plane_stress, vonMises_shell
from .materials.mohr_coulomb import (
    MohrCoulomb2D,
    MohrCoulomb3D,
    RobustMohrCoulomb2D,
    MohrCoulomb2D_plane_stress,
)
from .materials.drucker_prager import DruckerPrager
from .materials.rankine import (
    Rankine2D,
    Rankine3D,
    OrthotropicRankine2D,
    L1Rankine2D,
    OrthotropicL1Rankine2D,
    L1Rankine3D,
)
from .materials.tsai_wu import TsaiWu
from .materials.sdp_approximations import MohrCoulomb3D_approx, Rankine3D_approx

from .materials.gurson import Gurson
from .materials.hosford import Hosford_plane_stress

from .approaches import KinematicApproach, StaticApproach

import dolfin

dolfin.parameters["refinement_algorithm"] = "plaza_with_parent_facets"
